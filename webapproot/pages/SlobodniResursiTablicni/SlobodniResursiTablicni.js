dojo.declare("SlobodniResursiTablicni", wm.Page, {
    start: function() {
        try {} catch (e) {
            app.toastError(this.name + ".start() Failed: " + e.toString());

        }
    },

    btnSlobodniResursiClick2: function(inSender) {
        try {
            var pocetak = new Date(this.dateTimePocetakVoznje.displayValue);
            var zavrsetak = new Date(this.dateTimeKrajVoznje.displayValue);
            if (zavrsetak < pocetak) {
                alert("Pogrešan odnos datuma početka i datuma kraja perioda!");
                return;
            }
            this.labelOD.setCaption("");
            this.labelDO.setCaption("");

            var strDatum = app.dateTimeFormat(this.dateTimePocetakVoznje.getDisplayValue());
            this.labelOD.setCaption("OD: " + strDatum);

            strDatum = app.dateTimeFormat(this.dateTimeKrajVoznje.getDisplayValue());
            this.labelDO.setCaption("DO: " + strDatum);

        } catch (e) {
            console.error('ERROR IN btnSlobodniResursiClick2: ' + e);
        }
    },

    dojoGridSlobodniVozaciCustomFieldFormat: function(inValue, rowId, cellId, cellField, cellObj, rowObj) {

        try {
            return '<button onclick=wm.Page.getPage("SlobodniResursiTablicni").dialogKreiranjeVoznje_SlobodniResursi_Vozaci_Show();>Kreiraj vožnju</button>';
        } catch (e) {
            console.error('ERROR IN dojoGridSlobodniVozaciCustomFieldFormat: ' + e);
        }
    },

    dojoGridSlobodnaVozilaCustomField1Format: function(inValue, rowId, cellId, cellField, cellObj, rowObj) {

        try {
            return '<button type="button" onclick=wm.Page.getPage("SlobodniResursiTablicni").dialogKreiranjeVoznje_SlobodniResursi_Vozila_Show();>Kreiraj vožnju</button>';
        } catch (e) {
            console.error('ERROR IN dojoGridSlobodnaVozilaCustomField1Format: ' + e);
        }
    },

    serviceVarInsertVoznjaVozacSuccess: function(inSender, inDeprecated) {
        try {
            if (inSender.getData().dataValue === false) {
                alert("Odabrani resursi su zauzeti u definiranom terminu!");
                //app.liveForm2.beginDataInsert();
            } else {
                //app.liveForm2.insertData();
                this.dialogKreiranjeVoznje_SlobodniResursi_Vozaci.dismiss();
                this.serviceVarGetVoznje.update();
                this.serviceVarGetSlobodniBusevi.update();
                this.serviceVarGetSlobodniVozaci.update();
            }
        } catch (e) {
            console.error('ERROR IN serviceVarInsertVoznjaVozacSuccess: ' + e);
        }
    },


    serviceVarInsertVoznjaBusSuccess: function(inSender, inDeprecated) {
        try {
            if (inSender.getData().dataValue === false) {
                alert("Odabrani resursi su zauzeti u definiranom terminu!");
                //app.liveForm1.beginDataInsert();
            } else {
                //app.liveForm1.insertData();
                this.dialogKreiranjeVoznje_SlobodniResursi.dismiss();
                this.serviceVarGetVoznje.update();
                this.serviceVarGetSlobodniBusevi.update();
                this.serviceVarGetSlobodniVozaci.update();
            }
        } catch (e) {
            console.error('ERROR IN serviceVarInsertVoznjaBusSuccess: ' + e);
        }
    },

    dojoGrid1VrijemePolaskaVoznjaFormat: function(inValue, rowId, cellId, cellField, cellObj, rowObj) {
        try {
            return app.dateTimeFormat(inValue);
        } catch (e) {
            console.error('ERROR IN dojoGrid1VrijemePolaskaVoznjaFormat: ' + e);
        }


    },

    dojoGrid1VrijemeDolaskaVoznjaFormat: function(inValue, rowId, cellId, cellField, cellObj, rowObj) {    
        try {
            return app.dateTimeFormat(inValue);
        } catch (e) {
            console.error('ERROR IN dojoGrid1VrijemeDolaskaVoznjaFormat: ' + e);
        }
    },

    /**
     * metode za dialog kreiranja vožnji u slobodnim resursima
     * */

    cancelButton1Click: function(inSender) {
        try {
            this.dialogKreiranjeVoznje_SlobodniResursi.dismiss();
        } catch (e) {
            console.error('ERROR IN cancelButton1Click: ' + e);

        }
    },
    liveForm1Success: function(inSender, inData) {
        try {
            this.dialogKreiranjeVoznje_SlobodniResursi.dismiss();

        } catch (e) {
            console.error('ERROR IN liveForm1Success: ' + e);
        }
    },
    dialogKreiranjeVoznje_SlobodniResursi_Vozila_Show: function() {
        this.dialogKreiranjeVoznje_SlobodniResursi.show();
        this.liveForm1.beginDataInsert();

        this.vrijemeDolaskaVoznjaEditor1.setReadonly(true);

        this.vrijemePolaskaVoznjaEditor1.setReadonly(true);

        if (this.dojoGridSlobodniVozaci.getRowCount() === 0) {

            app.toastWarning("U odabranom terminu nema slobodnih vozača!");

            this.dialogKreiranjeVoznje_SlobodniResursi.dismiss();

        }

    },

    dialogKreiranjeVoznje_SlobodniResursi_Vozaci_Show: function() {

        this.dialogKreiranjeVoznje_SlobodniResursi_Vozaci.show();

        this.liveForm2.beginDataInsert();
        this.vrijemePolaskaVoznjaEditor2.setReadonly(true);
        this.vrijemeDolaskaVoznjaEditor2.setReadonly(true);

        if (this.dojoGridSlobodnaVozila.getRowCount() === 0) {
            app.toastWarning("U odabranom terminu nema slobodnih vozila!");
            this.dialogKreiranjeVoznje_SlobodniResursi_Vozaci.dismiss();
        }
    },

    liveForm2Success: function(inSender, inData) {
        try {
            this.dialogKreiranjeVoznje_SlobodniResursi_Vozaci.dismiss();
        } catch (e) {
            console.error('ERROR IN liveForm2Success: ' + e);
        }
    },
    cancelButton2Click: function(inSender) {
        try {
            this.dialogKreiranjeVoznje_SlobodniResursi_Vozaci.dismiss();
        } catch (e) {
            console.error('ERROR IN cancelButton2Click: ' + e);

        }
    },

    saveButton2Click: function(inSender) {
        try {
            if (this.liveForm2.operation == "insert") {

                this.liveForm2.populateDataOutput();
                console.log("Pozivanje custom funkcije za upis vožnji");
                this.serviceVarInsertVoznjaVozac.update();
            }
        } catch (e) {
            console.error('ERROR IN saveButton2Click: ' + e);

        }
    },

    saveButton1Click: function(inSender) {
        try {
            if (this.liveForm1.operation == "insert") {

                this.liveForm1.populateDataOutput();

                console.log("Pozivanje custom funkcije za upis vožnji");

                this.serviceVarInsertVoznjaBus.update();

            }

        } catch (e) {

            console.error('ERROR IN saveButton1Click: ' + e);

        }

    },

    vrijemePolaskaVoznjaEditor2ReadOnlyNodeFormat: function(inSender, inValue) {
        try {
            return app.dateTimeFormat(inValue);
        } catch (e) {
            console.error('ERROR IN vrijemePolaskaVoznjaEditor2ReadOnlyNodeFormat: ' + e);
        }

    },

    vrijemeDolaskaVoznjaEditor2ReadOnlyNodeFormat: function(inSender, inValue) {
        try {
            return app.dateTimeFormat(inValue);
        } catch (e) {
            console.error('ERROR IN vrijemeDolaskaVoznjaEditor2ReadOnlyNodeFormat: ' + e);
        }
    },


    vrijemePolaskaVoznjaEditor1ReadOnlyNodeFormat: function(inSender, inValue) {
        try {
            return app.dateTimeFormat(inValue);
        } catch (e) {
            console.error('ERROR IN vrijemePolaskaVoznjaEditor1ReadOnlyNodeFormat: ' + e);
        }
    },


    vrijemeDolaskaVoznjaEditor1ReadOnlyNodeFormat: function(inSender, inValue) {
        try {
            return app.dateTimeFormat(inValue);
        } catch (e) {
            console.error('ERROR IN vrijemeDolaskaVoznjaEditor1ReadOnlyNodeFormat: ' + e);
        }
    },

    dateTimePocetakVoznjeChange: function(inSender) {
        try {

            if (this.dateTimePocetakVoznje.getDisplayValue() != "" && this.dateTimeKrajVoznje.getDisplayValue() != "") {
                this.btnSlobodniResursi.enable();
            } else {
                this.btnSlobodniResursi.disable();
            }
        } catch (e) {
            console.error('ERROR IN dateTimePocetakVoznjeChange: ' + e);
        }
    },



    dateTimeKrajVoznjeChange: function(inSender) {
        try {
            if (this.dateTimeKrajVoznje.getDisplayValue() != "" && this.dateTimePocetakVoznje.getDisplayValue() != "") {
                this.btnSlobodniResursi.enable();
            } else {
                this.btnSlobodniResursi.disable();
            }
        } catch (e) {
            console.error('ERROR IN dateTimeKrajVoznjeChange: ' + e);
        }
    },
    dojoGrid1CustomField4Format: function( inValue, rowId, cellId, cellField, cellObj, rowObj) {
	  try {
		  if (inValue === null) {
                return "<div style='color:red; width:100%; height:100%;'>Nenaplaćena</div>";
            } else if (typeof inValue === 'number') {
                return "<div style='color:green; width:100%; height:100%;'>Naplaćena</div>";;
            } else {
                return "Otvorena";
            }		  
	  } catch(e) {
		  console.error('ERROR IN dojoGrid1CustomField4Format: ' + e); 
	  } 
  },
  _end: 0

});