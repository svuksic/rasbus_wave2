SlobodniResursiTablicni.widgets = {
	serviceVarGetSlobodniBusevi: ["wm.ServiceVariable", {"operation":"getSlobodniBusevi","service":"glavni"}, {"onSuccess":"serviceVarGetSlobodniBuseviSuccess"}, {
		input: ["wm.ServiceInput", {"type":"getSlobodniBuseviInputs"}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire1: ["wm.Wire", {"expression":undefined,"source":"dateTimePocetakVoznje.dataValue","targetProperty":"pocetakVoznje"}, {}],
				wire: ["wm.Wire", {"expression":undefined,"source":"dateTimeKrajVoznje.dataValue","targetProperty":"krajVoznje"}, {}]
			}]
		}]
	}],
	serviceVarGetVoznje: ["wm.ServiceVariable", {"operation":"getVoznje","service":"glavni"}, {}, {
		input: ["wm.ServiceInput", {"type":"getVoznjeInputs"}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire: ["wm.Wire", {"expression":undefined,"source":"dateTimePocetakVoznje.dataValue","targetProperty":"krajVoznje"}, {}],
				wire1: ["wm.Wire", {"expression":undefined,"source":"dateTimeKrajVoznje.dataValue","targetProperty":"pocetakVoznje"}, {}]
			}]
		}]
	}],
	serviceVarGetSlobodniVozaci: ["wm.ServiceVariable", {"operation":"getSlobodniVozaci","service":"glavni"}, {}, {
		input: ["wm.ServiceInput", {"type":"getSlobodniVozaciInputs"}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire: ["wm.Wire", {"expression":undefined,"source":"dateTimeKrajVoznje.dataValue","targetProperty":"krajVoznje"}, {}],
				wire1: ["wm.Wire", {"expression":undefined,"source":"dateTimePocetakVoznje.dataValue","targetProperty":"pocetakVoznje"}, {}]
			}]
		}]
	}],
	serviceVarInsertVoznjaVozac: ["wm.ServiceVariable", {"operation":"dodavanjeVoznje","service":"glavni"}, {"onSuccess":"serviceVarInsertVoznjaVozacSuccess"}, {
		input: ["wm.ServiceInput", {"type":"dodavanjeVoznjeInputs"}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire: ["wm.Wire", {"expression":undefined,"source":"liveForm2.dataOutput.bus.idBus","targetProperty":"busPar"}, {}],
				wire2: ["wm.Wire", {"expression":undefined,"source":"liveForm2.dataOutput.vrijemeDolaskaVoznja","targetProperty":"datumDolaskaPar"}, {}],
				wire1: ["wm.Wire", {"expression":undefined,"source":"liveForm2.dataOutput.vrijemePolaskaVoznja","targetProperty":"datumPolaskaPar"}, {}],
				wire3: ["wm.Wire", {"expression":undefined,"source":"liveForm2.dataOutput.mjestoDolaskaVoznja","targetProperty":"mjestoDolaskaPar"}, {}],
				wire4: ["wm.Wire", {"expression":undefined,"source":"liveForm2.dataOutput.mjestoPolaskaVoznja","targetProperty":"mjestoPolaskaPar"}, {}],
				wire5: ["wm.Wire", {"expression":undefined,"source":"liveForm2.dataOutput.opisVoznja","targetProperty":"opisVoznja"}, {}],
				wire6: ["wm.Wire", {"expression":undefined,"source":"liveForm2.dataOutput.oznakaVoznja","targetProperty":"oznakaVoznja"}, {}],
				wire7: ["wm.Wire", {"expression":undefined,"source":"liveForm2.dataOutput.vozac.idvozac","targetProperty":"vozacPar"}, {}],
				wire8: ["wm.Wire", {"expression":undefined,"source":"liveForm2.dataOutput.narucitelj.idnarucitelj","targetProperty":"naruciteljPar"}, {}],
				wire9: ["wm.Wire", {"expression":undefined,"source":"ugovorenaCijenaSlobodnoVozilo.dataValue","targetProperty":"ugovorenaCijena"}, {}]
			}]
		}]
	}],
	serviceVarInsertVoznjaBus: ["wm.ServiceVariable", {"operation":"dodavanjeVoznje","service":"glavni"}, {"onSuccess":"serviceVarInsertVoznjaBusSuccess"}, {
		input: ["wm.ServiceInput", {"type":"dodavanjeVoznjeInputs"}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire: ["wm.Wire", {"expression":undefined,"source":"liveForm1.dataOutput.bus.idBus","targetProperty":"busPar"}, {}],
				wire2: ["wm.Wire", {"expression":undefined,"source":"liveForm1.dataOutput.vrijemeDolaskaVoznja","targetProperty":"datumDolaskaPar"}, {}],
				wire1: ["wm.Wire", {"expression":undefined,"source":"liveForm1.dataOutput.vrijemePolaskaVoznja","targetProperty":"datumPolaskaPar"}, {}],
				wire3: ["wm.Wire", {"expression":undefined,"source":"liveForm1.dataOutput.mjestoDolaskaVoznja","targetProperty":"mjestoDolaskaPar"}, {}],
				wire4: ["wm.Wire", {"expression":undefined,"source":"liveForm1.dataOutput.mjestoPolaskaVoznja","targetProperty":"mjestoPolaskaPar"}, {}],
				wire6: ["wm.Wire", {"expression":undefined,"source":"liveForm1.dataOutput.oznakaVoznja","targetProperty":"oznakaVoznja"}, {}],
				wire5: ["wm.Wire", {"expression":undefined,"source":"liveForm1.dataOutput.opisVoznja","targetProperty":"opisVoznja"}, {}],
				wire7: ["wm.Wire", {"expression":undefined,"source":"liveForm1.dataOutput.vozac.idvozac","targetProperty":"vozacPar"}, {}],
				wire8: ["wm.Wire", {"expression":undefined,"source":"liveForm1.dataOutput.narucitelj.idnarucitelj","targetProperty":"naruciteljPar"}, {}],
				wire9: ["wm.Wire", {"expression":undefined,"source":"ugovorenaCijenaEditorSlobodniBus.dataValue","targetProperty":"ugovorenaCijena"}, {}]
			}]
		}]
	}],
	dialogKreiranjeVoznje_SlobodniResursi_Vozaci: ["wm.DesignableDialog", {"buttonBarId":"buttonBar1","containerWidgetId":"containerWidget1","height":"360px","title":"kreiranje vožnje"}, {}, {
		containerWidget1: ["wm.Container", {"_classes":{"domNode":["wmdialogcontainer","MainContent"]},"autoScroll":true,"height":"100%","horizontalAlign":"left","margin":"0","padding":"5","verticalAlign":"top","width":"100%"}, {}, {
			liveForm2: ["wm.LiveForm", {"fitToContentHeight":true,"height":"288px","horizontalAlign":"left","readonly":true,"verticalAlign":"top"}, {"onSuccess":"liveForm2Success"}, {
				binding: ["wm.Binding", {}, {}, {
					wire: ["wm.Wire", {"expression":undefined,"source":"app.liveVarVoznjaSlobodniResursiTablicni","targetProperty":"dataSet"}, {}]
				}],
				oznakaVoznjaEditor2: ["wm.Text", {"caption":"Oznaka vožnje:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"oznakaVoznja","height":"26px","readonly":true,"width":"100%"}, {}],
				mjestoPolaskaVoznjaEditor2: ["wm.Text", {"caption":"Mjesto polaska:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"mjestoPolaskaVoznja","height":"26px","readonly":true,"required":true,"width":"100%"}, {}],
				mjestoDolaskaVoznjaEditor2: ["wm.Text", {"caption":"Mjesto dolaska:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"mjestoDolaskaVoznja","height":"26px","readonly":true,"required":true,"width":"100%"}, {}],
				vrijemePolaskaVoznjaEditor2: ["wm.DateTime", {"caption":"Vrijeme polaska:","captionSize":"200px","emptyValue":"emptyString","formField":"vrijemePolaskaVoznja","formatter":"vrijemePolaskaVoznjaEditor2ReadOnlyNodeFormat","height":"26px","readonly":true,"width":"100%"}, {}, {
					binding: ["wm.Binding", {}, {}, {
						wire: ["wm.Wire", {"expression":undefined,"source":"[slobodniResursiTablicni].dateTimePocetakVoznje.dataValue","targetProperty":"defaultInsert"}, {}]
					}]
				}],
				vrijemeDolaskaVoznjaEditor2: ["wm.DateTime", {"caption":"Vrijeme dolaska:","captionSize":"200px","emptyValue":"emptyString","formField":"vrijemeDolaskaVoznja","formatter":"vrijemeDolaskaVoznjaEditor2ReadOnlyNodeFormat","height":"26px","readonly":true,"width":"100%"}, {}, {
					binding: ["wm.Binding", {}, {}, {
						wire: ["wm.Wire", {"expression":undefined,"source":"[slobodniResursiTablicni].dateTimeKrajVoznje.dataValue","targetProperty":"defaultInsert"}, {}]
					}]
				}],
				opisVoznjaEditor2: ["wm.Text", {"caption":"Napomena:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"opisVoznja","height":"26px","readonly":true,"width":"100%"}, {}],
				ugovorenaCijenaSlobodnoVozilo: ["wm.Text", {"caption":"Ugovorena cijena(Kn):","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"ugovorenaCijenaVoznja","height":"26px","invalidMessage":"Format cijene.","readonly":true,"regExp":"^[0-9]*\\.[0-9][0-9]$","width":"100%"}, {}],
				vozacLookup2: ["wm.Lookup", {"autoDataSet":false,"caption":"Vozač:","captionSize":"200px","displayExpression":"${imeVozac}+\" \"+${prezimeVozac}","formField":"vozac","readonly":true,"required":true,"width":"100%"}, {}, {
					binding: ["wm.Binding", {}, {}, {
						wire: ["wm.Wire", {"expression":undefined,"source":"app.liveVarVozac_SlobodniResursi","targetProperty":"dataSet"}, {}]
					}]
				}],
				busLookup2: ["wm.Lookup", {"autoDataSet":false,"caption":"Vozilo:","captionSize":"200px","displayExpression":"${garazniBrojBus}+\" \"+${markaBus}+\" \"+${modelBus}","displayField":"napomenaBus","formField":"bus","readonly":true,"required":true,"width":"100%"}, {}, {
					binding: ["wm.Binding", {}, {}, {
						wire: ["wm.Wire", {"expression":undefined,"source":"[slobodniResursiTablicni].serviceVarGetSlobodniBusevi","targetProperty":"dataSet"}, {}]
					}]
				}],
				lookup2: ["wm.Lookup", {"caption":"Naručitelj:","captionSize":"200px","displayField":"nazivNarucitelj","formField":"narucitelj","height":"26px","readonly":true,"width":"100%"}, {}],
				liveForm2EditPanel: ["wm.EditPanel", {"height":"32px","isCustomized":true,"liveForm":"liveForm2","lock":false,"operationPanel":"operationPanel2","savePanel":"savePanel2"}, {}, {
					savePanel2: ["wm.Panel", {"height":"100%","horizontalAlign":"right","layoutKind":"left-to-right","showing":false,"verticalAlign":"top","width":"100%"}, {}, {
						saveButton2: ["wm.Button", {"caption":"Kreiraj","margin":"4"}, {"onclick":"saveButton2Click"}, {
							binding: ["wm.Binding", {}, {}, {
								wire: ["wm.Wire", {"expression":undefined,"source":"liveForm2EditPanel.formInvalid","targetProperty":"disabled"}, {}]
							}]
						}],
						cancelButton2: ["wm.Button", {"caption":"Prekini","margin":"4"}, {"onclick":"cancelButton2Click"}]
					}],
					operationPanel2: ["wm.Panel", {"height":"100%","horizontalAlign":"right","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}]
				}]
			}]
		}],
		buttonBar1: ["wm.Panel", {"_classes":{"domNode":["dialogfooter"]},"border":"1,0,0,0","borderColor":"#333333","height":"32px","horizontalAlign":"right","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}]
	}],
	dialogKreiranjeVoznje_SlobodniResursi: ["wm.DesignableDialog", {"buttonBarId":"buttonBar","containerWidgetId":"containerWidget","height":"360px","title":"kreiranje vožnje"}, {"onShow":"dialogKreiranjeVoznje_SlobodniResursiShow"}, {
		containerWidget: ["wm.Container", {"_classes":{"domNode":["wmdialogcontainer","MainContent"]},"autoScroll":true,"height":"100%","horizontalAlign":"left","margin":"0","padding":"5","verticalAlign":"top","width":"100%"}, {}, {
			liveForm1: ["wm.LiveForm", {"fitToContentHeight":true,"height":"288px","horizontalAlign":"left","readonly":true,"verticalAlign":"top"}, {"onSuccess":"liveForm1Success"}, {
				binding: ["wm.Binding", {}, {}, {
					wire: ["wm.Wire", {"expression":undefined,"source":"app.liveVarVoznjaSlobodniResursiTablicni","targetProperty":"dataSet"}, {}]
				}],
				oznakaVoznjaEditor1: ["wm.Text", {"caption":"Oznaka:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"oznakaVoznja","height":"26px","readonly":true,"width":"100%"}, {}],
				mjestoPolaskaVoznjaEditor1: ["wm.Text", {"caption":"Mjesto polaska:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"mjestoPolaskaVoznja","height":"26px","readonly":true,"required":true,"width":"100%"}, {}],
				mjestoDolaskaVoznjaEditor1: ["wm.Text", {"caption":"Mjesto dolaska:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"mjestoDolaskaVoznja","height":"26px","readonly":true,"required":true,"width":"100%"}, {}],
				vrijemePolaskaVoznjaEditor1: ["wm.DateTime", {"caption":"Vrijeme polaska:","captionSize":"200px","emptyValue":"emptyString","formField":"vrijemePolaskaVoznja","formatter":"vrijemePolaskaVoznjaEditor1ReadOnlyNodeFormat","height":"26px","readonly":true,"width":"100%"}, {}, {
					binding: ["wm.Binding", {}, {}, {
						wire: ["wm.Wire", {"expression":undefined,"source":"[slobodniResursiTablicni].dateTimePocetakVoznje.dataValue","targetProperty":"defaultInsert"}, {}]
					}]
				}],
				vrijemeDolaskaVoznjaEditor1: ["wm.DateTime", {"caption":"Vrijeme dolaska:","captionSize":"200px","emptyValue":"emptyString","formField":"vrijemeDolaskaVoznja","formatter":"vrijemeDolaskaVoznjaEditor1ReadOnlyNodeFormat","height":"26px","readonly":true,"width":"100%"}, {}, {
					binding: ["wm.Binding", {}, {}, {
						wire: ["wm.Wire", {"expression":undefined,"source":"[slobodniResursiTablicni].dateTimeKrajVoznje.dataValue","targetProperty":"defaultInsert"}, {}]
					}]
				}],
				opisVoznjaEditor1: ["wm.Text", {"caption":"Napomena:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"opisVoznja","height":"26px","readonly":true,"width":"100%"}, {}],
				ugovorenaCijenaEditorSlobodniBus: ["wm.Text", {"caption":"Ugovorena cijena(kn):","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"ugovorenaCijenaVoznja","height":"26px","invalidMessage":"Format cijene.","readonly":true,"regExp":"^[0-9]*\\.[0-9][0-9]$","width":"100%"}, {}],
				vozacLookup1: ["wm.Lookup", {"autoDataSet":false,"caption":"Vozač:","captionSize":"200px","displayExpression":"${imeVozac}+\" \"+${prezimeVozac}","formField":"vozac","readonly":true,"required":true,"width":"100%"}, {}, {
					binding: ["wm.Binding", {}, {}, {
						wire: ["wm.Wire", {"expression":undefined,"source":"[slobodniResursiTablicni].serviceVarGetSlobodniVozaci","targetProperty":"dataSet"}, {}]
					}]
				}],
				busLookup1: ["wm.Lookup", {"autoDataSet":false,"caption":"Vozilo:","captionSize":"200px","displayExpression":"${garazniBrojBus}+\" \"+${markaBus}+\" \"+${modelBus}","formField":"bus","readonly":true,"required":true,"width":"100%"}, {}, {
					binding: ["wm.Binding", {}, {}, {
						wire: ["wm.Wire", {"expression":undefined,"source":"app.liveVarBus_SlobodniResursi","targetProperty":"dataSet"}, {}]
					}]
				}],
				lookup1: ["wm.Lookup", {"caption":"Naručitelj:","captionSize":"200px","displayField":"nazivNarucitelj","formField":"narucitelj","height":"26px","readonly":true,"width":"100%"}, {}],
				liveForm1EditPanel: ["wm.EditPanel", {"height":"32px","isCustomized":true,"liveForm":"liveForm1","lock":false,"operationPanel":"operationPanel1","savePanel":"savePanel1"}, {}, {
					savePanel1: ["wm.Panel", {"height":"100%","horizontalAlign":"right","layoutKind":"left-to-right","showing":false,"verticalAlign":"top","width":"100%"}, {}, {
						saveButton1: ["wm.Button", {"caption":"Kreiraj","margin":"4"}, {"onclick":"saveButton1Click"}, {
							binding: ["wm.Binding", {}, {}, {
								wire: ["wm.Wire", {"expression":undefined,"source":"liveForm1EditPanel.formInvalid","targetProperty":"disabled"}, {}]
							}]
						}],
						cancelButton1: ["wm.Button", {"caption":"Prekini","margin":"4"}, {"onclick":"cancelButton1Click"}]
					}],
					operationPanel1: ["wm.Panel", {"height":"100%","horizontalAlign":"center","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}]
				}]
			}]
		}],
		buttonBar: ["wm.Panel", {"_classes":{"domNode":["dialogfooter"]},"border":"1,0,0,0","borderColor":"#333333","height":"32px","horizontalAlign":"right","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}]
	}],
	layoutBox1: ["wm.Layout", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"752px"}, {}, {
		panel1: ["wm.Panel", {"height":"120px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
			fancyPanelDefiniranjePerioda: ["wm.FancyPanel", {"innerLayoutKind":"left-to-right","title":"Odabir željenog perioda"}, {}, {
				panel2: ["wm.Panel", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"324px"}, {}, {
					dateTimePocetakVoznje: ["wm.DateTime", {"caption":"Početak vožnje:","captionSize":"120px","displayValue":"","formatter":"dateTimePocetakVoznjeReadOnlyNodeFormat","width":"100%"}, {"onchange":"dateTimePocetakVoznjeChange"}],
					spacer1: ["wm.Spacer", {"height":"5px","width":"100%"}, {}],
					dateTimeKrajVoznje: ["wm.DateTime", {"caption":"Kraj vožnje:","captionSize":"120px","displayValue":"","width":"100%"}, {"onchange":"dateTimeKrajVoznjeChange"}],
					panel6: ["wm.Panel", {"height":"100%","horizontalAlign":"right","layoutKind":"left-to-right","verticalAlign":"middle","width":"100%"}, {}, {
						btnSlobodniResursi: ["wm.Button", {"caption":"Slobodni resursi","disabled":true,"margin":"4","width":"203px"}, {"onclick":"serviceVarGetSlobodniBusevi","onclick1":"serviceVarGetSlobodniVozaci","onclick2":"btnSlobodniResursiClick2","onclick3":"serviceVarGetVoznje"}]
					}]
				}],
				panel3: ["wm.Panel", {"height":"100%","horizontalAlign":"right","verticalAlign":"top","width":"235px"}, {}, {
					label1: ["wm.Label", {"border":"0","caption":"Željeni period","padding":"4"}, {}],
					labelOD: ["wm.Label", {"border":"0","caption":"OD:","padding":"4"}, {}],
					labelDO: ["wm.Label", {"border":"0","caption":"DO:","padding":"4"}, {}]
				}],
				panel4: ["wm.Panel", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"60px"}, {}]
			}]
		}],
		panel5: ["wm.Panel", {"height":"180px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
			fancyPanel1: ["wm.FancyPanel", {"title":"Slobodni vozači","width":"50%"}, {}, {
				dojoGridSlobodniVozaci: ["wm.DojoGrid", {"columns":[{"show":true,"id":"idvozac","title":"ID","width":"30px","displayType":"Number","noDelete":true,"align":"center","formatFunc":""},{"show":true,"id":"imeVozac","title":"Ime","width":"100%","displayType":"Text","noDelete":true,"align":"center","formatFunc":""},{"show":true,"id":"prezimeVozac","title":"Prezime","width":"100%","displayType":"Text","noDelete":true,"align":"center","formatFunc":""},{"show":false,"id":"datumRodjenjaVozac","title":"DatumRodjenjaVozac","width":"100%","displayType":"Date","noDelete":true,"align":"left","formatFunc":"wm_date_formatter"},{"show":false,"id":"datumVozackaVozac","title":"DatumVozackaVozac","width":"100%","displayType":"Date","noDelete":true,"align":"left","formatFunc":"wm_date_formatter"},{"show":false,"id":"datumPutovnicaVozac","title":"DatumPutovnicaVozac","width":"100%","displayType":"Date","noDelete":true,"align":"left","formatFunc":"wm_date_formatter"},{"show":false,"id":"datumOsobnaVozac","title":"DatumOsobnaVozac","width":"100%","displayType":"Date","noDelete":true,"align":"left","formatFunc":"wm_date_formatter"},{"id":"customField","isCustomField":true,"expression":"","show":true,"width":"100%","formatFunc":"dojoGridSlobodniVozaciCustomFieldFormat","title":"Akcija","align":"center"}],"height":"100%","margin":"4"}, {}, {
					binding: ["wm.Binding", {}, {}, {
						wire: ["wm.Wire", {"expression":undefined,"source":"serviceVarGetSlobodniVozaci","targetProperty":"dataSet"}, {}]
					}]
				}]
			}],
			fancyPanel2: ["wm.FancyPanel", {"title":"Slobodna vozila","width":"50%"}, {}, {
				dojoGridSlobodnaVozila: ["wm.DojoGrid", {"columns":[{"show":false,"id":"idBus","title":"IdBus","width":"80px","displayType":"Number","noDelete":true,"align":"right","formatFunc":""},{"show":true,"id":"garazniBrojBus","title":"GB","width":"80px","displayType":"Text","noDelete":true,"align":"center","formatFunc":""},{"show":true,"id":"modelBus","title":"Model","width":"90px","displayType":"Text","noDelete":true,"align":"center","formatFunc":""},{"show":true,"id":"markaBus","title":"Marka","width":"100px","displayType":"Text","noDelete":true,"align":"center","formatFunc":""},{"show":false,"id":"brojSjedalaBus","title":"BrojSjedalaBus","width":"80px","displayType":"Number","noDelete":true,"align":"right","formatFunc":""},{"show":false,"id":"datumKupnjeBus","title":"DatumKupnjeBus","width":"80px","displayType":"Date","noDelete":true,"align":"left","formatFunc":"wm_date_formatter"},{"show":false,"id":"datumZadnjeRegistracijeBus","title":"DatumZadnjeRegistracijeBus","width":"80px","displayType":"Date","noDelete":true,"align":"left","formatFunc":"wm_date_formatter"},{"show":false,"id":"datumZadnjegServisaBus","title":"DatumZadnjegServisaBus","width":"80px","displayType":"Date","noDelete":true,"align":"left","formatFunc":"wm_date_formatter"},{"show":false,"id":"napomenaBus","title":"NapomenaBus","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"id":"customField1","isCustomField":true,"expression":"","show":true,"width":"100%","formatFunc":"dojoGridSlobodnaVozilaCustomField1Format","title":"Akcija","align":"center"}],"height":"100%","margin":"4"}, {}, {
					binding: ["wm.Binding", {}, {}, {
						wire: ["wm.Wire", {"expression":undefined,"source":"serviceVarGetSlobodniBusevi","targetProperty":"dataSet"}, {}]
					}]
				}]
			}]
		}],
		panel7: ["wm.Panel", {"height":"180px","horizontalAlign":"left","verticalAlign":"top","width":"100%"}, {}, {
			fancyPanel3: ["wm.FancyPanel", {"title":"Vožnje - zauzeti resursi"}, {}, {
				dojoGrid1: ["wm.DojoGrid", {"columns":[{"show":false,"id":"idvoznja","title":"Idvoznja","width":"80px","displayType":"Number","noDelete":true,"align":"right","formatFunc":""},{"show":true,"id":"oznakaVoznja","title":"Oznaka","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"opisVoznja","title":"Opis","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"mjestoPolaskaVoznja","title":"Polazak","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"mjestoDolaskaVoznja","title":"Dolazak","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"vrijemePolaskaVoznja","title":"Polazak","width":"80px","displayType":"Date","noDelete":true,"align":"left","formatFunc":"dojoGrid1VrijemePolaskaVoznjaFormat"},{"show":true,"id":"vrijemeDolaskaVoznja","title":"Dolazak","width":"80px","displayType":"Date","noDelete":true,"align":"left","formatFunc":"dojoGrid1VrijemeDolaskaVoznjaFormat"},{"id":"customField","isCustomField":true,"expression":"${bus.garazniBrojBus}","show":true,"width":"100%","title":"Garažni broj"},{"id":"customField1","isCustomField":true,"expression":"${bus.markaBus}","show":true,"width":"100%","title":"Marka vozila"},{"id":"customField2","isCustomField":true,"expression":"${bus.modelBus}","show":true,"width":"100%","title":"Model vozila"},{"id":"customField3","isCustomField":true,"expression":"${vozac.imeVozac}+\"  \"+${vozac.prezimeVozac}","show":true,"width":"100%","title":"Vozač"},{"show":false,"id":"ugovorenaCijenaVoznja","title":"UgovorenaCijenaVoznja","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"id":"customField4","isCustomField":true,"expression":"${zatvorenavoznja.naplata}","show":true,"width":"100%","title":"Status","formatFunc":"dojoGrid1CustomField4Format"}],"height":"100%","margin":"4"}, {}, {
					binding: ["wm.Binding", {}, {}, {
						wire: ["wm.Wire", {"expression":undefined,"source":"serviceVarGetVoznje","targetProperty":"dataSet"}, {}]
					}]
				}]
			}]
		}]
	}]
}