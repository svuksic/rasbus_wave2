var otvorenTabPregledVoznji = false;
var buseviString = "";

dojo.declare("OdabirVoznji", wm.Page, {
    start: function() {
        try {            
        } catch (e) {
            app.toastError(this.name + ".start() Failed: " + e.toString());
        }
    },

    //metoda za dodavanje odabranih buseva
    buttonOdaberiBusClick: function(inSender) {
        try {            
            app.varOdabraniBusevi.addItem(this.selectMenuVozila.selectedItem);
            buseviString += this.selectMenuVozila.selectedItem.getData().idBus+",";            
        } catch (e) {
            console.error('ERROR IN buttonOdaberiBusClick: ' + e);
        }
    },
    
    //metoda za kreiranje grafickog prikaza
    btnGrafickiPrikazClick: function(inSender) {
        try {
            if (app.varOdabraniBusevi.getCount() > 0 && this.selectMenuPeriod.getDisplayValue() !== "" && this.datumPocetka.getDisplayValue() !== "") {
                app.varListaOdabranihBusevaId.setValue("dataValue", buseviString);
                this.serviceVarIsThereVoznja.update();                
            }else{
                app.toastWarning("Potrebno popuniti sva polja!");
            }
        } catch (e) {
            console.error('ERROR IN btnGrafickiPrikazClick: ' + e);
        }
    
    },
    
    datumPocetkaChange: function(inSender) {
	  try {
		  app.varDatumPocetkaPerioda.setValue("dataValue",this.datumPocetka.displayValue);
	  } catch(e) {
		  console.error('ERROR IN datumPocetkaChange: ' + e); 
	  } 
  },
  
  selectMenuPeriodChange: function(inSender) {
	  try {
		  app.varPeriodOdabrani.setValue("dataValue",this.selectMenuPeriod.selectedItem.getData().dataValue);		  
	  } catch(e) {
		  console.error('ERROR IN selectMenuPeriodChange: ' + e); 
	  } 
  },
  
  //za odabir svih buseva
  btnDodajSveClick: function(inSender) {
      try {
          buseviString = "";
          for(var i=0; i<this.busVarDB.getCount(); i++){
              app.varOdabraniBusevi.addItem(this.busVarDB.getItem(i));
              buseviString += this.busVarDB.getItem(i).getValue("idBus") + ",";
          }                    
      } catch (e) {
          console.error('ERROR IN btnDodajSveClick: ' + e);
      }
  },
  
  //za brisanje svih buseva iz liste odabranih
  btnBrisiSveClick: function(inSender) {
	  try {     
          buseviString = "";             
          app.varOdabraniBusevi.clearData();   
	  } catch(e) {
		  console.error('ERROR IN btnBrisiSveClick: ' + e); 
	  } 
  },
  
  //za brisanje odabranog busa
  btnBrisiBusClick: function(inSender) {
      try {
          for (var i = 0; i < app.varOdabraniBusevi.getCount(); i++) {
              var index = this.listOdabranihBuseva.getSelectedIndex();
              var item = this.listOdabranihBuseva.getItem(index);
              if (app.varOdabraniBusevi.getItem(i).getValue("idBus") === item.getData().idBus) {
                  app.varOdabraniBusevi.removeItem(i);
              }
          }
          buseviString = "";
          for (var ii = 0; ii < app.varOdabraniBusevi.getCount(); ii++) {
              buseviString += this.busVarDB.getItem(ii).getValue("idBus") + ",";
          }
      } catch (e) {
          console.error('ERROR IN btnBrisiBusClick: ' + e);
      }
  },
  
  //ako postoje podaci za prikaz
  serviceVarIsThereVoznjaSuccess: function(inSender, inDeprecated) {
	  try {          
		  if(inSender.getData().dataValue === true){              
              if (app.varNacinPregleda.getValue("dataValue") == "graficki") {    
                    if (otvorenTabPregledVoznji === false) {
                        otvorenTabPregledVoznji = true;
                        var l = new wm.Layer({
                            owner: main,
                            parent: main.tabLayers1,
                            caption: "Pregled vožnji prema vozilu od: "+app.varDatumPocetkaPerioda.getValue("dataValue"),
                            width: "100%",
                            height: "100%",
                            closable: true,
                            name: "pregled",
                            onCloseOrDestroy: function okini(item) {
                                otvorenTabPregledVoznji = false;
                                l.destroy();
                                c.destroy();
                                main.tabLayers1.getActiveLayer().setValue("width", "100px");
                                main.tabLayers1.getActiveLayer().setValue("height", "100%");
                            }
                        });
    
                        var c = new wm.PageContainer({
                            owner: main,
                            parent: l,
                            width: "100%",
                            height: "100%",
                            pageName: "PregledVoznjiGrafika"
                        });
    
                        main.tabLayers1.setLayer(l);
                    }
                } else if (app.varNacinPregleda.getValue("dataValue") == "tablicni") {
                    main.designableDialogPregledVoznji.show();
                }
		  }else{
              app.toastWarning("Nema resursa u odabranom terminu!");
		  }
	  } catch(e) {
		  console.error('ERROR IN serviceVarIsThereVoznjaSuccess: ' + e); 
	  } 
  },
  
  _end: 0
});