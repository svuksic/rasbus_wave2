OdabirVoznji.widgets = {
	serviceVariableGrafickiPrikaz: ["wm.ServiceVariable", {"operation":"getGrafickiPrikaz","service":"glavni"}, {}, {
		input: ["wm.ServiceInput", {"type":"getGrafickiPrikazInputs"}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire: ["wm.Wire", {"expression":undefined,"source":"datumPocetka.dataValue","targetProperty":"datumPocetka"}, {}],
				wire1: ["wm.Wire", {"expression":undefined,"source":"selectMenuPeriod.dataValue","targetProperty":"period"}, {}],
				wire2: ["wm.Wire", {"expression":undefined,"source":"selectMenuVozila.dataValue","targetProperty":"vozilo"}, {}],
				wire3: ["wm.Wire", {"expression":undefined,"source":"app.varListaOdabranihBusevaId.dataValue","targetProperty":"listaOdabranihBuseva"}, {}],
				wire4: ["wm.Wire", {"expression":undefined,"source":"app.varPeriod.dataValue","targetProperty":"periodPar"}, {}]
			}]
		}]
	}],
	busVarDB: ["wm.LiveVariable", {"liveSource":"com.rasbusdb.data.Bus"}, {}],
	serviceVarIsThereVoznja: ["wm.ServiceVariable", {"operation":"isThereResult","service":"glavni"}, {"onSuccess":"serviceVarIsThereVoznjaSuccess"}, {
		input: ["wm.ServiceInput", {"type":"isThereResultInputs"}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire: ["wm.Wire", {"expression":undefined,"source":"app.varDatumPocetkaPerioda.dataValue","targetProperty":"datumPocetka"}, {}],
				wire2: ["wm.Wire", {"expression":undefined,"source":"app.varPeriodOdabrani.dataValue","targetProperty":"periodPar"}, {}],
				wire1: ["wm.Wire", {"expression":undefined,"source":"app.varListaOdabranihBusevaId.dataValue","targetProperty":"listaOdabranihBuseva"}, {}]
			}]
		}]
	}],
	layoutBox1: ["wm.Layout", {"_classes":{"domNode":["wm_BackgroundColor_VeryLightGray"]},"height":"100%","horizontalAlign":"center","verticalAlign":"middle","width":"100%"}, {}, {
		panel5: ["wm.Panel", {"_classes":{"domNode":["wm_BorderBottomStyle_Curved4px","wm_BorderTopStyle_Curved4px","wm_BorderShadow_StrongShadow","wm_BackgroundColor_LightGray"]},"height":"357px","horizontalAlign":"center","margin":"2,0,0,2","verticalAlign":"middle","width":"367px"}, {}, {
			datumPocetka: ["wm.Date", {"_classes":{"domNode":["wm_FontColor_Black","wm_TextDecoration_Bold"]},"caption":"Datum početka perioda:","captionAlign":"left","captionSize":"170px","displayValue":"","formatter":"datumPocetkaReadOnlyNodeFormat","width":"350px"}, {"onchange":"datumPocetkaChange"}, {
				binding: ["wm.Binding", {}, {}, {
					wire: ["wm.Wire", {"expression":undefined,"source":"app.varDatumPocetkaPerioda.dataValue","targetProperty":"dataValue"}, {}]
				}]
			}],
			spacer1: ["wm.Spacer", {"height":"9px","width":"350px"}, {}],
			selectMenuPeriod: ["wm.SelectMenu", {"_classes":{"domNode":["wm_FontColor_Black","wm_TextDecoration_Bold"]},"caption":"Period:","captionAlign":"left","captionSize":"170px","dataField":"name","displayField":"dataValue","displayValue":"","height":"30px","margin":"5,0,0,0","width":"350px"}, {"onchange":"selectMenuPeriodChange"}, {
				binding: ["wm.Binding", {}, {}, {
					wire: ["wm.Wire", {"expression":undefined,"source":"app.varPeriod","targetProperty":"dataSet"}, {}]
				}]
			}],
			spacer2: ["wm.Spacer", {"height":"12px","width":"350px"}, {}],
			selectMenuVozila: ["wm.SelectMenu", {"_classes":{"domNode":["wm_FontColor_Black","wm_TextDecoration_Bold"]},"caption":"Vozilo:","captionAlign":"left","captionSize":"170px","dataField":"idBus","displayExpression":"${garazniBrojBus}+\" \"+${markaBus}+\" \"+${modelBus}","displayValue":"","width":"350px"}, {"onchange":"selectMenuVozilaChange"}, {
				binding: ["wm.Binding", {}, {}, {
					wire: ["wm.Wire", {"expression":undefined,"source":"busVarDB","targetProperty":"dataSet"}, {}]
				}]
			}],
			spacer3: ["wm.Spacer", {"height":"17px","width":"351px"}, {}],
			panel1: ["wm.Panel", {"height":"177px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
				panel2: ["wm.Panel", {"height":"176px","horizontalAlign":"left","verticalAlign":"top","width":"151px"}, {}, {
					buttonOdaberiBus: ["wm.Button", {"_classes":{"domNode":["wm_FontColor_Black"]},"caption":"Dodaj vozilo","margin":"4","width":"143px"}, {"onclick":"buttonOdaberiBusClick"}],
					btnBrisiBus: ["wm.Button", {"_classes":{"domNode":["wm_FontColor_Black"]},"caption":"Briši vozilo","margin":"4","width":"142px"}, {"onclick":"btnBrisiBusClick"}],
					btnDodajSve: ["wm.Button", {"_classes":{"domNode":["wm_FontColor_Black"]},"caption":"Dodaj sva vozila","height":"56px","margin":"4","width":"140px"}, {"onclick":"btnDodajSveClick"}],
					btnBrisiSve: ["wm.Button", {"_classes":{"domNode":["wm_FontColor_Black"]},"caption":"Briši sva vozila","height":"53px","margin":"4","width":"140px"}, {"onclick":"btnBrisiSveClick"}]
				}],
				panel3: ["wm.Panel", {"height":"177px","horizontalAlign":"left","verticalAlign":"top","width":"50%"}, {}, {
					lblBusevi: ["wm.Label", {"_classes":{"domNode":["wm_TextDecoration_Bold"]},"border":"0","caption":"Odabrana vozila:","height":"23px","padding":"4"}, {}],
					listOdabranihBuseva: ["wm.List", {"_classes":{"domNode":["wm_BackgroundColor_VeryLightGray","wm_BorderShadow_WeakShadow"]},"dataFields":"garazniBrojBus,modelBus,markaBus","headerVisible":false,"imageList":"","width":"200px"}, {}, {
						binding: ["wm.Binding", {}, {}, {
							wire: ["wm.Wire", {"expression":undefined,"source":"app.varOdabraniBusevi","targetProperty":"dataSet"}, {}]
						}]
					}]
				}]
			}],
			panel4: ["wm.Panel", {"height":"48px","horizontalAlign":"center","layoutKind":"left-to-right","verticalAlign":"middle","width":"100%"}, {}, {
				btnGrafickiPrikaz: ["wm.Button", {"_classes":{"domNode":["wm_FontColor_Black"]},"caption":"Generiraj prikaz vožnji","margin":"4","width":"295px"}, {"onclick":"btnGrafickiPrikazClick"}]
			}]
		}]
	}]
}