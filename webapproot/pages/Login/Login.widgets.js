Login.widgets = {
	layoutBox: ["wm.Layout", {"height":"100%"}, {}, {
		loginMainPanel: ["wm.Panel", {"height":"100%","horizontalAlign":"center","padding":"10","verticalAlign":"center","width":"100%"}, {}, {
			title: ["wm.Label", {"_classes":{"domNode":["wm_FontSizePx_14px","wm_FontColor_Black","wm_TextDecoration_Bold"]},"align":"center","border":"0","caption":"RasBus-dobrodošli","height":"20px","padding":"4","width":"188px"}, {}],
			loginInputPanel: ["wm.HeaderContentPanel", {"height":"177px","horizontalAlign":"center","padding":"10","verticalAlign":"center","width":"329px"}, {}, {
				panel1: ["wm.Panel", {"height":"65px","horizontalAlign":"center","layoutKind":"left-to-right","verticalAlign":"middle","width":"317px"}, {}, {
					panel2: ["wm.Panel", {"height":"100%","horizontalAlign":"right","layoutKind":"left-to-right","verticalAlign":"middle","width":"200px"}, {}, {
						picture1: ["wm.Picture", {"border":"0","height":"100%","source":"resources/images/Logo.gif","width":"69px"}, {}],
						panel3: ["wm.Panel", {"height":"100%","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"middle","width":"132px"}, {}, {
							panel4: ["wm.Panel", {"height":"100%","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"middle","width":"100%"}, {}, {
								label3: ["wm.Label", {"_classes":{"domNode":["wm_FontColor_White","wm_FontSizePx_20px","wm_TextDecoration_Bold"]},"border":"0","caption":"RasBUS","height":"33px","padding":"4","width":"93px"}, {}, {
									format: ["wm.DataFormatter", {}, {}]
								}],
								panel5: ["wm.Panel", {"height":"33px","horizontalAlign":"left","verticalAlign":"top","width":"38px"}, {}, {
									panel6: ["wm.Panel", {"height":"7px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}],
									verzija: ["wm.Label", {"_classes":{"domNode":["wm_FontColor_White","wm_TextDecoration_Bold"]},"border":"0","caption":"v1.0","padding":"4"}, {}]
								}]
							}]
						}]
					}],
					picture2: ["wm.Picture", {"border":"0","height":"100%","source":"resources/images/imagelists/failed.png","width":"89px"}, {}]
				}],
				usernameInput: ["wm.Text", {"_classes":{"domNode":["wm_TextDecoration_Bold","wm_FontColor_White"]},"caption":"Korisničko ime:","captionSize":"120px","dataValue":undefined,"displayValue":""}, {}],
				passwordInput: ["wm.Text", {"_classes":{"domNode":["wm_TextDecoration_Bold","wm_FontColor_White"]},"caption":"Lozinka:","captionSize":"120px","dataValue":undefined,"displayValue":"","password":true}, {}],
				loginButtonPanel: ["wm.Panel", {"height":"50px","horizontalAlign":"right","layoutKind":"left-to-right","padding":"4","width":"315px"}, {}, {
					loginErrorMsg: ["wm.Label", {"_classes":{"domNode":["wm_TextDecoration_Bold"]},"align":"center","border":"0","caption":" ","height":"100%","padding":"4","singleLine":false,"width":"130px"}, {}, {
						format: ["wm.DataFormatter", {}, {}]
					}],
					loginButton: ["wm.Button", {"caption":"Prijava","height":"100%","margin":"4","width":"90px"}, {"onclick":"loginButtonClick","onclick1":"app.varSecurity"}],
					Odustani: ["wm.Button", {"caption":"Odustani","margin":"4","width":"86px"}, {"onclick":"OdustaniClick"}]
				}]
			}]
		}]
	}]
}