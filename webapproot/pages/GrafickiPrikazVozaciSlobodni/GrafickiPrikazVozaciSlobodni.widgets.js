GrafickiPrikazVozaciSlobodni.widgets = {
	serviceVarGrafickiVozaciSlobodni: ["wm.ServiceVariable", {"autoUpdate":true,"operation":"getSlobodniVozaciGraficki","service":"glavni","startUpdate":true}, {"onSuccess":"serviceVarGrafickiVozaciSlobodniSuccess"}, {
		input: ["wm.ServiceInput", {"type":"getSlobodniVozaciGrafickiInputs"}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire: ["wm.Wire", {"expression":undefined,"source":"[definiranjeTerminaSlobodniVozaci].dateKrajVoznje.dataValue","targetProperty":"krajVoznje"}, {}],
				wire1: ["wm.Wire", {"expression":undefined,"source":"[definiranjeTerminaSlobodniVozaci].datePocetakVoznje.dataValue","targetProperty":"pocetakVoznje"}, {}]
			}]
		}]
	}],
	layoutSlobodniVozaci: ["wm.Layout", {"height":"100%","horizontalAlign":"left","verticalAlign":"bottom","width":"100%"}, {}, {
		htmlGrafickiPrikazHeader: ["wm.Html", {"border":"0","height":"5%","html":"<div id=\"header\" style=\"background:#F0F0F0; height:100%; width:100%\" overflow=\"\" :=\"\" auto;=\"\"></div>"}, {}],
		htmlGrafickiPrikaz: ["wm.Html", {"border":"0","height":"95%","html":"<div id=\"povrsina\" style=\"background:#F0F0F0; height:100%; width:100%\" overflow=\"\" :=\"\" auto;=\"\"></div>"}, {}]
	}]
}