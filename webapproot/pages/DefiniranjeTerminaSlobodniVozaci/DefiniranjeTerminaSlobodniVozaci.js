var slobodniVozaciGrafickiPregledOtvoren = false;
var l;
dojo.declare("DefiniranjeTerminaSlobodniVozaci", wm.Page, {
    start: function() {
        try {

        } catch (e) {
            app.toastError(this.name + ".start() Failed: " + e.toString());
        }
    },

    btnGrafickiPrikazClick: function(inSender) {
        try {
            var pocetak = new Date(this.datePocetakVoznje.displayValue);
            var zavrsetak = new Date(this.dateKrajVoznje.displayValue);
            if (zavrsetak < pocetak) {
                alert("Pogrešan odnos datuma početka i datuma kraja perioda!");
                return;
            } else if (Math.floor(((zavrsetak / (1000 * 60 * 60 * 24)) - (pocetak / (1000 * 60 * 60 * 24)))) > 30) {
                alert("Moguće je odabrati period od najviše 31 dan");
                return;
            }
            if (slobodniVozaciGrafickiPregledOtvoren === false) {
                slobodniVozaciGrafickiPregledOtvoren = true;
                app.varNacinPregledaVozac.setValue("dataValue", "tablicniVozac");
                l = new wm.Layer({
                    owner: main,
                    parent: main.tabLayers1,
                    caption: "Slobodni vozači",
                    width: "100%",
                    height: "100%",
                    closable: true,
                    name: "PregledGrafickiSlobodniVozaci",
                    onCloseOrDestroy: function okini(item) {
                        slobodniVozaciGrafickiPregledOtvoren = false;
                        l.destroy();
                        c.destroy();
                        //ovo tu jer se inca pozdaina cudno ponasa nakon gasenja
                        main.tabLayers1.getActiveLayer().setValue("width", "100px");
                        main.tabLayers1.getActiveLayer().setValue("height", "100%");
                    }
                });

                var c = new wm.PageContainer({
                    owner: main,
                    parent: l,
                    width: "100%",
                    height: "100%",
                    pageName: "GrafickiPrikazVozaciSlobodni"
                });
            }
            main.tabLayers1.setLayer(l);

        } catch (e) {
            console.error('ERROR IN btnGrafickiPrikazClick: ' + e);
        }
    },
    datePocetakVoznjeReadOnlyNodeFormat: function(inSender, inValue) {
        try {
            return app.dateFormat(inValue);
        } catch (e) {
            console.error('ERROR IN datePocetakVoznjeReadOnlyNodeFormat: ' + e);
        }
    },
    dateKrajVoznjeChange: function(inSender) {
        try {
            if (this.datePocetakVoznje.getDisplayValue() != "" && this.dateKrajVoznje.getDisplayValue() != "") {
                this.btnGrafickiPrikaz.enable();
            } else {
                this.btnGrafickiPrikaz.disable();
            }
        } catch (e) {
            console.error('ERROR IN dateKrajVoznjeChange: ' + e);
        }
    },
    datePocetakVoznjeBlur: function(inSender) {
        try {
            //   this.datePocetakVoznje.setReadonly(true);
        } catch (e) {
            console.error('ERROR IN datePocetakVoznjeBlur: ' + e);
        }
    },
    dateKrajVoznjeReadOnlyNodeFormat: function(inSender, inValue) {
        try {
            return app.dateFormat(inValue);

        } catch (e) {
            console.error('ERROR IN dateKrajVoznjeReadOnlyNodeFormat: ' + e);
        }
    },
    dateKrajVoznjeBlur: function(inSender) {
        try {
            // this.dateKrajVoznje.setReadonly(true);
        } catch (e) {
            console.error('ERROR IN dateKrajVoznjeBlur: ' + e);
        }
    },
    datePocetakVoznjeChange: function(inSender) {
        try {
            if (this.dateKrajVoznje.getDisplayValue() != "" && this.datePocetakVoznje.getDisplayValue() != "") {
                this.btnGrafickiPrikaz.enable();
            } else {
                this.btnGrafickiPrikaz.disable();
            }

        } catch (e) {
            console.error('ERROR IN datePocetakVoznjeChange: ' + e);
        }
    },
    _end: 0
});