DefiniranjeTerminaSlobodniVozaci.widgets = {
	ServiceMinimalniDatum: ["wm.ServiceVariable", {"operation":"getSlobodniVozaci","service":"glavni"}, {}, {
		input: ["wm.ServiceInput", {"type":"getSlobodniVozaciInputs"}, {}]
	}],
	layoutBox1: ["wm.Layout", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"100%"}, {}, {
		panel6: ["wm.Panel", {"height":"374px","horizontalAlign":"center","layoutKind":"left-to-right","verticalAlign":"middle","width":"100%"}, {}, {
			panel5: ["wm.Panel", {"_classes":{"domNode":["wm_BorderBottomStyle_Curved4px","wm_BorderTopStyle_Curved4px","wm_BorderShadow_StrongShadow","wm_BackgroundColor_LightGray"]},"height":"143px","horizontalAlign":"left","margin":"2,0,0,2","verticalAlign":"top","width":"367px"}, {}, {
				spacer1: ["wm.Spacer", {"height":"9px","width":"350px"}, {}],
				panel1: ["wm.Panel", {"height":"24px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"bottom","width":"100%"}, {}, {
					datePocetakVoznje: ["wm.Date", {"caption":"Početak vožnje:","captionSize":"150px","customToHtml":"datePocetakVoznjeCustomToHtml","dataValue":undefined,"displayValue":"","formatter":"datePocetakVoznjeReadOnlyNodeFormat","required":true,"useLocalTime":true,"width":"327px"}, {"onblur":"datePocetakVoznjeBlur","onchange":"datePocetakVoznjeChange"}]
				}],
				spacer2: ["wm.Spacer", {"height":"12px","width":"350px"}, {}],
				panel2: ["wm.Panel", {"height":"24px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"bottom","width":"100%"}, {}, {
					dateKrajVoznje: ["wm.Date", {"caption":"Kraj vožnje:","captionSize":"150px","dataValue":undefined,"displayValue":"","formatter":"dateKrajVoznjeReadOnlyNodeFormat","required":true,"width":"327px"}, {"onblur":"dateKrajVoznjeBlur","onchange":"dateKrajVoznjeChange"}]
				}],
				panel4: ["wm.Panel", {"height":"48px","horizontalAlign":"center","layoutKind":"left-to-right","verticalAlign":"middle","width":"100%"}, {}, {
					btnGrafickiPrikaz: ["wm.Button", {"_classes":{"domNode":["wm_FontColor_Black"]},"caption":"Generiraj prikaz","disabled":true,"margin":"4","width":"295px"}, {"onclick":"btnGrafickiPrikazClick"}]
				}]
			}]
		}]
	}]
}