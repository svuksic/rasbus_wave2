OdabirVoznjiVozac.widgets = {
	vozacVarDB: ["wm.LiveVariable", {"liveSource":"com.rasbusdb.data.Vozac"}, {}],
	serviceVarIsThereVozac: ["wm.ServiceVariable", {"operation":"isThereResultVozac","service":"glavni"}, {"onSuccess":"serviceVarIsThereVozacSuccess"}, {
		input: ["wm.ServiceInput", {"type":"isThereResultVozacInputs"}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire1: ["wm.Wire", {"expression":undefined,"source":"app.varListaOdabranihVozacaID.dataValue","targetProperty":"listaOdabranihVozaca"}, {}],
				wire2: ["wm.Wire", {"expression":undefined,"source":"app.varPeriodOdabrani.dataValue","targetProperty":"periodPar"}, {}],
				wire: ["wm.Wire", {"expression":undefined,"source":"app.varDatumPocetkaPerioda.dataValue","targetProperty":"datumPocetka"}, {}]
			}]
		}]
	}],
	layoutBox1: ["wm.Layout", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"100%"}, {}, {
		layoutBox2: ["wm.Layout", {"_classes":{"domNode":["wm_BackgroundColor_VeryLightGray"]},"height":"100%","horizontalAlign":"center","verticalAlign":"middle","width":"100%"}, {}, {
			panel6: ["wm.Panel", {"height":"374px","horizontalAlign":"center","layoutKind":"left-to-right","verticalAlign":"middle","width":"100%"}, {}, {
				panel5: ["wm.Panel", {"_classes":{"domNode":["wm_BorderBottomStyle_Curved4px","wm_BorderTopStyle_Curved4px","wm_BorderShadow_StrongShadow","wm_BackgroundColor_LightGray"]},"height":"357px","horizontalAlign":"left","margin":"2,0,0,2","verticalAlign":"top","width":"367px"}, {}, {
					datumPocetka: ["wm.Date", {"_classes":{"domNode":["wm_FontColor_Black","wm_TextDecoration_Bold"]},"caption":"Datum početka perioda:","captionAlign":"left","captionSize":"170px","displayValue":"","formatter":"datumPocetkaReadOnlyNodeFormat","width":"350px"}, {"onchange":"datumPocetkaChange"}, {
						binding: ["wm.Binding", {}, {}, {
							wire: ["wm.Wire", {"expression":undefined,"source":"app.varDatumPocetkaPerioda.dataValue","targetProperty":"dataValue"}, {}]
						}]
					}],
					spacer1: ["wm.Spacer", {"height":"9px","width":"350px"}, {}],
					selectMenuPeriod: ["wm.SelectMenu", {"_classes":{"domNode":["wm_FontColor_Black","wm_TextDecoration_Bold"]},"caption":"Period:","captionAlign":"left","captionSize":"170px","dataField":"name","displayField":"dataValue","displayValue":"","height":"30px","margin":"5,0,0,0","width":"350px"}, {"onchange":"selectMenuPeriodChange"}, {
						binding: ["wm.Binding", {}, {}, {
							wire: ["wm.Wire", {"expression":undefined,"source":"app.varPeriod","targetProperty":"dataSet"}, {}]
						}]
					}],
					spacer2: ["wm.Spacer", {"height":"12px","width":"350px"}, {}],
					selectMenuVozaci: ["wm.SelectMenu", {"_classes":{"domNode":["wm_TextDecoration_Bold","wm_FontColor_Black"]},"caption":"Vozač:","captionAlign":"left","captionSize":"170px","displayExpression":"${imeVozac}+\" \"+${prezimeVozac}","displayValue":"","width":"350px"}, {"onchange":"selectMenuVozaciChange"}, {
						binding: ["wm.Binding", {}, {}, {
							wire: ["wm.Wire", {"expression":undefined,"source":"vozacVarDB","targetProperty":"dataSet"}, {}]
						}]
					}],
					spacer3: ["wm.Spacer", {"height":"17px","width":"351px"}, {}],
					panel1: ["wm.Panel", {"height":"177px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
						panel2: ["wm.Panel", {"height":"176px","horizontalAlign":"left","verticalAlign":"top","width":"151px"}, {}, {
							buttonOdaberiVozac: ["wm.Button", {"_classes":{"domNode":["wm_FontColor_Black"]},"caption":"Dodaj vozača","margin":"4","width":"143px"}, {"onclick":"buttonOdaberiVozacClick"}],
							btnBrisiVozac: ["wm.Button", {"_classes":{"domNode":["wm_FontColor_Black"]},"caption":"Briši vozača","margin":"4","width":"142px"}, {"onclick":"btnBrisiVozacClick"}],
							btnDodajSve: ["wm.Button", {"_classes":{"domNode":["wm_FontColor_Black"]},"caption":"Dodaj sve vozače","height":"56px","margin":"4","width":"140px"}, {"onclick":"btnDodajSveClick"}],
							btnBrisiSve: ["wm.Button", {"_classes":{"domNode":["wm_FontColor_Black"]},"caption":"Briši sve vozače","height":"53px","margin":"4","width":"140px"}, {"onclick":"btnBrisiSveClick"}]
						}],
						panel3: ["wm.Panel", {"height":"177px","horizontalAlign":"left","verticalAlign":"top","width":"50%"}, {}, {
							lblBusevi: ["wm.Label", {"_classes":{"domNode":["wm_TextDecoration_Bold"]},"border":"0","caption":"Odabrani vozači:","height":"23px","padding":"4"}, {}],
							listOdabranihVozaca: ["wm.List", {"_classes":{"domNode":["wm_BackgroundColor_VeryLightGray","wm_BorderTopStyle_NoCurve","wm_BorderBottomStyle_NoCurve","wm_BorderShadow_WeakShadow"]},"dataFields":"imeVozac,prezimeVozac","headerVisible":false,"imageList":"","width":"200px"}, {}, {
								binding: ["wm.Binding", {}, {}, {
									wire: ["wm.Wire", {"expression":undefined,"source":"app.varOdabraniVozaci","targetProperty":"dataSet"}, {}]
								}]
							}]
						}]
					}],
					panel4: ["wm.Panel", {"height":"48px","horizontalAlign":"center","layoutKind":"left-to-right","verticalAlign":"middle","width":"100%"}, {}, {
						btnGrafickiPrikaz: ["wm.Button", {"_classes":{"domNode":["wm_FontColor_Black"]},"caption":"Generiraj prikaz vožnji","margin":"4","width":"295px"}, {"onclick":"btnGrafickiPrikazClick"}]
					}]
				}]
			}]
		}]
	}]
}