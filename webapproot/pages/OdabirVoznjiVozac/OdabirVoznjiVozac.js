var otvorenTabPregledVoznjiVozac = false;
var vozaciString = "";

dojo.declare("OdabirVoznjiVozac", wm.Page, {
    start: function() {
        try {} catch (e) {
            app.toastError(this.name + ".start() Failed: " + e.toString());
        }
    },

    buttonOdaberiVozacClick: function(inSender) {
        try {

            app.varOdabraniVozaci.addItem(this.selectMenuVozaci.selectedItem);
            vozaciString += this.selectMenuVozaci.selectedItem.getData().idvozac + ",";
        } catch (e) {
            console.error('ERROR IN buttonOdaberiVozacClick: ' + e);
        }
    },

    btnGrafickiPrikazClick: function(inSender) {
        try {
            if (app.varOdabraniVozaci.getCount() > 0 && this.selectMenuPeriod.getDisplayValue() !== "" && this.datumPocetka.getDisplayValue() !== "") {
                app.varListaOdabranihVozacaID.setValue("dataValue", vozaciString);
                this.serviceVarIsThereVozac.update();
            } else {
                app.toastWarning("Potrebno popuniti sva polja!");
            }
        } catch (e) {
            console.error('ERROR IN btnGrafickiPrikazClick: ' + e);
        }
    },

    serviceVarIsThereVozacSuccess: function(inSender, inDeprecated) {
        try {
            if (inSender.getData().dataValue === true) {
                if (app.varNacinPregledaVozac.getValue("dataValue") == "grafickiVozac") {
                    if (otvorenTabPregledVoznjiVozac === false) {
                        otvorenTabPregledVoznjiVoazc = true;
                        var l = new wm.Layer({
                            owner: main,
                            parent: main.tabLayers1,
                            caption: "Pregled vožnji prema vozaču od: " + app.varDatumPocetkaPerioda.getValue("dataValue"),
                            width: "100%",
                            height: "100%",
                            closable: true,
                            name: "pregled",
                            onCloseOrDestroy: function okini(item) {
                                otvorenTabPregledVoznjiVozac = false;
                                l.destroy();
                                c.destroy();
                                main.tabLayers1.getActiveLayer().setValue("width", "100px");
                                main.tabLayers1.getActiveLayer().setValue("height", "100%");
                            }
                        });

                        var c = new wm.PageContainer({
                            owner: main,
                            parent: l,
                            width: "100%",
                            height: "100%",
                            pageName: "PregledVoznjiGrafikaVozac"
                        });

                        main.tabLayers1.setLayer(l);
                    }
                } else if (app.varNacinPregledaVozac.getValue("dataValue") == "tablicniVozac") {
                    main.designableDialogpregledVoznjiVozac.show();
                }
            } else {
                app.toastWarning("Nema resursa u odabranom terminu!");
            }
        } catch (e) {
            console.error('ERROR IN serviceVarIsThereVozacSuccess: ' + e);
        }
    },

    datumPocetkaChange: function(inSender) {
        try {
            app.varDatumPocetkaPerioda.setValue("dataValue", this.datumPocetka.displayValue);
        } catch (e) {
            console.error('ERROR IN datumPocetkaChange: ' + e);
        }
    },
    selectMenuPeriodChange: function(inSender) {
        try {
            app.varPeriodOdabrani.setValue("dataValue", this.selectMenuPeriod.selectedItem.getData().dataValue);
        } catch (e) {
            console.error('ERROR IN selectMenuPeriodChange: ' + e);
        }
    },


    btnDodajSveClick: function(inSender) {
        try {
            vozaciString = "";
            for (var i = 0; i < this.vozacVarDB.getCount(); i++) {
                app.varOdabraniVozaci.addItem(this.vozacVarDB.getItem(i));
                vozaciString += this.vozacVarDB.getItem(i).getValue("idvozac") + ",";
            }
        } catch (e) {
            console.error('ERROR IN btnDodajSveClick: ' + e);
        }
    },
    btnBrisiSveClick: function(inSender) {
        try {
            vozaciString = "";
            app.varOdabraniVozaci.clearData();
        } catch (e) {
            console.error('ERROR IN btnBrisiSveClick: ' + e);
        }
    },
    btnBrisiVozacClick: function(inSender) {
        try {
            for (var i = 0; i < app.varOdabraniVozaci.getCount(); i++) {
                var index = this.listOdabranihVozaca.getSelectedIndex();
                var item = this.listOdabranihVozaca.getItem(index);
                if (app.varOdabraniVozaci.getItem(i).getValue("idvozac") === item.getData().idvozac) {
                    app.varOdabraniVozaci.removeItem(i);
                }
            }
            buseviString = "";
            for (var ii = 0; ii < app.varOdabraniVozaci.getCount(); ii++) {
                vozaciString += this.vozacVarDB.getItem(ii).getValue("idvozac") + ",";
            }
        } catch (e) {
            console.error('ERROR IN btnBrisiVozacClick: ' + e);
        }
    },
    _end: 0
});