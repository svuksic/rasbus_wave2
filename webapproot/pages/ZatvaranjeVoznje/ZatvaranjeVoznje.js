var selectedItem;
dojo.declare("ZatvaranjeVoznje", wm.Page, {
    start: function() {
        try {



            var t1 = this.dialogOtvoreneVoznje.titleBar;
            t1.moveControl(t1.c$[3], 1);
            t1.moveControl(t1.c$[0], 4);
            t1.reflow();

        } catch (e) {
            app.toastError(this.name + ".start() Failed: " + e.toString());
        }
    },

    dojoGrid1CustomFieldFormat: function(inValue, rowId, cellId, cellField, cellObj, rowObj) {
        try {
            return '<button type="button" onclick=wm.Page.getPage("ZatvaranjeVoznje").odaberiVoznju(' + rowId + ');>Odaberi vožnju</button>';
        } catch (e) {
            console.error('ERROR IN dojoGrid1CustomFieldFormat: ' + e);
        }
    },

    odaberiVoznju: function(rowId) {
        selectedItem = this.dojoGrid1.getRow(rowId);
        this.textIdVoznje.setDataValue(selectedItem.idvoznja);
        var selectedRow = this.dojoGrid1.getRow(rowId);
        this.textOznakaVoznje.setDisplayValue(selectedRow.oznakaVoznja);
        this.textOznakaVoznje.setShowing(true);
        this.textOpisVoznje.setDisplayValue(selectedRow.opisVoznja);
        this.textOpisVoznje.setShowing(true);
        this.textMjestoPolaska.setDisplayValue(selectedRow.mjestoPolaskaVoznja);
        this.textMjestoPolaska.setShowing(true);
        this.textMjestoDolaska.setDisplayValue(selectedRow.mjestoDolaskaVoznja);
        this.textMjestoDolaska.setShowing(true);
        d = new Date(selectedRow.vrijemePolaskaVoznja);
        strDatum = d.getDate() + "." + (d.getMonth() + 1) + "." + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes();
        this.textVrijemePolaska.setDisplayValue(strDatum);
        this.textVrijemePolaska.setShowing(true);
        d = new Date(selectedRow.vrijemeDolaskaVoznja);
        strDatum = d.getDate() + "." + (d.getMonth() + 1) + "." + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes();
        this.textVrijemeDolaska.setDisplayValue(strDatum);
        this.textVrijemeDolaska.setShowing(true);

        this.splitter1.setShowing(true);

        this.numKilometri.setShowing(true);
        this.textTroskovi.setShowing(true);

        this.buttonSpremiZatvorenuVoznju.setShowing(true);

        this.dialogOtvoreneVoznje.dismiss();
    },

    buttonSpremiZatvorenuVoznjuClick: function(inSender) {
        try {
            this.varZatvorenaVoznja.setValue("idzatvorenaVoznja", selectedItem.idvoznja);
            this.varZatvorenaVoznja.setValue("kilometri", this.numKilometri.getDataValue());
            this.varZatvorenaVoznja.setValue("troskovi", this.currencyTroskovi.getDataValue());
            this.liveForm1.setDataSet(this.varZatvorenaVoznja);
            this.liveForm1.insertData();
            this.ocistiPoljaPodataka();
            app.toastSuccess("Zatvorena vožnja s oznakom: " + selectedItem.oznakaVoznja, 1000);

        } catch (e) {
            console.error('ERROR IN buttonSpremiZatvorenuVoznjuClick: ' + e);
        }
    },

    ocistiPoljaPodataka: function() {
        this.textOznakaVoznje.setShowing(false);
        this.textOznakaVoznje.clear();
        this.textOpisVoznje.setShowing(false);
        this.textOpisVoznje.clear();
        this.textMjestoPolaska.setShowing(false);
        this.textMjestoPolaska.clear();
        this.textMjestoDolaska.setShowing(false);
        this.textMjestoDolaska.clear();
        this.textVrijemePolaska.setShowing(false);
        this.textVrijemePolaska.clear();
        this.textVrijemeDolaska.setShowing(false);
        this.textVrijemeDolaska.clear();

        this.splitter1.setShowing(false);

        this.numKilometri.setShowing(false);
        this.numKilometri.clear();
        this.textTroskovi.setShowing(false);
        this.textTroskovi.clear();

        this.buttonSpremiZatvorenuVoznju.setShowing(false);
    },
    dojoGrid1VrijemePolaskaVoznjaFormat: function(inValue, rowId, cellId, cellField, cellObj, rowObj) {
        try {
            var d = new Date(inValue);
            var strDatum = d.getDate() + "." + (d.getMonth() + 1) + "." + d.getFullYear();
            return strDatum;
        } catch (e) {
            console.error('ERROR IN dojoGrid1VrijemePolaskaVoznjaFormat: ' + e);
        }
    },
    dojoGrid1VrijemeDolaskaVoznjaFormat: function(inValue, rowId, cellId, cellField, cellObj, rowObj) {
        try {
            var d = new Date(inValue);
            var strDatum = d.getDate() + "." + (d.getMonth() + 1) + "." + d.getFullYear();
            return strDatum;
        } catch (e) {
            console.error('ERROR IN dojoGrid1VrijemeDolaskaVoznjaFormat: ' + e);
        }
    },

    serviceVarZatvoriVoznjuSuccess: function(inSender, inDeprecated) {
        try {
            this.ocistiPoljaPodataka();
            app.toastSuccess("Zatvorena vožnja s oznakom: " + selectedItem.oznakaVoznja, 1000);
        } catch (e) {
            console.error('ERROR IN serviceVarZatvoriVoznjuSuccess: ' + e);
        }
    },
    ResetVrijemePolaskaClick: function(inSender) {
        try {
            this.dateVrijemePolaska.setDataValue("");

        } catch (e) {
            console.error('ERROR IN ResetVrijemePolaskaClick: ' + e);
        }
    },
    ResetVrijemeDolaskaClick: function(inSender) {
        try {
            this.dateVrijemeDolaska.setDataValue("");

        } catch (e) {
            console.error('ERROR IN ResetVrijemeDolaskaClick: ' + e);
        }
    },
    ///metoda za filtriranje sa vrmenskom zadrškom
    callServiceVarGetOtvoreneVoznje: function(){
       try {
          if (this._mytimeout) {
              window.clearTimeout(this._mytimeout);
          } 
          this.serviceVarGetOtvoreneVoznje.input.setValue('garazniBroj', this.textGarBroj.getDataValue());
          this.serviceVarGetOtvoreneVoznje.input.setValue('mjestoDolaska', this.textMjeDolaska.getDataValue());
          this.serviceVarGetOtvoreneVoznje.input.setValue('mjestoPolaska', this.textMjePolaska.getDataValue());
          this.serviceVarGetOtvoreneVoznje.input.setValue('prezimeVozaca', this.textPrezVozac.getDataValue());
          this.serviceVarGetOtvoreneVoznje.input.setValue('vrijemeDolaska', this.dateVrijemeDolaska.getDataValue());
          this.serviceVarGetOtvoreneVoznje.input.setValue('vrijemePolaska', this.dateVrijemePolaska.getDataValue());
          this.serviceVarGetOtvoreneVoznje.input.setValue('oznakaVoznje', this.textFilterOznaka.getDataValue());
          this._mytimeout = window.setTimeout(dojo.hitch(app, 'filterGetOtvoreneVoznje', this.serviceVarGetOtvoreneVoznje), app.filteriTimeOut);		  
	  } catch(e) {
		  console.error('ERROR IN textMjestoDolaskaFilterChange: ' + e); 
	  }
  },
    textMjePolaskaChange: function(inSender) {
	  try {
		  this.callServiceVarGetOtvoreneVoznje();
	  } catch(e) {
		  console.error('ERROR IN textMjePolaskaChange: ' + e); 
	  } 
  },
  textMjeDolaskaChange: function(inSender) {
	  try {
		  this.callServiceVarGetOtvoreneVoznje();		  
	  } catch(e) {
		  console.error('ERROR IN textMjeDolaskaChange: ' + e); 
	  } 
  },
  textPrezVozacChange: function(inSender) {
	  try {		  
		  this.callServiceVarGetOtvoreneVoznje();
	  } catch(e) {
		  console.error('ERROR IN textPrezVozacChange: ' + e); 
	  } 
  },
  textGarBrojChange: function(inSender) {
	  try {		  
		  this.callServiceVarGetOtvoreneVoznje();
	  } catch(e) {
		  console.error('ERROR IN textGarBrojChange: ' + e); 
	  } 
  },
  dateVrijemePolaskaChange: function(inSender) {
	  try {		  
		  this.callServiceVarGetOtvoreneVoznje();
	  } catch(e) {
		  console.error('ERROR IN dateVrijemePolaskaChange: ' + e); 
	  } 
  },
  dateVrijemeDolaskaChange: function(inSender) {
	  try {		  
		  this.callServiceVarGetOtvoreneVoznje();
	  } catch(e) {
		  console.error('ERROR IN dateVrijemeDolaskaChange: ' + e); 
	  } 
  },
  textFilterOznakaChange: function(inSender) {
	  try {		  
		  this.callServiceVarGetOtvoreneVoznje();
	  } catch(e) {
		  console.error('ERROR IN textFilterOznakaChange: ' + e); 
	  } 
  },
  _end: 0
});