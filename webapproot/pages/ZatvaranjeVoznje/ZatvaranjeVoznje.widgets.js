ZatvaranjeVoznje.widgets = {
	dialogOtvoreneVoznje: ["wm.DesignableDialog", {"buttonBarId":"buttonBar","containerWidgetId":"containerWidget","modal":false,"title":"Otvorene vožnje","width":"800px"}, {"onShow":"serviceVarGetOtvoreneVoznje"}, {
		containerWidget: ["wm.Container", {"_classes":{"domNode":["wmdialogcontainer","MainContent"]},"autoScroll":true,"height":"100%","horizontalAlign":"left","margin":"0","padding":"5","verticalAlign":"top","width":"100%"}, {}, {
			panel4: ["wm.Panel", {"height":"80px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
				panel6: ["wm.Panel", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"255px"}, {}, {
					textMjePolaska: ["wm.Text", {"caption":"Mjesto polaska:","captionSize":"120px","changeOnKey":true,"dataValue":undefined,"displayValue":"","resetButton":true,"width":"250px"}, {"onchange":"textMjePolaskaChange"}],
					textMjeDolaska: ["wm.Text", {"caption":"Mjesto dolaska:","captionSize":"120px","changeOnKey":true,"dataValue":undefined,"displayValue":"","resetButton":true,"width":"250px"}, {"onchange":"textMjeDolaskaChange"}],
					textPrezVozac: ["wm.Text", {"caption":"Prezime vozača:","captionSize":"120px","changeOnKey":true,"dataValue":undefined,"displayValue":"","resetButton":true,"width":"250px"}, {"onchange":"textPrezVozacChange"}]
				}],
				panel7: ["wm.Panel", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"283px"}, {}, {
					textGarBroj: ["wm.Text", {"caption":"Garažni broj vozila:","captionSize":"150px","changeOnKey":true,"dataValue":undefined,"displayValue":"","resetButton":true,"width":"275px"}, {"onchange":"textGarBrojChange"}],
					panel8: ["wm.Panel", {"height":"24px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"bottom","width":"100%"}, {}, {
						dateVrijemePolaska: ["wm.Date", {"caption":"Vrijeme polaska:","captionSize":"150px","dataValue":undefined,"displayValue":"","width":"250px"}, {"onchange":"dateVrijemePolaskaChange"}],
						ResetVrijemePolaska: ["wm.Picture", {"_classes":{"domNode":["wm_Mouse_pointer"]},"border":"0","height":"20px","source":"resources/images/Reset.png","width":"25px"}, {"onclick":"ResetVrijemePolaskaClick"}]
					}],
					panel9: ["wm.Panel", {"_classes":{"domNode":["wm_Mouse_pointer"]},"height":"24px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"bottom","width":"100%"}, {}, {
						dateVrijemeDolaska: ["wm.Date", {"caption":"Vrijeme dolaska:","captionSize":"150px","dataValue":undefined,"displayValue":"","width":"250px"}, {"onchange":"dateVrijemeDolaskaChange"}],
						ResetVrijemeDolaska: ["wm.Picture", {"border":"0","height":"20px","source":"resources/images/Reset.png","width":"25px"}, {"onclick":"ResetVrijemeDolaskaClick"}]
					}]
				}],
				panel10: ["wm.Panel", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"100%"}, {}, {
					textFilterOznaka: ["wm.Text", {"caption":"Oznaka:","captionSize":"80px","changeOnKey":true,"dataValue":undefined,"displayValue":"","resetButton":true,"width":"200px"}, {"onchange":"textFilterOznakaChange"}]
				}]
			}],
			dojoGrid1: ["wm.DojoGrid", {"columns":[{"show":false,"id":"idvoznja","title":"Idvoznja","width":"80px","displayType":"Number","noDelete":true,"align":"right","formatFunc":""},{"show":true,"id":"oznakaVoznja","title":"Oznaka","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"opisVoznja","title":"Napomena","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"mjestoPolaskaVoznja","title":"Mjesto polaska","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"mjestoDolaskaVoznja","title":"Mjesto dolaska","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"vrijemePolaskaVoznja","title":"Vrijeme polaska","width":"80px","displayType":"Date","noDelete":true,"align":"left","formatFunc":"dojoGrid1VrijemePolaskaVoznjaFormat"},{"show":true,"id":"vrijemeDolaskaVoznja","title":"Vrijeme dolaska","width":"80px","displayType":"Date","noDelete":true,"align":"left","formatFunc":"dojoGrid1VrijemeDolaskaVoznjaFormat"},{"show":false,"id":"ugovorenaCijenaVoznja","title":"Ugovorena cijena","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"id":"customField","isCustomField":true,"expression":"","show":true,"width":"100%","title":"Odaberi","formatFunc":"dojoGrid1CustomFieldFormat"}],"margin":"4"}, {}, {
				binding: ["wm.Binding", {}, {}, {
					wire: ["wm.Wire", {"expression":undefined,"source":"serviceVarGetOtvoreneVoznje","targetProperty":"dataSet"}, {}]
				}]
			}]
		}],
		buttonBar: ["wm.Panel", {"_classes":{"domNode":["dialogfooter"]},"height":"32px","horizontalAlign":"right","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}]
	}],
	varZatvorenaVoznja: ["wm.Variable", {"type":"com.rasbusdb.data.Zatvorenavoznja"}, {}],
	liveVarZatvoreneVoznje: ["wm.LiveVariable", {"liveSource":"com.rasbusdb.data.Zatvorenavoznja"}, {}],
	serviceVarZatvoriVoznju: ["wm.ServiceVariable", {"operation":"zatvaranjeVoznje","service":"glavni"}, {"onSuccess":"serviceVarZatvoriVoznjuSuccess"}, {
		input: ["wm.ServiceInput", {"type":"zatvaranjeVoznjeInputs"}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire: ["wm.Wire", {"expression":undefined,"source":"textIdVoznje.dataValue","targetProperty":"idVoznje"}, {}],
				wire1: ["wm.Wire", {"expression":undefined,"source":"numKilometri.dataValue","targetProperty":"kilometri"}, {}],
				wire2: ["wm.Wire", {"expression":undefined,"source":"textTroskovi.dataValue","targetProperty":"troskovi"}, {}]
			}]
		}]
	}],
	serviceVarGetOtvoreneVoznje: ["wm.ServiceVariable", {"operation":"getOtvoreneVoznje","service":"glavni"}, {}, {
		input: ["wm.ServiceInput", {"type":"getOtvoreneVoznjeInputs"}, {}]
	}],
	layoutBox1: ["wm.Layout", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"714px"}, {}, {
		fancyPanel1: ["wm.FancyPanel", {"title":"Zatvaranje vožnje"}, {}, {
			panel1: ["wm.Panel", {"height":"100%","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
				panel2: ["wm.Panel", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"50%"}, {}, {
					panel5: ["wm.Panel", {"height":"30px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
						button1: ["wm.Button", {"caption":"Odaberi vožnju","iconUrl":"resources/images/Searchicon16x16.png","margin":"4","width":"183px"}, {"onclick":"dialogOtvoreneVoznje.show"}]
					}],
					textOznakaVoznje: ["wm.Text", {"caption":"Oznaka vožnje:","captionSize":"150px","dataValue":undefined,"displayValue":"","height":"30px","readonly":true,"showing":false,"width":"100%"}, {}],
					textOpisVoznje: ["wm.Text", {"caption":"Napomena:","captionSize":"150px","dataValue":undefined,"displayValue":"","height":"30px","readonly":true,"showing":false,"width":"100%"}, {}],
					textMjestoPolaska: ["wm.Text", {"caption":"Mjesto polaska:","captionSize":"150px","dataValue":undefined,"displayValue":"","height":"30px","readonly":true,"showing":false,"width":"100%"}, {}],
					textMjestoDolaska: ["wm.Text", {"caption":"Mjesto dolaska:","captionSize":"150px","dataValue":undefined,"displayValue":"","height":"30px","readonly":true,"showing":false,"width":"100%"}, {}],
					textVrijemePolaska: ["wm.Text", {"caption":"Vrijeme polaska:","captionSize":"150px","dataValue":undefined,"displayValue":"","height":"30px","readonly":true,"showing":false,"width":"100%"}, {}],
					textVrijemeDolaska: ["wm.Text", {"caption":"Vrijeme dolaska:","captionSize":"150px","dataValue":undefined,"displayValue":"","height":"30px","readonly":true,"showing":false,"width":"100%"}, {}],
					splitter1: ["wm.Splitter", {"height":"10px","showing":false,"width":"100%"}, {}],
					numKilometri: ["wm.Number", {"caption":"Kilometri:","captionSize":"150px","displayValue":"","height":"30px","invalidMessage":"Duljina puta u Km!","places":"1","showing":false}, {}],
					textTroskovi: ["wm.Text", {"caption":"Troškovi(Kn):","captionSize":"150px","dataValue":undefined,"displayValue":"","height":"30px","invalidMessage":"Dormat cijena.","regExp":"^[0-9]*\\.[0-9][0-9]$","showing":false}, {}],
					buttonSpremiZatvorenuVoznju: ["wm.Button", {"caption":"Zatvori odabranu vožnju","margin":"4","showing":false,"width":"238px"}, {"onclick":"serviceVarZatvoriVoznju"}],
					textIdVoznje: ["wm.Text", {"caption":"text1","dataValue":undefined,"displayValue":"","showing":false}, {}]
				}],
				panel3: ["wm.Panel", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"50%"}, {}, {
					liveForm1: ["wm.LiveForm", {"fitToContentHeight":true,"height":"110px","horizontalAlign":"left","readonly":true,"showing":false,"verticalAlign":"top"}, {}, {
						binding: ["wm.Binding", {}, {}, {
							wire: ["wm.Wire", {"expression":undefined,"source":"varZatvorenaVoznja","targetProperty":"dataSet"}, {}]
						}],
						idzatvorenaVoznjaEditor1: ["wm.Number", {"caption":"IdzatvorenaVoznja","captionSize":"200px","emptyValue":"zero","formField":"idzatvorenaVoznja","height":"26px","readonly":true,"required":true,"width":"100%"}, {}],
						kilometriEditor1: ["wm.Number", {"caption":"Kilometri","captionSize":"200px","emptyValue":"zero","formField":"kilometri","height":"26px","readonly":true,"width":"100%"}, {}],
						troskoviEditor1: ["wm.Text", {"caption":"Troskovi","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"troskovi","height":"26px","readonly":true,"width":"100%"}, {}],
						liveForm1EditPanel: ["wm.EditPanel", {"height":"32px","liveForm":"liveForm1","operationPanel":"operationPanel1","savePanel":"savePanel1"}, {}, {
							savePanel1: ["wm.Panel", {"height":"100%","horizontalAlign":"right","layoutKind":"left-to-right","showing":false,"verticalAlign":"top","width":"100%"}, {}, {
								saveButton1: ["wm.Button", {"caption":"Save","margin":"4"}, {"onclick":"liveForm1EditPanel.saveData"}, {
									binding: ["wm.Binding", {}, {}, {
										wire: ["wm.Wire", {"expression":undefined,"source":"liveForm1EditPanel.formInvalid","targetProperty":"disabled"}, {}]
									}]
								}],
								cancelButton1: ["wm.Button", {"caption":"Cancel","margin":"4"}, {"onclick":"liveForm1EditPanel.cancelEdit"}]
							}],
							operationPanel1: ["wm.Panel", {"height":"100%","horizontalAlign":"right","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
								newButton1: ["wm.Button", {"caption":"New","margin":"4"}, {"onclick":"liveForm1EditPanel.beginDataInsert"}],
								updateButton1: ["wm.Button", {"caption":"Update","margin":"4"}, {"onclick":"liveForm1EditPanel.beginDataUpdate"}, {
									binding: ["wm.Binding", {}, {}, {
										wire: ["wm.Wire", {"expression":undefined,"source":"liveForm1EditPanel.formUneditable","targetProperty":"disabled"}, {}]
									}]
								}],
								deleteButton1: ["wm.Button", {"caption":"Delete","margin":"4"}, {"onclick":"liveForm1EditPanel.deleteData"}, {
									binding: ["wm.Binding", {}, {}, {
										wire: ["wm.Wire", {"expression":undefined,"source":"liveForm1EditPanel.formUneditable","targetProperty":"disabled"}, {}]
									}]
								}]
							}]
						}]
					}]
				}]
			}]
		}]
	}]
}