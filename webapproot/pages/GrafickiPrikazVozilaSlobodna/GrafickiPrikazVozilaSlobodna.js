dojo.declare("GrafickiPrikazVozilaSlobodna", wm.Page, {
    start: function() {
        try {
            this.connect(this.layoutSlobodnaVozila, "onShow", this, this.onLoadGrafickiPrikaz());
        } catch (e) {
            app.toastError(this.name + ".start() Failed: " + e.toString());
        }
    },

    onLoadGrafickiPrikaz: function(inSender, inDeprecated) {
        try {
            this.serviceVarGrafickiVozilaSlobodni.update();
        } catch (e) {
            app.toastError(this.name + ".start() Failed: " + e.toString());
        }
    },
      serviceVarGrafickiVozilaSlobodniSuccess: function(inSender, inDeprecated) {
        try {
        
		   // funkcije koje zbog firefoxa moraju biti prije pozivanja navedene
            /**
             * Funkcija povećava vrijednost x za 3, sve do 21 pa počinje natrag od 0 povećavati
             * služi za ispisivanje pomoćnih sati dana.- 3h, 9h, 15h, 21h
             * @param 
             * @returns vraca vrijednost sata u razmacima od 3h
             */

            function makeSequence() {
                var x = -3;
                return function() {
                    if (x == 21) {
                        x = 3;
                    }
                    else {
                        x += 6;
                    }
                    return x;
                };
            }
			
            //služi za ispisivanje pomoćnih sati dana.- 12h,0h
            function makeSequence2() {
                var x = -12;
                return function() {
                    if (x == 12) {
                        x = 0;
                    }
                    else {
                        x += 12;
                    }
                    return x;
                };
            }
            
            /**
             * Funkcija koja izračunava razliku između datuma od kojeg želimo ispis i datuma u kojoj počinje ispis
             * @param početni datum koji se uzima iz prijašnje forme, datum u kojem počinje ispis
             * @returns vraca razliku između dvaju parametara kao cijeli broj
             */

            function daysBetween(t1, t2) {
                var one_day = 1000 * 60 * 60 * 24;
                var x = t1.split(".");
                var y = t2.split(".");
                //date format(Fullyear,month,date) 
                var date1 = new Date(x[2], (x[1] - 1), x[0]);

                var date2 = new Date(y[2], (y[1] - 1), y[0]);
                var month1 = x[1] - 1;
                var month2 = y[1] - 1;

                _Diff = Math.ceil((date2.getTime() - date1.getTime()) / (one_day));
                return _Diff;
            }

            /**
             * Funkcija koja računa razliku između dvaju vremena i vraća rezultat u satima
             * @param prvo vrijeme, drugo vrijeme
             * @returns vraca rezultat tj. razliku između dva parametara u satima
             */
            // izračunavanje minuta tijekom kojih traje vožnja
            function get_time_difference(t1, t2) {
                var re = /[\.:]|, /;
                var date1 = t1.split(re).map(Number);
                var date2 = t2.split(re).map(Number);

                var diffMs = Date.UTC(date1[2], date1[1] - 1, date1[0], date1[3], date1[4], date1[5]) - Date.UTC(date2[2], date2[1] - 1, date2[0], date2[3], date2[4], date2[5]);

                var mins = diffMs / (60 * 1000);
                return mins;
            }


            /**
             * Funkcija koja dodaje dan, to jest povećava dan za jedan kod ispisa datuma dana
             * @param početni datum koji se uzima iz prijašnje forme, broj dana koji se dodaju u datum
             * @returns vraca datum u obliku dd.MM.yyy pri čemu se kao takvi datumi ispisuju u ispisu
             */
            //dodaj dan
            function addDays2(dateString, add) {
                dateSplit = dateString.split('.');
                date = new Date(dateSplit[2], dateSplit[1] - 1, dateSplit[0]);
                date.setDate(date.getDate() + add - 1);
                newDateString = (date.getDate() + "." + (date.getMonth() + 1) + "." + (date.getFullYear()) + ".");
                return newDateString;
            }
            function addDays(dateString, add) {
                dateSplit = dateString.split('.');
                date = new Date(dateSplit[2], dateSplit[1] - 1, dateSplit[0]);
                date.setDate(date.getDate() + add - 1);
                newDateString = (date.getDate() + "." + (date.getMonth() + 1) + ".");
                return newDateString;
            }
			
            var ZauzetiResurs = 0;
            var ZauzetiResursSlika = 0;
            // usporedi dva datuma prema kojima se ispisuje i boja i slikica busa tj vožnje
            function usporedi(prvidatum, drugidatum, pocetalvoznje, krajvoznje) {
                var prvo = prvidatum.split('-');
                var drugo = drugidatum.split('-');
                var trece = pocetalvoznje.split('.');
                var cetvrto = krajvoznje.split('.');

                var firstDate = new Date();
                firstDate.setFullYear(prvo[0], (prvo[1] - 1), prvo[2]);
                var secondDate = new Date();
                secondDate.setFullYear(drugo[0], (drugo[1] - 1), drugo[2]);
                var thirdDate = new Date();
                thirdDate.setFullYear(trece[2], (trece[1] - 1), trece[0]);
                var fourthDate = new Date();
                fourthDate.setFullYear(cetvrto[2], (cetvrto[1] - 1), cetvrto[0]);
                if (((fourthDate < firstDate) && (thirdDate < firstDate)) || ((thirdDate > secondDate) && (fourthDate > secondDate))) {
                    ZauzetiResurs = 0;
                } else {
                    ZauzetiResurs = 1;
                }
            }

            /**
             * Funkcija pretvara minute u pixsele i izracunava gdje je potrebno zapoceti crtanje voznje 
             * @param pocetak vozne u minutama
             * @returns vraca poziciju na kojoj treba nacrtati odredjene minute
             */
            // do kud da crta - do koje minuta s obzirom da je preostali dio ekrana podijeljen na minuta
            function getKrajVoznje(KrajkVoznjeMin) {
                divWidth = dojo.byId("povrsinaVozila").offsetWidth;
                if ((podaci.busevi[0].trajanje == 1) || (podaci.busevi[0].trajanje == 3)){
        			oneMinPixels = (divWidth - 79) / 10080;	
				} else if ((podaci.busevi[0].trajanje == 2) || (podaci.busevi[0].trajanje == 4)){
					oneMinPixels = (divWidth - 79) / 11950;		
				} else if ((podaci.busevi[0].trajanje == 5) || (podaci.busevi[0].trajanje == 7)){
					oneMinPixels = (divWidth - 79) / 24480;
				} else if ((podaci.busevi[0].trajanje == 6) || (podaci.busevi[0].trajanje == 8)){
					oneMinPixels = (divWidth - 79) / 24480;	
				} else if ((podaci.busevi[0].trajanje == 9) || (podaci.busevi[0].trajanje == 11)){
					oneMinPixels = (divWidth - 79) / 28800;	
				} else if ((podaci.busevi[0].trajanje == 10) || (podaci.busevi[0].trajanje == 12)){
					oneMinPixels = (divWidth - 79) / 28800;	
				} else if ((podaci.busevi[0].trajanje == 13) || (podaci.busevi[0].trajanje == 15)){
					oneMinPixels = (divWidth - 79) / 36000;	
				} else if ((podaci.busevi[0].trajanje == 14) || (podaci.busevi[0].trajanje == 16)){
					oneMinPixels = (divWidth - 79) / 36000;	
				} else if ((podaci.busevi[0].trajanje == 17) || (podaci.busevi[0].trajanje == 19)){
					oneMinPixels = (divWidth - 79) / 41760;	
				} else if ((podaci.busevi[0].trajanje == 18) || (podaci.busevi[0].trajanje == 20)){
					oneMinPixels = (divWidth - 79) / 41760;	
				} else if ((podaci.busevi[0].trajanje >= 21) || (podaci.busevi[0].trajanje <= 33)){
					oneMinPixels = (divWidth - 79) / 47520;	
				} 
                Kraj = KrajkVoznjeMin * oneMinPixels;
                return Kraj;
            }

            /**
             * Funkcija pretvara minute u pixsele i izracunava gdje je potrebno zapoceti crtanje voznje 
             * @param pocetak vozne u minutama
             * @returns vraca poziciju na kojoj treba nacrtati odredjene minute
             */
            function getPocetakVoznje(pocetakVoznjeMin) {
                divWidth = dojo.byId("povrsinaVozila").offsetWidth;
               if ((podaci.busevi[0].trajanje == 1) || (podaci.busevi[0].trajanje == 3)){
    				oneMinPixels = (divWidth - 79) / 10080;	
				} else if ((podaci.busevi[0].trajanje == 2) || (podaci.busevi[0].trajanje == 4)){
					oneMinPixels = (divWidth - 79) / 11950;		
				} else if ((podaci.busevi[0].trajanje == 5) || (podaci.busevi[0].trajanje == 7)){
					oneMinPixels = (divWidth - 79) / 24480;
				} else if ((podaci.busevi[0].trajanje == 6) || (podaci.busevi[0].trajanje == 8)){
					oneMinPixels = (divWidth - 79) / 24480;	
				} else if ((podaci.busevi[0].trajanje == 9) || (podaci.busevi[0].trajanje == 11)){
					oneMinPixels = (divWidth - 79) / 28800;	
				} else if ((podaci.busevi[0].trajanje == 10) || (podaci.busevi[0].trajanje == 12)){
					oneMinPixels = (divWidth - 79) / 28800;	
				} else if ((podaci.busevi[0].trajanje == 13) || (podaci.busevi[0].trajanje == 15)){
					oneMinPixels = (divWidth - 79) / 36000;	
				} else if ((podaci.busevi[0].trajanje == 14) || (podaci.busevi[0].trajanje == 16)){
					oneMinPixels = (divWidth - 79) / 36000;	
				} else if ((podaci.busevi[0].trajanje == 17) || (podaci.busevi[0].trajanje == 19)){
					oneMinPixels = (divWidth - 79) / 41760;	
				} else if ((podaci.busevi[0].trajanje == 18) || (podaci.busevi[0].trajanje == 20)){
					oneMinPixels = (divWidth - 79) / 41760;	
				} else if ((podaci.busevi[0].trajanje >= 21) || (podaci.busevi[0].trajanje <= 33)){
					oneMinPixels = (divWidth - 79) / 47520;	
				} 
                pocetakVoznje = pocetakVoznjeMin * oneMinPixels + 79;
                return pocetakVoznje;
            }

            /**
             * Funkcija pretvara minute u pixsele i izracunava gdje je potrebno zavrsiti crtanje voznje 
             * @param kraj voznje u mintama
             * @returns vraca poziciju na kojoj treba nacrtati odredjene minute
             */
            function getTrajanje(krajVoznjeMin, pocetakVoznjeMin) {
                //npr
                //pocetak voznje u 8:30 preracunati u minute (510 min) + 79px jer je tolko duga oznaka za bus
                zavrsetakVoznje = krajVoznjeMin * oneMinPixels + 79;
                pocetakVoznje = pocetakVoznjeMin * oneMinPixels + 79;
                trajanjeVoznje = zavrsetakVoznje - pocetakVoznje;
                return trajanjeVoznje;
            }

            /**
             * Pretvara vrijeme iz normalnog oblika prikaza u minute
             * @param vrijeme u oblik string npr (13:00)
             * @returns vraca vrijeme u minutama npr. (13 * 60)
             */
            function hoursToMinutes(time) {
                timee = time.split(":");
                hours = parseInt(timee[0]);
                minutes = parseInt(timee[1]);

                result = hours * 60 + minutes;

                return parseInt(result);
            }

            /**
             * funkcija za crtanje voznji
             * @param startOfRidePix pocetak voznje u pix
             * @param endOfRideInPix zavrsetak voznje u pix
             * @param yPositionOfRide y pozicija busa za koji se crta voznja 
             */
            function drawRideSlobodna(startOfRidePix, endOfRidePix, yPositionOfRide) {
                if (ZauzetiResurs == 0) {
                    var voznja = surface.createRect({
                        x: startOfRidePix,
                        y: yPositionOfRide + 8,
                        width: endOfRidePix,
                        height: 50
                    }).setStroke({
                        cap: "round",
                        color: "#f00"
                    }).setFill({
                        type: "linear",
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 420,
                        colors: [{
                            offset: 0,
                            color: "#003b80"
                        },
                        {
                            offset: 0.5,
                            color: "#0072e5"
                        },
                        {
                            offset: 1,
                            color: "#4ea1fc"
                        }]
                    });
                } else if (ZauzetiResurs == 1) {
                    var voznja = surface.createRect({
                        x: startOfRidePix,
                        y: yPositionOfRide + 8,
                        width: endOfRidePix,
                        height: 50
                    }).setStroke({
                        cap: "round",
                        color: "#f00"
                    }).setFill({
                        type: "linear",
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 420,
                        colors: [{
                            offset: 0,
                            color: "#D00000"
                        },
                        {
                            offset: 0.5,
                            color: "#C80000"
                        },
                        {
                            offset: 1,
                            color: "#C80000"
                        }]
                    });
                }
            }

/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////

            dojo.require("dojo");
            dojo.require("dojox.gfx");
            dojo.require("dojo._base.json");
            dojo.require("dijit.Tooltip");
            dojo.require("dojox.gfx.fx");
            dojo.require("dojo.colors");
            //Ĺˇirina i visina ekrana na kojem se sve iscrtava
            var divHeight = dojo.byId("povrsinaVozila").offsetHeight;
            var divWidth = dojo.byId("povrsinaVozila").offsetWidth;
            //širina i visina područja na kojem se iscrtavaju vremenske oznake
            var divHeightOznake = dojo.byId("headerVozila").offsetHeight;
            var divWidthOznake = dojo.byId("headerVozila").offsetWidth;
            
            //dobiveni podaci sa servera
            var podaci = dojo._base.json.fromJson(inSender.getData().dataValue);
                  
            //na temelju broja slobodnih vozača definira se visina ekrana za grafički prikaz
            //broj buseva + 1 puta 80 pixela
            izracunanaVisina = (podaci.busevi.length + 1) * 80 ;
            if(izracunanaVisina > divHeight){
                divHeight = izracunanaVisina;
                //kako se pojavi vertikalni scroll za njegovu veičinu je potrebno smanjiti širinu ekrana
                //pomocni surface koji sluzi samo za uzimanje sirine ekrana sa vertikalnim scrollom
                var surface = dojox.gfx.createSurface("povrsina", divWidth, divHeight);
                divWidth = dojo.byId("povrsina").offsetWidth;
                surface.destroy();
            }

            // varijabla koja ekran dijelu na minute
            var oneMinPixels = 0;
			
			// boja termina
			var fillObj = {
				colors: [
					{ offset: 0,   color: [51, 153, 204, 0.1] },
					{ offset: 0.5, color: [51, 153, 204, 0.1] },
					{ offset: 1,   color: [51, 153, 204, 0.1] }
				]
			};

            // crtanje grafike
            //alert(inSender.getData().dataValue);
            var surface = dojox.gfx.createSurface("povrsinaVozila", divWidth, divHeight);
            var surfaceOznake = dojox.gfx.createSurface("headerVozila", divWidthOznake, divHeightOznake);

           // dodijeljivanje vrijednost podijeljenih minuta
    		if ((podaci.busevi[0].trajanje == 1) || (podaci.busevi[0].trajanje == 3)){
				oneMinPixels = (divWidth - 79) / 10080;	
			} else if ((podaci.busevi[0].trajanje == 2) || (podaci.busevi[0].trajanje == 4)){
				oneMinPixels = (divWidth - 79) / 11950;		
			} else if ((podaci.busevi[0].trajanje == 5) || (podaci.busevi[0].trajanje == 7)){
				oneMinPixels = (divWidth - 79) / 24480;
			} else if ((podaci.busevi[0].trajanje == 6) || (podaci.busevi[0].trajanje == 8)){
				oneMinPixels = (divWidth - 79) / 24480;	
			} else if ((podaci.busevi[0].trajanje == 9) || (podaci.busevi[0].trajanje == 11)){
				oneMinPixels = (divWidth - 79) / 28800;	
			} else if ((podaci.busevi[0].trajanje == 10) || (podaci.busevi[0].trajanje == 12)){
				oneMinPixels = (divWidth - 79) / 28800;	
			} else if ((podaci.busevi[0].trajanje == 13) || (podaci.busevi[0].trajanje == 15)){
				oneMinPixels = (divWidth - 79) / 36000;	
			} else if ((podaci.busevi[0].trajanje == 14) || (podaci.busevi[0].trajanje == 16)){
				oneMinPixels = (divWidth - 79) / 36000;	
			} else if ((podaci.busevi[0].trajanje == 17) || (podaci.busevi[0].trajanje == 19)){
				oneMinPixels = (divWidth - 79) / 41760;	
			} else if ((podaci.busevi[0].trajanje == 18) || (podaci.busevi[0].trajanje == 20)){
				oneMinPixels = (divWidth - 79) / 41760;	
			} else if ((podaci.busevi[0].trajanje >= 21) || (podaci.busevi[0].trajanje <= 33)){
				oneMinPixels = (divWidth - 79) / 47520;	
			} 		
				
			//crtanje dnevnih linija
			for(var i = 0; i <= 50000; i = i + 1440){			
				surface.createLine({ x1: i*oneMinPixels + 79, y1: 5, x2:i*oneMinPixels + 79, y2:divHeight }).setStroke({color: "#000000", width: "0.75"});
			}
		
			//crtanje pomoćnih linija svakih 12 sata
			for(var i = 720; i <= 50000; i = i + 1440){				
				surface.createLine({ x1: i*oneMinPixels + 79, y1: 15, x2:i*oneMinPixels + 79, y2:divHeight }).setStroke({color: "#000000", width: "0.32"});
			}

			if (podaci.busevi[0].trajanje <= 20){
				//crtanje pomoćnih linija svakih 6 sata
				for(var i = 360; i <= 50000; i = i + 720){				
					surface.createLine({ x1: i*oneMinPixels + 79, y1: 20, x2:i*oneMinPixels + 79, y2:divHeight }).setStroke({color: "#000000", width: "0.175"});
				}
						
				//crtanje pomoćnih linija svakih 3 sata
				//crtanje naziva pomoćnih sati - 3h, 9h, 15h, 21h
				var sequence = makeSequence();
				for(var i = 180; i <= 50000; i = i + 180){				
					surface.createLine({ x1: i*oneMinPixels + 79, y1: 20, x2:i*oneMinPixels + 79, y2:divHeight }).setStroke({style: "Dot", color: "#000000", width: "0.5"});
				}
				for(var i = 180; i <= 50000; i = i + 360){				
					surfaceOznake.createText({ x: i*oneMinPixels + 79, y: 40, text: sequence(), align: "middle" })
					.setFont({ family: "Calibri", size: "7pt", weight: "bold" }) 
					.setFill("#330066");
				}
				//crtanje naziva pomoćnih sati - 12h, 0h
				var sequence2 = makeSequence2();
				for(var i = 0; i <= 50000; i = i + 720){				
					surfaceOznake.createText({ x: i*oneMinPixels + 79, y: 30, text: sequence2() + "h", align: "middle" })
					.setFont({ family: "Calibri", size: "10pt", weight: "bold" }) 
					.setFill("#000000");
				}
			} else if (podaci.busevi[0].trajanje >= 21){
				var sequence2 = makeSequence2();
				for(var i = 0; i <= 50000; i = i + 720){				
					surfaceOznake.createText({ x: i*oneMinPixels + 79, y: 30, text: sequence2() + "h", align: "middle" })
					.setFont({ family: "Calibri", size: "7pt", weight: "bold" }) 
					.setFill("#000000");
				}
			}

			// ispis dana
			for(var i = 720; i <= 50000; i = i + 1440){		
				if ((podaci.busevi[0].trajanje >= 1) && (podaci.busevi[0].trajanje <= 4)){
					surfaceOznake.createText({ x: i*oneMinPixels + 79 + 2, y: 18, text: addDays2(podaci.busevi[0].voznje[0].pregledOD,i/1440+1), align: "middle" })
					.setFont({ family: "Calibri", size: "14pt", weight: "bold" }) 
					.setFill("#330066");
				} else if ((podaci.busevi[0].trajanje >= 5) && (podaci.busevi[0].trajanje <= 8)){		
					surfaceOznake.createText({ x: i*oneMinPixels + 79 + 2, y: 18, text: addDays(podaci.busevi[0].voznje[0].pregledOD,i/1440+1), align: "middle" })
					.setFont({ family: "Calibri", size: "14pt", weight: "bold" }) 
					.setFill("#330066");
				} else if ((podaci.busevi[0].trajanje >= 9) && (podaci.busevi[0].trajanje <= 20)){		
					surfaceOznake.createText({ x: i*oneMinPixels + 79 + 2, y: 18, text: addDays(podaci.busevi[0].voznje[0].pregledOD,i/1440+1), align: "middle" })
					.setFont({ family: "Calibri", size: "12pt", weight: "bold" }) 
					.setFill("#330066");
				} else if (podaci.busevi[0].trajanje >= 21){		
					surfaceOznake.createText({ x: i*oneMinPixels + 79 + 2, y: 18, text: addDays(podaci.busevi[0].voznje[0].pregledOD,i/1440+1), align: "middle" })
					.setFont({ family: "Calibri", size: "7pt", weight: "bold" }) 
					.setFill("#330066");
				}
			}
			
            //crtanje vozila
            var pocni = 0;
            var startYPoint = 40;
            var startXPoint = -1;
            var busWidth = 55;
            var busHeight = 50;
            var numberOfBuses = 0;
            var numberOfRides;
            //ovu petlju je potrebno odvrtiti za svako vozilo
            for (numberOfBuses = 0; numberOfBuses < podaci.busevi.length; numberOfBuses++) {
                surface.createText({
                    x: startXPoint + 5 + (busWidth / 2) - 20,
                    y: startYPoint + 58,
                    text: "GB: " + podaci.busevi[numberOfBuses].GB,
                    align: "start"
                }).setFont({
                    family: "Calibri",
                    size: "11pt",
                    weight: "bold"
                }).setFill("blue");
                surface.createText({
                    x: startXPoint - 2 + (busWidth / 2) - 20,
                    y: startYPoint + 75,
                    text: podaci.busevi[numberOfBuses].marka + " " + podaci.busevi[numberOfBuses].model,
                    align: "start"
                }).setFont({
                    family: "Calibri",
                    size: "11pt",
                    weight: "bold"
                }).setFill("blue");
                ZauzetiResursSlika = 0;

                for (numberOfRides = 0; numberOfRides < podaci.busevi[numberOfBuses].voznje.length; numberOfRides++) {
                    try {
                        format1 = podaci.busevi[numberOfBuses].voznje[numberOfRides].pocetak_datum + ", " + podaci.busevi[numberOfBuses].voznje[numberOfRides].pocetak_vrijeme;
                        format2 = podaci.busevi[numberOfBuses].voznje[numberOfRides].kraj_datum + ", " + podaci.busevi[numberOfBuses].voznje[numberOfRides].kraj_vrijeme;

                        if (daysBetween(podaci.busevi[numberOfBuses].voznje[numberOfRides].pregledOD, podaci.busevi[numberOfBuses].voznje[numberOfRides].pocetak_datum) > 3) {
                            pocni = (daysBetween(podaci.busevi[numberOfBuses].voznje[numberOfRides].pregledOD, podaci.busevi[numberOfBuses].voznje[numberOfRides].pocetak_datum)) * 1440 + hoursToMinutes(podaci.busevi[numberOfBuses].voznje[numberOfRides].pocetak_vrijeme);
                        } else {
                            pocni = (daysBetween(podaci.busevi[numberOfBuses].voznje[numberOfRides].pregledOD, podaci.busevi[numberOfBuses].voznje[numberOfRides].pocetak_datum)) * 1440 + hoursToMinutes(podaci.busevi[numberOfBuses].voznje[numberOfRides].pocetak_vrijeme);
                        }
                        zavrsi = (daysBetween(podaci.busevi[numberOfBuses].voznje[numberOfRides].pregledOD, podaci.busevi[numberOfBuses].voznje[numberOfRides].kraj_datum)) * 1440 + hoursToMinutes(podaci.busevi[numberOfBuses].voznje[numberOfRides].kraj_vrijeme);

                        if (daysBetween(podaci.busevi[numberOfBuses].voznje[numberOfRides].pregledOD, podaci.busevi[numberOfBuses].voznje[numberOfRides].pocetak_datum) < 0) {
                            positionStart = getPocetakVoznje(pocni) - getPocetakVoznje(pocni) + 79;
                            positionEnd = getKrajVoznje(zavrsi);
                        } else {
                            positionStart = getPocetakVoznje(pocni);
                            positionEnd = getKrajVoznje(get_time_difference(format2, format1));
                        }

                        usporedi(podaci.busevi[numberOfBuses].terminOD, podaci.busevi[numberOfBuses].terminDo, podaci.busevi[numberOfBuses].voznje[numberOfRides].pocetak_datum, podaci.busevi[numberOfBuses].voznje[numberOfRides].kraj_datum);
                        if (ZauzetiResurs == 1) {
                            //        surface.createImage({x: startXPoint+16, y: startYPoint, width: busWidth, height: busHeight , src:"ikone/bus_zauzeti2.gif"});    
                            //        surface.createImage({x: startXPoint+16, y: startYPoint, width: busWidth, height: busHeight , src:"ikone/bus_zauzeti2.gif"});    
                            ZauzetiResursSlika = 1;
                        } else if (ZauzetiResurs == 0) {
                            //		surface.createImage({x: startXPoint+16, y: startYPoint, width: busWidth, height: busHeight , src:"ikone/Bus_icon.gif"});					
                        }
                        drawRideSlobodna(positionStart, positionEnd, startYPoint + 5);
                    }
                    catch (err) {}
                }
                if (ZauzetiResursSlika == 1) {
                    surface.createImage({
                        x: startXPoint + 16,
                        y: startYPoint,
                        width: busWidth,
                        height: busHeight,
                        src: "ikone/bus_zauzeti2.gif"
                    });
                } else if (ZauzetiResursSlika == 0) {
                    surface.createImage({
                        x: startXPoint + 16,
                        y: startYPoint,
                        width: busWidth,
                        height: busHeight,
                        src: "ikone/bus_.gif"
                    });
                }

                startYPoint += busHeight + 30;
            }

            //okvir traženog termina    
			var rectangle;
			if ((podaci.busevi[0].trajanje == 1)){
				for(var i = 4320; i <= 4320; i = i + 1140){		
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 42, width: i*oneMinPixels/3, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 0, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 2){
				for(var i = 4320; i <= 4800; i = i + 1140){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 42, width: i*oneMinPixels/3*2, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 0, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 3){
				for(var i = 2880; i <= 4020; i = i + 2280){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 42, width: i*oneMinPixels/2*3, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 4){
				for(var i = 2880; i <= 4320; i = i + 2280){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 42, width: i*oneMinPixels/2*4, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 5){
				for(var i = 7200; i <= 7680; i = i + 2280){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 42, width: i*oneMinPixels, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 6){
				for(var i = 7200; i <= 7920; i = i + 2280){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 42, width: i*oneMinPixels/5*6, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 7){
				for(var i = 5760; i <= 7920; i = i + 2280){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 42, width: i*oneMinPixels/4*7, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 8){
				for(var i = 5760; i <= 8640; i = i + 3420){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 42, width: i*oneMinPixels*2, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 9){
				for(var i = 7200; i <= 10080; i = i + 3420){
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 42, width: i*oneMinPixels/5*9, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 10){
				for(var i = 7200; i <= 11160; i = i + 4560){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 42, width: i*oneMinPixels*2, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 11){
				for(var i = 5760; i <= 10800; i = i + 5700){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 42, width: i*oneMinPixels/4*11, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 12){
				for(var i = 5760; i <= 10800; i = i + 5700){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 42, width: i*oneMinPixels*3, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 13){
				for(var i = 7200; i <= 12960; i = i + 6840){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 42, width: i*oneMinPixels/5*13, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 14){
				for(var i = 7200; i <= 13680; i = i + 6840){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 42, width: i*oneMinPixels/5*14, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 15){
				for(var i = 5760; i <= 13680; i = i + 7980){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 42, width: i*oneMinPixels/4*15, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 16){
				for(var i = 5760; i <= 14440; i = i + 9120){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 42, width: i*oneMinPixels*4, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 17){
				for(var i = 7200; i <= 15840; i = i + 9120){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 42, width: i*oneMinPixels/5*17, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 18){
				for(var i = 7200; i <= 16560; i = i + 10260){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 42, width: i*oneMinPixels/5*18, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 19){
				for(var i = 5760; i <= 16560; i = i + 11140){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 42, width: i*oneMinPixels/4*19, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 20){
				for(var i = 5760; i <= 17280; i = i + 12540){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 42, width: i*oneMinPixels*5, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 21){
				for(var i = 7200; i <= 18720; i = i + 12540){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 32, width: i*oneMinPixels/5*21, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 22){
				for(var i = 7200; i <= 19440; i = i + 12540){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 32, width: i*oneMinPixels/5*22, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 23){
				for(var i = 5760; i <= 19440; i = i + 14820){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 32, width: i*oneMinPixels/4*23, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 24){
				for(var i = 5760; i <= 20160; i = i + 14820){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 32, width: i*oneMinPixels*6, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 25){
				for(var i = 4320; i <= 20160; i = i + 15960){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 32, width: i*oneMinPixels/3*25, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 26){
				for(var i = 4320; i <= 20880; i = i + 17100){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 32, width: i*oneMinPixels/3*26, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 27){
				for(var i = 4320; i <= 21600; i = i + 18240){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 32, width: i*oneMinPixels*9, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 28){
				for(var i = 2880; i <= 21600; i = i + 19380){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 32, width: i*oneMinPixels*14, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 29){
				for(var i = 2880; i <= 22320; i = i + 21660){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 32, width: i*oneMinPixels*14.5, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 30){
				for(var i = 1440; i <= 22320; i = i + 21660){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 32, width: i*oneMinPixels*30, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			} else if (podaci.busevi[0].trajanje == 31){
				for(var i = 1440; i <= 23040; i = i + 21660){	
					rectangle = surface.createRect({ x: i*oneMinPixels + 79, y: 32, width: i*oneMinPixels*31, height: divHeight }).setFill(dojo.mixin({
						type: "radial",
						x1: 0, y1: 0,
						x2: 300, y2: 0
					}, fillObj));
				}
			}

			//animacija punjenja odabranog termina
			dojox.gfx.fx.animateFill({
			    shape: rectangle,
				duration: 3500,
				color: { start: [51, 153, 204, 0.05], end: [51, 153, 204, 0.65] }
			}).play();
            
           //crtanje voznji, tooltipova, podataka i animacija
            
            function drawRideAgain(startOfRidePix, endOfRidePix, yPositionOfRide, DriverName, ID, pocetak, zavrsetak, oznakaVoznje) {
                if (ZauzetiResurs == 0) {
                    var voznja = surface.createRect({
                        x: startOfRidePix,
                        y: yPositionOfRide + 8,
                        width: endOfRidePix,
                        height: 50
                    }).setStroke({
                        cap: "round",
                        color: [208, 0, 0, 0]
                    }).setFill({
                        type: "linear",
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 420,
                        colors: [{
                            offset: 0,
                            color: "#003b80"
                        },
                        {
                            offset: 0.5,
                            color: "#0072e5"
                        },
                        {
                            offset: 1,
                            color: "#4ea1fc"
                        }]
                    });
                } else if (ZauzetiResurs == 1) {
                    var voznja = surface.createRect({
                        x: startOfRidePix,
                        y: yPositionOfRide + 8,
                        width: endOfRidePix,
                        height: 50
                    }).setStroke({
                        cap: "round",
                        color: [208, 0, 0, 0]
                    }).setFill({
                        type: "linear",
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 420,
                        colors: [{
                            offset: 0,
                            color: [208, 0, 0, 0]
                        },
                        {
                            offset: 0.5,
                            color: [208, 0, 0, 0]
                        },
                        {
                            offset: 1,
                            color: [208, 0, 0, 0]
                        }]
                    });
                }
                ID = ID+"vozila";
                voznja.rawNode.id = ID;
                if (ZauzetiResurs == 0) {
                    new dijit.Tooltip({
                        connectId: [ID],
                        label: "<img src=ikone/vozac.gif align=middle alt=Podaci o vožnji /> <br/><b>Ime i prezime vozača:</b> " + DriverName + "<br/>" + "<b>Početak vožnje:</b> " + pocetak + "<br/>" + "<b>Završetak vožnje: </b> " + zavrsetak + "<br/><b>Oznaka vožnje:</b> " + oznakaVoznje,
                        showDelay: 1
                    });
                }
                else if (ZauzetiResurs == 1) {
                    new dijit.Tooltip({
                        connectId: [ID],
                        label: "<img src=ikone/vozac_zauzeti.gif align=middle alt=Podaci o vožnji /> <br/><b>Ime i prezime vozača:</b> " + DriverName + "<br/>" + "<b>Početak vožnje:</b> " + pocetak + "<br/>" + "<b>Završetak vožnje: </b> " + zavrsetak + "<br/><b>Oznaka vožnje:</b> " + oznakaVoznje,
                        showDelay: 1
                    });
                }

            	if (ZauzetiResurs == 0) {
					voznja.connect("onmouseover",function(e) {
						dojox.gfx.fx.animateStroke({
						    shape: voznja,
						    duration: 50,
						    color: { start: "black", end: "f00" },
						    width: { end: 3 },
						    join:  { values: ["miter", "bevel", "round"] }
						}).play();
						voznja.setFill("#330066 ");
					});	
					
				voznja.connect("onmouseleave",function(e) {
					voznja.setFill({ type:"linear", 
						x1:0,                           
						y1:0,   
						x2:0,   
						y2:420,                         
						colors: [
						         { offset: 0,   color: "#003b80" },
						         { offset: 0.5, color: "#0072e5" },
						         { offset: 1,   color: "#4ea1fc" }
						         ]
					});
					dojox.gfx.fx.animateStroke({
					    shape: voznja,
					    duration: 100,
					    color: { start: "f00", end: [208, 0, 0, 0] },
					    width: { end: 1 },
					    join:  { values: ["miter", "bevel", "round"] }
					}).play();
				});  

				} else if (ZauzetiResurs == 1) {
					voznja.connect("onmouseover",function(e) {
						dojox.gfx.fx.animateStroke({
						    shape: voznja,
						    duration: 50,
						    color: { start: "black", end: "f00" },
						    width: { end: 3 },
						    join:  { values: ["miter", "bevel", "round"] }
						}).play();
						voznja.setFill("#A80000 ");
					});
					
					voznja.connect("onmouseleave",function(e) {
						voznja.setFill({ type:"linear", 
							x1:0,                           
							y1:0,   
							x2:0,   
							y2:420,                         
							colors: [
							         { offset: 0,   color: [208, 0, 0, 0] },
							         { offset: 0.5, color: [208, 0, 0, 0] },
							         { offset: 1,   color: [208, 0, 0, 0] }
							         ]
						});
						dojox.gfx.fx.animateStroke({
						    shape: voznja,
						    duration: 100,
						    color: { start: "f00", end: [208, 0, 0, 0] },
						    width: { end: 1 },
						    join:  { values: ["miter", "bevel", "round"] }
						}).play();
					});  	
				}
            }
            
            //crtanje vozila
            var pocni = 0;
            var startYPoint = 40;
            var startXPoint = -1;
            var busWidth = 55;
            var busHeight = 50;
            var numberOfBuses = 0;
            var numberOfRides;
            var vozac = "";
            var IDVoznje = "";
            for (numberOfBuses = 0; numberOfBuses < podaci.busevi.length; numberOfBuses++) {
                ZauzetiResursSlika = 0;
                for (numberOfRides = 0; numberOfRides < podaci.busevi[numberOfBuses].voznje.length; numberOfRides++) {
                    try {
                        vozac = podaci.busevi[numberOfBuses].voznje[numberOfRides].vozac;
                        format1 = podaci.busevi[numberOfBuses].voznje[numberOfRides].pocetak_datum + ", " + podaci.busevi[numberOfBuses].voznje[numberOfRides].pocetak_vrijeme;
                        format2 = podaci.busevi[numberOfBuses].voznje[numberOfRides].kraj_datum + ", " + podaci.busevi[numberOfBuses].voznje[numberOfRides].kraj_vrijeme;
                        IDVoznje = podaci.busevi[numberOfBuses].voznje[numberOfRides].IDvoznje;
                        OznakaVoznje = podaci.busevi[numberOfBuses].voznje[numberOfRides].oznakaVoznje;

                        if (daysBetween(podaci.busevi[numberOfBuses].voznje[numberOfRides].pregledOD, podaci.busevi[numberOfBuses].voznje[numberOfRides].pocetak_datum) > 3) {
                            pocni = (daysBetween(podaci.busevi[numberOfBuses].voznje[numberOfRides].pregledOD, podaci.busevi[numberOfBuses].voznje[numberOfRides].pocetak_datum)) * 1440 + hoursToMinutes(podaci.busevi[numberOfBuses].voznje[numberOfRides].pocetak_vrijeme);
                        } else {
                            pocni = (daysBetween(podaci.busevi[numberOfBuses].voznje[numberOfRides].pregledOD, podaci.busevi[numberOfBuses].voznje[numberOfRides].pocetak_datum)) * 1440 + hoursToMinutes(podaci.busevi[numberOfBuses].voznje[numberOfRides].pocetak_vrijeme);
                        }
                        zavrsi = (daysBetween(podaci.busevi[numberOfBuses].voznje[numberOfRides].pregledOD, podaci.busevi[numberOfBuses].voznje[numberOfRides].kraj_datum)) * 1440 + hoursToMinutes(podaci.busevi[numberOfBuses].voznje[numberOfRides].kraj_vrijeme);

                        if (daysBetween(podaci.busevi[numberOfBuses].voznje[numberOfRides].pregledOD, podaci.busevi[numberOfBuses].voznje[numberOfRides].pocetak_datum) < 0) {
                            positionStart = getPocetakVoznje(pocni) - getPocetakVoznje(pocni) + 79;
                            positionEnd = getKrajVoznje(zavrsi);
                        } else {
                            positionStart = getPocetakVoznje(pocni);
                            positionEnd = getKrajVoznje(get_time_difference(format2, format1));
                        }

                        usporedi(podaci.busevi[numberOfBuses].terminOD, podaci.busevi[numberOfBuses].terminDo, podaci.busevi[numberOfBuses].voznje[numberOfRides].pocetak_datum, podaci.busevi[numberOfBuses].voznje[numberOfRides].kraj_datum);
                        if (ZauzetiResurs == 1) {
                            //        surface.createImage({x: startXPoint+16, y: startYPoint, width: busWidth, height: busHeight , src:"ikone/bus_zauzeti2.gif"});    
                            ZauzetiResursSlika = 1;
                        } else if (ZauzetiResurs == 0) {
                            //        surface.createImage({x: startXPoint+16, y: startYPoint, width: busWidth, height: busHeight , src:"ikone/Bus_icon.gif"});                    
                        }
                        drawRideAgain(positionStart, positionEnd, startYPoint + 5, vozac, IDVoznje, format1, format2, OznakaVoznje);
                    }
                    catch (err) {}
                }

                startYPoint += busHeight + 30;
            }
            

        } catch (e) {
            console.error('ERROR IN serviceVarGrafickiVozaciSlobodniSuccess: ' + e);
        }
    },
    _end: 0
});