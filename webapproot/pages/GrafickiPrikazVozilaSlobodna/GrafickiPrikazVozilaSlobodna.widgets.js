GrafickiPrikazVozilaSlobodna.widgets = {
	serviceVarGrafickiVozilaSlobodni: ["wm.ServiceVariable", {"autoUpdate":true,"operation":"getSlobodnaVozilaGraficki","service":"glavni","startUpdate":true}, {"onSuccess":"serviceVarGrafickiVozilaSlobodniSuccess"}, {
		input: ["wm.ServiceInput", {"type":"getSlobodnaVozilaGrafickiInputs"}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire: ["wm.Wire", {"expression":undefined,"source":"[definiranjeTerminaSlobodniVozila].datePocetakVoznje.dataValue","targetProperty":"pocetakVoznje"}, {}],
				wire1: ["wm.Wire", {"expression":undefined,"source":"[definiranjeTerminaSlobodniVozila].dateKrajVoznje.dataValue","targetProperty":"krajVoznje"}, {}]
			}]
		}]
	}],
	layoutSlobodniVozaci: ["wm.Layout", {"height":"100%","horizontalAlign":"left","verticalAlign":"bottom","width":"100%"}, {}, {
		htmlGrafiickiPrikazHeader: ["wm.Html", {"border":"0","height":"5%","html":"<div id=\"headerVozila\" style=\"background:#F0F0F0; height:100%; width:100%\" overflow=\"\" :=\"\" auto;=\"\"></div>"}, {}],
		htmlGrafickiPrikaz: ["wm.Html", {"border":"0","height":"95%","html":"<div id=\"povrsinaVozila\" style=\"background:#F0F0F0; height:100%; width:100%\" overflow=\"\" :=\"\" auto;=\"\"></div>"}, {}]
	}]
}