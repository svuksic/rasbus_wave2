var data_store = null;
dojo.declare("Main", wm.Page, {

    //konstante
    ulogaAdmin: 1,
    ulogaKorisnik: 2,

    onShow: function() {
        this.serviceVarGetRole.update();
    },

    start: function() {
        try {
            dojo.require("dojo.data.ItemFileReadStore");
            dojo.require("dijit.tree.ForestStoreModel");
            dojo.require("dijit.Tree");

            //kako bi se overridale funkcinalnosti live forme za insert, delete, update....sami moramo implementirati funkcinalnosti
            //main.voznjaLiveForm1.liveSaving = false;
            /**
             * Dodavanje ikona na tre view izbornik
             * @param {Object} item
             * @param {Object} opened
             */
            //ovo tu jer se inca pozdaina cudno ponasa nakon gasenja
            main.tabLayers1.getActiveLayer().setValue("width", "100px");
            main.tabLayers1.getActiveLayer().setValue("height", "100%");

            // za postavljanje s desne strane titlebar ikona - potrošeno oko 15 sati na te 3 linije :D
            // za dialog Vozac
            var t1 = main.designableDialogAdminVozac.titleBar;
            t1.moveControl(t1.c$[3], 1);
            t1.moveControl(t1.c$[0], 4);
            t1.reflow();
            // za dialog Vozilo
            var t2 = main.designableDialogAdminVozila.titleBar;
            t2.moveControl(t2.c$[3], 1);
            t2.moveControl(t2.c$[0], 4);
            t2.reflow();
            // za dialog Vožnje   
            var t3 = main.designableDialogAdminVoznja.titleBar;
            t3.moveControl(t3.c$[3], 1);
            t3.moveControl(t3.c$[0], 4);
            t3.reflow();
            // za dialog Narucitelja  
            var t4 = this.designableDialogAdminNarucitelj.titleBar;
            t4.moveControl(t4.c$[3], 1);
            t4.moveControl(t4.c$[0], 4);
            t4.reflow();
            // za dialog otvaranja vožnje 
            var t5 = this.designableDialogOtvaranjeVoznje.titleBar;
            t5.moveControl(t5.c$[3], 1);
            t5.moveControl(t5.c$[0], 4);
            t5.reflow();
            // za dialog administracije korisnika
            var t6 = this.designableDialogAdminUser.titleBar;
            t6.moveControl(t6.c$[3], 1);
            t6.moveControl(t6.c$[0], 4);
            t6.reflow();
            // za dialog administracije korisničkih prava
            var t7 = this.designableDialogAdminRole.titleBar;
            t7.moveControl(t7.c$[3], 1);
            t7.moveControl(t7.c$[0], 4);
            t7.reflow();
            // za dialog logova
            var t8 = this.designableDialogLogovi.titleBar;
            t8.moveControl(t8.c$[3], 1);
            t8.moveControl(t8.c$[0], 4);
            t8.reflow();


            function ikone(item, opened) {
                if (!item.root) {
                    if (!item.children) {
                        // Style the nodes that not have childrens
                        return "";
                    } else {
                        // Style the nodes that have childrens
                        return "";
                    }
                } else {
                    // Style the root node here
                    return "";
                }
            }

            /**
             * 
             *Izvrsava se nakon kaj se klikne na stavku tree view-a
             * @param {Object} item
             */
            var otvorenTabVozacPregledVoznjiGrafika = false;
            var otvorenTabVozacPregledVoznjiTablica = false;
            var otvorenTabVoziloPregledVoznjiGrafika = false;
            var otvorenTabVoziloPregledVoznjiTablica = false;
            var otvorenTabSlobodniVozaciGraficki = false;
            var otvorenTabSlobodniVozaciTablicni = false;
            var otvorenTabSlobodnaVozilaGraficki = false;
            var otvorenTabUpozorenja = false;
            var otvorenTabZatvaranjaVoznje = false;
            var otvorenTabNaplataVoznje = false;
            var otvorenTabNenaplaceneVoznje = false;
            var otvorenTabNaplaceneVoznje = false;
            var otvorenTabOtvoreneVoznje = false;

            function kliknutaStavkaMenija(item) {

                if (item.id == 1) {
                    main.designableDialogAdminVozac.show();
                }
                if (item.id == 2) {
                    main.designableDialogAdminVozila.show();
                }
                if (item.id == 3) {
                    main.designableDialogAdminVoznja.show();
                }
                if (item.id == 6) {
                    if (otvorenTabVozacPregledVoznjiGrafika === false) {
                        otvorenTabVozacPregledVoznjiGrafika = true;
                        app.varNacinPregledaVozac.setValue("dataValue", "grafickiVozac");
                        var l = new wm.Layer({
                            owner: main,
                            parent: main.tabLayers1,
                            caption: "Grafički pregled vožnji prema vozaču/ima",
                            width: "100%",
                            height: "100%",
                            closable: true,
                            destroyable: true,
                            showDirtyFlag: true,
                            name: item.id,
                            onCloseOrDestroy: function okini(item) {
                                otvorenTabVozacPregledVoznjiTablica = false;
                                otvorenTabVozacPregledVoznjiGrafika = false;
                                l.destroy();
                                c.destroy();
                                //ovo tu jer se inca pozdaina cudno ponasa nakon gasenja
                                main.tabLayers1.getActiveLayer().setValue("width", "100px");
                                main.tabLayers1.getActiveLayer().setValue("height", "100%");
                            }
                        });

                        var c = new wm.PageContainer({
                            owner: main,
                            parent: l,
                            width: "100%",
                            height: "100%",
                            pageName: "OdabirVoznjiVozac"
                        });

                        main.tabLayers1.setLayer(l);
                    } else {
                        main.tabLayers1.setLayerByCaption(item.id);
                    }
                }
                if (item.id == 7) {
                    if (otvorenTabVozacPregledVoznjiTablica === false) {
                        otvorenTabVozacPregledVoznjiTablica = true;
                        app.varNacinPregledaVozac.setValue("dataValue", "tablicniVozac");
                        var l = new wm.Layer({
                            owner: main,
                            parent: main.tabLayers1,
                            caption: "Tablični pregled vožnji prema vozaču/ima",
                            width: "100%",
                            height: "100%",
                            closable: true,
                            destroyable: true,
                            showDirtyFlag: true,
                            name: item.id,
                            onCloseOrDestroy: function okini(item) {
                                otvorenTabVozacPregledVoznjiTablica = false;
                                otvorenTabVozacPregledVoznjiGrafika = false;
                                l.destroy();
                                c.destroy();
                                //ovo tu jer se inca pozdaina cudno ponasa nakon gasenja
                                main.tabLayers1.getActiveLayer().setValue("width", "100px");
                                main.tabLayers1.getActiveLayer().setValue("height", "100%");
                            }
                        });

                        var c = new wm.PageContainer({
                            owner: main,
                            parent: l,
                            width: "100%",
                            height: "100%",
                            pageName: "OdabirVoznjiVozac"
                        });

                        main.tabLayers1.setLayer(l);
                    } else {
                        main.tabLayers1.setLayerByCaption(item.id);
                    }
                }
                if (item.id == 9) {
                    if (otvorenTabVoziloPregledVoznjiGrafika === false) {
                        app.varNacinPregleda.setValue("dataValue", "graficki");
                        otvorenTabVoziloPregledVoznjiGrafika = true;
                        var l = new wm.Layer({
                            owner: main,
                            parent: main.tabLayers1,
                            caption: "Grafički pregled vožnji prema vozilu/ima",
                            width: "100%",
                            height: "100%",
                            closable: true,
                            destroyable: true,
                            showDirtyFlag: true,
                            name: item.id,
                            onCloseOrDestroy: function okini(item) {
                                otvorenTabVoziloPregledVoznjiGrafika = false;
                                l.destroy();
                                c.destroy();
                                //ovo tu jer se inca pozdaina cudno ponasa nakon gasenja
                                main.tabLayers1.getActiveLayer().setValue("width", "100px");
                                main.tabLayers1.getActiveLayer().setValue("height", "100%");
                            }
                        });

                        var c = new wm.PageContainer({
                            owner: main,
                            parent: l,
                            width: "100%",
                            height: "100%",
                            pageName: "OdabirVoznji"
                        });

                        main.tabLayers1.setLayer(l);
                    } else {
                        main.tabLayers1.setLayerByCaption(item.id);
                    }
                }
                if (item.id == 10) {
                    if (otvorenTabVoziloPregledVoznjiTablica === false) {
                        otvorenTabVoziloPregledVoznjiTablica = true;
                        app.varNacinPregleda.setValue("dataValue", "tablicni");
                        var l = new wm.Layer({
                            owner: main,
                            parent: main.tabLayers1,
                            caption: "Tablični pregled vožnji prema vozilu/ima",
                            width: "100%",
                            height: "100%",
                            closable: true,
                            destroyable: true,
                            showDirtyFlag: true,
                            name: item.id,
                            onCloseOrDestroy: function okini(item) {
                                otvorenTabVoziloPregledVoznjiTablica = false;
                                otvorenTabVoziloPregledVoznjiGrafika = false;
                                l.destroy();
                                c.destroy();
                                //ovo tu jer se inca pozdaina cudno ponasa nakon gasenja
                                main.tabLayers1.getActiveLayer().setValue("width", "100px");
                                main.tabLayers1.getActiveLayer().setValue("height", "100%");
                            }
                        });

                        var c = new wm.PageContainer({
                            owner: main,
                            parent: l,
                            width: "100%",
                            height: "100%",
                            pageName: "OdabirVoznji"
                        });

                        main.tabLayers1.setLayer(l);
                    } else {
                        main.tabLayers1.setLayerByCaption(item.id);
                    }
                }
                if (item.id == 12) {
                    if (otvorenTabSlobodniVozaciGraficki === false) {
                        otvorenTabSlobodniVozaciGraficki = true;
                        var l = new wm.Layer({
                            owner: main,
                            parent: main.tabLayers1,
                            caption: "Termin - vozači",
                            width: "100%",
                            height: "100%",
                            closable: true,
                            destroyable: true,
                            showDirtyFlag: true,
                            name: item.id,
                            onCloseOrDestroy: function okini(item) {
                                otvorenTabSlobodniVozaciGraficki = false;
                                l.destroy();
                                c.destroy();
                                //ovo tu jer se inca pozdaina cudno ponasa nakon gasenja
                                main.tabLayers1.getActiveLayer().setValue("width", "100px");
                                main.tabLayers1.getActiveLayer().setValue("height", "100%");
                            }
                        });

                        var c = new wm.PageContainer({
                            owner: main,
                            parent: l,
                            width: "100%",
                            height: "100%",
                            pageName: "DefiniranjeTerminaSlobodniVozaci"
                        });

                        main.tabLayers1.setLayer(l);
                    } else {
                        main.tabLayers1.setLayerByCaption(item.id);
                    }
                }

                if (item.id == 13) {
                    if (otvorenTabSlobodniVozaciTablicni === false) {
                        otvorenTabSlobodniVozaciTablicni = true;
                        var l = new wm.Layer({
                            owner: main,
                            parent: main.tabLayers1,
                            caption: "Slobodni resursi",
                            width: "100%",
                            height: "100%",
                            closable: true,
                            destroyable: true,
                            showDirtyFlag: true,
                            name: item.id,
                            onCloseOrDestroy: function okini(item) {
                                otvorenTabSlobodniVozaciTablicni = false;
                                l.destroy();
                                c.destroy();
                                //ovo tu jer se inca pozdaina cudno ponasa nakon gasenja
                                main.tabLayers1.getActiveLayer().setValue("width", "100px");
                                main.tabLayers1.getActiveLayer().setValue("height", "100%");
                            }
                        });

                        var c = new wm.PageContainer({
                            owner: main,
                            parent: l,
                            width: "100%",
                            height: "100%",
                            pageName: "SlobodniResursiTablicni"
                        });

                        main.tabLayers1.setLayer(l);
                    } else {
                        main.tabLayers1.setLayerByCaption(item.id);
                    }
                }
                if (item.id == 14) {
                    if (otvorenTabSlobodnaVozilaGraficki === false) {
                        otvorenTabSlobodnaVozilaGraficki = true;
                        var l = new wm.Layer({
                            owner: main,
                            parent: main.tabLayers1,
                            caption: "Termin - vozila",
                            width: "100%",
                            height: "100%",
                            closable: true,
                            destroyable: true,
                            showDirtyFlag: true,
                            name: item.id,
                            onCloseOrDestroy: function okini(item) {
                                otvorenTabSlobodnaVozilaGraficki = false;
                                l.destroy();
                                c.destroy();
                                //ovo tu jer se inca pozdaina cudno ponasa nakon gasenja
                                main.tabLayers1.getActiveLayer().setValue("width", "100px");
                                main.tabLayers1.getActiveLayer().setValue("height", "100%");
                            }
                        });

                        var c = new wm.PageContainer({
                            owner: main,
                            parent: l,
                            width: "100%",
                            height: "100%",
                            pageName: "DefiniranjeTerminaSlobodniVozila"
                        });

                        main.tabLayers1.setLayer(l);
                    } else {
                        main.tabLayers1.setLayerByCaption(item.id);
                    }
                }
                if (item.id == 16) {
                    if (otvorenTabUpozorenja === false) {
                        otvorenTabUpozorenja = true;
                        var l = new wm.Layer({
                            owner: main,
                            parent: main.tabLayers1,
                            caption: "Upozorenja",
                            width: "100%",
                            height: "100%",
                            closable: true,
                            destroyable: true,
                            showDirtyFlag: true,
                            name: item.id,
                            onCloseOrDestroy: function okini(item) {
                                otvorenTabUpozorenja = false;
                                l.destroy();
                                c.destroy();
                                //ovo tu jer se inca pozdaina cudno ponasa nakon gasenja
                                main.tabLayers1.getActiveLayer().setValue("width", "100px");
                                main.tabLayers1.getActiveLayer().setValue("height", "100%");
                            }
                        });

                        var c = new wm.PageContainer({
                            owner: main,
                            parent: l,
                            width: "100%",
                            height: "100%",
                            pageName: "Warnings"
                        });

                        main.tabLayers1.setLayer(l);
                    } else {
                        main.tabLayers1.setLayerByCaption(item.id);
                    }
                }
                if (item.id == 17) {
                    main.designableDialogAdminNarucitelj.show();
                }
                if (item.id == 18) {
                    if (otvorenTabZatvaranjaVoznje === false) {
                        otvorenTabZatvaranjaVoznje = true;
                        var l = new wm.Layer({
                            owner: main,
                            parent: main.tabLayers1,
                            caption: "Zatvaranje vožnje",
                            width: "100%",
                            height: "100%",
                            closable: true,
                            destroyable: true,
                            showDirtyFlag: true,
                            name: item.id,
                            onCloseOrDestroy: function okini(item) {
                                otvorenTabZatvaranjaVoznje = false;
                                l.destroy();
                                c.destroy();
                                //ovo tu jer se inca pozdaina cudno ponasa nakon gasenja
                                main.tabLayers1.getActiveLayer().setValue("width", "100px");
                                main.tabLayers1.getActiveLayer().setValue("height", "100%");
                            }
                        });

                        var c = new wm.PageContainer({
                            owner: main,
                            parent: l,
                            width: "100%",
                            height: "100%",
                            pageName: "ZatvaranjeVoznje"
                        });

                        main.tabLayers1.setLayer(l);
                    } else {
                        main.tabLayers1.setLayerByCaption(item.id);
                    }
                }
                if (item.id == 19) {
                    if (otvorenTabNaplataVoznje === false) {
                        otvorenTabNaplataVoznje = true;
                        var l = new wm.Layer({
                            owner: main,
                            parent: main.tabLayers1,
                            caption: "Naplata vožnje",
                            width: "100%",
                            height: "100%",
                            closable: true,
                            destroyable: true,
                            showDirtyFlag: true,
                            name: item.id,
                            onCloseOrDestroy: function okini(item) {
                                otvorenTabNaplataVoznje = false;
                                l.destroy();
                                c.destroy();
                                //ovo tu jer se inca pozdaina cudno ponasa nakon gasenja
                                main.tabLayers1.getActiveLayer().setValue("width", "100px");
                                main.tabLayers1.getActiveLayer().setValue("height", "100%");
                            }
                        });

                        var c = new wm.PageContainer({
                            owner: main,
                            parent: l,
                            width: "100%",
                            height: "100%",
                            pageName: "NaplataVoznje"
                        });

                        main.tabLayers1.setLayer(l);
                    } else {
                        main.tabLayers1.setLayerByCaption(item.id);
                    }
                }
                if (item.id == 20) {
                    if (otvorenTabNenaplaceneVoznje === false) {
                        otvorenTabNenaplaceneVoznje = true;
                        var l = new wm.Layer({
                            owner: main,
                            parent: main.tabLayers1,
                            caption: "Nenaplaćene vožnje",
                            width: "100%",
                            height: "100%",
                            closable: true,
                            destroyable: true,
                            showDirtyFlag: true,
                            name: item.id,
                            onCloseOrDestroy: function okini(item) {
                                otvorenTabNenaplaceneVoznje = false;
                                l.destroy();
                                c.destroy();
                                //ovo tu jer se inca pozdaina cudno ponasa nakon gasenja
                                main.tabLayers1.getActiveLayer().setValue("width", "100px");
                                main.tabLayers1.getActiveLayer().setValue("height", "100%");
                            }
                        });

                        var c = new wm.PageContainer({
                            owner: main,
                            parent: l,
                            width: "100%",
                            height: "100%",
                            pageName: "NenaplaceneVoznje"
                        });

                        main.tabLayers1.setLayer(l);
                    } else {
                        main.tabLayers1.setLayerByCaption(item.id);
                    }
                }

                if (item.id == 21) {
                    if (otvorenTabNaplaceneVoznje === false) {
                        otvorenTabNaplaceneVoznje = true;
                        var l = new wm.Layer({
                            owner: main,
                            parent: main.tabLayers1,
                            caption: "Naplaćene vožnje",
                            width: "100%",
                            height: "100%",
                            closable: true,
                            destroyable: true,
                            showDirtyFlag: true,
                            name: item.id,
                            onCloseOrDestroy: function okini(item) {
                                otvorenTabNaplaceneVoznje = false;
                                l.destroy();
                                c.destroy();
                                //ovo tu jer se inca pozdaina cudno ponasa nakon gasenja
                                main.tabLayers1.getActiveLayer().setValue("width", "100px");
                                main.tabLayers1.getActiveLayer().setValue("height", "100%");
                            }
                        });

                        var c = new wm.PageContainer({
                            owner: main,
                            parent: l,
                            width: "100%",
                            height: "100%",
                            pageName: "NaplaceneVoznje"
                        });
                        main.tabLayers1.setLayer(l);
                    } else {
                        main.tabLayers1.setLayerByCaption(item.id);
                    }
                }
                if (item.id == 23) {
                    main.designableDialogOtvaranjeVoznje.show();
                }
                if (item.id == 25) {
                    main.designableDialogAdminUser.show();
                }
                if (item.id == 26) {
                    main.designableDialogAdminRole.show();
                }
                if (item.id == 27) {
                    main.designableDialogLogovi.show();
                }
                if (item.id == 28) {
                    if (otvorenTabOtvoreneVoznje === false) {
                        otvorenTabOtvoreneVoznje = true;
                        var l = new wm.Layer({
                            owner: main,
                            parent: main.tabLayers1,
                            caption: "Otvorene vožnje",
                            width: "100%",
                            height: "100%",
                            closable: true,
                            destroyable: true,
                            showDirtyFlag: true,
                            name: item.id,
                            onCloseOrDestroy: function okini(item) {
                                otvorenTabOtvoreneVoznje = false;
                                l.destroy();
                                c.destroy();
                                //ovo tu jer se inca pozdaina cudno ponasa nakon gasenja
                                main.tabLayers1.getActiveLayer().setValue("width", "100px");
                                main.tabLayers1.getActiveLayer().setValue("height", "100%");
                            }
                        });

                        var c = new wm.PageContainer({
                            owner: main,
                            parent: l,
                            width: "100%",
                            height: "100%",
                            pageName: "OtvoreneVoznje"
                        });
                        main.tabLayers1.setLayer(l);
                    } else {
                        main.tabLayers1.setLayerByCaption(item.id);
                    }
                }
            }

            var dojo_data = {
                identifier: 'id',
                label: 'text',
                items: [{
                    id: '-1',
                    text: 'root',
                    children: [{
                        _reference: '0'
                    },
                    {
                        _reference: '11'
                    },
                    {
                        _reference: '16'
                    }]
                },
                {
                    id: '0',
                    text: 'ADMINISTRACIJA',
                    forestRoot: true,
                    children: [{
                        _reference: '1'
                    },
                    {
                        _reference: '2'
                    },
                    {
                        _reference: '3'
                    },
                    {
                        _reference: '17'
                    }]
                },
                {
                    id: '11',
                    text: 'UPRAVLJANJE RESURSIMA',
                    forestRoot: true,
                    children: [{
                        _reference: '13'
                    },
                    {
                        _reference: '12'
                    },
                    {
                        _reference: '14'
                    }]
                },
                {
                    id: '22',
                    text: 'UPRAVLJANJE VOŽNJAMA',
                    forestRoot: true,
                    children: [{
                        _reference: '23'
                    },
                    {
                        _reference: '18'
                    },
                    {
                        _reference: '19'
                    }]
                },
                {
                    id: '5',
                    text: 'PREGLEDI',
                    forestRoot: true,
                    children: [{
                        _reference: '28'
                    },
                    {
                        _reference: '20'
                    },
                    {
                        _reference: '21'
                    }]
                },
                {
                    id: '16',
                    text: 'UPOZORENJA',
                    forestRoot: true
                },
                {
                    id: '1',
                    text: 'Vozači'
                },
                {
                    id: '2',
                    text: 'Vozila'
                },
                {
                    id: '3',
                    text: 'Vožnje'
                },
                {
                    id: '6',
                    text: 'Pregled vožnje(g)'
                },
                {
                    id: '7',
                    text: 'Pregled vožnje(t)'
                },
                {
                    id: '9',
                    text: 'Pregled vožnje(g)'
                },
                {
                    id: '10',
                    text: 'Pregled vožnje(t)'
                },
                {
                    id: '12',
                    text: 'Slobodni vozači(g)'
                },
                {
                    id: '13',
                    text: 'Slobodni resursi'
                },
                {
                    id: '14',
                    text: 'Slobodna vozila(g)'
                },
                {
                    id: '15',
                    text: 'Slobodna vozila(t)'
                },
                {
                    id: '17',
                    text: 'Naručitelji'
                },
                {
                    id: '18',
                    text: 'Zatvaranje vožnji'
                },
                {
                    id: '19',
                    text: 'Naplata vožnji'
                },
                {
                    id: '20',
                    text: 'Nenaplaćene vožnje'
                },
                {
                    id: '21',
                    text: 'Naplaćene vožnje'
                },
                {
                    id: '23',
                    text: 'Otvaranje vožnje'
                },
                {
                    id: '25',
                    text: 'Korisnici'
                },
                {
                    id: '26',
                    text: 'Uloge'
                },
                {
                    id: '28',
                    text: 'Otvorene vožnje'
                }]
            };

            data_store = new dojo.data.ItemFileWriteStore({
                data: dojo_data
            });

            var tree_model = new dijit.tree.ForestStoreModel({
                rootId: '-1',
                store: data_store,
                query: {
                    forestRoot: true
                },
                childrenAttrs: ["children"]
            });
            if (typeof tree_view === "undefined") {
                tree_view = new dijit.Tree({
                    model: tree_model,
                    showRoot: false,
                    autoExpand: false,
                    branchIcons: true,
                    nodeIcons: true,
                    getIconClass: ikone,
                    openOnClick: true,
                    onClick: kliknutaStavkaMenija
                }, 'classTreePane');

            } else {
                tree_view.destroyRecursive();
                tree_view = new dijit.Tree({
                    model: tree_model,
                    showRoot: false,
                    autoExpand: false,
                    branchIcons: true,
                    nodeIcons: true,
                    getIconClass: ikone,
                    openOnClick: true,
                    onClick: kliknutaStavkaMenija
                }, 'classTreePane');
            }

        } catch (e) {
            app.toastError(this.name + ".start() Failed: " + e.toString());
        }
    },


    //>>custom funkcije za sejvanje voznji u bazu
    saveButton3Click: function(inSender) {
        try {
            if (main.voznjaLiveForm1.operation == "insert") {
                main.voznjaLiveForm1.populateDataOutput();
                console.log("Pozivanje custom funkcije za upis vožnji");
                main.serviceVarInsertVoznja.update();
            }
            else if (main.voznjaLiveForm1.operation == "update") {
                this.voznjaLiveForm1.updateData();
            }
        } catch (e) {
            console.error('ERROR IN saveButton1Click: ' + e);
        }

    },

    deleteButton3Click: function(inSender) {
        try {
            this.voznjaLiveForm1.deleteData();
        } catch (e) {
            console.error('ERROR IN deleteButton3Click: ' + e);
        }
    },

    voznjaLiveForm1Success: function(inSender, inData) {
        try {
            if (this.voznjaLiveForm1.operation !== "delete") {
                main.voznjaLiveForm1.endEdit();
            }
            this.serviceVarGetVoznjeForma.update();
        } catch (e) {
            console.error('ERROR IN saveButton1Click: ' + e);
        }
    },

    serviceVarGetVoznjeFormaSuccess: function(inSender, inDeprecated) {
        try {
            this.voznjaDojoGrid.setDataSet(this.serviceVarGetVoznjeForma);
        } catch (e) {
            console.error('ERROR IN serviceVarGetVoznjeFormaSuccess: ' + e);
        }
    },

    serviceVarInsertVoznjaSuccess: function(inSender, inDeprecated) {
        try {
            main.voznjaLiveForm1.endEdit();
            this.serviceVarGetVoznjeForma.update();
            if (inSender.getData().dataValue === false) {
                app.toastError("Odabrani resursi su zauzeti u definiranom terminu!", 1500);
            }
        } catch (e) {
            console.error('ERROR IN serviceVarInsertVoznjaSuccess: ' + e);
        }
    },

    //<<custom funkcije za sejvanje voznji u bazu
    //kod loadanja dialoga za tablicni prikaza voznji puni se grid
    designableDialogPregledVoznjiShow: function(inSender) {
        try {
            if (this.serviceVarGetVoznjeTablica.canUpdate()) this.serviceVarGetVoznjeTablica.update();
            else app.alert("Try again later");
        } catch (e) {
            console.error('ERROR IN designableDialogPregledVoznjiShow: ' + e);
        }

    },

    serviceVarGetVoznjeTablicaSuccess: function(inSender, inDeprecated) {
        try {
            this.dojoGrid1.setDataSet(this.serviceVarGetVoznjeTablica);
        } catch (e) {
            console.error('ERROR IN serviceVarGetVoznjeTablicaSuccess: ' + e);
        }
    },

    //filter kod pregleda voznji
    txtGarazniBrojFilterChange: function(inSender) {
        try {
            //kad je prazna kucica vraca sve 
            if (this.txtGarazniBrojFilter.getDisplayValue() === "") {
                this.serviceVarGetVoznjeTablica.update();
            } else {
                this.serviceVarGetVoznjeGarBroj.update();
            }
        } catch (e) {
            console.error('ERROR IN txtGarazniBrojFilterChange: ' + e);
        }
    },

    serviceVarGetVoznjeGarBrojSuccess: function(inSender, inDeprecated) {
        try {
            this.dojoGrid1.setDataSet(this.serviceVarGetVoznjeGarBroj);
        } catch (e) {
            console.error('ERROR IN serviceVarGetVoznjeGarBrojSuccess: ' + e);
        }
    },

    //filter voznji po datumu
    dateDatumPolaskaFilterChange: function(inSender) {
        try {
            this.serviceVarGetVoznjeDatPolaska.update();
        } catch (e) {
            console.error('ERROR IN dateDatumPolaskaFilterChange: ' + e);
        }
    },

    serviceVarGetVoznjeDatPolaskaSuccess: function(inSender, inDeprecated) {
        try {
            this.dojoGrid1.setDataSet(this.serviceVarGetVoznjeDatPolaska);
        } catch (e) {
            console.error('ERROR IN serviceVarGetVoznjeDatPolaskaSuccess: ' + e);
        }
    },

    //ovo su sve custom formateri datuma jer lokalizacija hr ne radi - Vozila
    busDojoGridDatumKupnjeBusFormat: function(inValue, rowId, cellId, cellField, cellObj, rowObj) {
        try {
            return app.dateFormat(inValue);
        } catch (e) {
            console.error('ERROR IN busDojoGridDatumKupnjeBusFormat: ' + e);
        }
    },
    busDojoGridDatumZadnjeRegistracijeBusFormat: function(inValue, rowId, cellId, cellField, cellObj, rowObj) {
        try {
            return app.dateFormat(inValue);
        } catch (e) {
            console.error('ERROR IN busDojoGridDatumZadnjeRegistracijeBusFormat: ' + e);
        }
    },
    busDojoGridDatumZadnjegServisaBusFormat: function(inValue, rowId, cellId, cellField, cellObj, rowObj) {
        try {
            return app.dateFormat(inValue);
        } catch (e) {
            console.error('ERROR IN busDojoGridDatumZadnjegServisaBusFormat: ' + e);
        }
    },
    datumKupnjeBusEditor1ReadOnlyNodeFormat: function(inSender, inValue) {
        try {
            return app.dateFormat(inValue);
        } catch (e) {
            console.error('ERROR IN datumKupnjeBusEditor1ReadOnlyNodeFormat: ' + e);
        }
    },
    datumZadnjeRegistracijeBusEditor1ReadOnlyNodeFormat: function(inSender, inValue) {
        try {
            return app.dateFormat(inValue);
        } catch (e) {
            console.error('ERROR IN datumZadnjeRegistracijeBusEditor1ReadOnlyNodeFormat: ' + e);
        }
    },
    datumZadnjegServisaBusEditor1ReadOnlyNodeFormat: function(inSender, inValue) {
        try {
            return app.dateFormat(inValue);
        } catch (e) {
            console.error('ERROR IN datumZadnjegServisaBusEditor1ReadOnlyNodeFormat: ' + e);
        }
    },

    //ovo su sve custom formateri datuma jer lokalizacija hr ne radi - Vozaci
    vozacDojoGridDatumRodjenjaVozacFormat: function(inValue, rowId, cellId, cellField, cellObj, rowObj) {
        try {
            return app.dateFormat(inValue);
        } catch (e) {
            console.error('ERROR IN vozacDojoGridDatumRodjenjaVozacFormat: ' + e);
        }
    },
    vozacDojoGridDatumVozackaVozacFormat: function(inValue, rowId, cellId, cellField, cellObj, rowObj) {
        try {
            return app.dateFormat(inValue);
        } catch (e) {
            console.error('ERROR IN vozacDojoGridDatumVozackaVozacFormat: ' + e);
        }
    },
    vozacDojoGridDatumPutovnicaVozacFormat: function(inValue, rowId, cellId, cellField, cellObj, rowObj) {
        try {
            return app.dateFormat(inValue);
        } catch (e) {
            console.error('ERROR IN vozacDojoGridDatumPutovnicaVozacFormat: ' + e);
        }
    },
    vozacDojoGridDatumOsobnaVozacFormat: function(inValue, rowId, cellId, cellField, cellObj, rowObj) {
        try {
            return app.dateFormat(inValue);
        } catch (e) {
            console.error('ERROR IN vozacDojoGridDatumOsobnaVozacFormat: ' + e);
        }
    },
    datumRodjenjaVozacEditor1ReadOnlyNodeFormat: function(inSender, inValue) {
        try {
            return app.dateFormat(inValue);
        } catch (e) {
            console.error('ERROR IN datumRodjenjaVozacEditor1ReadOnlyNodeFormat: ' + e);
        }
    },
    datumVozackaVozacEditor1ReadOnlyNodeFormat: function(inSender, inValue) {
        try {
            return app.dateFormat(inValue);
        } catch (e) {
            console.error('ERROR IN datumVozackaVozacEditor1ReadOnlyNodeFormat: ' + e);
        }
    },
    datumPutovnicaVozacEditor1ReadOnlyNodeFormat: function(inSender, inValue) {
        try {
            return app.dateFormat(inValue);
        } catch (e) {
            console.error('ERROR IN datumPutovnicaVozacEditor1ReadOnlyNodeFormat: ' + e);
        }
    },
    datumOsobnaVozacEditor1ReadOnlyNodeFormat: function(inSender, inValue) {
        try {
            return app.dateFormat(inValue);
        } catch (e) {
            console.error('ERROR IN datumOsobnaVozacEditor1ReadOnlyNodeFormat: ' + e);
        }
    },

    //ovo su sve custom formateri datuma jer lokalizacija hr ne radi - Voznja
    voznjaDojoGridVrijemePolaskaVoznjaFormat: function(inValue, rowId, cellId, cellField, cellObj, rowObj) {
        try {
            return app.dateFormat(inValue);
        } catch (e) {
            console.error('ERROR IN voznjaDojoGridVrijemePolaskaVoznjaFormat: ' + e);
        }
    },
    voznjaDojoGridVrijemeDolaskaVoznjaFormat: function(inValue, rowId, cellId, cellField, cellObj, rowObj) {
        try {
            return app.dateFormat(inValue);
        } catch (e) {
            console.error('ERROR IN voznjaDojoGridVrijemeDolaskaVoznjaFormat: ' + e);
        }
    },
    vrijemePolaskaVoznjaEditor1ReadOnlyNodeFormat: function(inSender, inValue) {
        try {
            return app.dateTimeFormat(inValue);
        } catch (e) {
            console.error('ERROR IN vrijemePolaskaVoznjaEditor1ReadOnlyNodeFormat: ' + e);
        }
    },
    vrijemeDolaskaVoznjaEditor1ReadOnlyNodeFormat: function(inSender, inValue) {
        try {
            return app.dateTimeFormat(inValue);
        } catch (e) {
            console.error('ERROR IN vrijemeDolaskaVoznjaEditor1ReadOnlyNodeFormat: ' + e);
        }
    },

    designableDialogpregledVoznjiVozacShow: function(inSender) {
        try {
            if (this.serviceVarGetVoznjeTablicaVozac.canUpdate()) this.serviceVarGetVoznjeTablicaVozac.update();
        } catch (e) {
            console.error('ERROR IN designableDialogpregledVoznjiVozacShow: ' + e);
        }
    },
    serviceVarGetVoznjeTablicaVozacSuccess: function(inSender, inDeprecated) {
        try {
            this.dojoGrid2.setDataSet(this.serviceVarGetVoznjeTablicaVozac);
        } catch (e) {
            console.error('ERROR IN serviceVarGetVoznjeTablicaVozacSuccess: ' + e);
        }
    },
    servicePrintIzvjestajResult: function(inSender, inDeprecated) {
        try {
            var width = 1024;
            var height = 768;
            var left = parseInt((screen.availWidth / 2) - (width / 2));
            var top = parseInt((screen.availHeight / 2) - (height / 2));

            // pol toga netreba tu, ali želja je bila da prozor ima svoje ime gore.. i da nema adressbar... ali u ovom trenutko to nije moguće izvesti
            var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top + ", fullscreen=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no";

            var win = window.open("", "subWind", windowFeatures);
            var doc = win.document;
            doc.open("text/html");
            //hardcodirano za Cloudfoundry
            //doc.write("<iframe width='100%' height='100%' name='reportes1' id='reportes1' src='http://rasbus.cloudfoundry.com/" + inDeprecated + "'>");
            //hardcodirano za Amazon
            //doc.write("<iframe width='100%' height='100%' name='reportes1' id='reportes1' src='http://ec2-50-16-91-22.compute-1.amazonaws.com:8080/rasbus-test/" + inDeprecated + "'>");
            //doc.write("<iframe width='100%' height='100%' name='reportes1' id='reportes1' src='http://ec2-50-16-91-22.compute-1.amazonaws.com:8080/rasbus-prez/" + inDeprecated + "'>");
            //doc.write("<iframe width='100%' height='100%' name='reportes1' id='reportes1' src='http://ec2-50-16-91-22.compute-1.amazonaws.com:8080/rasbus-prod/" + inDeprecated + "'>");
            //alert(window.location.protocol + "//" + window.location.hostname + ":" + window.location.port + "/" + wm.application.type + "/" + inDeprecated);
            var url = document.URL.replace("index.html",inDeprecated);
            doc.write("<iframe width='100%' height='100%' name='reportes1' id='reportes1' src='" + url + "'>");
            //doc.write("<iframe width='100%' height='100%' name='reportes1' id='reportes1' src='" + window.location.protocol + "//" + window.location.hostname + ":" + window.location.port + "/" + wm.application.type + "/" + inDeprecated + "'>");
            doc.close();

        } catch (e) {
            console.error('ERROR IN servicePrintIzvjestajResult: ' + e);
        }
    },
    btnReportVozilaClick: function(inSender) {
        try {
            var typeofdoc = "PDF";
            var src = "ReportSvaVozila2.jasper";
            var garazniBroj = this.searchBus.getDataValue();

            var paramVrsta = "- bez odabranih kriterija -";
            if (document.getElementById("dijit_form_TextBox_0").value !== "") {
                paramVrsta = "- prema garažnom broju -";
            }
            var paramLogo = "../../resources/images/reports/logoReport.gif";
            var paramVozilo = "../../resources/images/reports/buspicReportBack.gif";
            var hash = {
                "typeDoc": typeofdoc,
                "file": src,
                "paramLogo": paramLogo,
                "paramGarazniBroj": garazniBroj
            };
            this.serviceVarGetReportBus.input.setValue("parameters", hash);
            this.serviceVarGetReportBus.update();
        } catch (e) {
            console.error('ERROR IN btnReportVozilaClick: ' + e);
        }
    },

    btnReportVoznjeClick: function(inSender) {
        try {
            document.getElementById("main_panel11").style.cursor = "wait";
            var typeofdoc = "PDF";
            var src = "ReportSveVoznje2.jasper";
            var paramVrsta = "- bez odabranih kriterija -";
            var paramUpit = "";
            var mjestoPolaska = this.textMjestoPolaska.getDataValue();
            var mjestoDolaska = this.textMjestoDolaskaFilter.getDataValue();
            var prezimeVozaca = this.textVozacFilter.getDataValue();
            var garazniBroj = this.textVoziloFilter.getDataValue();
            var vrijemePolaska = this.dateVrijemePolaskaFilter.getDataValue();
            var vrijemeDolaska = this.dateVrijemeDolaskaFilter.getDataValue();
            var oznakaVoznje = this.textFilterOznaka.getDataValue();

            var paramLogo = "../../resources/images/reports/logoReport.gif";
            var paramRideBack = "../../resources/images/reports/rideback.gif";
            var hash = {
                "typeDoc": typeofdoc,
                "file": src,
                "paramLogo": paramLogo,
                "mjestoPolaska": mjestoPolaska,
                "mjestoDolaska": mjestoDolaska,
                "prezimeVozaca": prezimeVozaca,
                "garazniBroj": garazniBroj,
                "vrijemePolaska": vrijemePolaska,
                "vrijemeDolaska": vrijemeDolaska,
                "oznakaVoznje": oznakaVoznje
            };
            this.serviceVarGetReportVoznje.input.setValue("parameters", hash);
            this.serviceVarGetReportVoznje.update();
        } catch (e) {
            console.error('ERROR IN btnReportVoznjeClick: ' + e);
        }
    },

    btnReportVozaciClick: function(inSender) {
        try {
            var typeofdoc = "PDF";
            var src = "ReportSviVozaci2.jasper";
            var paramLogo = "../../resources/images/reports/logoReport.gif";
            var paramVozaci = "../../resources/images/reports/driverpicReportBack.gif";
            var paramPrezime = this.searchVozac.getDataValue();
            var hash = {
                "typeDoc": typeofdoc,
                "file": src,
                "paramLogo": paramLogo,
                "paramPrezime": paramPrezime
            };
            this.serviceVarGetReportVozaci.input.setValue("parameters", hash);
            this.serviceVarGetReportVozaci.update();

        } catch (e) {
            console.error('ERROR IN btnReportVozaciClick: ' + e);
        }
    },

    btnReportNaruciteljiClick: function(inSender) {
        try {
            var typeofdoc = "PDF";
            var src = "ReportSviNarucitelji.jasper";
            var paramLogo = "../../resources/images/reports/logoReport.gif";
            var paramVozaci = "../../resources/images/reports/driverpicReportBack.gif";
            var paramNazivNarucitelja = this.textNazivNaruciteljaFilter.getDataValue();
            var hash = {
                "typeDoc": typeofdoc,
                "file": src,
                "paramLogo": paramLogo,
                "paramNazivNarucitelja": paramNazivNarucitelja
            };
            this.serviceVarGetReportNarucitelji.input.setValue("parameters", hash);
            this.serviceVarGetReportNarucitelji.update();

        } catch (e) {
            console.error('ERROR IN btnReportVozaciClick: ' + e);
        }
    },

    designableDialogAdminVoznjaShow: function(inSender) {
        try {
            this.naruciteljLookup1.update();
            this.busLookup1.update();
            this.vozacLookup1.update();
            //refreshanje grida
            this.serviceVarGetVoznjeForma.update();
        } catch (e) {
            console.error('ERROR IN designableDialogAdminVoznjaShow: ' + e);
        }
    },

    voznjaDojoGridCustomField1Format: function(inValue, rowId, cellId, cellField, cellObj, rowObj) {
        try {
            if (inValue === null) {
                return "Zatvorena";
            } else if (typeof inValue === 'number') {
                return "Naplaćena";
            } else {
                return "Otvorena";
            }
        } catch (e) {
            console.error('ERROR IN voznjaDojoGridCustomField1Format: ' + e);
        }
    },
    serviceVarEditVoznjaSuccess: function(inSender, inDeprecated) {
        try {
            main.voznjaLiveForm1.endEdit();
            this.serviceVarGetVoznjeForma.update();
            if (inSender.getData().dataValue === false) {
                app.toastError("Odabrani resursi su zauzeti u definiranom terminu!", 1000);
            }
        } catch (e) {
            console.error('ERROR IN serviceVarEditVoznjaSuccess: ' + e);
        }
    },

    voznjaDojoGridSelectionChange: function(inSender) {
        try {
            this.updateButton3.setDisabled(false);
            this.deleteButton3.setDisabled(false);
        } catch (e) {
            console.error('ERROR IN voznjaDojoGridSelectionChange: ' + e);
        }
    },
    cancelButton3Click: function(inSender) {
        try {
            this.voznjaLiveForm1.endEdit();
        } catch (e) {
            console.error('ERROR IN cancelButton3Click: ' + e);
        }
    },
    updateButton3Click1: function(inSender) {
        try {
            this.vrijemePolaskaVoznjaEditor1.setReadonly(true);
            this.vrijemeDolaskaVoznjaEditor1.setReadonly(true);
            this.vozacLookup1.setReadonly(true);
            this.busLookup1.setReadonly(true);
        } catch (e) {
            console.error('ERROR IN updateButton3Click1: ' + e);
        }
    },
    designableDialogOtvaranjeVoznjeShow: function(inSender) {
        try {
            this.naruciteljLookup2.update();
            this.busLookup2.update();
            this.vozacLookup2.update();
            this.serviceVarGetVoznjeOtvaranje.update();
        } catch (e) {
            console.error('ERROR IN designableDialogOtvaranjeVoznjeShow: ' + e);
        }
    },
    btnReportVoznje1Click: function(inSender) {
        try {
            this.btnReportVoznjeClick(inSender);

        } catch (e) {
            console.error('ERROR IN btnReportVoznje1Click: ' + e);
        }
    },

    voznjaLiveForm2Success: function(inSender, inData) {
        try {
            this.voznjaLiveForm1Success(inSender, inData);

        } catch (e) {
            console.error('ERROR IN voznjaLiveForm2Success: ' + e);
        }
    },
    designableDialogAdminVoznja2Show: function(inSender) {
        try {
            this.designableDialogAdminVoznjaShow(inSender);

        } catch (e) {
            console.error('ERROR IN designableDialogAdminVoznja2Show: ' + e);
        }
    },
    btnReportVoznje2Click: function(inSender) {
        try {
            this.btnReportVoznjeClick(inSender);

        } catch (e) {
            console.error('ERROR IN btnReportVoznje2Click: ' + e);
        }
    },
    voznjaDojoGrid2SelectionChange: function(inSender) {
        try {
            this.voznjaDojoGridSelectionChange(inSender);

        } catch (e) {
            console.error('ERROR IN voznjaDojoGrid2SelectionChange: ' + e);
        }
    },
    voznjaLiveForm4Success: function(inSender, inData) {
        try {
            this.voznjaLiveForm1Success(inSender, inData);

        } catch (e) {
            console.error('ERROR IN voznjaLiveForm4Success: ' + e);
        }
    },
    saveButton5Click: function(inSender) {
        try {
            if (main.voznjaLiveForm2.operation == "insert") {
                main.voznjaLiveForm2.populateDataOutput();
                console.log("Pozivanje custom funkcije za upis vožnji");
                main.serviceVarInsertVoznjaOtvaranje.update();
            }
        } catch (e) {
            console.error('ERROR IN saveButton5Click: ' + e);
        }
    },
    serviceVarInsertVoznjaOtvaranjeSuccess: function(inSender, inDeprecated) {
        try {
            main.voznjaLiveForm2.endEdit();
            this.serviceVarGetVoznjeOtvaranje.update();
            if (inSender.getData().dataValue === false) {
                app.toastError("Odabrani resursi su zauzeti u definiranom terminu!", 1500);
            }
        } catch (e) {
            console.error('ERROR IN serviceVarInsertVoznjaOtvaranjeSuccess: ' + e);
        }
    },
    designableDialogAdminVozilaShow: function(inSender) {
        try {
            this.busDojoGrid.select(0);
        } catch (e) {
            console.error('ERROR IN designableDialogAdminVozilaShow: ' + e);
        }
    },

    designableDialogAdminVozacShow: function(inSender) {
        try {
            this.vozacDojoGrid.select(0);
        } catch (e) {
            console.error('ERROR IN designableDialogAdminVozacShow: ' + e);
        }
    },
    designableDialogAdminNaruciteljShow: function(inSender) {
        try {
            this.naruciteljDojoGrid.select(0);
        } catch (e) {
            console.error('ERROR IN designableDialogAdminNaruciteljShow: ' + e);
        }
    },

    serviceVarGetRoleSuccess: function(inSender, inDeprecated) {
        try {
            var korisnici = null;
            //komplikacije da bi se dinamički dodalo children u meni

            function gotKorisnici(item) {
                //alert(data_store.getValue(item, "text"));
                if (data_store.isItem(item)) {
                    korisnici = item;
                    data_store.fetchItemByIdentity({
                        identity: '26',
                        onItem: gotUloge,
                        onError: failed
                    });
                } else {
                    console.log("Unable to locate the item with identity [id]");
                }
            }

            function gotUloge(item) {
                //alert(data_store.getValue(item, "text"));
                if (data_store.isItem(item)) {
                    data_store.newItem({
                        id: '24',
                        text: 'UPRAVLJANJE KORISNICIMA',
                        forestRoot: true,
                        children: [korisnici, item]
                    });
                } else {
                    console.log("Unable to locate the item with identity [id]");
                }
            }

            function failed() {}

            //dodavanje UPRAVLJANJE KORISNICIMA
            if (inSender.getItem(0).getValue("dataValue") == main.ulogaAdmin) {
                data_store.fetchItemByIdentity({
                    identity: '25',
                    onItem: gotKorisnici,
                    onError: failed
                });
            }
            //dodavanje LOGOVA
            if (inSender.getItem(0).getValue("dataValue") == main.ulogaAdmin) {
                data_store.newItem({
                    id: '27',
                    text: 'LOGOVI',
                    forestRoot: true
                });
            }



            function saveDone() {
                //izvršava se ako je dodavanje uspješno
                //alert("Done saving.");
            }

            function saveFailed() {
                //izvršava se ako dodavanje nije uspješno
                //alert("Save failed.");
            }
            data_store.save({
                onComplete: saveDone,
                onError: saveFailed
            });
        } catch (e) {
            console.error('ERROR IN serviceVarGetRoleSuccess: ' + e);
        }
    },
    designableDialogAdminUserShow: function(inSender) {
        try {
            this.userDojoGrid.select(0);
        } catch (e) {
            console.error('ERROR IN designableDialogAdminUserShow: ' + e);
        }
    },
    designableDialogAdminRoleShow: function(inSender) {
        try {
            this.roleDojoGrid.select(0);
        } catch (e) {
            console.error('ERROR IN designableDialogAdminRoleShow: ' + e);
        }
    },
    ResetVrijemePolaskaVoznjeClick: function(inSender) {
        try {
            this.dateVrijemePolaskaFilter.setDataValue("");

        } catch (e) {
            console.error('ERROR IN ResetVrijemePolaskaVoznjeClick: ' + e);
        }
    },
    ResetVrijemeDolaskaVoznjeClick: function(inSender) {
        try {
            this.dateVrijemeDolaskaFilter.setDataValue("");

        } catch (e) {
            console.error('ERROR IN ResetVrijemeDolaskaVoznjeClick: ' + e);
        }
    },
    ResetVrijemeDolaskaVoznjeOtvaranjeClick: function(inSender) {
        try {
            this.dateVrijemePolaskaFilter1.setDataValue("");

        } catch (e) {
            console.error('ERROR IN ResetVrijemeDolaskaVoznjeOtvaranjeClick: ' + e);
        }
    },
    ResetVrijemeDOlaskaVoznjeOtvaranjeClick: function(inSender) {
        try {
            this.dateVrijemeDolaskaFilter1.setDataValue("");

        } catch (e) {
            console.error('ERROR IN ResetVrijemeDOlaskaVoznjeOtvaranjeClick: ' + e);
        }
    },
    logoviDojoGridServerskoVrijemeFormat: function(inValue, rowId, cellId, cellField, cellObj, rowObj) {
        try {
            return app.dateTimeFormat(inValue);
        } catch (e) {
            console.error('ERROR IN logoviDojoGridServerskoVrijemeFormat: ' + e);
        }
    },
    
  designableDialogLogoviShow: function(inSender){
	  try {		  
		  this.serviceVarGetLogovi.update();
	  } catch(e) {
		  console.error('ERROR IN designableDialogLogoviShow: ' + e); 
	  } 
  },
  //custom filtiranje live variabli sa vremenskom zadrškom
  searchBusChange: function(inSender) {
	  try {	  
          if (this._mytimeout) {
              window.clearTimeout(this._mytimeout);
          }
          //app - klasa iz koje se poziva funkcija
          //filterVozilo - naziv funkcije koja se poziva 
          //this.busLiveVariable1 - live variabla koja se filtrira
          //garazniBrojBus - polje prema kojem filtriramo
          //this.searchBus.getDataValue - vrijednost polja prema kojem filtriramo
          this._mytimeout = window.setTimeout(dojo.hitch(app, 'filterVozilo', this.busLiveVariable1 ,'garazniBrojBus',this.searchBus.getDataValue() ), 500);
	  } catch(e) {
		  console.error('ERROR IN searchBusChange: ' + e); 
	  } 
  },
  //custom filtiranje live variabli sa vremenskom zadrškom
  searchVozacChange: function(inSender) {
	  try {
          if (this._mytimeout) {
              window.clearTimeout(this._mytimeout);
          }
		  //app - klasa iz koje se poziva funkcija
          //filterVozilo - naziv funkcije koja se poziva 
          //this.vozacLiveVariable1 - live variabla koja se filtrira
          //prezimeVozac - polje prema kojem filtriramo
          //this.searchVozac.getDataValue - vrijednost polja prema kojem filtriramo
          this._mytimeout = window.setTimeout(dojo.hitch(app, 'filterVozac', this.vozacLiveVariable1 ,'prezimeVozac',this.searchVozac.getDataValue() ), 500);		  
	  } catch(e) {
		  console.error('ERROR IN searchVozacChange: ' + e); 
	  } 
  },
  //custom filtiranje live variabli sa vremenskom zadrškom
  textNazivNaruciteljaFilterChange: function(inSender) {
	  try {	
          if (this._mytimeout) {
              window.clearTimeout(this._mytimeout);
          }
		  //app - klasa iz koje se poziva funkcija
          //filterNarucitelj - naziv funkcije koja se poziva 
          //this.vozacLiveVariable1 - live variabla koja se filtrira
          //nazivNarucitelj - polje prema kojem filtriramo
          //this.searchVozac.getDataValue - vrijednost polja prema kojem filtriramo
          this._mytimeout = window.setTimeout(dojo.hitch(app, 'filterNarucitelj', this.naruciteljLiveVariable1 ,'nazivNarucitelj',this.textNazivNaruciteljaFilter.getDataValue() ), 500);    	  
	  } catch(e) {
		  console.error('ERROR IN textNazivNaruciteljaFilterChange: ' + e); 
	  } 
  },
  ///metode za filtriranje administracije voznji sa vremenskom zadrskom
  textMjestoPolaskaChange: function(inSender) {
	  try {	
          this.callServiceVarGetVoznjaForma();         
	  } catch(e) {
		  console.error('ERROR IN textMjestoPolaskaChange: ' + e); 
	  } 
  },
  textMjestoDolaskaFilterChange: function(inSender) {
	  try {
		  this.callServiceVarGetVoznjaForma();		  
	  } catch(e) {
		  console.error('ERROR IN textMjestoDolaskaFilterChange: ' + e); 
	  } 
  },
  textVozacFilterChange: function(inSender) {
	  try {
		  this.callServiceVarGetVoznjaForma();		  
	  } catch(e) {
		  console.error('ERROR IN textVozacFilterChange: ' + e); 
	  } 
  },
  textVoziloFilterChange: function(inSender) {
	  try {
          this.callServiceVarGetVoznjaForma();		  
	  } catch(e) {
		  console.error('ERROR IN textVoziloFilterChange: ' + e); 
	  } 
  },
  dateVrijemePolaskaFilterChange: function(inSender) {
	  try {		  
		  this.callServiceVarGetVoznjaForma();
	  } catch(e) {
		  console.error('ERROR IN dateVrijemePolaskaFilterChange: ' + e); 
	  } 
  },
  dateVrijemeDolaskaFilterChange: function(inSender) {
	  try {		  
		  this.callServiceVarGetVoznjaForma();
	  } catch(e) {
		  console.error('ERROR IN dateVrijemeDolaskaFilterChange: ' + e); 
	  } 
  },
  textFilterOznakaChange: function(inSender) {
      try {
		  this.callServiceVarGetVoznjaForma();		  
	  } catch(e) {
		  console.error('ERROR IN textFilterOznakaChange: ' + e); 
	  } 
  },
  callServiceVarGetVoznjaForma: function(){
       try {
          if (this._mytimeout) {
              window.clearTimeout(this._mytimeout);
          }
    	  this.serviceVarGetVoznjeForma.input.setValue('garazniBroj', this.textVoziloFilter.getDataValue());
          this.serviceVarGetVoznjeForma.input.setValue('mjestoDolaska', this.textMjestoDolaskaFilter.getDataValue());
          this.serviceVarGetVoznjeForma.input.setValue('mjestoPolaska', this.textMjestoPolaska.getDataValue());
          this.serviceVarGetVoznjeForma.input.setValue('prezimeVozaca', this.textVozacFilter.getDataValue());
          this.serviceVarGetVoznjeForma.input.setValue('vrijemeDolaska', this.dateVrijemeDolaskaFilter.getDataValue());
          this.serviceVarGetVoznjeForma.input.setValue('vrijemePolaska', this.dateVrijemePolaskaFilter.getDataValue());
          this.serviceVarGetVoznjeForma.input.setValue('oznakaVoznje', this.textFilterOznaka.getDataValue());
          this._mytimeout = window.setTimeout(dojo.hitch(app, 'filterVoznjaForm', this.serviceVarGetVoznjeForma), app.filteriTimeOut);		  
	  } catch(e) {
		  console.error('ERROR IN textMjestoDolaskaFilterChange: ' + e); 
	  }
  },
  //metoda za filtiranje s vremenskim odmakom
  callServiceVarGetVoznjaOtvaranje: function(){
       try {
          if (this._mytimeout) {
              window.clearTimeout(this._mytimeout);
          }
          this.serviceVarGetVoznjeOtvaranje.input.setValue('garazniBroj', this.textVoziloFilter1.getDataValue());
          this.serviceVarGetVoznjeOtvaranje.input.setValue('mjestoDolaska', this.textMjestoDolaskaFilter1.getDataValue());
          this.serviceVarGetVoznjeOtvaranje.input.setValue('mjestoPolaska', this.textMjestoPolaska1.getDataValue());
          this.serviceVarGetVoznjeOtvaranje.input.setValue('prezimeVozaca', this.textVozacFilter1.getDataValue());
          this.serviceVarGetVoznjeOtvaranje.input.setValue('vrijemeDolaska', this.dateVrijemeDolaskaFilter1.getDataValue());
          this.serviceVarGetVoznjeOtvaranje.input.setValue('vrijemePolaska', this.dateVrijemePolaskaFilter1.getDataValue());
          this.serviceVarGetVoznjeOtvaranje.input.setValue('oznakaVoznje', this.textFilterOznakaOtvaranje.getDataValue());
          this._mytimeout = window.setTimeout(dojo.hitch(app, 'filterVoznjaOtvaranje', this.serviceVarGetVoznjeOtvaranje), app.filteriTimeOut);		  
	  } catch(e) {
		  console.error('ERROR IN textMjestoDolaskaFilterChange: ' + e); 
	  }
  },
  textMjestoPolaska1Change: function(inSender) {
	  try {		  
		  this.callServiceVarGetVoznjaOtvaranje();
	  } catch(e) {
		  console.error('ERROR IN textMjestoPolaska1Change: ' + e); 
	  } 
  },
  textMjestoDolaskaFilter1Change: function(inSender) {
	  try {		  
		  this.callServiceVarGetVoznjaOtvaranje();
	  } catch(e) {
		  console.error('ERROR IN textMjestoDolaskaFilter1Change: ' + e); 
	  } 
  },
  textVozacFilter1Change: function(inSender) {
	  try {		  
		  this.callServiceVarGetVoznjaOtvaranje();
	  } catch(e) {
		  console.error('ERROR IN textVozacFilter1Change: ' + e); 
	  } 
  },
  textVoziloFilter1Change: function(inSender) {
	  try {		  
		  this.callServiceVarGetVoznjaOtvaranje();
	  } catch(e) {
		  console.error('ERROR IN textVoziloFilter1Change: ' + e); 
	  } 
  },
  dateVrijemePolaskaFilter1Change: function(inSender) {
	  try {		  
		  this.callServiceVarGetVoznjaOtvaranje();
	  } catch(e) {
		  console.error('ERROR IN dateVrijemePolaskaFilter1Change: ' + e); 
	  } 
  },
  dateVrijemeDolaskaFilter1Change: function(inSender) {
	  try {		  
		  this.callServiceVarGetVoznjaOtvaranje();
	  } catch(e) {
		  console.error('ERROR IN dateVrijemeDolaskaFilter1Change: ' + e); 
	  } 
  },
  textFilterOznakaOtvaranjeChange: function(inSender) {
      try {
		  this.callServiceVarGetVoznjaOtvaranje();		  
	  } catch(e) {
		  console.error('ERROR IN textFilterOznakaOtvaranjeChange: ' + e); 
	  } 
  },
  //metoda za filtiranje s vremenskim odmakom
  callServiceVarGetLogovi: function(){
       try {
          if (this._mytimeout) {
              window.clearTimeout(this._mytimeout);
          }
          this.serviceVarGetLogovi.input.setValue('sadrzaj', this.textFilterSadrzaj.getDataValue());
          this.serviceVarGetLogovi.input.setValue('vrijemeKraj', this.filterDateKraj.getDataValue());
          this.serviceVarGetLogovi.input.setValue('vrijemePocetak', this.filterDatePocetak.getDataValue());
          this._mytimeout = window.setTimeout(dojo.hitch(app, 'filterGetLogovi', this.serviceVarGetLogovi), 500);    	  
	  } catch(e) {
		  console.error('ERROR IN textMjestoDolaskaFilterChange: ' + e); 
	  }
  },
  textFilterSadrzajChange: function(inSender) {
	  try {		  
		  this.callServiceVarGetLogovi();
	  } catch(e) {
		  console.error('ERROR IN textFilterSadrzajChange: ' + e); 
	  } 
  },
  filterDatePocetakChange: function(inSender) {
	  try {
          if(this.filterDateKraj.getDataValue() != ''){
		    this.callServiceVarGetLogovi();
          }
	  } catch(e) {
		  console.error('ERROR IN filterDatePocetakChange: ' + e); 
	  } 
  },
  filterDateKrajChange: function(inSender) {
	  try {
          if(this.filterDatePocetak.getDataValue() != ''){
		    this.callServiceVarGetLogovi();
          }
	  } catch(e) {
		  console.error('ERROR IN filterDateKrajChange: ' + e); 
	  } 
  },
  ResetVrijemeOdLogoviClick: function(inSender) {
	  try {
		  this.filterDatePocetak.setDataValue('');		  
	  } catch(e) {
		  console.error('ERROR IN ResetVrijemeOdLogoviClick: ' + e); 
	  } 
  },
  ResetVrijemeDoLogoviClick: function(inSender) {
	  try {
		  this.filterDateKraj.setDataValue('');		  
	  } catch(e) {
		  console.error('ERROR IN ResetVrijemeDoLogoviClick: ' + e); 
	  } 
  },  
  _end: 0
});