Main.widgets = {
	busLiveVariable1: ["wm.LiveVariable", {"designMaxResults":500,"ignoreCase":true,"liveSource":"app.busLiveView1"}, {}],
	vozacLiveVariable1: ["wm.LiveVariable", {"designMaxResults":500,"ignoreCase":true,"liveSource":"app.vozacLiveView1"}, {}],
	serviceVarInsertVoznja: ["wm.ServiceVariable", {"operation":"dodavanjeVoznje","service":"glavni"}, {"onSuccess":"serviceVarInsertVoznjaSuccess"}, {
		input: ["wm.ServiceInput", {"type":"dodavanjeVoznjeInputs"}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire: ["wm.Wire", {"expression":undefined,"source":"voznjaLiveForm1.dataOutput.bus.idBus","targetProperty":"busPar"}, {}],
				wire2: ["wm.Wire", {"expression":undefined,"source":"vrijemeDolaskaVoznjaEditor1.dataValue","targetProperty":"datumDolaskaPar"}, {}],
				wire1: ["wm.Wire", {"expression":undefined,"source":"vrijemePolaskaVoznjaEditor1.dataValue","targetProperty":"datumPolaskaPar"}, {}],
				wire3: ["wm.Wire", {"expression":undefined,"source":"mjestoDolaskaVoznjaEditor1.dataValue","targetProperty":"mjestoDolaskaPar"}, {}],
				wire5: ["wm.Wire", {"expression":undefined,"source":"opisVoznjaEditor1.dataValue","targetProperty":"opisVoznja"}, {}],
				wire6: ["wm.Wire", {"expression":undefined,"source":"oznakaVoznjaEditor1.dataValue","targetProperty":"oznakaVoznja"}, {}],
				wire7: ["wm.Wire", {"expression":undefined,"source":"voznjaLiveForm1.dataOutput.vozac.idvozac","targetProperty":"vozacPar"}, {}],
				wire4: ["wm.Wire", {"expression":undefined,"source":"mjestoPolaskaVoznjaEditor1.dataValue","targetProperty":"mjestoPolaskaPar"}, {}],
				wire8: ["wm.Wire", {"expression":undefined,"source":"voznjaLiveForm1.dataOutput.narucitelj.idnarucitelj","targetProperty":"naruciteljPar"}, {}],
				wire9: ["wm.Wire", {"expression":undefined,"source":"ugovorenaCijenaEditor.dataValue","targetProperty":"ugovorenaCijena"}, {}]
			}]
		}]
	}],
	serviceVarGetVoznjeTablica: ["wm.ServiceVariable", {"operation":"getVoznjeBusevi","service":"glavni"}, {"onSuccess":"serviceVarGetVoznjeTablicaSuccess"}, {
		input: ["wm.ServiceInput", {"type":"getVoznjeBuseviInputs"}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire: ["wm.Wire", {"expression":undefined,"source":"app.varDatumPocetkaPerioda.dataValue","targetProperty":"datumPocetka"}, {}],
				wire1: ["wm.Wire", {"expression":undefined,"source":"app.varListaOdabranihBusevaId.dataValue","targetProperty":"listaOdabranihBuseva"}, {}],
				wire2: ["wm.Wire", {"expression":undefined,"source":"app.varPeriodOdabrani.dataValue","targetProperty":"periodPar"}, {}]
			}]
		}]
	}],
	serviceVarGetVoznjeGarBroj: ["wm.ServiceVariable", {"operation":"getVoznjeBuseviGarazniBroj","service":"glavni"}, {"onSuccess":"serviceVarGetVoznjeGarBrojSuccess"}, {
		input: ["wm.ServiceInput", {"type":"getVoznjeBuseviGarazniBrojInputs"}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire: ["wm.Wire", {"expression":undefined,"source":"app.varDatumPocetkaPerioda.dataValue","targetProperty":"datumPocetka"}, {}],
				wire1: ["wm.Wire", {"expression":undefined,"source":"app.varListaOdabranihBusevaId.dataValue","targetProperty":"listaOdabranihBuseva"}, {}],
				wire2: ["wm.Wire", {"expression":undefined,"source":"app.varPeriodOdabrani.dataValue","targetProperty":"periodPar"}, {}],
				wire3: ["wm.Wire", {"expression":undefined,"source":"txtGarazniBrojFilter.dataValue","targetProperty":"garazniBroj"}, {}]
			}]
		}]
	}],
	serviceVarGetVoznjeDatPolaska: ["wm.ServiceVariable", {"operation":"getVoznjeBuseviDatumPolaska","service":"glavni"}, {"onSuccess":"serviceVarGetVoznjeDatPolaskaSuccess"}, {
		input: ["wm.ServiceInput", {"type":"getVoznjeBuseviDatumPolaskaInputs"}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire: ["wm.Wire", {"expression":undefined,"source":"app.varDatumPocetkaPerioda.dataValue","targetProperty":"datumPocetka"}, {}],
				wire1: ["wm.Wire", {"expression":undefined,"source":"app.varListaOdabranihBusevaId.dataValue","targetProperty":"listaOdabranihBuseva"}, {}],
				wire2: ["wm.Wire", {"expression":undefined,"source":"app.varPeriodOdabrani.dataValue","targetProperty":"periodPar"}, {}],
				wire3: ["wm.Wire", {"expression":undefined,"source":"dateDatumPolaskaFilter.dataValue","targetProperty":"datumPolaska"}, {}]
			}]
		}]
	}],
	serviceVarGetVoznjeTablicaVozac: ["wm.ServiceVariable", {"operation":"getVoznjeVozac","service":"glavni"}, {"onSuccess":"serviceVarGetVoznjeTablicaVozacSuccess"}, {
		input: ["wm.ServiceInput", {"type":"getVoznjeVozacInputs"}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire: ["wm.Wire", {"expression":undefined,"source":"app.varDatumPocetkaPerioda.dataValue","targetProperty":"datumPocetka"}, {}],
				wire1: ["wm.Wire", {"expression":undefined,"source":"app.varListaOdabranihVozacaID.dataValue","targetProperty":"listaOdabranihVozaca"}, {}],
				wire2: ["wm.Wire", {"expression":undefined,"source":"app.varPeriodOdabrani.dataValue","targetProperty":"periodPar"}, {}]
			}]
		}]
	}],
	serviceVariable1: ["wm.ServiceVariable", {"operation":"logout","service":"securityService"}, {"onSuccess":"navigationCallLogout"}, {
		input: ["wm.ServiceInput", {"type":"logoutInputs"}, {}]
	}],
	navigationCallLogout: ["wm.NavigationCall", {"operation":"gotoPage"}, {}, {
		input: ["wm.ServiceInput", {"type":"gotoPageInputs"}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire: ["wm.Wire", {"expression":"\"Login\"","targetProperty":"pageName"}, {}]
			}]
		}]
	}],
	navigationCallPrint: ["wm.NavigationCall", {"operation":"gotoPage"}, {}, {
		input: ["wm.ServiceInput", {"type":"gotoPageInputs"}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire: ["wm.Wire", {"expression":"\"IzvjestajRaspVoznji\"","targetProperty":"pageName"}, {}]
			}]
		}]
	}],
	servicePrintIzvjestaj: ["wm.ServiceVariable", {"operation":"getReport","service":"Izvjestaji"}, {"onResult":"servicePrintIzvjestajResult"}, {
		input: ["wm.ServiceInput", {"type":"getReportInputs"}, {}]
	}],
	liveVarVoznja_Admin: ["wm.LiveVariable", {"liveSource":"app.voznjaAdminLiveView"}, {}, {
		binding: ["wm.Binding", {}, {}, {
			wire: ["wm.Wire", {"expression":"if(${voznjaDojoGrid.isRowSelected}){\n\t${voznjaDojoGrid.selectedItem.idvoznja}\n}","source":false,"targetProperty":"filter.idvoznja"}, {}]
		}]
	}],
	naruciteljLiveVariable1: ["wm.LiveVariable", {"ignoreCase":true,"liveSource":"app.NaruciteljliveView"}, {}],
	serviceVarGetVoznjeForma: ["wm.ServiceVariable", {"operation":"getVoznjaForm","service":"glavni"}, {}, {
		input: ["wm.ServiceInput", {"type":"getVoznjaFormInputs"}, {}]
	}],
	serviceVarGetVoznjeOtvaranje: ["wm.ServiceVariable", {"autoUpdate":true,"operation":"getVoznjaForm","service":"glavni"}, {}, {
		input: ["wm.ServiceInput", {"type":"getVoznjaFormInputs"}, {}]
	}],
	liveVarOtvaranjeVoznje: ["wm.LiveVariable", {"liveSource":"app.voznjaAdminLiveView"}, {}, {
		binding: ["wm.Binding", {}, {}, {
			wire: ["wm.Wire", {"expression":undefined,"source":"voznjaDojoGrid1.selectedItem.idvoznja","targetProperty":"filter.idvoznja"}, {}]
		}]
	}],
	serviceVarInsertVoznjaOtvaranje: ["wm.ServiceVariable", {"operation":"dodavanjeVoznje","service":"glavni"}, {"onSuccess":"serviceVarInsertVoznjaOtvaranjeSuccess"}, {
		input: ["wm.ServiceInput", {"type":"dodavanjeVoznjeInputs"}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire: ["wm.Wire", {"expression":undefined,"source":"busLookup2.selectedItem.idBus","targetProperty":"busPar"}, {}],
				wire1: ["wm.Wire", {"expression":undefined,"source":"vrijemeDolaskaVoznjaEditor2.dataValue","targetProperty":"datumDolaskaPar"}, {}],
				wire2: ["wm.Wire", {"expression":undefined,"source":"vrijemePolaskaVoznjaEditor2.dataValue","targetProperty":"datumPolaskaPar"}, {}],
				wire3: ["wm.Wire", {"expression":undefined,"source":"mjestoDolaskaVoznjaEditor2.dataValue","targetProperty":"mjestoDolaskaPar"}, {}],
				wire4: ["wm.Wire", {"expression":undefined,"source":"mjestoPolaskaVoznjaEditor2.dataValue","targetProperty":"mjestoPolaskaPar"}, {}],
				wire5: ["wm.Wire", {"expression":undefined,"source":"naruciteljLookup2.selectedItem.idnarucitelj","targetProperty":"naruciteljPar"}, {}],
				wire6: ["wm.Wire", {"expression":undefined,"source":"opisVoznjaEditor2.dataValue","targetProperty":"opisVoznja"}, {}],
				wire7: ["wm.Wire", {"expression":undefined,"source":"oznakaVoznjaEditor2.dataValue","targetProperty":"oznakaVoznja"}, {}],
				wire8: ["wm.Wire", {"expression":undefined,"source":"ugovorenaCijenaEditor1.dataValue","targetProperty":"ugovorenaCijena"}, {}],
				wire9: ["wm.Wire", {"expression":undefined,"source":"vozacLookup2.selectedItem.idvozac","targetProperty":"vozacPar"}, {}]
			}]
		}]
	}],
	serviceVarGetReportBus: ["wm.ServiceVariable", {"operation":"getBusReport","service":"Izvjestaji"}, {"onResult":"servicePrintIzvjestajResult"}, {
		input: ["wm.ServiceInput", {"type":"getBusReportInputs"}, {}]
	}],
	serviceVarGetReportVozaci: ["wm.ServiceVariable", {"operation":"getVozaciReport","service":"Izvjestaji"}, {"onResult":"servicePrintIzvjestajResult"}, {
		input: ["wm.ServiceInput", {"type":"getVozaciReportInputs"}, {}]
	}],
	serviceVarGetReportVoznje: ["wm.ServiceVariable", {"operation":"getVoznjeReport","service":"Izvjestaji"}, {"onResult":"servicePrintIzvjestajResult"}, {
		input: ["wm.ServiceInput", {"type":"getVoznjeReportInputs"}, {}]
	}],
	serviceVarGetReportNarucitelji: ["wm.ServiceVariable", {"operation":"getNaruciteljiReport","service":"Izvjestaji"}, {"onResult":"servicePrintIzvjestajResult"}, {
		input: ["wm.ServiceInput", {"type":"getNaruciteljiReportInputs"}, {}]
	}],
	serviceVarGetRole: ["wm.ServiceVariable", {"operation":"getUserRoles","service":"securityService"}, {"onSuccess":"serviceVarGetRoleSuccess"}, {
		input: ["wm.ServiceInput", {"type":"getUserRolesInputs"}, {}]
	}],
	userLiveVariable1: ["wm.LiveVariable", {"liveSource":"app.userLiveView1"}, {}],
	roleLiveVariable1: ["wm.LiveVariable", {"liveSource":"app.roleLiveView1"}, {}],
	designableDialogAdminVozila: ["wm.DesignableDialog", {"buttonBarId":"buttonBar","containerWidgetId":"containerWidgetVozila","height":"670px","modal":false,"title":"Administracija vozila","width":"858px"}, {"onShow":"designableDialogAdminVozilaShow"}, {
		containerWidgetVozila: ["wm.Container", {"_classes":{"domNode":["wmdialogcontainer","MainContent"]},"autoScroll":true,"height":"100%","horizontalAlign":"left","margin":"0","padding":"5","verticalAlign":"top","width":"858px"}, {}, {
			panelsearchBUS: ["wm.Panel", {"height":"24px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
				searchBus: ["wm.Text", {"caption":"Garažni broj:","captionAlign":"left","changeOnKey":true,"dataValue":undefined,"displayValue":"","resetButton":true,"width":"250px"}, {"onchange":"searchBusChange"}],
				panel14: ["wm.Panel", {"height":"100%","horizontalAlign":"right","verticalAlign":"top","width":"100%"}, {}, {
					btnReportVozila: ["wm.Picture", {"_classes":{"domNode":["wm_Mouse_pointer"]},"border":"0","height":"23px","hint":"Ispis izvještaja","source":"resources/images/buttons/PDF.png","width":"51px"}, {"onclick":"btnReportVozilaClick"}]
				}]
			}],
			busLivePanel1: ["wm.LivePanel", {"horizontalAlign":"left","verticalAlign":"top","width":"840px"}, {}, {
				busGridPanel: ["wm.FancyPanel", {"height":"276px","title":"Popis svih vozila"}, {}, {
					busDojoGrid: ["wm.DojoGrid", {"_classes":{"domNode":["omgDataGrid"]},"columns":[{"show":false,"id":"idBus","title":"ID Vozila","width":"47px","displayType":"Number","noDelete":true,"align":"center","formatFunc":""},{"show":true,"id":"garazniBrojBus","title":"Garažni broj","width":"60px","displayType":"Text","noDelete":true,"align":"center","formatFunc":""},{"show":true,"id":"markaBus","title":"Marka","width":"100%","displayType":"Text","noDelete":true,"align":"center","formatFunc":""},{"show":true,"id":"modelBus","title":"Model","width":"100%","displayType":"Text","noDelete":true,"align":"center","formatFunc":""},{"show":true,"id":"brojSjedalaBus","title":"Broj sjedala","width":"100%","displayType":"Number","noDelete":true,"align":"center","formatFunc":""},{"show":true,"id":"datumKupnjeBus","title":"Datum kupnje","width":"100%","displayType":"Date","noDelete":true,"align":"center","formatFunc":"busDojoGridDatumKupnjeBusFormat"},{"show":true,"id":"datumZadnjeRegistracijeBus","title":"Datum registracije","width":"100%","displayType":"Date","noDelete":true,"align":"center","formatFunc":"busDojoGridDatumZadnjeRegistracijeBusFormat"},{"show":true,"id":"datumZadnjegServisaBus","title":"Datum zadnjeg servisa","width":"100%","displayType":"Date","noDelete":true,"align":"center","formatFunc":"busDojoGridDatumZadnjegServisaBusFormat"},{"show":true,"id":"napomenaBus","title":"Napomena","width":"100%","displayType":"Text","noDelete":true,"align":"center","formatFunc":""}],"height":"248px","imageList":"","localizationStructure":{},"margin":"4"}, {}, {
						binding: ["wm.Binding", {}, {}, {
							wire: ["wm.Wire", {"expression":undefined,"source":"busLiveVariable1","targetProperty":"dataSet"}, {}]
						}]
					}]
				}],
				busDetailsPanel: ["wm.FancyPanel", {"title":"Podaci za odabrano vozilo"}, {}, {
					busLiveForm1: ["wm.LiveForm", {"confirmDelete":"Jeste li sigurni da želite izbrisati podatke?","fitToContentHeight":true,"height":"240px","horizontalAlign":"left","margin":"0,40,0,40","readonly":true,"verticalAlign":"top"}, {"onSuccess":"busLiveVariable1"}, {
						binding: ["wm.Binding", {}, {}, {
							wire: ["wm.Wire", {"expression":undefined,"source":"busDojoGrid.selectedItem","targetProperty":"dataSet"}, {}]
						}],
						garazniBrojBusEditor1: ["wm.Text", {"caption":"Garažni broj:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"garazniBrojBus","height":"26px","readonly":true,"width":"100%"}, {}],
						markaBusEditor1: ["wm.Text", {"caption":"Marka:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"markaBus","height":"26px","readonly":true,"required":true,"width":"100%"}, {}],
						modelBusEditor1: ["wm.Text", {"caption":"Model:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"modelBus","height":"26px","readonly":true,"width":"100%"}, {}],
						brojSjedalaBusEditor1: ["wm.Number", {"caption":"Broj sjedala:","captionSize":"200px","formField":"brojSjedalaBus","height":"26px","invalidMessage":"Numeričko polje.","readonly":true,"width":"100%"}, {}],
						datumKupnjeBusEditor1: ["wm.DateTime", {"caption":"Datum kupnje:","captionSize":"200px","dateMode":"Date","emptyValue":"emptyString","formField":"datumKupnjeBus","formatter":"datumKupnjeBusEditor1ReadOnlyNodeFormat","height":"26px","readonly":true,"required":true,"width":"100%"}, {}],
						datumZadnjeRegistracijeBusEditor1: ["wm.DateTime", {"caption":"Datum zadnje registracije:","captionSize":"200px","dateMode":"Date","emptyValue":"emptyString","formField":"datumZadnjeRegistracijeBus","formatter":"datumZadnjeRegistracijeBusEditor1ReadOnlyNodeFormat","height":"26px","readonly":true,"required":true,"width":"100%"}, {}],
						datumZadnjegServisaBusEditor1: ["wm.DateTime", {"caption":"Datum zadnjeg servisa:","captionSize":"200px","dateMode":"Date","emptyValue":"emptyString","formField":"datumZadnjegServisaBus","formatter":"datumZadnjegServisaBusEditor1ReadOnlyNodeFormat","height":"26px","readonly":true,"width":"100%"}, {}],
						napomenaBusEditor1: ["wm.Text", {"caption":"Napomena:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"napomenaBus","height":"26px","readonly":true,"width":"100%"}, {}],
						busLiveForm1EditPanel: ["wm.EditPanel", {"height":"32px","isCustomized":true,"liveForm":"busLiveForm1","lock":false,"operationPanel":"operationPanel1","savePanel":"savePanel1"}, {}, {
							savePanel1: ["wm.Panel", {"height":"100%","horizontalAlign":"right","layoutKind":"left-to-right","showing":false,"verticalAlign":"top","width":"100%"}, {}, {
								saveButton1: ["wm.Button", {"caption":"Spremi","margin":"4"}, {"onclick":"busLiveForm1EditPanel.saveData"}, {
									binding: ["wm.Binding", {}, {}, {
										wire: ["wm.Wire", {"expression":undefined,"source":"busLiveForm1EditPanel.formInvalid","targetProperty":"disabled"}, {}]
									}]
								}],
								cancelButton1: ["wm.Button", {"caption":"Prekini","margin":"4"}, {"onclick":"busLiveForm1EditPanel.cancelEdit"}]
							}],
							operationPanel1: ["wm.Panel", {"height":"100%","horizontalAlign":"right","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
								newButton1: ["wm.Button", {"caption":"Dodaj","margin":"4"}, {"onclick":"busLiveForm1EditPanel.beginDataInsert"}],
								updateButton1: ["wm.Button", {"caption":"Ažuriraj","margin":"4"}, {"onclick":"busLiveForm1EditPanel.beginDataUpdate"}, {
									binding: ["wm.Binding", {}, {}, {
										wire: ["wm.Wire", {"expression":undefined,"source":"busLiveForm1EditPanel.formUneditable","targetProperty":"disabled"}, {}]
									}]
								}],
								deleteButton1: ["wm.Button", {"caption":"Obriši","margin":"4"}, {"onclick":"busLiveForm1EditPanel.deleteData"}, {
									binding: ["wm.Binding", {}, {}, {
										wire: ["wm.Wire", {"expression":undefined,"source":"busLiveForm1EditPanel.formUneditable","targetProperty":"disabled"}, {}]
									}]
								}]
							}]
						}]
					}]
				}]
			}]
		}],
		buttonBar: ["wm.Panel", {"_classes":{"domNode":["dialogfooter"]},"height":"40px","horizontalAlign":"right","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}]
	}],
	designableDialogAdminVoznja: ["wm.DesignableDialog", {"buttonBarId":"buttonBar","containerWidgetId":"containerWidgetVoznje","height":"680px","modal":false,"title":"Vožnje","width":"850px"}, {"onShow":"designableDialogAdminVoznjaShow"}, {
		containerWidgetVoznje: ["wm.Container", {"_classes":{"domNode":["wmdialogcontainer","MainContent"]},"autoScroll":true,"height":"100%","horizontalAlign":"left","margin":"0","padding":"5","verticalAlign":"top","width":"100%"}, {}, {
			panel13: ["wm.Panel", {"height":"77px","horizontalAlign":"right","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
				panel12: ["wm.Panel", {"height":"76px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
					panel16: ["wm.Panel", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"285px"}, {}, {
						textMjestoPolaska: ["wm.Text", {"caption":"Mjesto polaska:","captionSize":"105px","changeOnKey":true,"dataValue":undefined,"displayValue":"","resetButton":true,"width":"280px"}, {"onchange":"textMjestoPolaskaChange"}],
						textMjestoDolaskaFilter: ["wm.Text", {"caption":"Mjesto dolaska: ","captionSize":"105px","changeOnKey":true,"dataValue":undefined,"displayValue":"","resetButton":true,"width":"280px"}, {"onchange":"textMjestoDolaskaFilterChange"}],
						textVozacFilter: ["wm.Text", {"caption":"Prezime vozača:","captionSize":"105px","changeOnKey":true,"dataValue":undefined,"displayValue":"","resetButton":true,"width":"280px"}, {"onchange":"textVozacFilterChange"}]
					}],
					panel17: ["wm.Panel", {"height":"73px","horizontalAlign":"left","verticalAlign":"top","width":"280px"}, {}, {
						textVoziloFilter: ["wm.Text", {"caption":"Garažni broj:","captionSize":"120px","changeOnKey":true,"dataValue":undefined,"displayValue":"","resetButton":true,"width":"275px"}, {"onchange":"textVoziloFilterChange"}],
						panel9: ["wm.Panel", {"height":"24px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"bottom","width":"100%"}, {}, {
							dateVrijemePolaskaFilter: ["wm.Date", {"caption":"Vrijeme polaska:","captionAlign":"left","captionSize":"120px","dataValue":"","displayValue":"","emptyValue":"emptyString","width":"250px"}, {"onchange":"dateVrijemePolaskaFilterChange"}],
							ResetVrijemePolaskaVoznje: ["wm.Picture", {"_classes":{"domNode":["wm_Mouse_pointer"]},"border":"0","height":"20px","source":"resources/images/Reset.png","width":"25px"}, {"onclick":"ResetVrijemePolaskaVoznjeClick"}]
						}],
						panel24: ["wm.Panel", {"height":"24px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"middle","width":"100%"}, {}, {
							dateVrijemeDolaskaFilter: ["wm.Date", {"caption":"Vrijeme dolaska:","captionAlign":"left","captionSize":"120px","dataValue":undefined,"displayValue":"","width":"250px"}, {"onchange":"dateVrijemeDolaskaFilterChange"}],
							ResetVrijemeDolaskaVoznje: ["wm.Picture", {"_classes":{"domNode":["wm_Mouse_pointer"]},"border":"0","height":"20px","source":"resources/images/Reset.png","width":"25px"}, {"onclick":"ResetVrijemeDolaskaVoznjeClick"}]
						}]
					}],
					panel18: ["wm.Panel", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"100%"}, {}, {
						textFilterOznaka: ["wm.Text", {"caption":"Oznaka:","captionSize":"70px","changeOnKey":true,"dataValue":undefined,"displayValue":"","resetButton":true,"width":"220px"}, {"onchange":"textFilterOznakaChange"}]
					}]
				}],
				btnReportVoznje: ["wm.Picture", {"_classes":{"domNode":["wm_Mouse_pointer"]},"border":"0","height":"30px","hint":"Ispis izvještaja","source":"resources/images/buttons/PDF.png","width":"30px"}, {"onclick":"btnReportVoznjeClick"}]
			}],
			voznjaLivePanel1: ["wm.LivePanel", {"horizontalAlign":"left","verticalAlign":"top"}, {}, {
				voznjaGridPanel: ["wm.FancyPanel", {"height":"210px","title":"Popis svih vožnji"}, {}, {
					voznjaDojoGrid: ["wm.DojoGrid", {"_classes":{"domNode":["omgDataGrid"]},"columns":[{"show":false,"id":"idvoznja","title":"ID","width":"80px","displayType":"Number","noDelete":true,"align":"right","formatFunc":""},{"show":true,"id":"oznakaVoznja","title":"Oznaka","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"opisVoznja","title":"Opis","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"mjestoPolaskaVoznja","title":"Mjesto polaska","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"mjestoDolaskaVoznja","title":"Mjesto dolaska","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"vrijemePolaskaVoznja","title":"Vrijeme polaska","width":"80px","displayType":"Date","noDelete":true,"align":"left","formatFunc":"voznjaDojoGridVrijemePolaskaVoznjaFormat"},{"show":true,"id":"vrijemeDolaskaVoznja","title":"Vrijeme dolaska","width":"80px","displayType":"Date","noDelete":true,"align":"left","formatFunc":"voznjaDojoGridVrijemeDolaskaVoznjaFormat"},{"show":false,"id":"ugovorenaCijenaVoznja","title":"UgovorenaCijenaVoznja","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"id":"customField1","isCustomField":true,"expression":"${zatvorenavoznja.naplata}","show":true,"width":"100%","title":"Status","formatFunc":"voznjaDojoGridCustomField1Format"}],"height":"180px","margin":"4"}, {"onSelectionChange":"voznjaDojoGridSelectionChange"}, {
						binding: ["wm.Binding", {}, {}, {
							wire: ["wm.Wire", {"expression":undefined,"source":"serviceVarGetVoznjeForma","targetProperty":"dataSet"}, {}]
						}]
					}]
				}],
				voznjaDetailsPanel: ["wm.FancyPanel", {"fitToContentHeight":true,"height":"317px","title":"Podaci za odabranu vožnju"}, {}, {
					voznjaLiveForm1: ["wm.LiveForm", {"confirmDelete":"Jeste li sigurni da želite izbrisati podatke?","fitToContentHeight":true,"height":"288px","horizontalAlign":"left","readonly":true,"verticalAlign":"top","width":"817px"}, {"onSuccess":"voznjaLiveForm1Success"}, {
						binding: ["wm.Binding", {}, {}, {
							wire: ["wm.Wire", {"expression":undefined,"source":"liveVarVoznja_Admin","targetProperty":"dataSet"}, {}]
						}],
						idvoznjaEditor1: ["wm.Number", {"caption":"Idvoznja","captionSize":"200px","emptyValue":"zero","formField":"idvoznja","height":"26px","readonly":true,"required":true,"showing":false,"width":"100%"}, {}],
						oznakaVoznjaEditor1: ["wm.Text", {"caption":"Oznaka vožnje:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"oznakaVoznja","height":"26px","readonly":true,"width":"100%"}, {}],
						mjestoPolaskaVoznjaEditor1: ["wm.Text", {"caption":"Mjesto polaska:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"mjestoPolaskaVoznja","height":"26px","invalidMessage":"Obavezno polje!","readonly":true,"required":true,"width":"100%"}, {}],
						mjestoDolaskaVoznjaEditor1: ["wm.Text", {"caption":"Mjesto dolaska:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"mjestoDolaskaVoznja","height":"26px","invalidMessage":"Obavezno polje!","readonly":true,"required":true,"width":"100%"}, {}],
						vrijemePolaskaVoznjaEditor1: ["wm.DateTime", {"caption":"Vrijeme polaska:","captionSize":"200px","emptyValue":"emptyString","formField":"vrijemePolaskaVoznja","formatter":"vrijemePolaskaVoznjaEditor1ReadOnlyNodeFormat","height":"26px","invalidMessage":"Obavezno polje!","readonly":true,"required":true,"width":"100%"}, {"onchange":"vrijemePolaskaVoznjaEditor1Change"}],
						vrijemeDolaskaVoznjaEditor1: ["wm.DateTime", {"caption":"Vrijeme dolaska:","captionSize":"200px","emptyValue":"emptyString","formField":"vrijemeDolaskaVoznja","formatter":"vrijemeDolaskaVoznjaEditor1ReadOnlyNodeFormat","height":"26px","invalidMessage":"Obavezno polje!","readonly":true,"required":true,"width":"100%"}, {}],
						opisVoznjaEditor1: ["wm.Text", {"caption":"Napomena:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"opisVoznja","height":"26px","readonly":true,"width":"100%"}, {}],
						ugovorenaCijenaEditor: ["wm.Text", {"caption":"Ugovorena cijena(kn):","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"ugovorenaCijenaVoznja","height":"26px","invalidMessage":"Format cijene.","readonly":true,"regExp":"^[0-9]*\\.[0-9][0-9]$","width":"100%"}, {}, {
							format: ["wm.DataFormatter", {}, {}]
						}],
						vozacLookup1: ["wm.Lookup", {"caption":"Vozač:","captionSize":"200px","displayExpression":"var text = \"\";\nif ( (${imeVozac} == null) || (${prezimeVozac} == null) ) {\ntext;\n} else {\n${imeVozac}+\" \"+${prezimeVozac};\n}","emptyValue":"emptyString","formField":"vozac","readonly":true,"required":true,"width":"100%"}, {}],
						busLookup1: ["wm.Lookup", {"caption":"Vozilo:","captionSize":"200px","displayExpression":"var text = \"\";\nif ( (${garazniBrojBus} == null) || (${modelBus} == null) || (${markaBus} == null) ) {\ntext;\n} else {\n${garazniBrojBus}+\" \"+${modelBus}+\" \"+${markaBus};\n}","displayField":"napomenaBus","emptyValue":"emptyString","formField":"bus","readonly":true,"required":true,"width":"100%"}, {}],
						naruciteljLookup1: ["wm.Lookup", {"caption":"Naručitelj:","captionSize":"200px","displayExpression":"${nazivNarucitelj}","displayField":"nazivNarucitelj","formField":"narucitelj","height":"26px","readonly":true,"width":"100%"}, {}],
						voznjaLiveForm1EditPanel: ["wm.EditPanel", {"height":"32px","isCustomized":true,"liveForm":"voznjaLiveForm1","lock":false,"operationPanel":"operationPanel3","savePanel":"savePanel3"}, {}, {
							savePanel3: ["wm.Panel", {"height":"100%","horizontalAlign":"right","layoutKind":"left-to-right","showing":false,"verticalAlign":"top","width":"100%"}, {}, {
								saveButton3: ["wm.Button", {"caption":"Spremi","margin":"4"}, {"onclick":"saveButton3Click"}, {
									binding: ["wm.Binding", {}, {}, {
										wire: ["wm.Wire", {"expression":undefined,"source":"voznjaLiveForm1EditPanel.formInvalid","targetProperty":"disabled"}, {}]
									}]
								}],
								cancelButton3: ["wm.Button", {"caption":"Odustani","margin":"4"}, {"onclick":"cancelButton3Click"}]
							}],
							operationPanel3: ["wm.Panel", {"height":"100%","horizontalAlign":"right","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
								newButton3: ["wm.Button", {"caption":"Dodaj","margin":"4"}, {"onclick":"voznjaLiveForm1EditPanel.beginDataInsert"}],
								updateButton3: ["wm.Button", {"caption":"Ažuriraj","margin":"4"}, {"onclick":"voznjaLiveForm1EditPanel.beginDataUpdate","onclick1":"updateButton3Click1"}, {
									binding: ["wm.Binding", {}, {}, {
										wire: ["wm.Wire", {"expression":undefined,"source":"voznjaLiveForm1EditPanel.formUneditable","targetProperty":"disabled"}, {}]
									}]
								}],
								deleteButton3: ["wm.Button", {"caption":"Briši","margin":"4"}, {"onclick":"deleteButton3Click"}, {
									binding: ["wm.Binding", {}, {}, {
										wire: ["wm.Wire", {"expression":undefined,"source":"voznjaLiveForm1EditPanel.formUneditable","targetProperty":"disabled"}, {}]
									}]
								}]
							}]
						}]
					}]
				}]
			}]
		}],
		buttonBar2: ["wm.Panel", {"_classes":{"domNode":["dialogfooter"]},"border":"1,0,0,0","height":"32px","horizontalAlign":"right","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}]
	}],
	loadingUlaz: ["wm.LoadingDialog", {"_classes":{"domNode":["wm_BackgroundColor_Blue"]}}, {}, {
		binding: ["wm.Binding", {}, {}, {
			wire: ["wm.Wire", {"expression":undefined,"source":"app.varSecurity","targetProperty":"serviceVariableToTrack"}, {}],
			wire1: ["wm.Wire", {"expression":undefined,"source":"RasBUS","targetProperty":"widgetToCover"}, {}]
		}]
	}],
	designableDialogAdminVozac: ["wm.DesignableDialog", {"buttonBarId":"buttonBar","containerWidgetId":"containerWidgetVozac","height":"600px","modal":false,"title":"Administracija vozača","width":"870px"}, {"onShow":"designableDialogAdminVozacShow"}, {
		containerWidgetVozac: ["wm.Container", {"_classes":{"domNode":["wmdialogcontainer","MainContent"]},"autoScroll":true,"height":"100%","horizontalAlign":"left","margin":"0","padding":"5","verticalAlign":"top","width":"859px"}, {}, {
			panelsearchVOZAC: ["wm.Panel", {"height":"25px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
				searchVozac: ["wm.Text", {"caption":"Prezime:","captionAlign":"left","captionSize":"80px","changeOnKey":true,"dataValue":undefined,"displayValue":"","resetButton":true,"width":"302px"}, {"onchange":"searchVozacChange"}],
				panel11: ["wm.Panel", {"height":"100%","horizontalAlign":"right","verticalAlign":"top","width":"100%"}, {}, {
					btnReportVozaci: ["wm.Picture", {"_classes":{"domNode":["wm_Mouse_pointer"]},"border":"0","height":"24px","hint":"Ispis izvještaja","source":"resources/images/buttons/PDF.png","width":"55px"}, {"onclick":"btnReportVozaciClick"}]
				}]
			}],
			vozacLivePanel1: ["wm.LivePanel", {"horizontalAlign":"left","verticalAlign":"top","width":"849px"}, {}, {
				vozacGridPanel: ["wm.FancyPanel", {"height":"262px","title":"Popis svih vozača"}, {}, {
					vozacDojoGrid: ["wm.DojoGrid", {"_classes":{"domNode":["omgDataGrid"]},"caseSensitiveSort":false,"columns":[{"show":false,"id":"idvozac","title":"ID","width":"80px","displayType":"Number","noDelete":true,"align":"center","formatFunc":""},{"show":true,"id":"imeVozac","title":"Ime","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"prezimeVozac","title":"Prezime","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"datumRodjenjaVozac","title":"Datum rođenja","width":"115px","displayType":"Date","noDelete":true,"align":"left","formatFunc":"vozacDojoGridDatumRodjenjaVozacFormat"},{"show":true,"id":"datumVozackaVozac","title":"Datum isteka vozačke","width":"80px","displayType":"Date","noDelete":true,"align":"left","formatFunc":"vozacDojoGridDatumVozackaVozacFormat"},{"show":true,"id":"datumPutovnicaVozac","title":"Datum isteka putovnice","width":"80px","displayType":"Date","noDelete":true,"align":"left","formatFunc":"vozacDojoGridDatumPutovnicaVozacFormat"},{"show":true,"id":"datumOsobnaVozac","title":"Datum isteka osobne","width":"80px","displayType":"Date","noDelete":true,"align":"left","formatFunc":"vozacDojoGridDatumOsobnaVozacFormat"}],"height":"100%","margin":"4"}, {}, {
						binding: ["wm.Binding", {}, {}, {
							wire: ["wm.Wire", {"expression":undefined,"source":"vozacLiveVariable1","targetProperty":"dataSet"}, {}]
						}]
					}]
				}],
				vozacDetailsPanel: ["wm.FancyPanel", {"fitToContentHeight":true,"height":"217px","title":"Podaci za odabranog vozača"}, {}, {
					vozacLiveForm1: ["wm.LiveForm", {"confirmDelete":"jeste li sigurni da želite izbrisati podatke?","fitToContentHeight":true,"height":"188px","horizontalAlign":"left","margin":"0,40,0,40","readonly":true,"verticalAlign":"top"}, {"onSuccess":"vozacLiveVariable1"}, {
						binding: ["wm.Binding", {}, {}, {
							wire: ["wm.Wire", {"expression":undefined,"source":"vozacDojoGrid.selectedItem","targetProperty":"dataSet"}, {}]
						}],
						imeVozacEditor1: ["wm.Text", {"caption":"Ime:","captionSize":"220px","dataValue":"","emptyValue":"emptyString","formField":"imeVozac","height":"26px","readonly":true,"required":true,"width":"100%"}, {}],
						prezimeVozacEditor1: ["wm.Text", {"caption":"Prezime:","captionSize":"220px","dataValue":"","emptyValue":"emptyString","formField":"prezimeVozac","height":"26px","readonly":true,"required":true,"width":"100%"}, {}],
						datumRodjenjaVozacEditor1: ["wm.DateTime", {"caption":"Datum rođenja:","captionSize":"220px","dateMode":"Date","emptyValue":"emptyString","formField":"datumRodjenjaVozac","formatter":"datumRodjenjaVozacEditor1ReadOnlyNodeFormat","height":"26px","readonly":true,"required":true,"useLocalTime":true,"width":"100%"}, {}],
						datumVozackaVozacEditor1: ["wm.DateTime", {"caption":"Datum isteka vozačke:","captionSize":"220px","dateMode":"Date","emptyValue":"emptyString","formField":"datumVozackaVozac","formatter":"datumVozackaVozacEditor1ReadOnlyNodeFormat","height":"26px","readonly":true,"required":true,"useLocalTime":true,"width":"100%"}, {}],
						datumPutovnicaVozacEditor1: ["wm.DateTime", {"caption":"Datum isteka putovnice:","captionSize":"220px","dateMode":"Date","emptyValue":"emptyString","formField":"datumPutovnicaVozac","formatter":"datumPutovnicaVozacEditor1ReadOnlyNodeFormat","height":"26px","readonly":true,"required":true,"useLocalTime":true,"width":"100%"}, {}],
						datumOsobnaVozacEditor1: ["wm.DateTime", {"caption":"Datum isteka osobne iskaznice:","captionSize":"220px","dateMode":"Date","emptyValue":"emptyString","formField":"datumOsobnaVozac","formatter":"datumOsobnaVozacEditor1ReadOnlyNodeFormat","height":"26px","readonly":true,"required":true,"useLocalTime":true,"width":"100%"}, {}],
						vozacLiveForm1EditPanel: ["wm.EditPanel", {"height":"32px","isCustomized":true,"liveForm":"vozacLiveForm1","lock":false,"operationPanel":"operationPanel2","savePanel":"savePanel2"}, {}, {
							savePanel2: ["wm.Panel", {"height":"100%","horizontalAlign":"right","layoutKind":"left-to-right","showing":false,"verticalAlign":"top","width":"100%"}, {}, {
								saveButton2: ["wm.Button", {"caption":"Spremi","margin":"4"}, {"onclick":"vozacLiveForm1EditPanel.saveData"}, {
									binding: ["wm.Binding", {}, {}, {
										wire: ["wm.Wire", {"expression":undefined,"source":"vozacLiveForm1EditPanel.formInvalid","targetProperty":"disabled"}, {}]
									}]
								}],
								cancelButton2: ["wm.Button", {"caption":"Prekini","margin":"4"}, {"onclick":"vozacLiveForm1EditPanel.cancelEdit"}]
							}],
							operationPanel2: ["wm.Panel", {"height":"100%","horizontalAlign":"right","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
								newButton2: ["wm.Button", {"caption":"Dodaj","margin":"4"}, {"onclick":"vozacLiveForm1EditPanel.beginDataInsert"}],
								updateButton2: ["wm.Button", {"caption":"Ažuriraj","margin":"4"}, {"onclick":"vozacLiveForm1EditPanel.beginDataUpdate"}, {
									binding: ["wm.Binding", {}, {}, {
										wire: ["wm.Wire", {"expression":undefined,"source":"vozacLiveForm1EditPanel.formUneditable","targetProperty":"disabled"}, {}]
									}]
								}],
								deleteButton2: ["wm.Button", {"caption":"Briši","margin":"4"}, {"onclick":"vozacLiveForm1EditPanel.deleteData"}, {
									binding: ["wm.Binding", {}, {}, {
										wire: ["wm.Wire", {"expression":undefined,"source":"vozacLiveForm1EditPanel.formUneditable","targetProperty":"disabled"}, {}]
									}]
								}]
							}]
						}]
					}]
				}]
			}]
		}],
		buttonBar1: ["wm.Panel", {"_classes":{"domNode":["dialogfooter"]},"border":"1,0,0,0","height":"32px","horizontalAlign":"right","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}]
	}],
	designableDialogAdminNarucitelj: ["wm.DesignableDialog", {"buttonBarId":"buttonBar","containerWidgetId":"containerWidget","height":"520px","modal":false,"title":"Popis naručitelja","width":"850px"}, {"onShow":"designableDialogAdminNaruciteljShow"}, {
		containerWidget: ["wm.Container", {"_classes":{"domNode":["wmdialogcontainer","MainContent"]},"autoScroll":true,"height":"100%","horizontalAlign":"left","margin":"0","padding":"5","verticalAlign":"top","width":"100%"}, {}, {
			panel5: ["wm.Panel", {"height":"30px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
				textNazivNaruciteljaFilter: ["wm.Text", {"caption":"Naziv naručitelja:","captionSize":"150px","changeOnKey":true,"dataValue":undefined,"displayValue":"","resetButton":true}, {"onchange":"textNazivNaruciteljaFilterChange"}],
				panel7: ["wm.Panel", {"height":"100%","horizontalAlign":"right","verticalAlign":"top","width":"100%"}, {}, {
					btnReportNarucitelji: ["wm.Picture", {"_classes":{"domNode":["wm_Mouse_pointer"]},"border":"0","height":"24px","hint":"Ispis izvještaja","source":"resources/images/buttons/PDF.png","width":"55px"}, {"onclick":"btnReportNaruciteljiClick"}]
				}]
			}],
			naruciteljLivePanel1: ["wm.LivePanel", {"horizontalAlign":"left","verticalAlign":"top"}, {}, {
				naruciteljGridPanel: ["wm.FancyPanel", {"minHeight":"180","title":"Naručitelj"}, {}, {
					naruciteljDojoGrid: ["wm.DojoGrid", {"_classes":{"domNode":["omgDataGrid"]},"columns":[{"show":false,"id":"idnarucitelj","title":"ID","width":"80px","displayType":"Number","noDelete":true,"align":"right","formatFunc":""},{"show":true,"id":"nazivNarucitelj","title":"Naziv","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"adresaNarucitelj","title":"Adresa ","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"odgovornaOsbaNarucitelj","title":"Odgovorna osoba","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""}],"height":"100%","margin":"4"}, {}, {
						binding: ["wm.Binding", {}, {}, {
							wire: ["wm.Wire", {"expression":undefined,"source":"naruciteljLiveVariable1","targetProperty":"dataSet"}, {}]
						}]
					}]
				}],
				naruciteljDetailsPanel: ["wm.FancyPanel", {"fitToContentHeight":true,"height":"165px","title":"Detalji"}, {}, {
					naruciteljLiveForm1: ["wm.LiveForm", {"confirmDelete":"Jeste li sigurni da želite izbrisati odabranog naručitelja?","fitToContentHeight":true,"height":"136px","horizontalAlign":"left","margin":"0,40,0,40","readonly":true,"verticalAlign":"top"}, {"onSuccess":"naruciteljLiveVariable1"}, {
						binding: ["wm.Binding", {}, {}, {
							wire: ["wm.Wire", {"expression":undefined,"source":"naruciteljDojoGrid.selectedItem","targetProperty":"dataSet"}, {}]
						}],
						idnaruciteljEditor1: ["wm.Number", {"caption":"Šifra naručitelja:","captionSize":"200px","emptyValue":"zero","formField":"idnarucitelj","height":"26px","readonly":true,"required":true,"width":"100%"}, {}],
						nazivNaruciteljEditor1: ["wm.Text", {"caption":"Naziv naručitelja:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"nazivNarucitelj","height":"26px","readonly":true,"required":true,"width":"100%"}, {}],
						adresaNaruciteljEditor1: ["wm.Text", {"caption":"Adresa naručitelja:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"adresaNarucitelj","height":"26px","readonly":true,"width":"100%"}, {}],
						odgovornaOsbaNaruciteljEditor1: ["wm.Text", {"caption":"Odgovorna osoba:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"odgovornaOsbaNarucitelj","height":"26px","readonly":true,"required":true,"width":"100%"}, {}],
						naruciteljLiveForm1EditPanel: ["wm.EditPanel", {"height":"32px","isCustomized":true,"liveForm":"naruciteljLiveForm1","lock":false,"operationPanel":"operationPanel4","savePanel":"savePanel4"}, {}, {
							savePanel4: ["wm.Panel", {"height":"100%","horizontalAlign":"right","layoutKind":"left-to-right","showing":false,"verticalAlign":"top","width":"100%"}, {}, {
								saveButton4: ["wm.Button", {"caption":"Spremi","margin":"4"}, {"onclick":"naruciteljLiveForm1EditPanel.saveData"}, {
									binding: ["wm.Binding", {}, {}, {
										wire: ["wm.Wire", {"expression":undefined,"source":"naruciteljLiveForm1EditPanel.formInvalid","targetProperty":"disabled"}, {}]
									}]
								}],
								cancelButton4: ["wm.Button", {"caption":"Prekini","margin":"4"}, {"onclick":"naruciteljLiveForm1EditPanel.cancelEdit"}]
							}],
							operationPanel4: ["wm.Panel", {"height":"100%","horizontalAlign":"right","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
								newButton4: ["wm.Button", {"caption":"Kreiraj","margin":"4"}, {"onclick":"naruciteljLiveForm1EditPanel.beginDataInsert"}],
								updateButton4: ["wm.Button", {"caption":"Ažuriraj","margin":"4"}, {"onclick":"naruciteljLiveForm1EditPanel.beginDataUpdate"}, {
									binding: ["wm.Binding", {}, {}, {
										wire: ["wm.Wire", {"expression":undefined,"source":"naruciteljLiveForm1EditPanel.formUneditable","targetProperty":"disabled"}, {}]
									}]
								}],
								deleteButton4: ["wm.Button", {"caption":"Briši","margin":"4"}, {"onclick":"naruciteljLiveForm1EditPanel.deleteData"}, {
									binding: ["wm.Binding", {}, {}, {
										wire: ["wm.Wire", {"expression":undefined,"source":"naruciteljLiveForm1EditPanel.formUneditable","targetProperty":"disabled"}, {}]
									}]
								}]
							}]
						}]
					}]
				}]
			}]
		}],
		buttonBar5: ["wm.Panel", {"_classes":{"domNode":["dialogfooter"]},"border":"1,0,0,0","borderColor":"#333333","height":"32px","horizontalAlign":"right","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}]
	}],
	designableDialogOtvaranjeVoznje: ["wm.DesignableDialog", {"buttonBarId":"buttonBar","containerWidgetId":"containerWidget2","height":"730px","modal":false,"title":"Otvaranje vožnje","width":"850px"}, {"onShow":"designableDialogOtvaranjeVoznjeShow"}, {
		containerWidget2: ["wm.Container", {"_classes":{"domNode":["wmdialogcontainer","MainContent"]},"autoScroll":true,"height":"100%","horizontalAlign":"left","margin":"0","padding":"5","verticalAlign":"top","width":"100%"}, {}, {
			panel19: ["wm.Panel", {"height":"75px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
				panel20: ["wm.Panel", {"height":"79px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
					panel21: ["wm.Panel", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"255px"}, {}, {
						textMjestoPolaska1: ["wm.Text", {"caption":"Mjesto polaska:","captionSize":"105px","changeOnKey":true,"dataValue":undefined,"displayValue":"","resetButton":true,"width":"250px"}, {"onchange":"textMjestoPolaska1Change"}],
						textMjestoDolaskaFilter1: ["wm.Text", {"caption":"Mjesto dolaska: ","captionSize":"105px","changeOnKey":true,"dataValue":undefined,"displayValue":"","resetButton":true,"width":"250px"}, {"onchange":"textMjestoDolaskaFilter1Change"}],
						textVozacFilter1: ["wm.Text", {"caption":"Prezime vozača:","captionSize":"105px","changeOnKey":true,"dataValue":undefined,"displayValue":"","resetButton":true,"width":"250px"}, {"onchange":"textVozacFilter1Change"}]
					}],
					panel22: ["wm.Panel", {"height":"75px","horizontalAlign":"left","verticalAlign":"top","width":"280px"}, {}, {
						textVoziloFilter1: ["wm.Text", {"caption":"Garažni broj vozila:","captionSize":"130px","changeOnKey":true,"dataValue":undefined,"displayValue":"","resetButton":true,"width":"275px"}, {"onchange":"textVoziloFilter1Change"}],
						panel25: ["wm.Panel", {"height":"24px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"bottom","width":"100%"}, {}, {
							dateVrijemePolaskaFilter1: ["wm.Date", {"caption":"Vrijeme polaska:","captionAlign":"left","captionSize":"120px","dataValue":"","displayValue":"","emptyValue":"emptyString","width":"250px"}, {"onchange":"dateVrijemePolaskaFilter1Change"}],
							ResetVrijemeDolaskaVoznjeOtvaranje: ["wm.Picture", {"_classes":{"domNode":["wm_Mouse_pointer"]},"border":"0","height":"20px","source":"resources/images/Reset.png","width":"25px"}, {"onclick":"ResetVrijemeDolaskaVoznjeOtvaranjeClick"}]
						}],
						panel26: ["wm.Panel", {"height":"24px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"bottom","width":"100%"}, {}, {
							dateVrijemeDolaskaFilter1: ["wm.Date", {"caption":"Vrijeme dolaska:","captionAlign":"left","captionSize":"120px","dataValue":undefined,"displayValue":"","width":"250px"}, {"onchange":"dateVrijemeDolaskaFilter1Change"}],
							ResetVrijemeDOlaskaVoznjeOtvaranje: ["wm.Picture", {"_classes":{"domNode":["wm_Mouse_pointer"]},"border":"0","height":"20px","source":"resources/images/Reset.png","width":"25px"}, {"onclick":"ResetVrijemeDOlaskaVoznjeOtvaranjeClick"}]
						}]
					}],
					panel23: ["wm.Panel", {"height":"75px","horizontalAlign":"left","verticalAlign":"top","width":"100%"}, {}, {
						textFilterOznakaOtvaranje: ["wm.Text", {"caption":"Oznaka:","captionSize":"60px","changeOnKey":true,"dataValue":undefined,"displayValue":"","resetButton":true,"width":"180px"}, {"onchange":"textFilterOznakaOtvaranjeChange"}]
					}]
				}]
			}],
			voznjaLivePanel2: ["wm.LivePanel", {"horizontalAlign":"left","verticalAlign":"top"}, {}, {
				voznjaGridPanel1: ["wm.FancyPanel", {"height":"210px","title":"Popis svih vožnji"}, {}, {
					voznjaDojoGrid1: ["wm.DojoGrid", {"_classes":{"domNode":["omgDataGrid"]},"columns":[{"show":false,"id":"idvoznja","title":"ID","width":"80px","displayType":"Number","noDelete":true,"align":"right","formatFunc":""},{"show":true,"id":"oznakaVoznja","title":"Oznaka","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"opisVoznja","title":"Opis","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"mjestoPolaskaVoznja","title":"Mjesto polaska","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"mjestoDolaskaVoznja","title":"Mjesto dolaska","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"vrijemePolaskaVoznja","title":"Vrijeme polaska","width":"80px","displayType":"Date","noDelete":true,"align":"left","formatFunc":"voznjaDojoGridVrijemePolaskaVoznjaFormat"},{"show":true,"id":"vrijemeDolaskaVoznja","title":"Vrijeme dolaska","width":"80px","displayType":"Date","noDelete":true,"align":"left","formatFunc":"voznjaDojoGridVrijemeDolaskaVoznjaFormat"},{"show":false,"id":"ugovorenaCijenaVoznja","title":"UgovorenaCijenaVoznja","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"id":"customField1","isCustomField":true,"expression":"${zatvorenavoznja.naplata}","show":true,"width":"100%","title":"Status","formatFunc":"voznjaDojoGridCustomField1Format"}],"height":"180px","margin":"4"}, {}, {
						binding: ["wm.Binding", {}, {}, {
							wire: ["wm.Wire", {"expression":undefined,"source":"serviceVarGetVoznjeOtvaranje","targetProperty":"dataSet"}, {}]
						}]
					}]
				}],
				voznjaDetailsPanel1: ["wm.FancyPanel", {"fitToContentHeight":true,"height":"317px","title":"Podaci za odabranu vožnju"}, {}, {
					voznjaLiveForm2: ["wm.LiveForm", {"confirmDelete":"Jeste li sigurni da želite izbrisati podatke?","fitToContentHeight":true,"height":"288px","horizontalAlign":"left","readonly":true,"verticalAlign":"top","width":"817px"}, {"onSuccess":"voznjaLiveForm2Success"}, {
						binding: ["wm.Binding", {}, {}, {
							wire: ["wm.Wire", {"expression":undefined,"source":"liveVarOtvaranjeVoznje","targetProperty":"dataSet"}, {}]
						}],
						idvoznjaEditor2: ["wm.Number", {"caption":"Idvoznja","captionSize":"200px","emptyValue":"zero","formField":"idvoznja","height":"26px","readonly":true,"required":true,"showing":false,"width":"100%"}, {}],
						oznakaVoznjaEditor2: ["wm.Text", {"caption":"Oznaka vožnje:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"oznakaVoznja","height":"26px","readonly":true,"width":"100%"}, {}],
						mjestoPolaskaVoznjaEditor2: ["wm.Text", {"caption":"Mjesto polaska:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"mjestoPolaskaVoznja","height":"26px","invalidMessage":"Obavezno polje!","readonly":true,"required":true,"width":"100%"}, {}],
						mjestoDolaskaVoznjaEditor2: ["wm.Text", {"caption":"Mjesto dolaska:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"mjestoDolaskaVoznja","height":"26px","invalidMessage":"Obavezno polje!","readonly":true,"required":true,"width":"100%"}, {}],
						vrijemePolaskaVoznjaEditor2: ["wm.DateTime", {"caption":"Vrijeme polaska:","captionSize":"200px","emptyValue":"emptyString","formField":"vrijemePolaskaVoznja","formatter":"vrijemePolaskaVoznjaEditor1ReadOnlyNodeFormat","height":"26px","invalidMessage":"Obavezno polje!","readonly":true,"required":true,"width":"100%"}, {"onchange":"vrijemePolaskaVoznjaEditor2Change"}],
						vrijemeDolaskaVoznjaEditor2: ["wm.DateTime", {"caption":"Vrijeme dolaska:","captionSize":"200px","emptyValue":"emptyString","formField":"vrijemeDolaskaVoznja","formatter":"vrijemeDolaskaVoznjaEditor1ReadOnlyNodeFormat","height":"26px","invalidMessage":"Obavezno polje!","readonly":true,"required":true,"width":"100%"}, {}],
						opisVoznjaEditor2: ["wm.Text", {"caption":"Napomena:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"opisVoznja","height":"26px","readonly":true,"width":"100%"}, {}],
						ugovorenaCijenaEditor1: ["wm.Text", {"caption":"Ugovorena cijena(kn):","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"ugovorenaCijenaVoznja","height":"26px","invalidMessage":"Format cijene.","readonly":true,"regExp":"^[0-9]*\\.[0-9][0-9]$","width":"100%"}, {}, {
							format: ["wm.DataFormatter", {}, {}]
						}],
						vozacLookup2: ["wm.Lookup", {"caption":"Vozač:","captionSize":"200px","displayExpression":"var text = \"\";\nif ( (${imeVozac} == null) || (${prezimeVozac} == null) ) {\ntext;\n} else {\n${imeVozac}+\" \"+${prezimeVozac};\n}","emptyValue":"emptyString","formField":"vozac","readonly":true,"required":true,"width":"100%"}, {}],
						busLookup2: ["wm.Lookup", {"caption":"Vozilo:","captionSize":"200px","displayExpression":"var text = \"\";\nif ( (${garazniBrojBus} == null) || (${modelBus} == null) || (${markaBus} == null) ) {\ntext;\n} else {\n${garazniBrojBus}+\" \"+${modelBus}+\" \"+${markaBus};\n}","displayField":"napomenaBus","emptyValue":"emptyString","formField":"bus","readonly":true,"required":true,"width":"100%"}, {}],
						naruciteljLookup2: ["wm.Lookup", {"caption":"Naručitelj:","captionSize":"200px","displayExpression":"${nazivNarucitelj}","displayField":"nazivNarucitelj","formField":"narucitelj","height":"26px","readonly":true,"width":"100%"}, {}],
						voznjaLiveForm2EditPanel: ["wm.EditPanel", {"height":"32px","isCustomized":true,"liveForm":"voznjaLiveForm2","lock":false,"operationPanel":"operationPanel5","savePanel":"savePanel5"}, {}, {
							savePanel5: ["wm.Panel", {"height":"100%","horizontalAlign":"right","layoutKind":"left-to-right","showing":false,"verticalAlign":"top","width":"100%"}, {}, {
								saveButton5: ["wm.Button", {"caption":"Spremi","margin":"4"}, {"onclick":"saveButton5Click"}, {
									binding: ["wm.Binding", {}, {}, {
										wire: ["wm.Wire", {"expression":undefined,"source":"voznjaLiveForm2EditPanel.formInvalid","targetProperty":"disabled"}, {}]
									}]
								}],
								cancelButton5: ["wm.Button", {"caption":"Odustani","margin":"4"}, {"onclick":"voznjaLiveForm2EditPanel.cancelEdit"}]
							}],
							operationPanel5: ["wm.Panel", {"height":"100%","horizontalAlign":"right","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
								newButton5: ["wm.Button", {"caption":"Otvaranje vožnje","margin":"4","width":"150px"}, {"onclick":"voznjaLiveForm2EditPanel.beginDataInsert"}]
							}]
						}]
					}]
				}]
			}]
		}],
		buttonBar3: ["wm.Panel", {"_classes":{"domNode":["dialogfooter"]},"border":"1,0,0,0","borderColor":"#333333","height":"32px","horizontalAlign":"right","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}]
	}],
	loadingPrintanjeVozila: ["wm.LoadingDialog", {}, {}, {
		binding: ["wm.Binding", {}, {}, {
			wire1: ["wm.Wire", {"expression":undefined,"source":"RasBUS","targetProperty":"widgetToCover"}, {}],
			wire: ["wm.Wire", {"expression":undefined,"source":"serviceVarGetReportBus","targetProperty":"serviceVariableToTrack"}, {}]
		}],
		loadingPrintanjeOtvorenihVoznji: ["wm.LoadingDialog", {}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire1: ["wm.Wire", {"expression":undefined,"source":"RasBUS","targetProperty":"widgetToCover"}, {}]
			}]
		}],
		loadingPrintanjeVozila1: ["wm.LoadingDialog", {}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire1: ["wm.Wire", {"expression":undefined,"source":"RasBUS","targetProperty":"widgetToCover"}, {}],
				wire: ["wm.Wire", {"expression":undefined,"source":"serviceVarGetReportBus","targetProperty":"serviceVariableToTrack"}, {}]
			}]
		}],
		loadingPrintanjeOtvorenihVoznji1: ["wm.LoadingDialog", {}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire1: ["wm.Wire", {"expression":undefined,"source":"RasBUS","targetProperty":"widgetToCover"}, {}]
			}]
		}]
	}],
	loadingPrintanjeOtvorenihVoznji: ["wm.LoadingDialog", {}, {}, {
		binding: ["wm.Binding", {}, {}, {
			wire1: ["wm.Wire", {"expression":undefined,"source":"RasBUS","targetProperty":"widgetToCover"}, {}]
		}]
	}],
	loadingPrintanjeVozila1: ["wm.LoadingDialog", {}, {}, {
		binding: ["wm.Binding", {}, {}, {
			wire1: ["wm.Wire", {"expression":undefined,"source":"RasBUS","targetProperty":"widgetToCover"}, {}],
			wire: ["wm.Wire", {"expression":undefined,"source":"serviceVarGetReportBus","targetProperty":"serviceVariableToTrack"}, {}]
		}]
	}],
	loadingPrintanjeOtvorenihVoznji1: ["wm.LoadingDialog", {}, {}, {
		binding: ["wm.Binding", {}, {}, {
			wire1: ["wm.Wire", {"expression":undefined,"source":"RasBUS","targetProperty":"widgetToCover"}, {}]
		}]
	}],
	loadingPrintanjeOtvorenihVoznji2: ["wm.LoadingDialog", {}, {}, {
		binding: ["wm.Binding", {}, {}, {
			wire1: ["wm.Wire", {"expression":undefined,"source":"RasBUS","targetProperty":"widgetToCover"}, {}]
		}]
	}],
	loadingPrintanjeVozila2: ["wm.LoadingDialog", {}, {}, {
		binding: ["wm.Binding", {}, {}, {
			wire1: ["wm.Wire", {"expression":undefined,"source":"RasBUS","targetProperty":"widgetToCover"}, {}],
			wire: ["wm.Wire", {"expression":undefined,"source":"serviceVarGetReportBus","targetProperty":"serviceVariableToTrack"}, {}]
		}]
	}],
	loadingPrintanjeOtvorenihVoznji3: ["wm.LoadingDialog", {}, {}, {
		binding: ["wm.Binding", {}, {}, {
			wire1: ["wm.Wire", {"expression":undefined,"source":"RasBUS","targetProperty":"widgetToCover"}, {}]
		}]
	}],
	loadingPrintanjeVozaci: ["wm.LoadingDialog", {}, {}, {
		binding: ["wm.Binding", {}, {}, {
			wire: ["wm.Wire", {"expression":undefined,"source":"serviceVarGetReportVozaci","targetProperty":"serviceVariableToTrack"}, {}],
			wire1: ["wm.Wire", {"expression":undefined,"source":"RasBUS","targetProperty":"widgetToCover"}, {}]
		}]
	}],
	loadingPrintanjeVoznji: ["wm.LoadingDialog", {}, {}, {
		binding: ["wm.Binding", {}, {}, {
			wire: ["wm.Wire", {"expression":undefined,"source":"serviceVarGetReportVoznje","targetProperty":"serviceVariableToTrack"}, {}],
			wire1: ["wm.Wire", {"expression":undefined,"source":"RasBUS","targetProperty":"widgetToCover"}, {}]
		}]
	}],
	loadingPrintanjeNarucitelji: ["wm.LoadingDialog", {}, {}, {
		binding: ["wm.Binding", {}, {}, {
			wire: ["wm.Wire", {"expression":undefined,"source":"serviceVarGetReportNarucitelji","targetProperty":"serviceVariableToTrack"}, {}],
			wire1: ["wm.Wire", {"expression":undefined,"source":"RasBUS","targetProperty":"widgetToCover"}, {}]
		}]
	}],
	loadingPrinatnjeNenaplacenihVoznji: ["wm.LoadingDialog", {}, {}, {
		binding: ["wm.Binding", {}, {}, {
			wire1: ["wm.Wire", {"expression":undefined,"source":"RasBUS","targetProperty":"widgetToCover"}, {}],
			wire: ["wm.Wire", {"expression":undefined,"source":"[nenaplaceneVoznje].serviceVarGetReportNenaplaceneVoznje","targetProperty":"serviceVariableToTrack"}, {}]
		}]
	}],
	loadingPrintanjeNaplaceneVoznje: ["wm.LoadingDialog", {}, {}, {
		binding: ["wm.Binding", {}, {}, {
			wire: ["wm.Wire", {"expression":undefined,"source":"[nenaplaceneVoznje].serviceVarGetReportNenaplaceneVoznje","targetProperty":"serviceVariableToTrack"}, {}],
			wire1: ["wm.Wire", {"expression":undefined,"source":"RasBUS","targetProperty":"widgetToCover"}, {}]
		}]
	}],
	designableDialogAdminUser: ["wm.DesignableDialog", {"buttonBarId":"buttonBar","containerWidgetId":"containerWidget1","height":"500px","modal":false,"title":"Administracija korisnika","width":"800px"}, {"onShow":"designableDialogAdminUserShow"}, {
		containerWidget1: ["wm.Container", {"_classes":{"domNode":["wmdialogcontainer","MainContent"]},"autoScroll":true,"height":"100%","horizontalAlign":"left","margin":"0","padding":"5","verticalAlign":"top","width":"100%"}, {}, {
			userLivePanel1: ["wm.LivePanel", {"horizontalAlign":"left","verticalAlign":"top"}, {}, {
				userGridPanel: ["wm.FancyPanel", {"height":"231px","title":"Korisnici"}, {}, {
					userDojoGrid: ["wm.DojoGrid", {"_classes":{"domNode":["omgDataGrid"]},"columns":[{"show":false,"id":"iduser","title":"id","width":"80px","displayType":"Number","noDelete":true,"align":"center","formatFunc":""},{"show":true,"id":"ime","title":"Ime","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"prezime","title":"Prezime","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"username","title":"Korisničko ime","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"password","title":"Lozinka","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"id":"customField","isCustomField":true,"expression":"${role.naziv}","show":true,"width":"100%","title":"Naziv uloge"}],"height":"100%","margin":"4"}, {}, {
						binding: ["wm.Binding", {}, {}, {
							wire: ["wm.Wire", {"expression":undefined,"source":"userLiveVariable1","targetProperty":"dataSet"}, {}]
						}]
					}]
				}],
				userDetailsPanel: ["wm.FancyPanel", {"fitToContentHeight":true,"height":"191px","title":"Detalji","width":"782px"}, {}, {
					userLiveForm1: ["wm.LiveForm", {"confirmDelete":"Želite li izbrisati korisnika?","fitToContentHeight":true,"height":"162px","horizontalAlign":"left","margin":"0,40,0,40","readonly":true,"verticalAlign":"top"}, {"onSuccess":"userLiveVariable1"}, {
						binding: ["wm.Binding", {}, {}, {
							wire: ["wm.Wire", {"expression":undefined,"source":"userDojoGrid.selectedItem","targetProperty":"dataSet"}, {}]
						}],
						iduserEditor1: ["wm.Number", {"caption":"Iduser","captionSize":"200px","emptyValue":"zero","formField":"iduser","height":"26px","readonly":true,"required":true,"showing":false,"width":"100%"}, {}],
						imeEditor1: ["wm.Text", {"caption":"Ime:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"ime","height":"26px","readonly":true,"required":true,"width":"100%"}, {}],
						prezimeEditor1: ["wm.Text", {"caption":"Prezime:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"prezime","height":"26px","readonly":true,"required":true,"width":"100%"}, {}],
						usernameEditor1: ["wm.Text", {"caption":"korisničko ime:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"username","height":"26px","readonly":true,"required":true,"width":"100%"}, {}],
						passwordEditor1: ["wm.Text", {"caption":"Lozinka:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"password","height":"26px","readonly":true,"required":true,"width":"100%"}, {}],
						lookup1: ["wm.Lookup", {"caption":"Uloge:","captionSize":"200px","displayExpression":"if(${idrole} != null){\n\t${idrole}+\",\"+${naziv}\n}else{\n\t\"\"\n}","displayField":"opis","formField":"role","height":"26px","readonly":true,"required":true,"width":"100%"}, {}],
						userLiveForm1EditPanel: ["wm.EditPanel", {"height":"32px","isCustomized":true,"liveForm":"userLiveForm1","lock":false,"operationPanel":"operationPanel6","savePanel":"savePanel6"}, {}, {
							savePanel6: ["wm.Panel", {"height":"100%","horizontalAlign":"right","layoutKind":"left-to-right","showing":false,"verticalAlign":"top","width":"100%"}, {}, {
								saveButton6: ["wm.Button", {"caption":"Spremi","margin":"4"}, {"onclick":"userLiveForm1EditPanel.saveData"}, {
									binding: ["wm.Binding", {}, {}, {
										wire: ["wm.Wire", {"expression":undefined,"source":"userLiveForm1EditPanel.formInvalid","targetProperty":"disabled"}, {}]
									}]
								}],
								cancelButton6: ["wm.Button", {"caption":"Odustani","margin":"4"}, {"onclick":"userLiveForm1EditPanel.cancelEdit"}]
							}],
							operationPanel6: ["wm.Panel", {"height":"100%","horizontalAlign":"right","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
								newButton6: ["wm.Button", {"caption":"Novi","margin":"4"}, {"onclick":"userLiveForm1EditPanel.beginDataInsert"}],
								updateButton5: ["wm.Button", {"caption":"Ažuriraj","margin":"4"}, {"onclick":"userLiveForm1EditPanel.beginDataUpdate"}, {
									binding: ["wm.Binding", {}, {}, {
										wire: ["wm.Wire", {"expression":undefined,"source":"userLiveForm1EditPanel.formUneditable","targetProperty":"disabled"}, {}]
									}]
								}],
								deleteButton5: ["wm.Button", {"caption":"Obriši","margin":"4"}, {"onclick":"userLiveForm1EditPanel.deleteData"}, {
									binding: ["wm.Binding", {}, {}, {
										wire: ["wm.Wire", {"expression":undefined,"source":"userLiveForm1EditPanel.formUneditable","targetProperty":"disabled"}, {}]
									}]
								}]
							}]
						}]
					}]
				}]
			}]
		}],
		buttonBar4: ["wm.Panel", {"_classes":{"domNode":["dialogfooter"]},"border":"1,0,0,0","borderColor":"#333333","height":"32px","horizontalAlign":"right","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}]
	}],
	designableDialogAdminRole: ["wm.DesignableDialog", {"buttonBarId":"buttonBar","containerWidgetId":"containerWidget3","corner":"ccenter","modal":false,"positionNear":"","title":"Administracija uloga","width":"800px"}, {"onShow":"designableDialogAdminRoleShow"}, {
		containerWidget3: ["wm.Container", {"_classes":{"domNode":["wmdialogcontainer","MainContent"]},"autoScroll":true,"height":"100%","horizontalAlign":"left","margin":"0","padding":"5","verticalAlign":"top","width":"100%"}, {}, {
			roleLivePanel1: ["wm.LivePanel", {"horizontalAlign":"left","verticalAlign":"top"}, {}, {
				roleGridPanel: ["wm.FancyPanel", {"minHeight":"180","title":"Uloge"}, {}, {
					roleDojoGrid: ["wm.DojoGrid", {"_classes":{"domNode":["omgDataGrid"]},"columns":[{"show":false,"id":"idrole","title":"ID","width":"80px","displayType":"Number","noDelete":true,"align":"center","formatFunc":""},{"show":true,"id":"naziv","title":"Naziv","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"opis","title":"Opis","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""}],"height":"100%","margin":"4"}, {}, {
						binding: ["wm.Binding", {}, {}, {
							wire: ["wm.Wire", {"expression":undefined,"source":"roleLiveVariable1","targetProperty":"dataSet"}, {}]
						}]
					}]
				}],
				roleDetailsPanel: ["wm.FancyPanel", {"fitToContentHeight":true,"height":"113px","title":"Detalji"}, {}, {
					roleLiveForm1: ["wm.LiveForm", {"confirmDelete":"Želite li izbrisati ulogu?","fitToContentHeight":true,"height":"84px","horizontalAlign":"left","margin":"0,40,0,40","readonly":true,"verticalAlign":"top"}, {"onSuccess":"roleLiveVariable1"}, {
						binding: ["wm.Binding", {}, {}, {
							wire: ["wm.Wire", {"expression":undefined,"source":"roleDojoGrid.selectedItem","targetProperty":"dataSet"}, {}]
						}],
						idroleEditor1: ["wm.Number", {"caption":"Idrole","captionSize":"200px","emptyValue":"zero","formField":"idrole","height":"26px","readonly":true,"required":true,"showing":false,"width":"100%"}, {}],
						nazivEditor1: ["wm.Text", {"caption":"Naziv:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"naziv","height":"26px","readonly":true,"required":true,"width":"100%"}, {}],
						opisEditor1: ["wm.Text", {"caption":"Opis:","captionSize":"200px","dataValue":"","emptyValue":"emptyString","formField":"opis","height":"26px","readonly":true,"required":true,"width":"100%"}, {}],
						roleLiveForm1EditPanel: ["wm.EditPanel", {"height":"32px","isCustomized":true,"liveForm":"roleLiveForm1","lock":false,"operationPanel":"operationPanel7","savePanel":"savePanel7"}, {}, {
							savePanel7: ["wm.Panel", {"height":"100%","horizontalAlign":"right","layoutKind":"left-to-right","showing":false,"verticalAlign":"top","width":"100%"}, {}, {
								saveButton7: ["wm.Button", {"caption":"Spremi","margin":"4"}, {"onclick":"roleLiveForm1EditPanel.saveData"}, {
									binding: ["wm.Binding", {}, {}, {
										wire: ["wm.Wire", {"expression":undefined,"source":"roleLiveForm1EditPanel.formInvalid","targetProperty":"disabled"}, {}]
									}]
								}],
								cancelButton7: ["wm.Button", {"caption":"Odustani","margin":"4"}, {"onclick":"roleLiveForm1EditPanel.cancelEdit"}]
							}],
							operationPanel7: ["wm.Panel", {"height":"100%","horizontalAlign":"right","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
								newButton7: ["wm.Button", {"caption":"Novi","margin":"4"}, {"onclick":"roleLiveForm1EditPanel.beginDataInsert"}],
								updateButton6: ["wm.Button", {"caption":"Ažuriraj","margin":"4"}, {"onclick":"roleLiveForm1EditPanel.beginDataUpdate"}, {
									binding: ["wm.Binding", {}, {}, {
										wire: ["wm.Wire", {"expression":undefined,"source":"roleLiveForm1EditPanel.formUneditable","targetProperty":"disabled"}, {}]
									}]
								}],
								deleteButton6: ["wm.Button", {"caption":"Obriši","margin":"4"}, {"onclick":"roleLiveForm1EditPanel.deleteData"}, {
									binding: ["wm.Binding", {}, {}, {
										wire: ["wm.Wire", {"expression":undefined,"source":"roleLiveForm1EditPanel.formUneditable","targetProperty":"disabled"}, {}]
									}]
								}]
							}]
						}]
					}]
				}]
			}]
		}],
		buttonBar6: ["wm.Panel", {"_classes":{"domNode":["dialogfooter"]},"border":"1,0,0,0","borderColor":"#333333","height":"32px","horizontalAlign":"right","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}]
	}],
	designableDialogLogovi: ["wm.DesignableDialog", {"buttonBarId":"buttonBar","containerWidgetId":"containerWidget4","height":"500px","modal":false,"title":"Logovi","width":"800px"}, {"onShow":"designableDialogLogoviShow"}, {
		containerWidget4: ["wm.Container", {"_classes":{"domNode":["wmdialogcontainer","MainContent"]},"autoScroll":true,"height":"100%","horizontalAlign":"left","margin":"0","padding":"5","verticalAlign":"top","width":"100%"}, {}, {
			panel27: ["wm.Panel", {"height":"56px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
				panel28: ["wm.Panel", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"50%"}, {}, {
					textFilterSadrzaj: ["wm.Text", {"caption":"Sadržaj:","changeOnKey":true,"dataValue":undefined,"displayValue":"","resetButton":true}, {"onchange":"textFilterSadrzajChange"}]
				}],
				panel29: ["wm.Panel", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"50%"}, {}, {
					panel30: ["wm.Panel", {"height":"24px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
						filterDatePocetak: ["wm.Date", {"caption":"Od:","dataValue":undefined,"displayValue":""}, {"onchange":"filterDatePocetakChange"}],
						ResetVrijemeOdLogovi: ["wm.Picture", {"_classes":{"domNode":["wm_Mouse_pointer"]},"border":"0","height":"20px","source":"resources/images/Reset.png","width":"31px"}, {"onclick":"ResetVrijemeOdLogoviClick"}]
					}],
					panel31: ["wm.Panel", {"height":"24px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
						filterDateKraj: ["wm.Date", {"caption":"Do:","dataValue":undefined,"displayValue":""}, {"onchange":"filterDateKrajChange"}],
						ResetVrijemeDoLogovi: ["wm.Picture", {"_classes":{"domNode":["wm_Mouse_pointer"]},"border":"0","height":"20px","source":"resources/images/Reset.png","width":"31px"}, {"onclick":"ResetVrijemeDoLogoviClick"}]
					}]
				}]
			}],
			logoviLivePanel1: ["wm.LivePanel", {"autoScroll":false,"horizontalAlign":"left","verticalAlign":"top"}, {}, {
				binding: ["wm.Binding", {}, {}, {
					wire: ["wm.Wire", {"source":"logoviDialog","targetId":null,"targetProperty":"dialog"}, {}],
					wire1: ["wm.Wire", {"source":"logoviLiveForm1","targetId":null,"targetProperty":"liveForm"}, {}],
					wire2: ["wm.Wire", {"source":"logoviDojoGrid","targetId":null,"targetProperty":"dataGrid"}, {}],
					wire3: ["wm.Wire", {"source":"logoviSaveButton","targetId":null,"targetProperty":"saveButton"}, {}]
				}],
				logoviDojoGrid: ["wm.DojoGrid", {"columns":[{"show":true,"id":"idlogovi","title":"ID","width":"80px","displayType":"Number","noDelete":true,"align":"right","formatFunc":""},{"show":true,"id":"sadrzaj","title":"Sadržaj","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"serverskoVrijeme","title":"Serversko vrijeme","width":"80px","displayType":"Date","noDelete":true,"align":"left","formatFunc":"logoviDojoGridServerskoVrijemeFormat"}],"height":"100%","margin":"4"}, {"onCellDblClick":"logoviLivePanel1.popupLivePanelEdit"}, {
					binding: ["wm.Binding", {}, {}, {
						wire: ["wm.Wire", {"expression":undefined,"source":"serviceVarGetLogovi","targetProperty":"dataSet"}, {}]
					}]
				}],
				logoviGridButtonPanel: ["wm.Panel", {"height":"32px","horizontalAlign":"right","layoutKind":"left-to-right","showing":false,"verticalAlign":"top","width":"100%"}, {}, {
					logoviNewButton: ["wm.Button", {"caption":"New","margin":"4"}, {"onclick":"logoviLivePanel1.popupLivePanelInsert"}],
					logoviUpdateButton: ["wm.Button", {"caption":"Update","margin":"4"}, {"onclick":"logoviLivePanel1.popupLivePanelEdit"}, {
						binding: ["wm.Binding", {}, {}, {
							wire: ["wm.Wire", {"source":"logoviDojoGrid.emptySelection","targetId":null,"targetProperty":"disabled"}, {}]
						}]
					}],
					logoviDeleteButton: ["wm.Button", {"caption":"Delete","margin":"4"}, {"onclick":"logoviLiveForm1.deleteData"}, {
						binding: ["wm.Binding", {}, {}, {
							wire: ["wm.Wire", {"source":"logoviDojoGrid.emptySelection","targetId":null,"targetProperty":"disabled"}, {}]
						}]
					}]
				}]
			}]
		}],
		buttonBar7: ["wm.Panel", {"_classes":{"domNode":["dialogfooter"]},"border":"1,0,0,0","borderColor":"#333333","height":"32px","horizontalAlign":"right","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}]
	}],
	serviceVarGetLogovi: ["wm.ServiceVariable", {"autoUpdate":true,"operation":"getLogovi","service":"glavni"}, {}, {
		input: ["wm.ServiceInput", {"type":"getLogoviInputs"}, {}]
	}],
	loadingPrintanjeOtvoreneVoznje: ["wm.LoadingDialog", {}, {}, {
		binding: ["wm.Binding", {}, {}, {
			wire: ["wm.Wire", {"expression":undefined,"source":"[otvoreneVoznje].serviceVarOtvoreneVoznjeReport","targetProperty":"serviceVariableToTrack"}, {}],
			wire1: ["wm.Wire", {"expression":undefined,"source":"RasBUS","targetProperty":"widgetToCover"}, {}]
		}]
	}],
	RasBUS: ["wm.Layout", {"height":"100%","horizontalAlign":"left","verticalAlign":"middle","width":"875px"}, {}, {
		MenuonSide: ["wm.Template", {"autoScroll":true,"border":"1,0,1,1","height":"100%","horizontalAlign":"center","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
			panel1: ["wm.Panel", {"border":"0,1,0,0","borderColor":"#999999","height":"100%","horizontalAlign":"left","minHeight":600,"minWidth":900,"verticalAlign":"top","width":"98%"}, {}, {
				panel2: ["wm.HeaderContentPanel", {"height":"70px","horizontalAlign":"left","layoutKind":"left-to-right","margin":"0","padding":"0,10,0,10","verticalAlign":"middle","width":"100%"}, {}, {
					picture1: ["wm.Picture", {"border":"0","height":"69px","source":"resources/images/Logo.gif","width":"70px"}, {}],
					label3: ["wm.Label", {"_classes":{"domNode":["wm_FontColor_White","wm_FontSizePx_20px","wm_TextDecoration_Bold"]},"border":"0","caption":"RasBUS     v1.0","height":"35px","padding":"4","width":"95px"}, {}, {
						format: ["wm.DataFormatter", {}, {}]
					}],
					panel32: ["wm.Panel", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"100%"}, {}, {
						panel33: ["wm.Panel", {"height":"26px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}],
						label2: ["wm.Label", {"_classes":{"domNode":["wm_FontColor_White","wm_TextDecoration_Bold","wm_FontSizePx_12px"]},"border":"0","caption":"v1.0","height":"29px","padding":"4","singleLine":false,"width":"100%"}, {}]
					}],
					panel10: ["wm.Panel", {"height":"100%","horizontalAlign":"left","verticalAlign":"middle","width":"300px"}, {}, {
						panel15: ["wm.Panel", {"height":"63px","horizontalAlign":"right","verticalAlign":"middle","width":"300px"}, {}, {
							panel8: ["wm.Panel", {"height":"46px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"middle","width":"100%"}, {}, {
								label4: ["wm.Label", {"_classes":{"domNode":["wm_FontColor_White"]},"border":"0","caption":"Prijavljeni korisnik:","height":"39px","padding":"4","width":"123px"}, {}],
								logiraniKorisnik: ["wm.Label", {"_classes":{"domNode":["wm_FontColor_White","wm_FontSizePx_12px","wm_TextDecoration_Bold"]},"border":"0","height":"39px","padding":"4","width":"112px"}, {}, {
									binding: ["wm.Binding", {}, {}, {
										wire: ["wm.Wire", {"expression":undefined,"source":"app.varSecurity.dataValue","targetProperty":"caption"}, {}]
									}]
								}],
								htmlLogaoutButton: ["wm.Html", {"border":"0","html":"<div class=\"loginButton\"></div>"}, {"onclick":"serviceVariable1"}]
							}]
						}]
					}]
				}],
				panel3: ["wm.Panel", {"height":"100%","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
					panel4: ["wm.MainContentPanel", {"height":"100%","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
						fancyPanel1: ["wm.FancyPanel", {"margin":"2,2,2,0","title":"Izbornik","width":"251px"}, {}, {
							contentTree: ["wm.Content", {"border":"0","content":"classTreePane","height":"100%","width":"244px"}, {}]
						}],
						tabLayers1: ["wm.TabLayers", {}, {}, {
							layer1: ["wm.Layer", {"border":"1","borderColor":"#4284c1","caption":"Dobrodošli","height":"619px","horizontalAlign":"left","themeStyleType":"ContentPanel","verticalAlign":"top"}, {}]
						}]
					}]
				}],
				panel6: ["wm.HeaderContentPanel", {"height":"24px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
					picture2: ["wm.Picture", {"border":"0","height":"100%","source":"lib/wm/base/widget/themes/default/images/wmSmallLogo.png","width":"24px"}, {}],
					label1: ["wm.Label", {"_classes":{"domNode":["wm_FontSizePx_10px","wm_FontColor_White"]},"align":"right","border":"0","caption":"Copyright [RasBUS] 2013","height":"100%","padding":"4","width":"100%"}, {}, {
						format: ["wm.DataFormatter", {}, {}]
					}]
				}]
			}]
		}]
	}]
}