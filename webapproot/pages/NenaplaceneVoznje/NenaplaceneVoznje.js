dojo.declare("NenaplaceneVoznje", wm.Page, {
    start: function() {
        try {
            this.serviceVarNenaplaceneVoznje.update();
        } catch (e) {
            app.toastError(this.name + ".start() Failed: " + e.toString());
        }
    },

    dojoGrid1VrijemePolaskaVoznjaFormat: function(inValue, rowId, cellId, cellField, cellObj, rowObj) {
        try {
            return app.dateTimeFormat(inValue);
        } catch (e) {
            console.error('ERROR IN dojoGrid1VrijemePolaskaVoznjaFormat: ' + e);
        }
    },
    dojoGrid1VrijemeDolaskaVoznjaFormat: function(inValue, rowId, cellId, cellField, cellObj, rowObj) {
        try {
            return app.dateTimeFormat(inValue);
        } catch (e) {
            console.error('ERROR IN dojoGrid1VrijemeDolaskaVoznjaFormat: ' + e);
        }
    },

    btnReportNenaplaceneVoznjeClick: function(inSender) {
        try {
            var typeofdoc = "PDF";
            var src = "ReportNenaplaceneVoznje.jasper";
            var mjestoPolaska = this.textMjePolaska.getDataValue();
            var mjestoDolaska = this.textMjeDolaska.getDataValue();
            var prezimeVozaca = this.textPrezVozac.getDataValue();
            var garazniBroj = this.textGarBroj.getDataValue();
            var vrijemePolaska = this.dateVrijemePolaska.getDataValue();
            var vrijemeDolaska = this.dateVrijemeDolaska.getDataValue();
            var sifraNarucitelja = this.textSifraNarucitelja.getDataValue();
            var nazivNarucitelja = this.textNazivNarucitelja.getDataValue();
            var oznakaVoznje = this.textFilterOznaka.getDataValue();

            var paramLogo = "../../resources/images/reports/logoReport.gif";
            var paramRideBack = "../../resources/images/reports/rideback.gif";
            var hash = {
                "typeDoc": typeofdoc,
                "file": src,
                "paramLogo": paramLogo,
                "mjestoPolaska": mjestoPolaska,
                "mjestoDolaska": mjestoDolaska,
                "prezimeVozaca": prezimeVozaca,
                "garazniBroj": garazniBroj,
                "vrijemePolaska": vrijemePolaska,
                "vrijemeDolaska": vrijemeDolaska,
                "sifraNarucitelja": sifraNarucitelja,
                "nazivNarucitelja": nazivNarucitelja,
                "oznakaVoznje": oznakaVoznje
            };
            this.serviceVarGetReportNenaplaceneVoznje.input.setValue("parameters", hash);
            this.serviceVarGetReportNenaplaceneVoznje.update();
        } catch (e) {
            console.error('ERROR IN btnReportNenaplaceneVoznjeClick: ' + e);
        }
    },

    servicePrintIzvjestajResult: function(inSender, inDeprecated) {
        try {
            main.servicePrintIzvjestajResult(inSender, inDeprecated);

        } catch (e) {
            console.error('ERROR IN servicePrintIzvjestajResult: ' + e);
        }
    },
    ResetVrijemePolaskaClick: function(inSender) {
        try {
            this.dateVrijemePolaska.setDataValue("");

        } catch (e) {
            console.error('ERROR IN ResetVrijemePolaskaClick: ' + e);
        }
    },
    ResetVrijemeDolaskaClick: function(inSender) {
        try {
            this.dateVrijemeDolaska.setDataValue("");

        } catch (e) {
            console.error('ERROR IN ResetVrijemeDolaskaClick: ' + e);
        }
    },
    ///metoda za filtriranje sa vrmenskom zadrškom
    callServiceVarGetNenaplaceneVoznje: function(){
       try {
          if (this._mytimeout) {
              window.clearTimeout(this._mytimeout);
          }          
          this.serviceVarNenaplaceneVoznje.input.setValue('garazniBroj', this.textGarBroj.getDataValue());
          this.serviceVarNenaplaceneVoznje.input.setValue('mjestoDolaska', this.textMjeDolaska.getDataValue());
          this.serviceVarNenaplaceneVoznje.input.setValue('mjestoPolaska', this.textMjePolaska.getDataValue());
          this.serviceVarNenaplaceneVoznje.input.setValue('prezimeVozaca', this.textPrezVozac.getDataValue());
          this.serviceVarNenaplaceneVoznje.input.setValue('vrijemeDolaska', this.dateVrijemeDolaska.getDataValue());
          this.serviceVarNenaplaceneVoznje.input.setValue('vrijemePolaska', this.dateVrijemePolaska.getDataValue());
          this.serviceVarNenaplaceneVoznje.input.setValue('sifraNarucitelj', this.textSifraNarucitelja.getDataValue());
          this.serviceVarNenaplaceneVoznje.input.setValue('nazivNarucitelj', this.textNazivNarucitelja.getDataValue());
          this.serviceVarNenaplaceneVoznje.input.setValue('oznakaVoznje', this.textFilterOznaka.getDataValue());
          this._mytimeout = window.setTimeout(dojo.hitch(app, 'filterGetNenaplaceneVoznje', this.serviceVarNenaplaceneVoznje), app.filteriTimeOut);    	  
	  } catch(e) {
		  console.error('ERROR IN textMjestoDolaskaFilterChange: ' + e); 
	  }
  },
    textSifraNaruciteljaChange: function(inSender) {
	  try {		  
		  this.callServiceVarGetNenaplaceneVoznje();
	  } catch(e) {
		  console.error('ERROR IN textSifraNaruciteljaChange: ' + e); 
	  } 
  },
  textNazivNaruciteljaChange: function(inSender) {
	  try {		  
		  this.callServiceVarGetNenaplaceneVoznje();
	  } catch(e) {
		  console.error('ERROR IN textNazivNaruciteljaChange: ' + e); 
	  } 
  },
  textMjePolaskaChange: function(inSender) {
	  try {		  
		  this.callServiceVarGetNenaplaceneVoznje();
	  } catch(e) {
		  console.error('ERROR IN textMjePolaskaChange: ' + e); 
	  } 
  },
  textMjeDolaskaChange: function(inSender) {
	  try {		  
		  this.callServiceVarGetNenaplaceneVoznje();
	  } catch(e) {
		  console.error('ERROR IN textMjeDolaskaChange: ' + e); 
	  } 
  },
  textGarBrojChange: function(inSender) {
	  try {		  
		  this.callServiceVarGetNenaplaceneVoznje();
	  } catch(e) {
		  console.error('ERROR IN textGarBrojChange: ' + e); 
	  } 
  },
  dateVrijemePolaskaChange: function(inSender) {
	  try {		  
		  this.callServiceVarGetNenaplaceneVoznje();
	  } catch(e) {
		  console.error('ERROR IN dateVrijemePolaskaChange: ' + e); 
	  } 
  },
  dateVrijemeDolaskaChange: function(inSender) {
	  try {		  
		  this.callServiceVarGetNenaplaceneVoznje();
	  } catch(e) {
		  console.error('ERROR IN dateVrijemeDolaskaChange: ' + e); 
	  } 
  },
  textPrezVozacChange: function(inSender) {
	  try {		  
		  this.callServiceVarGetNenaplaceneVoznje();
	  } catch(e) {
		  console.error('ERROR IN textPrezVozacChange: ' + e); 
	  } 
  },
  textFilterOznakaChange: function(inSender) {
	  try {
		  this.callServiceVarGetNenaplaceneVoznje();		  
	  } catch(e) {
		  console.error('ERROR IN textFilterOznakaChange: ' + e); 
	  } 
  },
  _end: 0
});