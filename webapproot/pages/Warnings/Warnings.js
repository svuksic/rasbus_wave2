dojo.declare("Warnings", wm.Page, {
    start: function() {
        try {
            this.serviceVarOsobna.update();
            this.serviceVarPutovnica.update();
            this.serviceVarVozacka.update();
        } catch (e) {
            app.toastError(this.name + ".start() Failed: " + e.toString());
        }
    },

    dojoGrid3OsobnaFormat: function(inValue, rowId, cellId, cellField, cellObj, rowObj) {
        try {
            var d = new Date(inValue);
            var strDatum = d.getDate() + "." + (d.getMonth() + 1) + "." + d.getFullYear();
            return strDatum;
        } catch (e) {
            console.error('ERROR IN dojoGrid3OsobnaFormat: ' + e);
        }
    },
    dojoGrid1VozackaFormat: function(inValue, rowId, cellId, cellField, cellObj, rowObj) {
        try {
            var d = new Date(inValue);
            var strDatum = d.getDate() + "." + (d.getMonth() + 1) + "." + d.getFullYear();
            return strDatum;
        } catch (e) {
            console.error('ERROR IN dojoGrid1VozackaFormat: ' + e);
        }
    },
    dojoGrid2PutovnicaFormat: function(inValue, rowId, cellId, cellField, cellObj, rowObj) {
        try {
            var d = new Date(inValue);
            var strDatum = d.getDate() + "." + (d.getMonth() + 1) + "." + d.getFullYear();
            return strDatum;
        } catch (e) {
            console.error('ERROR IN dojoGrid2PutovnicaFormat: ' + e);
        }
    },
    dojoGrid3CustomFieldFormat: function( inValue, rowId, cellId, cellField, cellObj, rowObj) {
	  try {		  
		  var d = new Date(inValue);
          var today = new Date();
          var vrijemeIsteka = (today.getTime() - d.getTime()) / 86400000;
          if(d.getTime() <= today.getTime()){
              return "<div style='color: red;'>istekla</div>";
          }else if(vrijemeIsteka < 0 && vrijemeIsteka > -7){
              //ove koje isticu za da tjedan dana su žute
              return "<div style='color: yellow;'>valjana</div>";
          }else{
              return "<div style='color: green;'>valjana</div>";
          }
	  } catch(e) {
		  console.error('ERROR IN dojoGrid3CustomFieldFormat: ' + e); 
	  } 
  },
  dojoGrid1CustomFieldFormat: function( inValue, rowId, cellId, cellField, cellObj, rowObj) {
	  try {
		  var d = new Date(inValue);
          var today = new Date();
          var vrijemeIsteka = (today.getTime() - d.getTime()) / 86400000;
          if(d.getTime() <= today.getTime()){
              return "<div style='color: red;'>istekla</div>";
          }else if(vrijemeIsteka < 0 && vrijemeIsteka > -7){
              //ove koje isticu za da tjedan dana su žute
              return "<div style='color: yellow;'>valjana</div>";
          }else{
              return "<div style='color: green;'>valjana</div>";
          }		  
	  } catch(e) {
		  console.error('ERROR IN dojoGrid1CustomFieldFormat: ' + e); 
	  } 
  },
  dojoGrid2CustomFieldFormat: function( inValue, rowId, cellId, cellField, cellObj, rowObj) {
	  try {
		  var d = new Date(inValue);
          var today = new Date();
          var vrijemeIsteka = (today.getTime() - d.getTime()) / 86400000;
          if(d.getTime() <= today.getTime()){
              return "<div style='color: red;'>istekla</div>";
          }else if(vrijemeIsteka < 0 && vrijemeIsteka > -7){
              //ove koje isticu za da tjedan dana su žute
              return "<div style='color: yellow;'>valjana</div>";
          }else{
              return "<div style='color: green;'>valjana</div>";
          }		  
	  } catch(e) {
		  console.error('ERROR IN dojoGrid2CustomFieldFormat: ' + e); 
	  } 
  },
  _end: 0
});