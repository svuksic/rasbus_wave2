Warnings.widgets = {
	serviceVarVozacka: ["wm.ServiceVariable", {"maxResults":10,"operation":"getWarningsVozacka","service":"rasbusDB"}, {}, {
		input: ["wm.ServiceInput", {"type":"getWarningsVozackaInputs"}, {}]
	}],
	serviceVarPutovnica: ["wm.ServiceVariable", {"operation":"getWarningsPutovnica","service":"rasbusDB"}, {}, {
		input: ["wm.ServiceInput", {"type":"getWarningsPutovnicaInputs"}, {}]
	}],
	serviceVarOsobna: ["wm.ServiceVariable", {"operation":"getWarningsOsobna","service":"rasbusDB"}, {}, {
		input: ["wm.ServiceInput", {"type":"getWarningsOsobnaInputs"}, {}]
	}],
	layoutBox1: ["wm.Layout", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"100%"}, {}, {
		panel1: ["wm.Panel", {"_classes":{"domNode":["wm_BackgroundColor_VeryLightGray"]},"height":"100%","horizontalAlign":"center","verticalAlign":"top","width":"100%"}, {}, {
			fancyPanel3: ["wm.FancyPanel", {"height":"33%","title":"Osobne iskaznice"}, {}, {
				dojoGrid3: ["wm.DojoGrid", {"columns":[{"show":true,"id":"id","title":"Id","width":"50px","displayType":"Number","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"ime","title":"Ime","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"prezime","title":"Prezime","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"osobna","title":"Datum isteka","width":"100%","displayType":"Date","noDelete":true,"align":"left","formatFunc":"dojoGrid3OsobnaFormat"},{"id":"customField","isCustomField":true,"expression":"${osobna}","show":true,"width":"100%","title":"Status","formatFunc":"dojoGrid3CustomFieldFormat"}],"height":"200%","margin":"4"}, {}, {
					binding: ["wm.Binding", {}, {}, {
						wire: ["wm.Wire", {"expression":undefined,"source":"serviceVarOsobna","targetProperty":"dataSet"}, {}]
					}]
				}]
			}],
			fancyPanel1: ["wm.FancyPanel", {"height":"33%","title":"Vozačke dozvole"}, {}, {
				dojoGrid1: ["wm.DojoGrid", {"columns":[{"show":true,"id":"id","title":"Id","width":"50px","displayType":"Number","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"ime","title":"Ime","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"prezime","title":"Prezime","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"vozacka","title":"Datum isteka","width":"100%","displayType":"Date","noDelete":true,"align":"left","formatFunc":"dojoGrid1VozackaFormat"},{"id":"customField","isCustomField":true,"expression":"${vozacka}","show":true,"width":"100%","title":"Status","formatFunc":"dojoGrid1CustomFieldFormat"}],"height":"100%","margin":"4"}, {}, {
					binding: ["wm.Binding", {}, {}, {
						wire: ["wm.Wire", {"expression":undefined,"source":"serviceVarVozacka","targetProperty":"dataSet"}, {}]
					}]
				}]
			}],
			fancyPanel2: ["wm.FancyPanel", {"height":"33%","title":"Putovnice "}, {}, {
				dojoGrid2: ["wm.DojoGrid", {"columns":[{"show":true,"id":"id","title":"Id","width":"50px","displayType":"Number","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"ime","title":"Ime","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"prezime","title":"Prezime","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"putovnica","title":"Datum isteka","width":"100%","displayType":"Date","noDelete":true,"align":"left","formatFunc":"dojoGrid2PutovnicaFormat"},{"id":"customField","isCustomField":true,"expression":"${putovnica}","show":true,"width":"100%","title":"Status","formatFunc":"dojoGrid2CustomFieldFormat"}],"height":"100%","margin":"4"}, {}, {
					binding: ["wm.Binding", {}, {}, {
						wire: ["wm.Wire", {"expression":undefined,"source":"serviceVarPutovnica","targetProperty":"dataSet"}, {}]
					}]
				}]
			}]
		}]
	}]
}