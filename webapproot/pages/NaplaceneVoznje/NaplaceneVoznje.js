dojo.declare("NaplaceneVoznje", wm.Page, {
    start: function() {
        try {
            this.serviceVarNaplaceneVoznje.update();
        } catch (e) {
            app.toastError(this.name + ".start() Failed: " + e.toString());
        }
    },

    dojoGrid1VrijemePolaskaVoznjaFormat: function(inValue, rowId, cellId, cellField, cellObj, rowObj) {
        try {
            return app.dateTimeFormat(inValue);
        } catch (e) {
            console.error('ERROR IN dojoGrid1VrijemePolaskaVoznjaFormat: ' + e);
        }
    },
    dojoGrid1VrijemeDolaskaVoznjaFormat: function(inValue, rowId, cellId, cellField, cellObj, rowObj) {
        try {
            return app.dateTimeFormat(inValue);
        } catch (e) {
            console.error('ERROR IN dojoGrid1VrijemeDolaskaVoznjaFormat: ' + e);
        }
    },
    btnReportNaplaceneVoznjeClick: function(inSender) {
        try {
            var typeofdoc = "PDF";
            var src = "ReportNaplaceneVoznje.jasper";
            var mjestoPolaska = this.textMjePolaska.getDataValue();
            var mjestoDolaska = this.textMjeDolaska.getDataValue();
            var prezimeVozaca = this.textPrezVozac.getDataValue();
            var garazniBroj = this.textGarBroj.getDataValue();
            var vrijemePolaska = this.dateVrijemePolaska.getDataValue();
            var vrijemeDolaska = this.dateVrijemeDolaska.getDataValue();
            var sifraNarucitelja = this.textSifraNarucitelja.getDataValue();
            var nazivNarucitelja = this.textNazivNarucitelja.getDataValue();
            var oznakaVoznje = this.textFilterOznaka.getDataValue();

            var paramLogo = "../../resources/images/reports/logoReport.gif";
            var paramRideBack = "../../resources/images/reports/rideback.gif";
            var hash = {
                "typeDoc": typeofdoc,
                "file": src,
                "paramLogo": paramLogo,
                "mjestoPolaska": mjestoPolaska,
                "mjestoDolaska": mjestoDolaska,
                "prezimeVozaca": prezimeVozaca,
                "garazniBroj": garazniBroj,
                "vrijemePolaska": vrijemePolaska,
                "vrijemeDolaska": vrijemeDolaska,
                "sifraNarucitelja": sifraNarucitelja,
                "nazivNarucitelja": nazivNarucitelja,
                "oznakaVoznje": oznakaVoznje
            };
            this.serviceVarGetReportNaplaceneVoznje.input.setValue("parameters", hash);
            this.serviceVarGetReportNaplaceneVoznje.update();
        } catch (e) {
            console.error('ERROR IN btnReportNaplaceneVoznjeClick: ' + e);
        }
    },

    servicePrintIzvjestajResult: function(inSender, inDeprecated) {
        try {
            main.servicePrintIzvjestajResult(inSender, inDeprecated);
        } catch (e) {
            console.error('ERROR IN servicePrintIzvjestajResult: ' + e);
        }
    },
    picture1Click: function(inSender) {
        try {
            this.dateVrijemePolaska.setDataValue("");

        } catch (e) {
            console.error('ERROR IN picture1Click: ' + e);
        }
    },
    picture2Click: function(inSender) {
        try {
            this.dateVrijemeDolaska.setDataValue("");

        } catch (e) {
            console.error('ERROR IN picture2Click: ' + e);
        }
    },
    ///metoda za filtriranje sa vrmenskom zadrškom
    callServiceVarGetNaplaceneVoznje: function(){
       try {
          if (this._mytimeout) {
              window.clearTimeout(this._mytimeout);
          }          
          this.serviceVarNaplaceneVoznje.input.setValue('garazniBroj', this.textGarBroj.getDataValue());
          this.serviceVarNaplaceneVoznje.input.setValue('mjestoDolaska', this.textMjeDolaska.getDataValue());
          this.serviceVarNaplaceneVoznje.input.setValue('mjestoPolaska', this.textMjePolaska.getDataValue());
          this.serviceVarNaplaceneVoznje.input.setValue('prezimeVozaca', this.textPrezVozac.getDataValue());
          this.serviceVarNaplaceneVoznje.input.setValue('vrijemeDolaska', this.dateVrijemeDolaska.getDataValue());
          this.serviceVarNaplaceneVoznje.input.setValue('vrijemePolaska', this.dateVrijemePolaska.getDataValue());
          this.serviceVarNaplaceneVoznje.input.setValue('sifraNarucitelj', this.textSifraNarucitelja.getDataValue());
          this.serviceVarNaplaceneVoznje.input.setValue('nazivNarucitelj', this.textNazivNarucitelja.getDataValue());
          this.serviceVarNaplaceneVoznje.input.setValue('oznakaVoznje', this.textFilterOznaka.getDataValue());
          this._mytimeout = window.setTimeout(dojo.hitch(app, 'filterGetNaplaceneVoznje', this.serviceVarNaplaceneVoznje), app.filteriTimeOut);          
	  } catch(e) {
		  console.error('ERROR IN textMjestoDolaskaFilterChange: ' + e); 
	  }
  },
    textSifraNaruciteljaChange: function(inSender) {
	  try {		  
		  this.callServiceVarGetNaplaceneVoznje();
	  } catch(e) {
		  console.error('ERROR IN textSifraNaruciteljaChange: ' + e); 
	  } 
  },
  textNazivNaruciteljaChange: function(inSender) {
	  try {		  
		  this.callServiceVarGetNaplaceneVoznje();
	  } catch(e) {
		  console.error('ERROR IN textNazivNaruciteljaChange: ' + e); 
	  } 
  },
  textMjePolaskaChange: function(inSender) {
	  try {		  
		  this.callServiceVarGetNaplaceneVoznje();
	  } catch(e) {
		  console.error('ERROR IN textMjePolaskaChange: ' + e); 
	  } 
  },
  textMjeDolaskaChange: function(inSender) {
	  try {		  
		  this.callServiceVarGetNaplaceneVoznje();
	  } catch(e) {
		  console.error('ERROR IN textMjeDolaskaChange: ' + e); 
	  } 
  },
  textGarBrojChange: function(inSender) {
	  try {		  
		  this.callServiceVarGetNaplaceneVoznje();
	  } catch(e) {
		  console.error('ERROR IN textGarBrojChange: ' + e); 
	  } 
  },
  dateVrijemePolaskaChange: function(inSender) {
	  try {		  
		  this.callServiceVarGetNaplaceneVoznje();
	  } catch(e) {
		  console.error('ERROR IN dateVrijemePolaskaChange: ' + e); 
	  } 
  },
  dateVrijemeDolaskaChange: function(inSender) {
	  try {		  
		  this.callServiceVarGetNaplaceneVoznje();
	  } catch(e) {
		  console.error('ERROR IN dateVrijemeDolaskaChange: ' + e); 
	  } 
  },
  textPrezVozacChange: function(inSender) {
	  try {		  
		  this.callServiceVarGetNaplaceneVoznje();
	  } catch(e) {
		  console.error('ERROR IN textPrezVozacChange: ' + e); 
	  } 
  },
  textFilterOznakaChange: function(inSender) {
	  try {
		  this.callServiceVarGetNaplaceneVoznje();		  
	  } catch(e) {
		  console.error('ERROR IN textFilterOznakaChange: ' + e); 
	  } 
  },
  _end: 0
});