NaplaceneVoznje.widgets = {
	serviceVarNaplaceneVoznje: ["wm.ServiceVariable", {"operation":"getNaplaceneVoznje","service":"glavni"}, {}, {
		input: ["wm.ServiceInput", {"type":"getNaplaceneVoznjeInputs"}, {}]
	}],
	serviceVarGetReportNaplaceneVoznje: ["wm.ServiceVariable", {"operation":"getNaplaceneVoznjeReport","service":"Izvjestaji"}, {"onResult":"servicePrintIzvjestajResult"}, {
		input: ["wm.ServiceInput", {"type":"getNaplaceneVoznjeReportInputs"}, {}]
	}],
	layoutBox1: ["wm.Layout", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"100%"}, {}, {
		panel4: ["wm.Panel", {"height":"100px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
			panel6: ["wm.Panel", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"255px"}, {}, {
				textSifraNarucitelja: ["wm.Text", {"caption":"Šifra naručitelja:","captionSize":"120px","changeOnKey":true,"dataValue":undefined,"displayValue":"","resetButton":true,"width":"250px"}, {"onchange":"textSifraNaruciteljaChange"}],
				textNazivNarucitelja: ["wm.Text", {"caption":"Naziv naručitelja:","captionSize":"120px","changeOnKey":true,"dataValue":undefined,"displayValue":"","resetButton":true,"width":"250px"}, {"onchange":"textNazivNaruciteljaChange"}],
				textMjePolaska: ["wm.Text", {"caption":"Mjesto polaska:","captionSize":"120px","changeOnKey":true,"dataValue":undefined,"displayValue":"","resetButton":true,"width":"250px"}, {"onchange":"textMjePolaskaChange"}],
				textMjeDolaska: ["wm.Text", {"caption":"Mjesto dolaska:","captionSize":"120px","changeOnKey":true,"dataValue":undefined,"displayValue":"","resetButton":true,"width":"250px"}, {"onchange":"textMjeDolaskaChange"}]
			}],
			panel7: ["wm.Panel", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"279px"}, {}, {
				textGarBroj: ["wm.Text", {"caption":"Garažni broj vozila:","captionSize":"130px","changeOnKey":true,"dataValue":undefined,"displayValue":"","resetButton":true,"width":"275px"}, {"onchange":"textGarBrojChange"}],
				panel2: ["wm.Panel", {"height":"24px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"bottom","width":"100%"}, {}, {
					dateVrijemePolaska: ["wm.Date", {"caption":"Vrijeme polaska:","captionSize":"120px","dataValue":undefined,"displayValue":"","width":"250px"}, {"onchange":"dateVrijemePolaskaChange"}],
					picture1: ["wm.Picture", {"_classes":{"domNode":["wm_Mouse_pointer"]},"border":"0","height":"20px","source":"resources/images/Reset.png","width":"25px"}, {"onclick":"picture1Click"}]
				}],
				panel3: ["wm.Panel", {"height":"24px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"bottom","width":"100%"}, {}, {
					dateVrijemeDolaska: ["wm.Date", {"caption":"Vrijeme dolaska:","captionSize":"120px","dataValue":undefined,"displayValue":"","width":"250px"}, {"onchange":"dateVrijemeDolaskaChange"}],
					picture2: ["wm.Picture", {"_classes":{"domNode":["wm_Mouse_pointer"]},"border":"0","height":"20px","source":"resources/images/Reset.png","width":"25px"}, {"onclick":"picture2Click"}]
				}],
				textPrezVozac: ["wm.Text", {"caption":"Prezime vozača:","captionSize":"120px","changeOnKey":true,"dataValue":undefined,"displayValue":"","resetButton":true,"width":"275px"}, {"onchange":"textPrezVozacChange"}]
			}],
			panel5: ["wm.Panel", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"100%"}, {}, {
				textFilterOznaka: ["wm.Text", {"caption":"Oznaka vožnje:","captionSize":"120px","changeOnKey":true,"dataValue":undefined,"displayValue":"","resetButton":true,"width":"250px"}, {"onchange":"textFilterOznakaChange"}]
			}],
			panel1: ["wm.Panel", {"height":"100%","horizontalAlign":"right","verticalAlign":"top","width":"30px"}, {}, {
				btnReportNaplaceneVoznje: ["wm.Picture", {"_classes":{"domNode":["wm_Mouse_pointer"]},"border":"0","height":"24px","hint":"Ispis izvještaja","source":"resources/images/buttons/PDF.png","width":"30px"}, {"onclick":"btnReportNaplaceneVoznjeClick"}]
			}]
		}],
		fancyPanel1: ["wm.FancyPanel", {"title":"Naplaćene vožnje"}, {}, {
			dojoGrid1: ["wm.DojoGrid", {"columns":[{"id":"customField1","isCustomField":true,"expression":"${narucitelj.idnarucitelj}","show":true,"width":"100%","title":"Šifra naručitelja"},{"id":"customField","isCustomField":true,"expression":"${narucitelj.nazivNarucitelj}","show":true,"width":"100%","formatFunc":"","title":"Naručitelj"},{"show":false,"id":"idvoznja","title":"Idvoznja","width":"80px","displayType":"Number","noDelete":true,"align":"right","formatFunc":""},{"show":true,"id":"oznakaVoznja","title":"Oznaka vožnje","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"opisVoznja","title":"Opis","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"mjestoPolaskaVoznja","title":"Mjesto polaska","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"mjestoDolaskaVoznja","title":"Mjesto dolaska","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":""},{"show":true,"id":"vrijemePolaskaVoznja","title":"Vrijeme polaska","width":"80px","displayType":"Date","noDelete":true,"align":"left","formatFunc":"dojoGrid1VrijemePolaskaVoznjaFormat"},{"show":true,"id":"vrijemeDolaskaVoznja","title":"Vrijeme dolaska","width":"80px","displayType":"Date","noDelete":true,"align":"left","formatFunc":"dojoGrid1VrijemeDolaskaVoznjaFormat"},{"show":true,"id":"ugovorenaCijenaVoznja","title":"Ugovorena cijena","width":"100%","displayType":"Text","noDelete":true,"align":"left","formatFunc":"wm_currency_formatter"},{"id":"customField2","isCustomField":true,"expression":"${zatvorenavoznja.naplata}","show":true,"width":"100%","title":"Naplaćena cijena","formatFunc":"wm_currency_formatter"}],"margin":"4"}, {}, {
				binding: ["wm.Binding", {}, {}, {
					wire: ["wm.Wire", {"expression":undefined,"source":"serviceVarNaplaceneVoznje","targetProperty":"dataSet"}, {}]
				}]
			}]
		}]
	}]
}