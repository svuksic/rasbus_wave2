var slobodnaVozilaGrafickiPregledOtvoren = false;
var lVozila;
dojo.declare("DefiniranjeTerminaSlobodniVozila", wm.Page, {
    start: function() {
        try {

        } catch (e) {
            app.toastError(this.name + ".start() Failed: " + e.toString());
        }
    },

    btnGrafickiPrikazClick: function(inSender) {
        try {
            var pocetak = new Date(this.datePocetakVoznje.displayValue);
            var zavrsetak = new Date(this.dateKrajVoznje.displayValue);
            if (zavrsetak < pocetak) {
                alert("Pogrešan odnos datuma početka i kraja perioda!");
                return
            } else if (Math.floor(((zavrsetak / (1000 * 60 * 60 * 24)) - (pocetak / (1000 * 60 * 60 * 24)))) > 30) {
                alert("Moguće je odbrati period od najviše mjesec dana");
                return
            }

            if (slobodnaVozilaGrafickiPregledOtvoren === false) {
                slobodnaVozilaGrafickiPregledOtvoren = true;
                app.varNacinPregledaVozac.setValue("dataValue", "tablicniVozac");
                lVozila = new wm.Layer({
                    owner: main,
                    parent: main.tabLayers1,
                    caption: "Slobodna vozila",
                    width: "100%",
                    height: "100%",
                    closable: true,
                    name: "PregledGrafickiSlobodnaVozila",
                    onCloseOrDestroy: function okini(item) {
                        slobodnaVozilaGrafickiPregledOtvoren = false;
                        lVozila.destroy();
                        c.destroy();
                        //ovo tu jer se inca pozdaina cudno ponasa nakon gasenja
                        main.tabLayers1.getActiveLayer().setValue("width", "100px");
                        main.tabLayers1.getActiveLayer().setValue("height", "100%");
                    }
                });

                var c = new wm.PageContainer({
                    owner: main,
                    parent: lVozila,
                    width: "100%",
                    height: "100%",
                    pageName: "GrafickiPrikazVozilaSlobodna"
                });
            }
            main.tabLayers1.setLayer(lVozila);
        } catch (e) {
            console.error('ERROR IN btnGrafickiPrikazClick: ' + e);
        }
    },
    dateKrajVoznjeChange: function(inSender) {
        try {
            if (this.dateKrajVoznje.getDisplayValue() != "" && this.datePocetakVoznje.getDisplayValue() != "") {
                this.btnGrafickiPrikaz.enable();
            } else {
                this.btnGrafickiPrikaz.disable();
            }

        } catch (e) {
            console.error('ERROR IN dateKrajVoznjeChange: ' + e);
        }
    },
    datePocetakVoznjeChange: function(inSender) {
        try {
            if (this.datePocetakVoznje.getDisplayValue() != "" && this.dateKrajVoznje.getDisplayValue() != "") {
                this.btnGrafickiPrikaz.enable();
            } else {
                this.btnGrafickiPrikaz.disable();
            }

        } catch (e) {
            console.error('ERROR IN datePocetakVoznjeChange: ' + e);
        }
    },
    _end: 0
});