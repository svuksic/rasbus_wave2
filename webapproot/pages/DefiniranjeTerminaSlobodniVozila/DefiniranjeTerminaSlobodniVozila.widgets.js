DefiniranjeTerminaSlobodniVozila.widgets = {
	layoutBox1: ["wm.Layout", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"100%"}, {}, {
		panel6: ["wm.Panel", {"height":"374px","horizontalAlign":"center","layoutKind":"left-to-right","verticalAlign":"middle","width":"100%"}, {}, {
			panel5: ["wm.Panel", {"_classes":{"domNode":["wm_BorderBottomStyle_Curved4px","wm_BorderTopStyle_Curved4px","wm_BorderShadow_StrongShadow","wm_BackgroundColor_LightGray"]},"height":"143px","horizontalAlign":"left","margin":"2,0,0,2","verticalAlign":"top","width":"367px"}, {}, {
				spacer1: ["wm.Spacer", {"height":"9px","width":"350px"}, {}],
				datePocetakVoznje: ["wm.Date", {"caption":"Početak vožnje:","captionSize":"150px","dataValue":undefined,"displayValue":"","required":true,"useLocalTime":true,"width":"90%"}, {"onchange":"datePocetakVoznjeChange"}],
				spacer2: ["wm.Spacer", {"height":"12px","width":"350px"}, {}],
				dateKrajVoznje: ["wm.Date", {"caption":"Kraj vožnje:","captionSize":"150px","dataValue":undefined,"displayValue":"","required":true,"useLocalTime":true,"width":"90%"}, {"onchange":"dateKrajVoznjeChange"}],
				panel4: ["wm.Panel", {"height":"48px","horizontalAlign":"center","layoutKind":"left-to-right","verticalAlign":"middle","width":"100%"}, {}, {
					btnGrafickiPrikaz: ["wm.Button", {"_classes":{"domNode":["wm_FontColor_Black"]},"caption":"Generiraj prikaz","disabled":true,"margin":"4","width":"295px"}, {"onclick":"btnGrafickiPrikazClick"}]
				}]
			}]
		}]
	}]
}