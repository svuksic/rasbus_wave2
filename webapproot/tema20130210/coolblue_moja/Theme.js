{
	"wm.ToggleButton": {
		"border": "0", 
		"borderColor": "#2f96bf"
	}, 
	"wm.Button": {
		"border": "0", 
		"borderColor": "#2f96bf"
	}, 
	"wm.Layout": {
		"border": "2", 
		"borderColor": "#333333"
	}, 
	"wm.Bevel": {
		"bevelSize": "10", 
		"border": "1", 
		"borderColor": "#333333"
	}, 
	"wm.Splitter": {
		"bevelSize": "10", 
		"border": "1", 
		"borderColor": "#333333"
	}, 
	"wm.AccordionDecorator": {
		"captionBorder": "1", 
		"captionBorderColor": "#2f96bf"
	}, 
	"wm.AccordionLayers": {
		"border": "0", 
		"borderColor": "#2c6090", 
		"captionBorder": "2", 
		"layerBorder": "0", 
		"captionHeight": "30"
	}, 
	"wm.FancyPanel": {
		"margin": "2", 
		"border": "0", 
		"borderColor": "#4284c1", 
		"innerBorder": "1", 
		"labelHeight": "24"
	}, 
	"wm.TabLayers": {
		"layersType": "Tabs", 
		"margin": "0,2,0,2", 
		"clientBorder": "1", 
		"border": "0", 
		"isMajorContent": 1, 
		"clientBorderColor": "#4284c1", 
		"headerHeight": "29px", 
		"borderColor": "#999999"
	}, 
	"wm.WizardLayers": {
		"margin": "0,2,0,2", 
		"border": "0", 
		"clientBorder": "0", 
		"isMajorContent": 1, 
		"clientBorderColor": "#2c6090", 
		"borderColor": "#4284c1"
	}, 
	"wm.Layer": {
		"margin": "2,0,2,0"
	}, 
	"wm.Dialog": {
		"border": "1", 
		"borderColor": "#1f7197", 
		"titlebarBorder": "0,0,1,0", 
		"titlebarBorderColor": "#333333", 
		"containerClass": "Document", 
		"titlebarHeight": "22"
	}, 
	"wm.GenericDialog": {
		"border": "1", 
		"borderColor": "#1f7197", 
		"titlebarBorder": "0,0,1,0", 
		"titlebarBorderColor": "#333333", 
		"footerBorder": "1,0,0,0", 
		"footerBorderColor": "#333333", 
		"containerClass": "Document"
	}, 
	"wm.RichTextDialog": {
		"border": "1", 
		"borderColor": "#1f7197", 
		"titlebarBorder": "0,0,1,0", 
		"titlebarBorderColor": "#333333", 
		"footerBorder": "1,0,0,0", 
		"footerBorderColor": "#333333", 
		"containerClass": "Document"
	}, 
	"wm.PageDialog": {
		"border": "1", 
		"borderColor": "#1f7197", 
		"titlebarBorder": "0,0,1,0", 
		"titlebarBorderColor": "#333333", 
		"footerBorder": "1,0,0,0", 
		"footerBorderColor": "#333333", 
		"noBevel": true, 
		"containerClass": "MainContent"
	}, 
	"wm.DesignableDialog": {
		"border": "1", 
		"borderColor": "#1f7197", 
		"titlebarBorder": "0,0,1,0", 
		"titlebarBorderColor": "#333333", 
		"footerBorder": "1,0,0,0", 
		"footerBorderColor": "#333333", 
		"containerClass": "MainContent"
	}, 
	"wm.DojoMenu": {
		"padding": "0", 
		"border": "0", 
		"borderColor": "#333333"
	}, 
	"wm.List": {
		"margin": "0,2,0,2", 
		"border": "0", 
		"borderColor": "#333333"
	}, 
	"wm.dijit.ProgressBar": {
		"border": "0", 
		"borderColor": "#333333"
	}, 
	"wm.RichText": {
		"border": "0", 
		"borderColor": "#333333"
	}, 
	"wm.RoundedButton": {
		"border": "0", 
		"borderColor": "#333333"
	}, 
	"wm.DataGrid": {
		"border": "0", 
		"borderColor": "#333333"
	}, 
	"wm.Label": {
	}, 
	"wm.Picture": {
	}, 
	"wm.Spacer": {
	}, 
	"wm.Layers": {
		"border": "0"
	}, 
	"wm.PageContainer": {
	}, 
	"wm.Panel": {
		"borderColor": "#2c6090", 
		"border": "0"
	}, 
	"wm.CheckBoxEditor": {
	}, 
	"wm.CurrencyEditor": {
	}, 
	"wm.Text": {
		"border": "0", 
		"borderColor": "#fbfbfb"
	}, 
	"wm.SelectMenu": {
		"border": "0"
	}, 
	"wm.dijit.Calendar": {
		"borderColor": "#FBFBFB"
	}, 
	"wm.DojoGrid": {
		"border": "0", 
		"borderColor": "#2f96bf"
	}, 
	"wm.Control": {
		"borderColor": "#333333"
	}, 
	"wm.BusyButton": {
		"border": "0", 
		"borderColor": "#2f96bf"
	}, 
	"wm.Checkbox": {
		"border": "0"
	}, 
	"wm.ColorPicker": {
		"border": "0"
	}, 
	"wm.Currency": {
		"border": "0"
	}, 
	"wm.Date": {
		"border": "0"
	}, 
	"wm.Number": {
		"border": "0"
	}, 
	"wm.RadioButton": {
		"border": "0"
	}, 
	"wm.Slider": {
		"border": "0"
	}, 
	"wm.LargeTextArea": {
		"border": "0"
	}, 
	"wm.Time": {
		"border": "0"
	}, 
	"wm.WidgetsJsDialog": {
		"containerClass": "Document", 
		"border": "1", 
		"borderColor": "#1f7197"
	}, 
	"wm.FileUploadDialog": {
		"containerClass": "Document", 
		"border": "1", 
		"borderColor": "#1f7197"
	}, 
	"wm.ColorPickerDialog": {
		"border": "1", 
		"borderColor": "#1f7197"
	}, 
	"wm.MainContentPanel": {
		"border": "0", 
		"borderColor": "#205690"
	}, 
	"wm.HeaderContentPanel": {
		"border": "0", 
		"borderColor": "#000000"
	}, 
	"wm.EmphasizedContentPanel": {
		"border": "0", 
		"borderColor": "#000000"
	}, 
	"wm.WidgetList": {
	}, 
	"wm.PopupMenuButton": {
	}, 
	"wm.JsonStatus": {
	}, 
	"wm.PopupMenu": {
	}, 
	"wm.LoadingDialog": {
		"border": "0"
	}, 
	"wm.LiveForm": {
	}, 
	"wm.RelatedEditor": {
	}, 
	"wm.SimpleForm": {
	}, 
	"wm.DateTime": {
	}, 
	"wm.Lookup": {
	}, 
	"wm.FilteringLookup": {
	}, 
	"wm.DojoFileUpload": {
	}, 
	"wm.DojoFlashFileUpload": {
	}, 
	"wm.DojoGauge": {
		"borderColor": "#FBFBFB"
	}, 
	"wm.ListViewer": {
	}, 
	"wm.DijitDesigner": {
	}, 
	"wm.gadget.GoogleMap": {
	}
}