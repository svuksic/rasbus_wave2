IzvjestajRaspVoznji.widgets = {
	printSV: ["wm.ServiceVariable", {"operation":"getReport","service":"Izvjestaji"}, {"onResult":"printSVResult"}, {
		input: ["wm.ServiceInput", {"type":"getReportInputs"}, {}]
	}],
	layoutBox1: ["wm.Layout", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"100%"}, {}, {
		panel1: ["wm.Panel", {"height":"38px","horizontalAlign":"center","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}],
		panel2: ["wm.Panel", {"height":"622px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
			panel3: ["wm.Panel", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"50px"}, {}],
			panel4: ["wm.Panel", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"703px"}, {}, {
				panel5: ["wm.Panel", {"height":"55px","horizontalAlign":"center","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
					picture1: ["wm.Picture", {"border":"0","height":"81px","source":"resources/images/print.gif","width":"56px"}, {"onclick":"picture1Click"}]
				}],
				panel6: ["wm.Panel", {"height":"32px","horizontalAlign":"center","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
					btnPregled: ["wm.Button", {"caption":"Pregled","margin":"4"}, {"onclick":"btnPregledClick"}]
				}],
				html1: ["wm.Html", {"border":"0","height":"529px"}, {}]
			}]
		}]
	}]
}