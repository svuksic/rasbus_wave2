dojo.declare("IzvjestajRaspVoznji", wm.Page, {
    start: function() {
        try {


        } catch (e) {
            app.toastError(this.name + ".start() Failed: " + e.toString());
        }
    },
    btnPregledClick: function(inSender) {
        try {
            var typeofdoc = "PDF";
            var src = "IzvjestajRaspVozaca.jasper";
            var parLogo = "/com/bus.gif";
            var parSlika = "/com/bus2.gif";
            var hash = {
                "typeDoc": typeofdoc,
                "file": src,
                "parm_Logo": parLogo,
                "parmSlika": parSlika
            };
            this.printSV.input.setValue("parameters", hash);
            this.printSV.update();

        } catch (e) {
            console.error('ERROR IN btnPregledClick: ' + e);
        }
    },
    picture1Click: function(inSender) {
        try {
            frames['reportes1'].print();

        } catch (e) {
            console.error('ERROR IN picture1Click: ' + e);
        }
    },
    printSVResult: function(inSender, inDeprecated) {
        try {
            var win = window.open("Izvještaj_vožnje", "Ispis svih vožnji");
            var doc = win.document;
            doc.open("text/html");
            doc.write("<iframe width='100%' height='100%' name='reportes1' id='reportes1' src='" + window.location.protocol + "//" + window.location.hostname + ":" + window.location.port + "/" + wm.application.type + "/" + inDeprecated + "'>");
            doc.close();
        } catch (e) {
            console.error('ERROR IN printSVResult: ' + e);
        }
    },
    _end: 0
});