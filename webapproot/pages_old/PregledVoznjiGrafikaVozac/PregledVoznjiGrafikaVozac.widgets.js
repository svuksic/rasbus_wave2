PregledVoznjiGrafikaVozac.widgets = {
	serviceVariableGetGraphicsVozac: ["wm.ServiceVariable", {"operation":"getGrafickiPrikazVozac","service":"glavni"}, {"onSuccess":"serviceVariableGetGraphicsVozacSuccess"}, {
		input: ["wm.ServiceInput", {"type":"getGrafickiPrikazVozacInputs"}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire: ["wm.Wire", {"expression":undefined,"source":"app.varDatumPocetkaPerioda.dataValue","targetProperty":"datumPocetka"}, {}],
				wire1: ["wm.Wire", {"expression":undefined,"source":"app.varListaOdabranihVozacaID.dataValue","targetProperty":"listaOdabranihVozaca"}, {}],
				wire2: ["wm.Wire", {"expression":undefined,"source":"app.varPeriodOdabrani.dataValue","targetProperty":"periodPar"}, {}]
			}]
		}]
	}],
	layoutBox1: ["wm.Layout", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"100%"}, {}, {
		htmlGrafickiPrikazVozac: ["wm.Html", {"border":"0","height":"100%","html":"<div id=\"povrsinaVozac\" style=\"background:#F0F0F0; height:100%; width:100%\" overflow=\"\" :=\"\" auto;=\"\"></div>","imageList":""}, {}]
	}]
}