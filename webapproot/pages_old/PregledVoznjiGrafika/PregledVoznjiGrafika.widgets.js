PregledVoznjiGrafika.widgets = {
	serviceVariableGetGraphics: ["wm.ServiceVariable", {"operation":"getGrafickiPrikaz","service":"glavni"}, {"onSuccess":"serviceVariableGetGraphicsSuccess"}, {
		input: ["wm.ServiceInput", {"type":"getGrafickiPrikazInputs"}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire: ["wm.Wire", {"expression":undefined,"source":"app.varDatumPocetkaPerioda.dataValue","targetProperty":"datumPocetka"}, {}],
				wire2: ["wm.Wire", {"expression":undefined,"source":"app.varPeriod.dataValue","targetProperty":"period"}, {}],
				wire1: ["wm.Wire", {"expression":undefined,"source":"app.varListaOdabranihBusevaId.dataValue","targetProperty":"listaOdabranihBuseva"}, {}],
				wire3: ["wm.Wire", {"expression":undefined,"source":"app.varPeriodOdabrani.dataValue","targetProperty":"periodPar"}, {}]
			}]
		}]
	}],
	layoutBoxGrafickiPrikaz: ["wm.Layout", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"100%"}, {}, {
		htmlGrafickiPrikaz: ["wm.Html", {"border":"0","height":"100%","html":"<div id=\"povrsina\" style=\"background:#F0F0F0; height:100%; width:100%\" overflow : auto;></div>"}, {}]
	}]
}