-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: rasbus
-- ------------------------------------------------------
-- Server version	5.5.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bus`
--

DROP TABLE IF EXISTS `bus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bus` (
  `idBus` int(11) NOT NULL AUTO_INCREMENT,
  `GARAZNI_BROJ_BUS` varchar(45) DEFAULT NULL,
  `MODEL_BUS` varchar(45) DEFAULT NULL,
  `MARKA_BUS` varchar(45) DEFAULT NULL,
  `BROJ_SJEDALA_BUS` int(11) DEFAULT NULL,
  `DATUM_KUPNJE_BUS` datetime DEFAULT NULL,
  `DATUM_ZADNJE_REGISTRACIJE_BUS` datetime DEFAULT NULL,
  `DATUM_ZADNJEG_SERVISA_BUS` datetime DEFAULT NULL,
  `NAPOMENA_BUS` longtext,
  PRIMARY KEY (`idBus`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bus`
--

LOCK TABLES `bus` WRITE;
/*!40000 ALTER TABLE `bus` DISABLE KEYS */;
INSERT INTO `bus` VALUES (12,'0001','DAF','DAF',45,'2012-12-14 00:00:00','2012-12-14 00:00:00','2012-12-14 00:00:00','Dobar bus'),(13,'0002','VOLVO','VOLVO',46,'2012-12-14 00:00:00','2012-12-14 00:00:00','2012-12-14 00:00:00','Dobro grijanje'),(14,'0003','SETRA','SETRA',47,'2012-12-14 00:00:00','2012-12-14 00:00:00','2012-12-14 00:00:00','Najkvalitetniji meÄ‘u mojim busevima');
/*!40000 ALTER TABLE `bus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `narucitelj`
--

DROP TABLE IF EXISTS `narucitelj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `narucitelj` (
  `idnarucitelj` int(11) NOT NULL AUTO_INCREMENT,
  `NAZIV_NARUCITELJ` varchar(45) DEFAULT NULL,
  `ADRESA_NARUCITELJ` varchar(45) DEFAULT NULL,
  `ODGOVORNA_OSBA_NARUCITELJ` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idnarucitelj`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `narucitelj`
--

LOCK TABLES `narucitelj` WRITE;
/*!40000 ALTER TABLE `narucitelj` DISABLE KEYS */;
INSERT INTO `narucitelj` VALUES (4,'IV OŠ  Varaždin','Nepoznata bb','Ravnatelj'),(5,'Planinarsko društov Ivančica','Ivančica bb','Planinar'),(6,'Boxmark','BB','Direktor'),(7,'UNIVERZALNI','NEMA','NEMA'),(8,'TESTNI','TESTNA','TEST');
/*!40000 ALTER TABLE `narucitelj` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `idrole` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) DEFAULT NULL,
  `opis` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idrole`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'admin','Ima sve ovlasti'),(2,'user','Samo određene ovlasti'),(3,'gost','gost');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `ime` varchar(45) DEFAULT NULL,
  `prezime` varchar(45) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  PRIMARY KEY (`iduser`),
  KEY `userRoles` (`role`),
  CONSTRAINT `userRoles` FOREIGN KEY (`role`) REFERENCES `role` (`idrole`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (2,'Saša','Vukšić','svuksic','svuksic',2),(3,'Korisnik','Korisnik','korisnik','korisnik',2),(5,'Saša','Vukšić','admin','admin',1),(6,'a','a','a','a',1),(7,'b','b','b','c',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vozac`
--

DROP TABLE IF EXISTS `vozac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vozac` (
  `idvozac` int(11) NOT NULL AUTO_INCREMENT,
  `IME_VOZAC` varchar(45) DEFAULT NULL,
  `PREZIME_VOZAC` varchar(45) DEFAULT NULL,
  `DATUM_RODJENJA_VOZAC` date DEFAULT NULL,
  `DATUM_VOZACKA_VOZAC` date DEFAULT NULL,
  `DATUM_PUTOVNICA_VOZAC` date DEFAULT NULL,
  `DATUM_OSOBNA_VOZAC` date DEFAULT NULL,
  PRIMARY KEY (`idvozac`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vozac`
--

LOCK TABLES `vozac` WRITE;
/*!40000 ALTER TABLE `vozac` DISABLE KEYS */;
INSERT INTO `vozac` VALUES (7,'Saša','Vukšić','2012-12-14','2012-12-14','2012-12-14','2012-12-14'),(8,'Eustahije','Brzić','2012-12-14','2012-12-14','2012-12-14','2012-12-13'),(9,'Novi','Novi','2012-12-14','2012-12-14','2012-12-14','2012-12-14'),(10,'TEST','TEST','2012-12-17','2013-11-14','2013-01-17','2013-01-25'),(11,'Željko','Vukšić','2012-12-22','2012-12-22','2012-12-22','2012-12-30');
/*!40000 ALTER TABLE `vozac` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `voznja`
--

DROP TABLE IF EXISTS `voznja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voznja` (
  `idvoznja` int(11) NOT NULL AUTO_INCREMENT,
  `OZNAKA_VOZNJA` varchar(45) DEFAULT NULL,
  `OPIS_VOZNJA` longtext,
  `MJESTO_POLASKA_VOZNJA` varchar(45) DEFAULT NULL,
  `MJESTO_DOLASKA_VOZNJA` varchar(45) DEFAULT NULL,
  `VRIJEME_POLASKA_VOZNJA` datetime DEFAULT NULL,
  `VRIJEME_DOLASKA_VOZNJA` datetime DEFAULT NULL,
  `VOZAC_VOZNJA` int(11) DEFAULT NULL,
  `BUS_VOZNJA` int(11) DEFAULT NULL,
  `NARUCITELJ_VOZNJA` int(11) DEFAULT NULL,
  `UGOVORENA_CIJENA_VOZNJA` varchar(45) DEFAULT NULL,
  `ZATVORENA_VOZNJA_VOZNJA` int(11) DEFAULT NULL,
  PRIMARY KEY (`idvoznja`),
  KEY `VOZAC_VOZNJA_fk` (`VOZAC_VOZNJA`),
  KEY `BUS_VOZNJA_fk` (`BUS_VOZNJA`),
  KEY `NARUCITELJ_VOZNJA_fk` (`NARUCITELJ_VOZNJA`),
  KEY `ZATVORENA_VOZNJA_fk` (`ZATVORENA_VOZNJA_VOZNJA`),
  CONSTRAINT `BUS_VOZNJA_fk` FOREIGN KEY (`BUS_VOZNJA`) REFERENCES `bus` (`idBus`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `NARUCITELJ_VOZNJA_fk` FOREIGN KEY (`NARUCITELJ_VOZNJA`) REFERENCES `narucitelj` (`idnarucitelj`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `VOZAC_VOZNJA_fk` FOREIGN KEY (`VOZAC_VOZNJA`) REFERENCES `vozac` (`idvozac`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `ZATVORENA_VOZNJA_fk` FOREIGN KEY (`ZATVORENA_VOZNJA_VOZNJA`) REFERENCES `zatvorenavoznja` (`idzatvorenaVoznja`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=160 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `voznja`
--

LOCK TABLES `voznja` WRITE;
/*!40000 ALTER TABLE `voznja` DISABLE KEYS */;
INSERT INTO `voznja` VALUES (135,'0001','Izlet','VŽ','ZG','2012-12-14 03:00:00','2012-12-14 06:00:00',8,12,4,'480.00',135),(148,'0002','Izlet','VŽ','ZG','2012-12-18 02:00:00','2012-12-18 04:00:00',10,13,4,'450.00',148),(149,'0003','Dalje je sve moguÄ‡e','PU','RI','2012-12-18 07:00:00','2012-12-18 11:00:00',7,14,5,'850',149),(150,'0004','Idemo na izlet, potrebno se zaustavljati svakih pola sata','ZD','GS','2012-12-19 01:00:00','2012-12-19 03:00:00',8,13,5,'745',150),(151,'0005','Putovanjeee','Split','Dubrovnik','2012-12-19 10:00:00','2012-12-19 14:00:00',8,14,NULL,'450',151),(152,'0006','Putovanje hrvatskom','OS','ZG','2012-12-20 04:00:00','2012-12-20 09:00:00',9,13,NULL,'450',152),(153,'0007','nema napomene','zg','Zaprešić','2012-12-20 07:00:00','2012-12-20 10:00:00',7,14,7,'785.00',153),(154,'fs','sad','sd','sd','2012-12-01 01:00:00','2012-12-14 01:00:00',7,12,5,'458',154),(155,'0010','Test1','BJ','ZG','2012-12-24 01:00:00','2012-12-24 03:00:00',8,13,7,'450.00',155),(156,'0011','Test2','ZD','OS','2012-12-26 01:00:00','2012-12-26 03:00:00',8,13,7,'25.00',156),(157,'0015','Izelt u Ivanec','VŽ','Ivanec','2012-12-26 05:00:00','2012-12-26 14:00:00',7,13,5,'1450.00',157),(158,'0016','Koncert','VŽ','ČK','2012-12-28 04:00:00','2012-12-28 07:00:00',8,12,7,'450.00',NULL),(159,'0017','Koncert2','VŽ','ZG','2012-12-28 01:00:00','2012-12-28 04:00:00',9,14,7,'850.00',159);
/*!40000 ALTER TABLE `voznja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zatvorenavoznja`
--

DROP TABLE IF EXISTS `zatvorenavoznja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zatvorenavoznja` (
  `idzatvorenaVoznja` int(11) NOT NULL,
  `kilometri` double DEFAULT NULL,
  `troskovi` double DEFAULT NULL,
  `naplata` double DEFAULT NULL,
  PRIMARY KEY (`idzatvorenaVoznja`),
  KEY `voznja` (`idzatvorenaVoznja`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zatvorenavoznja`
--

LOCK TABLES `zatvorenavoznja` WRITE;
/*!40000 ALTER TABLE `zatvorenavoznja` DISABLE KEYS */;
INSERT INTO `zatvorenavoznja` VALUES (135,4587,0,7800),(148,45,2500,2600),(149,196,470,520),(150,458,1254,4587),(151,189,4500,4800),(152,350,850,NULL),(153,458,2541,2800),(154,25,250,NULL),(155,458,458,780.45),(156,45,500,NULL),(157,100,350,500),(159,450,500,445);
/*!40000 ALTER TABLE `zatvorenavoznja` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-01-29 21:20:49
