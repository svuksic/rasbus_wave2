package izvjestaji;

import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRTextExporter;
import net.sf.jasperreports.engine.export.JRTextExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.util.JRLoader;

import org.hibernate.Session;

import com.rasbusdb.RasbusDB;
import com.wavemaker.runtime.RuntimeAccess;

public class IzvjestajiFacade {
	
	public String getReport(@SuppressWarnings("rawtypes") Map parameters) throws Exception {

        try {
            @SuppressWarnings("deprecation")
			RasbusDB service = (RasbusDB) RuntimeAccess.getInstance().getService(RasbusDB.class);
            service.begin();
            Session session = service.getDataServiceManager().getSession();

            String fileName = String.valueOf(parameters.get("file"));
            String typeDocument = String.valueOf(parameters.get("typeDoc"));


            //URL fileUrl = this.getClass().getResource("/reportfiles/"+fileName);
            String jasperdatoteka = RuntimeAccess.getInstance().getSession().getServletContext().getRealPath("/reportfiles/"+fileName);
            
            @SuppressWarnings("deprecation")
			JasperReport report2 = (JasperReport) JRLoader.loadObject(jasperdatoteka);
            @SuppressWarnings({ "deprecation", "unchecked" })
			JasperPrint jasperPrint = JasperFillManager.fillReport(report2, parameters, session.connection());

            String pathWR = RuntimeAccess.getInstance().getSession().getServletContext().getRealPath("/myreports/");
            //String pathWR = this.getClass().getResource("/myreports/").toString();

            if (typeDocument.equals("PDF")) {
                fileName = fileName + ".pdf";
                JRPdfExporter exporter = new JRPdfExporter();
                exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, pathWR + "/" + fileName);
                exporter.exportReport();
            }
    else if(typeDocument.equals("HTML")){
      fileName=fileName+".html";
      JRHtmlExporter exporter = new JRHtmlExporter(); 
      exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint); 
      exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, pathWR+"/"+fileName);  
      exporter.exportReport(); 
    }
    else if(typeDocument.equals("XLS")){
      fileName=fileName+".xls";
      JRXlsExporter exporter = new JRXlsExporter(); 
      exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint); 
      exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, pathWR+"/"+fileName);  
      exporter.exportReport(); 
    }
    else if(typeDocument.equals("RTF")){
      fileName=fileName+".rtf";
      JRRtfExporter exporter = new JRRtfExporter(); 
      exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint); 
      exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, pathWR+"/"+fileName);  
      exporter.exportReport(); 
    }
    else if(typeDocument.equals("TXT")){
      fileName=fileName+".txt";
      JRExporter exporter = new JRTextExporter();
      Integer pageHeight=new Integer(jasperPrint.getPageHeight()); 
      Integer pageWidth = new Integer(jasperPrint.getPageWidth());
      exporter.setParameter(JRTextExporterParameter.JASPER_PRINT, jasperPrint); 
      exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, pathWR+"/"+fileName);  
      exporter.setParameter(JRTextExporterParameter.PAGE_WIDTH,pageWidth);
      exporter.setParameter(JRTextExporterParameter.PAGE_HEIGHT,pageHeight);
      exporter.setParameter(JRTextExporterParameter.CHARACTER_WIDTH,new Integer(11));
      exporter.setParameter(JRTextExporterParameter.CHARACTER_HEIGHT,new Integer(11));
      exporter.setParameter(
        JRTextExporterParameter.BETWEEN_PAGES_TEXT,
        new String("___________________________________________________________")
      );
     
      exporter.exportReport();
    }
    System.out.println(pathWR+fileName);
            return "myreports/" + fileName;

        }
        catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
    
    public String getReportNoDataSource(@SuppressWarnings("rawtypes") Map parameters) throws Exception {

        try {
            String fileName = String.valueOf(parameters.get("file"));
            String typeDocument = String.valueOf(parameters.get("typeDoc"));


            //URL fileUrl = this.getClass().getResource("/reportfiles/"+fileName);
            String jasperdatoteka = RuntimeAccess.getInstance().getSession().getServletContext().getRealPath("/reportfiles/"+fileName);
            
            @SuppressWarnings("deprecation")
			JasperReport report2 = (JasperReport) JRLoader.loadObject(jasperdatoteka);
            @SuppressWarnings({ "unchecked" })
			JasperPrint jasperPrint = JasperFillManager.fillReport(report2, parameters, new JREmptyDataSource());

            String pathWR = RuntimeAccess.getInstance().getSession().getServletContext().getRealPath("/myreports/");
            //String pathWR = this.getClass().getResource("/myreports/").toString();

            if (typeDocument.equals("PDF")) {
                fileName = fileName + ".pdf";
                JRPdfExporter exporter = new JRPdfExporter();
                exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, pathWR + "/" + fileName);
                exporter.exportReport();
            }
    else if(typeDocument.equals("HTML")){
      fileName=fileName+".html";
      JRHtmlExporter exporter = new JRHtmlExporter(); 
      exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint); 
      exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, pathWR+"/"+fileName);  
      exporter.exportReport(); 
    }
    else if(typeDocument.equals("XLS")){
      fileName=fileName+".xls";
      JRXlsExporter exporter = new JRXlsExporter(); 
      exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint); 
      exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, pathWR+"/"+fileName);  
      exporter.exportReport(); 
    }
    else if(typeDocument.equals("RTF")){
      fileName=fileName+".rtf";
      JRRtfExporter exporter = new JRRtfExporter(); 
      exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint); 
      exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, pathWR+"/"+fileName);  
      exporter.exportReport(); 
    }
    else if(typeDocument.equals("TXT")){
      fileName=fileName+".txt";
      JRExporter exporter = new JRTextExporter();
      Integer pageHeight=new Integer(jasperPrint.getPageHeight()); 
      Integer pageWidth = new Integer(jasperPrint.getPageWidth());
      exporter.setParameter(JRTextExporterParameter.JASPER_PRINT, jasperPrint); 
      exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, pathWR+"/"+fileName);  
      exporter.setParameter(JRTextExporterParameter.PAGE_WIDTH,pageWidth);
      exporter.setParameter(JRTextExporterParameter.PAGE_HEIGHT,pageHeight);
      exporter.setParameter(JRTextExporterParameter.CHARACTER_WIDTH,new Integer(11));
      exporter.setParameter(JRTextExporterParameter.CHARACTER_HEIGHT,new Integer(11));
      exporter.setParameter(
        JRTextExporterParameter.BETWEEN_PAGES_TEXT,
        new String("___________________________________________________________")
      );
     
      exporter.exportReport();
    }
    System.out.println(pathWR+fileName);
            return "myreports/" + fileName;

        }
        catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
    
    public String getReportDODataSource(@SuppressWarnings("rawtypes") Map parameters, JRDataSource dataSource) throws Exception {

        try {

            String fileName = String.valueOf(parameters.get("file"));
            String typeDocument = String.valueOf(parameters.get("typeDoc"));


            System.out.println("*x*");
            //URL fileUrl = this.getClass().getResource("/reportfiles/"+fileName);
            String jasperdatoteka = RuntimeAccess.getInstance().getSession().getServletContext().getRealPath("/reportfiles/"+fileName);
            System.out.println(jasperdatoteka+"*xx*");
            @SuppressWarnings("deprecation")
			JasperReport report2 = (JasperReport) JRLoader.loadObject(jasperdatoteka);
            @SuppressWarnings({ "unchecked" })
			JasperPrint jasperPrint = JasperFillManager.fillReport(report2, parameters, dataSource);
            System.out.println("*xxx*");
            String pathWR = RuntimeAccess.getInstance().getSession().getServletContext().getRealPath("/myreports/");
            //String pathWR = this.getClass().getResource("/myreports/").toString();

            if (typeDocument.equals("PDF")) {
                fileName = fileName + ".pdf";
                JRPdfExporter exporter = new JRPdfExporter();
                exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, pathWR + "/" + fileName);
                exporter.exportReport();
            }
    else if(typeDocument.equals("HTML")){
      fileName=fileName+".html";
      JRHtmlExporter exporter = new JRHtmlExporter(); 
      exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint); 
      exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, pathWR+"/"+fileName);  
      exporter.exportReport(); 
    }
    else if(typeDocument.equals("XLS")){
      fileName=fileName+".xls";
      JRXlsExporter exporter = new JRXlsExporter(); 
      exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint); 
      exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, pathWR+"/"+fileName);  
      exporter.exportReport(); 
    }
    else if(typeDocument.equals("RTF")){
      fileName=fileName+".rtf";
      JRRtfExporter exporter = new JRRtfExporter(); 
      exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint); 
      exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, pathWR+"/"+fileName);  
      exporter.exportReport(); 
    }
    else if(typeDocument.equals("TXT")){
      fileName=fileName+".txt";
      JRExporter exporter = new JRTextExporter();
      Integer pageHeight=new Integer(jasperPrint.getPageHeight()); 
      Integer pageWidth = new Integer(jasperPrint.getPageWidth());
      exporter.setParameter(JRTextExporterParameter.JASPER_PRINT, jasperPrint); 
      exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, pathWR+"/"+fileName);  
      exporter.setParameter(JRTextExporterParameter.PAGE_WIDTH,pageWidth);
      exporter.setParameter(JRTextExporterParameter.PAGE_HEIGHT,pageHeight);
      exporter.setParameter(JRTextExporterParameter.CHARACTER_WIDTH,new Integer(11));
      exporter.setParameter(JRTextExporterParameter.CHARACTER_HEIGHT,new Integer(11));
      exporter.setParameter(
        JRTextExporterParameter.BETWEEN_PAGES_TEXT,
        new String("___________________________________________________________")
      );
     
      exporter.exportReport();
    }
    System.out.println(pathWR+fileName);
            return "myreports/" + fileName;

        }
        catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }

}
