package izvjestaji;

import glavniServis.ServletMain;

import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;

import com.rasbusdb.RasbusDB;
import com.rasbusdb.data.Bus;
import com.rasbusdb.data.Narucitelj;
import com.rasbusdb.data.Vozac;
import com.rasbusdb.data.Voznja;
import com.wavemaker.runtime.RuntimeAccess;

public class Izvjestaji extends com.wavemaker.runtime.javaservice.JavaServiceSuperClass {

	/**ova je ostala samo da mogu još neko vrijeme raditi postojeci izvjestaji sa vlastitim upitom, datasourceom
	 * 
	 * @param parameters
	 * @return
	 * @throws Exception
	 */
	public String getReport(@SuppressWarnings("rawtypes") Map parameters) throws Exception {
		String result = "";
		try {
			IzvjestajiFacade izFacade = (IzvjestajiFacade) RuntimeAccess.getInstance().getSpringBean("IzvjestajiFacade");
			result = izFacade.getReport(parameters);
			return result;
		}
		catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
    
	public String getBusReport(@SuppressWarnings("rawtypes") Map parameters){
		String reportPath = "";		
		IzvjestajiFacade izFacade = (IzvjestajiFacade) RuntimeAccess.getInstance().getSpringBean("IzvjestajiFacade");

		String paramGarazniBroj = String.valueOf(parameters.get("paramGarazniBroj"));
		String hql = " select b from Bus b ";
		if(StringUtils.isNotEmpty(paramGarazniBroj) && paramGarazniBroj != null && !paramGarazniBroj.equals("null")){
			hql += " where b.garazniBrojBus like '%"+paramGarazniBroj+"%'";
		}		
		RasbusDB dbFacade = (RasbusDB) RuntimeAccess.getInstance().getServiceBean("rasbusDB");
		dbFacade.begin();
		Session session = dbFacade.getDataServiceManager().getSession();
		Query query = session.createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Bus> listaBuseva = query.list();
		dbFacade.commit();		
		try {
			reportPath = izFacade.getReportDODataSource(parameters, new JRBeanCollectionDataSource(listaBuseva));
		} catch (Exception e) {
			dbFacade.rollback();
			e.printStackTrace();
		}
		return reportPath;		
	}
	
	public String getVozaciReport(@SuppressWarnings("rawtypes") Map parameters){
		String reportPath = "";		
		IzvjestajiFacade izFacade = (IzvjestajiFacade) RuntimeAccess.getInstance().getSpringBean("IzvjestajiFacade");
		
		String paramPrezime = String.valueOf(parameters.get("paramPrezime"));
		String hql = " select v from Vozac v ";
		if(StringUtils.isNotEmpty(paramPrezime) && paramPrezime != null && !paramPrezime.equals("null")){
			hql += " where v.prezimeVozac like '%"+paramPrezime+"%'";
		}
		
		RasbusDB dbFacade = (RasbusDB) RuntimeAccess.getInstance().getServiceBean("rasbusDB");
		dbFacade.begin();
		Session session = dbFacade.getDataServiceManager().getSession();
		Query query = session.createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Vozac> listaVozaca = query.list();
		dbFacade.commit();		
		try {
			reportPath = izFacade.getReportDODataSource(parameters, new JRBeanCollectionDataSource(listaVozaca));
		} catch (Exception e) {
			dbFacade.rollback();
			e.printStackTrace();
		}
		return reportPath;		
	}
	
	public String getVoznjeReport(@SuppressWarnings("rawtypes") Map parameters){
		String reportPath = "";		
		IzvjestajiFacade izFacade = (IzvjestajiFacade) RuntimeAccess.getInstance().getSpringBean("IzvjestajiFacade");
		ServletMain servletMain = (ServletMain) RuntimeAccess.getInstance().getSpringBean("glavni");
		
		String mjestoPolaska = String.valueOf(parameters.get("mjestoPolaska"));
		String mjestoDolaska = String.valueOf(parameters.get("mjestoDolaska"));
		String prezimeVozaca = String.valueOf(parameters.get("prezimeVozaca"));
		String oznakaVoznje = String.valueOf(parameters.get("oznakaVoznje"));
		String garazniBroj = String.valueOf(parameters.get("garazniBroj"));
		String vrijemePolaska = String.valueOf(parameters.get("vrijemePolaska"));
		String vrijemeDolaska = String.valueOf(parameters.get("vrijemeDolaska"));

		List<Voznja> listaVoznji = servletMain.getVoznjaForm(mjestoPolaska, mjestoDolaska, prezimeVozaca, garazniBroj, vrijemePolaska, vrijemeDolaska, oznakaVoznje);	
		try {
			reportPath = izFacade.getReportDODataSource(parameters, new JRBeanCollectionDataSource(listaVoznji));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return reportPath;		
	}
	
	public String getNaruciteljiReport(@SuppressWarnings("rawtypes") Map parameters){
		String reportPath = "";		
		IzvjestajiFacade izFacade = (IzvjestajiFacade) RuntimeAccess.getInstance().getSpringBean("IzvjestajiFacade");

		String paramNazivNarucitelja = String.valueOf(parameters.get("paramNazivNarucitelja"));
		String hql = " select na from Narucitelj na ";
		if(StringUtils.isNotEmpty(paramNazivNarucitelja) && paramNazivNarucitelja != null && !paramNazivNarucitelja.equals("null")){
			hql += " where na.nazivNarucitelj like '"+paramNazivNarucitelja+"%'";
		}
		
		RasbusDB dbFacade = (RasbusDB) RuntimeAccess.getInstance().getServiceBean("rasbusDB");
		dbFacade.begin();
		Session session = dbFacade.getDataServiceManager().getSession();
		Query query = session.createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Narucitelj> listaNarucitelja = query.list();
		dbFacade.commit();		
		try {
			reportPath = izFacade.getReportDODataSource(parameters, new JRBeanCollectionDataSource(listaNarucitelja));
		} catch (Exception e) {
			dbFacade.rollback();
			e.printStackTrace();
		}
		return reportPath;		
	}
	
	public String getNenaplaceneVoznjeReport(@SuppressWarnings("rawtypes") Map parameters){
		String reportPath = "";	
		try {
			IzvjestajiFacade izFacade = (IzvjestajiFacade) RuntimeAccess.getInstance().getSpringBean("IzvjestajiFacade");
			ServletMain servletMain = (ServletMain) RuntimeAccess.getInstance().getSpringBean("glavni");

			String mjestoPolaska = String.valueOf(parameters.get("mjestoPolaska") != null ? parameters.get("mjestoPolaska") : null);
			String mjestoDolaska = String.valueOf(parameters.get("mjestoDolaska") != null ? parameters.get("mjestoDolaska") : null);
			String prezimeVozaca = String.valueOf(parameters.get("prezimeVozaca") != null ? parameters.get("prezimeVozaca") : null);
			String garazniBroj = String.valueOf(parameters.get("garazniBroj") != null ? parameters.get("garazniBroj") : null);
			String vrijemePolaska = String.valueOf(parameters.get("vrijemePolaska") != null ? parameters.get("vrijemePolaska") : null);
			String vrijemeDolaska = String.valueOf(parameters.get("vrijemeDolaska") != null ? parameters.get("vrijemeDolaska") : null);
			String oznakaVoznje = String.valueOf(parameters.get("oznakaVoznje") != null ? parameters.get("oznakaVoznje") : null);
			Integer sifraNarucitelj;
			try{
				sifraNarucitelj = Integer.parseInt(String.valueOf(parameters.get("sifraNarucitelja")));
			}catch(NumberFormatException nex){
				sifraNarucitelj = null;
			}
			String nazivNarucitelj = String.valueOf(parameters.get("nazivNarucitelja") != null ? parameters.get("nazivNarucitelja") : null);

			List<Voznja> listaVoznji = servletMain.getNenaplaceneVoznje(sifraNarucitelj, nazivNarucitelj, mjestoPolaska, mjestoDolaska, prezimeVozaca, garazniBroj, vrijemePolaska, vrijemeDolaska, oznakaVoznje);

			reportPath = izFacade.getReportDODataSource(parameters, new JRBeanCollectionDataSource(listaVoznji));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return reportPath;		
	}
	
	public String getNaplaceneVoznjeReport(@SuppressWarnings("rawtypes") Map parameters){
		String reportPath = "";	
		try {
			IzvjestajiFacade izFacade = (IzvjestajiFacade) RuntimeAccess.getInstance().getSpringBean("IzvjestajiFacade");
			ServletMain servletMain = (ServletMain) RuntimeAccess.getInstance().getSpringBean("glavni");

			String mjestoPolaska = String.valueOf(parameters.get("mjestoPolaska") != null ? parameters.get("mjestoPolaska") : null);
			String mjestoDolaska = String.valueOf(parameters.get("mjestoDolaska") != null ? parameters.get("mjestoDolaska") : null);
			String prezimeVozaca = String.valueOf(parameters.get("prezimeVozaca") != null ? parameters.get("prezimeVozaca") : null);
			String garazniBroj = String.valueOf(parameters.get("garazniBroj") != null ? parameters.get("garazniBroj") : null);
			String vrijemePolaska = String.valueOf(parameters.get("vrijemePolaska") != null ? parameters.get("vrijemePolaska") : null);
			String vrijemeDolaska = String.valueOf(parameters.get("vrijemeDolaska") != null ? parameters.get("vrijemeDolaska") : null);
			String oznakaVoznje = String.valueOf(parameters.get("oznakaVoznje") != null ? parameters.get("oznakaVoznje") : null);
			Integer sifraNarucitelj;
			try{
				sifraNarucitelj = Integer.parseInt(String.valueOf(parameters.get("sifraNarucitelja")));
			}catch(NumberFormatException nex){
				sifraNarucitelj = null;
			}
			String nazivNarucitelj = String.valueOf(parameters.get("nazivNarucitelja") != null ? parameters.get("nazivNarucitelja") : null);

			List<Voznja> listaVoznji = servletMain.getNaplaceneVoznje(sifraNarucitelj, nazivNarucitelj, mjestoPolaska, mjestoDolaska, prezimeVozaca, garazniBroj, vrijemePolaska, vrijemeDolaska, oznakaVoznje);

			reportPath = izFacade.getReportDODataSource(parameters, new JRBeanCollectionDataSource(listaVoznji));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return reportPath;		
	}
	
	public String getOtvoreneVoznjeReport(@SuppressWarnings("rawtypes") Map parameters){
		String reportPath = "";	
		try {
			IzvjestajiFacade izFacade = (IzvjestajiFacade) RuntimeAccess.getInstance().getSpringBean("IzvjestajiFacade");
			ServletMain servletMain = (ServletMain) RuntimeAccess.getInstance().getSpringBean("glavni");

			String mjestoPolaska = String.valueOf(parameters.get("mjestoPolaska") != null ? parameters.get("mjestoPolaska") : null);
			String mjestoDolaska = String.valueOf(parameters.get("mjestoDolaska") != null ? parameters.get("mjestoDolaska") : null);
			String prezimeVozaca = String.valueOf(parameters.get("prezimeVozaca") != null ? parameters.get("prezimeVozaca") : null);
			String garazniBroj = String.valueOf(parameters.get("garazniBroj") != null ? parameters.get("garazniBroj") : null);
			String vrijemePolaska = String.valueOf(parameters.get("vrijemePolaska") != null ? parameters.get("vrijemePolaska") : null);
			String vrijemeDolaska = String.valueOf(parameters.get("vrijemeDolaska") != null ? parameters.get("vrijemeDolaska") : null);
			String oznakaVoznje = String.valueOf(parameters.get("oznakaVoznje") != null ? parameters.get("oznakaVoznje") : null);
			Integer sifraNarucitelj;
			try{
				sifraNarucitelj = Integer.parseInt(String.valueOf(parameters.get("sifraNarucitelja")));
			}catch(NumberFormatException nex){
				sifraNarucitelj = null;
			}
			String nazivNarucitelj = String.valueOf(parameters.get("nazivNarucitelja") != null ? parameters.get("nazivNarucitelja") : null);

			List<Voznja> listaVoznji = servletMain.getOtvoreneVoznjePregled(sifraNarucitelj, nazivNarucitelj, mjestoPolaska, mjestoDolaska, prezimeVozaca, garazniBroj, vrijemePolaska, vrijemeDolaska, oznakaVoznje);			

			reportPath = izFacade.getReportDODataSource(parameters, new JRBeanCollectionDataSource(listaVoznji));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return reportPath;		
	}
}
