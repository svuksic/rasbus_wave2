package logger;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.hibernate.Session;

import com.rasbusdb.RasbusDB;
import com.rasbusdb.data.Logovi;
import com.rasbusdb.data.User;
import com.wavemaker.runtime.RuntimeAccess;
import com.wavemaker.runtime.security.SecurityService;

@Aspect
public class ServiceLogger {

	 @Pointcut("execution(* glavniServis.*.*(..))")
     public void webServiceMethods() { }

	 @Around("webServiceMethods()")
	 public Object profile(ProceedingJoinPoint pjp) throws Throwable {
		 SecurityService securityService = (SecurityService) RuntimeAccess.getInstance().getServiceBean("securityService");
		 RasbusDB dbFacade = (RasbusDB) RuntimeAccess.getInstance().getServiceBean("rasbusDB");
		 long start = System.currentTimeMillis();

		 final Signature signature = pjp.getStaticPart().getSignature();
		 if(signature instanceof MethodSignature){
			 final MethodSignature ms = (MethodSignature) signature;
			 Class<?>[] parameterTypes = ms.getParameterTypes();
			 String[] parameterNames = ms.getParameterNames();
			 Object[] argumentsValues = pjp.getArgs();
			 String parametars = "";
			 String methodData = "";
			 Integer brojac = 0;
			 //System.out.println("@@@@@@@@@@@@@@@Pozivam metodu: "+pjp.getSignature()+" Korisnik: "+securityService.getUserName());
			 System.out.println("@@@@@**********************************************@@@@");
			 for(final Class<?> pt : parameterTypes){
				 //System.out.println("Parametar:("+pt+") "+parameterNames[brojac]+" = "+argumentsValues[brojac]);
				 parametars = parametars + " ("+pt.getSimpleName()+") "+parameterNames[brojac]+"="+argumentsValues[brojac]+",";
				 brojac++;
			 }
			 try{
				 if(!pjp.getSignature().getName().equals("getLogovi")){
					 java.util.Date date = new java.util.Date();
					 methodData = "[VRIJEME:"+ date.toString()  +" KORISNIK: "+securityService.getUserName()+" METODA: "+pjp.getSignature().getName()+" PAREMETRI:"+parametars+"]";
					 System.out.println(methodData);
					 dbFacade.begin();
					 Session session = dbFacade.getDataServiceManager().getSession();
					 Logovi log = new Logovi();
					 log.setSadrzaj(methodData);
					 log.setServerskoVrijeme(date);
					 log.setUser((User)session.load(User.class, Integer.parseInt(securityService.getUserId())));
					 session.persist(log);
					 dbFacade.commit();
				 }
			 }catch(Exception ex){
				 System.out.println();
			 }
		 }		 		 
		 Object output = pjp.proceed();
		 long elapsedTime = System.currentTimeMillis() - start;
		 System.out.println("@@@@@@@@@@@@@@@izvršena je metoda: "+pjp.getSignature()+" Trajanje: "+elapsedTime+" ms");
		 return output;
	 }
}
