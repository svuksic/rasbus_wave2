package glavniServis;
import java.sql.*;

public class ConnectionBus
{
	public Connection conn = null;
    public Connection connect()
    {      
    	Connection result = null;
        try
        {        	
            String userName = "root";
            //String password = "12345";
            String password = "svuksic";
            String url = "jdbc:mysql://localhost:3306/rasbus";
            Class.forName ("com.mysql.jdbc.Driver").newInstance ();
            conn = DriverManager.getConnection (url, userName, password);
            System.out.println ("Database connection established");
            result = conn;
        }
        catch (Exception e)
        {
            System.err.println ("Cannot connect to database server \r\n"+e.toString());
            result = null;
        }
        
        return result;
    }
    
    public void closeConnection(Connection conn)
    {
    	try
        {
            conn.close ();
            System.out.println ("Database connection terminated");
        }
        catch (Exception e) { /* ignore close errors */ }
    }
    
    //za izvršavanje insert queryia
    public void executeInsertQuery(String query){  
    	Connection conn = null;
		try {
			conn = this.connect();
			Statement s = conn.createStatement();
			s.executeUpdate("set names utf8"); 
			s.executeUpdate("set character_set_results=utf8");  
			s.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.closeConnection(conn);
    }
    
    public ResultSet executeSelectQuery(String query){
    	Connection conn = null;
    	ResultSet rs = null;
    	try {
    		conn = this.connect();
    		Statement s = conn.createStatement();
			s.executeQuery("set names utf8"); 
			s.executeQuery("set character_set_results=utf8");  
    		s.executeQuery(query);
    		rs = s.getResultSet();
    	} catch (SQLException e) {
    		e.printStackTrace();
    	}
    	return rs;
    }
}
