package glavniServis;

import izvjestaji.Izvjestaji;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;

import valueObjects.VoznjaByBus;

import com.rasbusdb.RasbusDB;
import com.rasbusdb.data.Bus;
import com.rasbusdb.data.Logovi;
import com.rasbusdb.data.Narucitelj;
import com.rasbusdb.data.Vozac;
import com.rasbusdb.data.Voznja;
import com.rasbusdb.data.Zatvorenavoznja;
import com.wavemaker.runtime.RuntimeAccess;

public class ServletMain extends com.wavemaker.runtime.javaservice.JavaServiceSuperClass {
	
	@Autowired Izvjestaji reports;
/* Pass in one of FATAL, ERROR, WARN,  INFO and DEBUG to modify your log level;
     *  recommend changing this to FATAL or ERROR before deploying.  For info on these levels, look for tomcat/log4j documentation
     */
    public ServletMain() {
        super(INFO);
    }    
    
    public String sampleJavaOperation() {
        String result = null;     
        try {
            log(INFO, "Starting sample operation");
            result = "Hello World";
            log(INFO, "Returning " + result);
        } catch (Exception e) {
            log(ERROR, "The sample java service operation has failed", e);
        }
        return result;
    }

    public String getGrafickiPrikaz(String periodPar, String datumPocetka, String listaOdabranihBuseva) {

        String result = "";
        //za izračunavanje do kojeg datuma perioda ide upit iz baze za pregled vožnji
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        String datumDO = "0001-01-01";
        String datumi[] = datumPocetka.split("-");
        String period = periodPar;
        //privremeno rjesenje dok ne otkrijem kaako na sucelju dobiti format datuma s tockama
        String datum = datumi[2] + "." + datumi[1] + "." + datumi[0];
        String selectiraniID = listaOdabranihBuseva.substring(0, listaOdabranihBuseva.length() - 1);

        System.out.println(selectiraniID.length());
        System.out.println(period + " " + datum + " " + selectiraniID);
        //System.out.println("select idvoznja, DATE_FORMAT (VRIJEME_POLASKA_VOZNJA, '%d.%m.%Y'), DATE_FORMAT (VRIJEME_DOLASKA_VOZNJA, '%d.%m.%Y'), BUS_VOZNJA from voznja WHERE DATE(VRIJEME_POLASKA_VOZNJA) = '" + datum + "' AND BUS_VOZNJA IN (" +  selectiraniID + ")");
        //izracunavanje datuma perioda
        if (period.equals("1 Dan")) {
            try {
                cal.setTime(dateFormat.parse(datumPocetka));
                cal.add(Calendar.DATE, 0);
                datumDO = dateFormat.format(cal.getTime());
                System.out.println(datumDO);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (period.equals("1 Tjedan")) {
            try {
                cal.setTime(dateFormat.parse(datumPocetka));
                cal.add(Calendar.DATE, 7);
                datumDO = dateFormat.format(cal.getTime());
                System.out.println(datumDO);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (period.equals("1 Mjesec")) {
            try {
                cal.setTime(dateFormat.parse(datumPocetka));
                cal.add(Calendar.DATE, 30);
                datumDO = dateFormat.format(cal.getTime());
                System.out.println(datumDO);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        try {
            ConnectionBus connBus = new ConnectionBus();
            ResultSet rsBusevi = connBus.executeSelectQuery("select distinct BUS_VOZNJA from voznja WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) = '" + datumPocetka + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) = '" + datumDO + "')) AND BUS_VOZNJA IN ( " + selectiraniID + ")");
            System.out.println("select distinct BUS_VOZNJA from voznja WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) = '" + datumPocetka + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) = '" + datumDO + "')) AND BUS_VOZNJA IN ( " + selectiraniID + ")");
            System.out.println(rsBusevi.isBeforeFirst());
            if (rsBusevi.isBeforeFirst() && period.equals("1 Dan")) {
                result = "{\"busevi\":[ ";
                while (rsBusevi.next()) {
                    result += " {\"GB\":\"" + rsBusevi.getString("BUS_VOZNJA") + "\", \"voznje\": [";
                    System.out.println("dnevni - select v.idvoznja, DATE_FORMAT (v.VRIJEME_POLASKA_VOZNJA, '%d.%m.%Y') as datum_polaska, DATE_FORMAT (v.VRIJEME_DOLASKA_VOZNJA, '%d.%m.%Y') as datum_dolaska, DATE_FORMAT(v.VRIJEME_POLASKA_VOZNJA, '%T') as vrijeme_polaska, DATE_FORMAT(v.VRIJEME_DOLASKA_VOZNJA, '%T') as vrijeme_dolaska, v.BUS_VOZNJA, b.PREZIME_VOZAC as prezime, b.IME_VOZAC as ime from rasbus.voznja as v INNER JOIN rasbus.vozac as b ON b.idvozac = v.VOZAC_VOZNJA WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "')) AND BUS_VOZNJA = " + rsBusevi.getString("BUS_VOZNJA"));
                    ResultSet rs = connBus.executeSelectQuery("select v.idvoznja, DATE_FORMAT (v.VRIJEME_POLASKA_VOZNJA, '%d.%m.%Y') as datum_polaska, DATE_FORMAT (v.VRIJEME_DOLASKA_VOZNJA, '%d.%m.%Y') as datum_dolaska, DATE_FORMAT(v.VRIJEME_POLASKA_VOZNJA, '%T') as vrijeme_polaska, DATE_FORMAT(v.VRIJEME_DOLASKA_VOZNJA, '%T') as vrijeme_dolaska, v.BUS_VOZNJA, b.PREZIME_VOZAC as prezime, b.IME_VOZAC as ime from rasbus.voznja as v INNER JOIN rasbus.vozac as b ON b.idvozac = v.VOZAC_VOZNJA WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "')) AND BUS_VOZNJA = " + rsBusevi.getString("BUS_VOZNJA"));
                    while (rs.next()) {
                        result += " {\"IDvoznje\":\"" + rs.getString("idvoznja") + "\", \"Vozac\":\"" + rs.getString("Ime") + " " + rs.getString("Prezime") + "\", \"pocetak_datum\":\"" + rs.getString("datum_polaska") + "\",\"pocetak_vrijeme\":\"" + rs.getString("vrijeme_polaska") + "\",\"kraj_datum\":\"" + rs.getString("datum_dolaska") + "\",\"kraj_vrijeme\":\"" + rs.getString("vrijeme_dolaska") + "\",\"pregledOD\":\"" + datum + "\",\"vozac\":\"" + rs.getString("prezime") + " " + rs.getString("ime") + "\"},";
                    }
                    result = result.substring(0, result.length() - 1);
                    result += "]},";
                }
                result = result.substring(0, result.length() - 1);
                result += "]}";
                System.out.println(result);
            } else if (rsBusevi.isBeforeFirst() && period.equals("1 Tjedan")) {
                result = "{\"busevi\":[ ";
                while (rsBusevi.next()) {
                    result += " {\"GB\":\"" + rsBusevi.getString("BUS_VOZNJA") + "\", \"voznje\": [";
                    System.out.println("tjedni - select v.idvoznja, DATE_FORMAT (v.VRIJEME_POLASKA_VOZNJA, '%d.%m.%Y') as datum_polaska, DATE_FORMAT (v.VRIJEME_DOLASKA_VOZNJA, '%d.%m.%Y') as datum_dolaska, DATE_FORMAT(v.VRIJEME_POLASKA_VOZNJA, '%T') as vrijeme_polaska, DATE_FORMAT(v.VRIJEME_DOLASKA_VOZNJA, '%T') as vrijeme_dolaska, v.BUS_VOZNJA, b.PREZIME_VOZAC as prezime, b.IME_VOZAC as ime from rasbus.voznja as v INNER JOIN rasbus.vozac as b ON b.idvozac = v.VOZAC_VOZNJA WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "')) AND BUS_VOZNJA = " + rsBusevi.getString("BUS_VOZNJA"));
                    ResultSet rs = connBus.executeSelectQuery("select v.idvoznja, DATE_FORMAT (v.VRIJEME_POLASKA_VOZNJA, '%d.%m.%Y') as datum_polaska, DATE_FORMAT (v.VRIJEME_DOLASKA_VOZNJA, '%d.%m.%Y') as datum_dolaska, DATE_FORMAT(v.VRIJEME_POLASKA_VOZNJA, '%T') as vrijeme_polaska, DATE_FORMAT(v.VRIJEME_DOLASKA_VOZNJA, '%T') as vrijeme_dolaska, v.BUS_VOZNJA, b.PREZIME_VOZAC as prezime, b.IME_VOZAC as ime from rasbus.voznja as v INNER JOIN rasbus.vozac as b ON b.idvozac = v.VOZAC_VOZNJA WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "')) AND BUS_VOZNJA = " + rsBusevi.getString("BUS_VOZNJA"));
                    while (rs.next()) {
                        result += " {\"IDvoznje\":\"" + rs.getString("idvoznja") + "\", \"Vozac\":\"" + rs.getString("Ime") + " " + rs.getString("Prezime") + "\",\"pocetak_datum\":\"" + rs.getString("datum_polaska") + "\",\"pocetak_vrijeme\":\"" + rs.getString("vrijeme_polaska") + "\",\"kraj_datum\":\"" + rs.getString("datum_dolaska") + "\",\"kraj_vrijeme\":\"" + rs.getString("vrijeme_dolaska") + "\",\"pregledOD\":\"" + datum + "\"},";
                    }
                    result = result.substring(0, result.length() - 1);
                    result += "]},";
                }
                result = result.substring(0, result.length() - 1);
                result += "]}";
                System.out.println(result + "tjedni");
            } else if (rsBusevi.isBeforeFirst() && period.equals("1 Mjesec")) {
                result = "{\"busevi\":[ ";
                while (rsBusevi.next()) {
                    result += " {\"GB\":\"" + rsBusevi.getString("BUS_VOZNJA") + "\", \"voznje\": [";
                    System.out.println("mjesecni - select v.idvoznja, DATE_FORMAT (v.VRIJEME_POLASKA_VOZNJA, '%d.%m.%Y') as datum_polaska, DATE_FORMAT (v.VRIJEME_DOLASKA_VOZNJA, '%d.%m.%Y') as datum_dolaska, DATE_FORMAT(v.VRIJEME_POLASKA_VOZNJA, '%T') as vrijeme_polaska, DATE_FORMAT(v.VRIJEME_DOLASKA_VOZNJA, '%T') as vrijeme_dolaska, v.BUS_VOZNJA, b.PREZIME_VOZAC as prezime, b.IME_VOZAC as ime from rasbus.voznja as v INNER JOIN rasbus.vozac as b ON b.idvozac = v.VOZAC_VOZNJA WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "')) AND BUS_VOZNJA = " + rsBusevi.getString("BUS_VOZNJA"));
                    ResultSet rs = connBus.executeSelectQuery("select v.idvoznja, DATE_FORMAT (v.VRIJEME_POLASKA_VOZNJA, '%d.%m.%Y') as datum_polaska, DATE_FORMAT (v.VRIJEME_DOLASKA_VOZNJA, '%d.%m.%Y') as datum_dolaska, DATE_FORMAT(v.VRIJEME_POLASKA_VOZNJA, '%T') as vrijeme_polaska, DATE_FORMAT(v.VRIJEME_DOLASKA_VOZNJA, '%T') as vrijeme_dolaska, v.BUS_VOZNJA, b.PREZIME_VOZAC as prezime, b.IME_VOZAC as ime from rasbus.voznja as v INNER JOIN rasbus.vozac as b ON b.idvozac = v.VOZAC_VOZNJA WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "')) AND BUS_VOZNJA = " + rsBusevi.getString("BUS_VOZNJA"));
                    while (rs.next()) {
                        result += " {\"IDvoznje\":\"" + rs.getString("idvoznja") + "\", \"Vozac\":\"" + rs.getString("Ime") + " " + rs.getString("Prezime") + "\",\"pocetak_datum\":\"" + rs.getString("datum_polaska") + "\",\"pocetak_vrijeme\":\"" + rs.getString("vrijeme_polaska") + "\",\"kraj_datum\":\"" + rs.getString("datum_dolaska") + "\",\"kraj_vrijeme\":\"" + rs.getString("vrijeme_dolaska") + "\",\"pregledOD\":\"" + datum + "\"},";
                    }
                    result = result.substring(0, result.length() - 1);
                    result += "]},";
                }
                result = result.substring(0, result.length() - 1);
                result += "]}";
                System.out.println(result + "mjesecni");
            } else {
                result = "0";
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    }

    /**
     * custom funkcija za spremanje voznje na formi za voznje
     * @param oznakaVoznja
     * @param opisVoznja
     * @param mjestoPolaskaPar
     * @param mjestoDolaskaPar
     * @param datumPolaskaPar
     * @param datumDolaskaPar
     * @param busPar
     * @param vozacPar
     * @param naruciteljPar
     * @param ugovorenaCijena
     * @return
     */
    public Boolean dodavanjeVoznje(String oznakaVoznja, String opisVoznja, String mjestoPolaskaPar, String mjestoDolaskaPar, Timestamp datumPolaskaPar, Timestamp datumDolaskaPar, Integer busPar, Integer vozacPar, Integer naruciteljPar, String ugovorenaCijena) {
        System.out.println(oznakaVoznja + " " + opisVoznja + " " + mjestoPolaskaPar + " " + mjestoDolaskaPar + " " + datumPolaskaPar + " " + datumDolaskaPar + " " + busPar + " " + vozacPar);

        Boolean result = false;

        @SuppressWarnings("unused")
		String oznakaVoznje = oznakaVoznja;
        String mjestoPolaska = mjestoPolaskaPar;
        String mjestoDolaska = mjestoDolaskaPar;
        @SuppressWarnings("unused")
		String opisVoznje = opisVoznja;
        Timestamp datumPolaska = datumPolaskaPar;
        Timestamp datumDolaska = datumDolaskaPar;
        Integer vozac = vozacPar;
        Integer vozilo = busPar;
        Integer narucitelj = naruciteljPar;
        RasbusDB dbFacade = (RasbusDB) RuntimeAccess.getInstance().getServiceBean("rasbusDB");

        System.out.println("select * from voznja where VRIJEME_POLASKA_VOZNJA <= '" + datumPolaska + "' && VRIJEME_DOLASKA_VOZNJA >= '" + datumPolaska + "' and (BUS_VOZNJA = " + vozilo + " or VOZAC_VOZNJA = " + vozac + ");");
        System.out.println("select * from voznja where VRIJEME_POLASKA_VOZNJA <= '" + datumDolaska + "' && VRIJEME_DOLASKA_VOZNJA >= '" + datumDolaska + "' and (BUS_VOZNJA = " + vozilo + " or VOZAC_VOZNJA = " + vozac + ");");

        try {
            dbFacade.begin();
            String sql = "select * from voznja where VRIJEME_POLASKA_VOZNJA <= '" + datumPolaska + "' && VRIJEME_DOLASKA_VOZNJA >= '" + datumPolaska + "' and (BUS_VOZNJA = " + vozilo + " or VOZAC_VOZNJA = " + vozac + ")";
            String sql2 = "select * from voznja where VRIJEME_POLASKA_VOZNJA <= '" + datumDolaska + "' && VRIJEME_DOLASKA_VOZNJA >= '" + datumDolaska + "' and (BUS_VOZNJA = " + vozilo + " or VOZAC_VOZNJA = " + vozac + ")";
            String sql3 = "select * from voznja where VRIJEME_POLASKA_VOZNJA >= '" + datumPolaska + "' && VRIJEME_DOLASKA_VOZNJA <= '" + datumDolaska + "' and (BUS_VOZNJA = " + vozilo + " or VOZAC_VOZNJA = " + vozac + ")";
            Session session = dbFacade.getDataServiceManager().getSession();
            SQLQuery query = session.createSQLQuery(sql);
            query.addEntity(Voznja.class);@SuppressWarnings("unchecked")
            List < Voznja > rsBusevi = query.list();
            SQLQuery query2 = session.createSQLQuery(sql2);
            query2.addEntity(Voznja.class);@SuppressWarnings("unchecked")
            List < Voznja > rsBusevi2 = query2.list();
            SQLQuery query3 = session.createSQLQuery(sql3);
            query3.addEntity(Voznja.class);@SuppressWarnings("unchecked")
            List < Voznja > rsBusevi3 = query3.list();

            System.out.println(rsBusevi);
            System.out.println(rsBusevi2);
            System.out.println(rsBusevi3);
            //ako postoji vec rezrvacija za taj bus i tog vozaca u to vrijeme
            if (rsBusevi.size() == 0 && rsBusevi2.size() == 0 && rsBusevi3.size() == 0) {
                dbFacade.begin();
                Voznja voznja = new Voznja();
                voznja.setOznakaVoznja(oznakaVoznja);
                voznja.setOpisVoznja(opisVoznja);
                voznja.setMjestoPolaskaVoznja(mjestoPolaska);
                voznja.setMjestoDolaskaVoznja(mjestoDolaska);
                voznja.setVrijemePolaskaVoznja(new Date(datumPolaska.getTime()));
                voznja.setVrijemeDolaskaVoznja(new Date(datumDolaska.getTime()));
                if(vozac != null)
                	voznja.setVozac((Vozac)session.load(Vozac.class,new Integer(vozac)));
                if(vozilo != null)
                	voznja.setBus((Bus)session.load(Bus.class, new Integer(vozilo)));
                if(narucitelj != null)
                	voznja.setNarucitelj((Narucitelj)session.load(Narucitelj.class, new Integer(narucitelj)));
                voznja.setUgovorenaCijenaVoznja(ugovorenaCijena);
                session.save(voznja);
                //sql = "insert into voznja(OZNAKA_VOZNJA, OPIS_VOZNJA, MJESTO_POLASKA_VOZNJA, MJESTO_DOLASKA_VOZNJA, VRIJEME_POLASKA_VOZNJA, VRIJEME_DOLASKA_VOZNJA, VOZAC_VOZNJA, BUS_VOZNJA, NARUCITELJ_VOZNJA, UGOVORENA_CIJENA_VOZNJA) values('" + oznakaVoznje + "', '" + opisVoznje + "', '" + mjestoPolaska + "', '" + mjestoDolaska + "', '" + datumPolaska + "', '" + datumDolaska + "', '" + vozac + "','" + vozilo + "','"+narucitelj+"','"+ugovorenaCijena+"')";
                //query = session.createSQLQuery(sql);
                //query.executeUpdate();
                dbFacade.commit();
                result = true;
            } else {
                System.out.println("zauzeti resursi u traženo doba!");
                result = false;
            }
        } catch (Exception e) {
            dbFacade.rollback();
            System.out.println("Greška kod upisa vožnje u bazu!");
            e.printStackTrace();
            result = false;
        }
        return result;
    }
    
    
    /**
     * vraca voznje za određeni period po 
     * @throws ParseException 
     */
    public ArrayList < VoznjaByBus > getVoznjeBusevi(String periodPar, String datumPocetka, String listaOdabranihBuseva) {

        System.out.println("Parametri: " + periodPar + " " + datumPocetka + " " + listaOdabranihBuseva);

        ConnectionBus connBus = new ConnectionBus();
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String datumDO = "0001-01-01";
        String period = periodPar;

        ArrayList < VoznjaByBus > result = new ArrayList < VoznjaByBus > ();

        //izracunavanje datuma perioda
        if (period.equals("1 Dan")) {
            try {
                cal.setTime(dateFormat.parse(datumPocetka));
                cal.add(Calendar.DATE, 0);
                datumDO = dateFormat.format(cal.getTime());
                System.out.println(datumDO);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (period.equals("1 Tjedan")) {
            try {
                cal.setTime(dateFormat.parse(datumPocetka));
                cal.add(Calendar.DATE, 7);
                datumDO = dateFormat.format(cal.getTime());
                System.out.println(datumDO);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (period.equals("1 Mjesec")) {
            try {
                cal.setTime(dateFormat.parse(datumPocetka));
                cal.add(Calendar.DATE, 30);
                datumDO = dateFormat.format(cal.getTime());
                System.out.println(datumDO);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        String selectiraniID = listaOdabranihBuseva.substring(0, listaOdabranihBuseva.length() - 1);
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd.mm.yyyy H:m:s");

        System.out.println("Datum do:" + datumDO);
        System.out.println("select distinct BUS_VOZNJA from voznja WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) = '" + datumPocetka + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) = '" + datumDO + "')) AND BUS_VOZNJA IN ( " + selectiraniID + ")");

        ResultSet rsBusevi = connBus.executeSelectQuery("select distinct BUS_VOZNJA from voznja WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) = '" + datumPocetka + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) = '" + datumDO + "')) AND BUS_VOZNJA IN ( " + selectiraniID + ")");
        try {
            while (rsBusevi.next()) {
                System.out.println("select v.OZNAKA_VOZNJA as oznakaVoznje, v.OPIS_VOZNJA as opisVoznje, bu.GARAZNI_BROJ_BUS as garazni_broj, v.idvoznja, DATE_FORMAT (v.VRIJEME_POLASKA_VOZNJA, '%d.%m.%Y') as datum_polaska, DATE_FORMAT (v.VRIJEME_DOLASKA_VOZNJA, '%d.%m.%Y') as datum_dolaska, DATE_FORMAT(v.VRIJEME_POLASKA_VOZNJA, '%T') as vrijeme_polaska, DATE_FORMAT(v.VRIJEME_DOLASKA_VOZNJA, '%T') as vrijeme_dolaska, v.BUS_VOZNJA, b.PREZIME_VOZAC as prezime, b.IME_VOZAC as ime from rasbus.voznja as v INNER JOIN rasbus.vozac as b ON b.idvozac = v.VOZAC_VOZNJA INNER JOIN rasbus.bus as bu on bu.idBus = v.BUS_VOZNJA WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "')) AND BUS_VOZNJA = " + rsBusevi.getString("BUS_VOZNJA"));
                ResultSet rs = connBus.executeSelectQuery("select v.OZNAKA_VOZNJA as oznakaVoznje, v.OPIS_VOZNJA as opisVoznje, bu.GARAZNI_BROJ_BUS as garazni_broj, v.idvoznja, DATE_FORMAT (v.VRIJEME_POLASKA_VOZNJA, '%d.%m.%Y') as datum_polaska, DATE_FORMAT (v.VRIJEME_DOLASKA_VOZNJA, '%d.%m.%Y') as datum_dolaska, DATE_FORMAT(v.VRIJEME_POLASKA_VOZNJA, '%T') as vrijeme_polaska, DATE_FORMAT(v.VRIJEME_DOLASKA_VOZNJA, '%T') as vrijeme_dolaska, v.BUS_VOZNJA, b.PREZIME_VOZAC as prezime, b.IME_VOZAC as ime from rasbus.voznja as v INNER JOIN rasbus.vozac as b ON b.idvozac = v.VOZAC_VOZNJA INNER JOIN rasbus.bus as bu on bu.idBus = v.BUS_VOZNJA WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "')) AND BUS_VOZNJA = " + rsBusevi.getString("BUS_VOZNJA"));
                while (rs.next()) {
                    VoznjaByBus voznja = new VoznjaByBus();
                    voznja.setDatumDolaska(dateFormat2.parse(rs.getString("datum_dolaska") + " " + rs.getString("vrijeme_dolaska")));
                    voznja.setVrijemeDolaska(dateFormat2.parse(rs.getString("datum_dolaska") + " " + rs.getString("vrijeme_dolaska")));
                    voznja.setDatumPolaska(dateFormat2.parse(rs.getString("datum_polaska") + " " + rs.getString("vrijeme_polaska")));
                    voznja.setVrijemePolaska(dateFormat2.parse(rs.getString("datum_polaska") + " " + rs.getString("vrijeme_polaska")));
                    voznja.setGarazniBroj(rs.getString("garazni_broj"));
                    voznja.setIdVoznja(Integer.parseInt(rs.getString("idvoznja")));
                    voznja.setImeVozaca(rs.getString("Ime"));
                    voznja.setPrezimeVozaca(rs.getString("Prezime"));
                    voznja.setOznakaVoznje(rs.getString("oznakaVoznje"));
                    voznja.setOpisVoznje(rs.getString("opisVoznje"));
                    result.add(voznja);
                }
            }
            System.out.println("Rezultat: " + result);
            return result;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        } catch (ParseException pe) {
            // TODO Auto-generated catch block
            pe.printStackTrace();
            return null;
        }
    }

    public ArrayList < VoznjaByBus > getVoznjeBuseviGarazniBroj(String periodPar, String datumPocetka, String listaOdabranihBuseva, String garazniBroj) {
        ArrayList < VoznjaByBus > result = new ArrayList < VoznjaByBus > ();
        System.out.println("Parametri: " + periodPar + " " + datumPocetka + " " + listaOdabranihBuseva + " " + garazniBroj);

        ConnectionBus connBus = new ConnectionBus();
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String datumDO = "0001-01-01";
        String period = periodPar;

        //izracunavanje datuma perioda
        if (period.equals("1 Dan")) {
            try {
                cal.setTime(dateFormat.parse(datumPocetka));
                cal.add(Calendar.DATE, 0);
                datumDO = dateFormat.format(cal.getTime());
                System.out.println(datumDO);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (period.equals("1 Tjedan")) {
            try {
                cal.setTime(dateFormat.parse(datumPocetka));
                cal.add(Calendar.DATE, 7);
                datumDO = dateFormat.format(cal.getTime());
                System.out.println(datumDO);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (period.equals("1 Mjesec")) {
            try {
                cal.setTime(dateFormat.parse(datumPocetka));
                cal.add(Calendar.DATE, 30);
                datumDO = dateFormat.format(cal.getTime());
                System.out.println(datumDO);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        String selectiraniID = listaOdabranihBuseva.substring(0, listaOdabranihBuseva.length() - 1);
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd.mm.yyyy H:m:s");

        System.out.println("Datum do:" + datumDO);
        System.out.println("select distinct BUS_VOZNJA from voznja WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) = '" + datumPocetka + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) = '" + datumDO + "')) AND BUS_VOZNJA IN ( " + selectiraniID + ")");

        ResultSet rsBusevi = connBus.executeSelectQuery("select distinct BUS_VOZNJA from voznja WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) = '" + datumPocetka + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) = '" + datumDO + "')) AND BUS_VOZNJA IN ( " + selectiraniID + ")");
        try {
            while (rsBusevi.next()) {
                System.out.println("select bu.GARAZNI_BROJ_BUS as garazni_broj, v.idvoznja, DATE_FORMAT (v.VRIJEME_POLASKA_VOZNJA, '%d.%m.%Y') as datum_polaska, DATE_FORMAT (v.VRIJEME_DOLASKA_VOZNJA, '%d.%m.%Y') as datum_dolaska, DATE_FORMAT(v.VRIJEME_POLASKA_VOZNJA, '%T') as vrijeme_polaska, DATE_FORMAT(v.VRIJEME_DOLASKA_VOZNJA, '%T') as vrijeme_dolaska, v.BUS_VOZNJA, b.PREZIME_VOZAC as prezime, b.IME_VOZAC as ime from rasbus.voznja as v INNER JOIN rasbus.vozac as b ON b.idvozac = v.VOZAC_VOZNJA INNER JOIN rasbus.bus as bu on bu.idBus = v.BUS_VOZNJA WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "')) AND BUS_VOZNJA = " + rsBusevi.getString("BUS_VOZNJA") + " AND bu.GARAZNI_BROJ_BUS LIKE '%" + garazniBroj + "%'");
                ResultSet rs = connBus.executeSelectQuery("select bu.GARAZNI_BROJ_BUS as garazni_broj, v.idvoznja, DATE_FORMAT (v.VRIJEME_POLASKA_VOZNJA, '%d.%m.%Y') as datum_polaska, DATE_FORMAT (v.VRIJEME_DOLASKA_VOZNJA, '%d.%m.%Y') as datum_dolaska, DATE_FORMAT(v.VRIJEME_POLASKA_VOZNJA, '%T') as vrijeme_polaska, DATE_FORMAT(v.VRIJEME_DOLASKA_VOZNJA, '%T') as vrijeme_dolaska, v.BUS_VOZNJA, b.PREZIME_VOZAC as prezime, b.IME_VOZAC as ime from rasbus.voznja as v INNER JOIN rasbus.vozac as b ON b.idvozac = v.VOZAC_VOZNJA INNER JOIN rasbus.bus as bu on bu.idBus = v.BUS_VOZNJA WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "')) AND BUS_VOZNJA = " + rsBusevi.getString("BUS_VOZNJA") + " AND bu.GARAZNI_BROJ_BUS LIKE '%" + garazniBroj + "%'");
                while (rs.next()) {
                    VoznjaByBus voznja = new VoznjaByBus();
                    voznja.setDatumDolaska(dateFormat2.parse(rs.getString("datum_dolaska") + " " + rs.getString("vrijeme_dolaska")));
                    voznja.setVrijemeDolaska(dateFormat2.parse(rs.getString("datum_dolaska") + " " + rs.getString("vrijeme_dolaska")));
                    voznja.setDatumPolaska(dateFormat2.parse(rs.getString("datum_polaska") + " " + rs.getString("vrijeme_polaska")));
                    voznja.setVrijemePolaska(dateFormat2.parse(rs.getString("datum_polaska") + " " + rs.getString("vrijeme_polaska")));
                    voznja.setGarazniBroj(rs.getString("garazni_broj"));
                    voznja.setIdVoznja(Integer.parseInt(rs.getString("idvoznja")));
                    voznja.setImeVozaca(rs.getString("Ime"));
                    voznja.setPrezimeVozaca(rs.getString("Prezime"));
                    result.add(voznja);
                }
            }
            System.out.println("Rezultat: " + result);
            return result;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        } catch (ParseException pe) {
            // TODO Auto-generated catch block
            pe.printStackTrace();
            return null;
        }
    }

    public ArrayList < VoznjaByBus > getVoznjeBuseviDatumPolaska(String periodPar, String datumPocetka, String listaOdabranihBuseva, Date datumPolaska) {
        ArrayList < VoznjaByBus > result = new ArrayList < VoznjaByBus > ();
        System.out.println("Parametri: " + periodPar + " " + datumPocetka + " " + listaOdabranihBuseva + " " + datumPolaska);

        ConnectionBus connBus = new ConnectionBus();
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String datumDO = "0001-01-01";
        String period = periodPar;

        StringBuilder sDatumPolaska = new StringBuilder(dateFormat.format(datumPolaska));

        //izracunavanje datuma perioda
        if (period.equals("1 Dan")) {
            try {
                cal.setTime(dateFormat.parse(datumPocetka));
                cal.add(Calendar.DATE, 0);
                datumDO = dateFormat.format(cal.getTime());
                System.out.println(datumDO);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (period.equals("1 Tjedan")) {
            try {
                cal.setTime(dateFormat.parse(datumPocetka));
                cal.add(Calendar.DATE, 7);
                datumDO = dateFormat.format(cal.getTime());
                System.out.println(datumDO);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (period.equals("1 Mjesec")) {
            try {
                cal.setTime(dateFormat.parse(datumPocetka));
                cal.add(Calendar.DATE, 30);
                datumDO = dateFormat.format(cal.getTime());
                System.out.println(datumDO);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        String selectiraniID = listaOdabranihBuseva.substring(0, listaOdabranihBuseva.length() - 1);
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd.mm.yyyy H:m:s");

        System.out.println("Datum do:" + datumDO);
        System.out.println("select distinct BUS_VOZNJA from voznja WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) = '" + datumPocetka + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) = '" + datumDO + "')) AND BUS_VOZNJA IN ( " + selectiraniID + ")");

        ResultSet rsBusevi = connBus.executeSelectQuery("select distinct BUS_VOZNJA from voznja WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) = '" + datumPocetka + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) = '" + datumDO + "')) AND BUS_VOZNJA IN ( " + selectiraniID + ")");
        try {
            while (rsBusevi.next()) {
                System.out.println("select bu.GARAZNI_BROJ_BUS as garazni_broj, v.idvoznja, DATE_FORMAT (v.VRIJEME_POLASKA_VOZNJA, '%d.%m.%Y') as datum_polaska, DATE_FORMAT (v.VRIJEME_DOLASKA_VOZNJA, '%d.%m.%Y') as datum_dolaska, DATE_FORMAT(v.VRIJEME_POLASKA_VOZNJA, '%T') as vrijeme_polaska, DATE_FORMAT(v.VRIJEME_DOLASKA_VOZNJA, '%T') as vrijeme_dolaska, v.BUS_VOZNJA, b.PREZIME_VOZAC as prezime, b.IME_VOZAC as ime from rasbus.voznja as v INNER JOIN rasbus.vozac as b ON b.idvozac = v.VOZAC_VOZNJA INNER JOIN rasbus.bus as bu on bu.idBus = v.BUS_VOZNJA WHERE BUS_VOZNJA = " + rsBusevi.getString("BUS_VOZNJA") + " AND DATE(VRIJEME_POLASKA_VOZNJA) = '" + sDatumPolaska + "'");
                ResultSet rs = connBus.executeSelectQuery("select bu.GARAZNI_BROJ_BUS as garazni_broj, v.idvoznja, DATE_FORMAT (v.VRIJEME_POLASKA_VOZNJA, '%d.%m.%Y') as datum_polaska, DATE_FORMAT (v.VRIJEME_DOLASKA_VOZNJA, '%d.%m.%Y') as datum_dolaska, DATE_FORMAT(v.VRIJEME_POLASKA_VOZNJA, '%T') as vrijeme_polaska, DATE_FORMAT(v.VRIJEME_DOLASKA_VOZNJA, '%T') as vrijeme_dolaska, v.BUS_VOZNJA, b.PREZIME_VOZAC as prezime, b.IME_VOZAC as ime from rasbus.voznja as v INNER JOIN rasbus.vozac as b ON b.idvozac = v.VOZAC_VOZNJA INNER JOIN rasbus.bus as bu on bu.idBus = v.BUS_VOZNJA WHERE  BUS_VOZNJA = " + rsBusevi.getString("BUS_VOZNJA") + " AND DATE(VRIJEME_POLASKA_VOZNJA) = '" + sDatumPolaska + "'");
                while (rs.next()) {
                    VoznjaByBus voznja = new VoznjaByBus();
                    voznja.setDatumDolaska(dateFormat2.parse(rs.getString("datum_dolaska") + " " + rs.getString("vrijeme_dolaska")));
                    voznja.setVrijemeDolaska(dateFormat2.parse(rs.getString("datum_dolaska") + " " + rs.getString("vrijeme_dolaska")));
                    voznja.setDatumPolaska(dateFormat2.parse(rs.getString("datum_polaska") + " " + rs.getString("vrijeme_polaska")));
                    voznja.setVrijemePolaska(dateFormat2.parse(rs.getString("datum_polaska") + " " + rs.getString("vrijeme_polaska")));
                    voznja.setGarazniBroj(rs.getString("garazni_broj"));
                    voznja.setIdVoznja(Integer.parseInt(rs.getString("idvoznja")));
                    voznja.setImeVozaca(rs.getString("Ime"));
                    voznja.setPrezimeVozaca(rs.getString("Prezime"));
                    result.add(voznja);
                }
            }
            System.out.println("Rezultat: " + result);
            return result;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        } catch (ParseException pe) {
            // TODO Auto-generated catch block
            pe.printStackTrace();
            return null;
        }
    }

/*
     * za provjeravanje ako postoje vožnje za odabrani bus
     * ako da vraca true
     */
    public Boolean isThereResult(String periodPar, String datumPocetka, String listaOdabranihBuseva) {
        Boolean result = false;
        String datumDO = "0001-01-01";
        String period = periodPar;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        System.out.println("xxxxxx" + listaOdabranihBuseva);
        String selectiraniID = listaOdabranihBuseva.substring(0, listaOdabranihBuseva.length() - 1);

        //izracunavanje datuma perioda
        if (period.equals("1 Dan")) {
            try {
                cal.setTime(dateFormat.parse(datumPocetka));
                cal.add(Calendar.DATE, 0);
                datumDO = dateFormat.format(cal.getTime());
                System.out.println(datumDO);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (period.equals("1 Tjedan")) {
            try {
                cal.setTime(dateFormat.parse(datumPocetka));
                cal.add(Calendar.DATE, 7);
                datumDO = dateFormat.format(cal.getTime());
                System.out.println(datumDO);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (period.equals("1 Mjesec")) {
            try {
                cal.setTime(dateFormat.parse(datumPocetka));
                cal.add(Calendar.DATE, 30);
                datumDO = dateFormat.format(cal.getTime());
                System.out.println(datumDO);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        RasbusDB dbFacade = (RasbusDB) RuntimeAccess.getInstance().getServiceBean("rasbusDB");
        try {
            dbFacade.begin();
            String sql = "select distinct BUS_VOZNJA from voznja WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) = '" + datumPocetka + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) = '" + datumDO + "')) AND BUS_VOZNJA IN ( " + selectiraniID + ")";
            Session session = dbFacade.getDataServiceManager().getSession();
            SQLQuery query = session.createSQLQuery(sql);@SuppressWarnings("unchecked")
            List < Voznja > rsBusevi = query.list();

            if (rsBusevi.size() > 0) {
                result = true;
            }
        } catch (Exception e) {
            result = false;
            dbFacade.rollback();
            e.printStackTrace();
        }
        return result;
    }

/*
     * za provjeravanje ako postoje vožnje za odabranog vozača
     * ako da vraca true
     */
    public Boolean isThereResultVozac(String periodPar, String datumPocetka, String listaOdabranihVozaca) {
        Boolean result = false;
        String datumDO = "0001-01-01";
        String period = periodPar;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        System.out.println("xxxxxx" + listaOdabranihVozaca);
        String selectiraniID = listaOdabranihVozaca.substring(0, listaOdabranihVozaca.length() - 1);

        //izracunavanje datuma perioda
        if (period.equals("1 Dan")) {
            try {
                cal.setTime(dateFormat.parse(datumPocetka));
                cal.add(Calendar.DATE, 0);
                datumDO = dateFormat.format(cal.getTime());
                System.out.println(datumDO);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (period.equals("1 Tjedan")) {
            try {
                cal.setTime(dateFormat.parse(datumPocetka));
                cal.add(Calendar.DATE, 7);
                datumDO = dateFormat.format(cal.getTime());
                System.out.println(datumDO);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (period.equals("1 Mjesec")) {
            try {
                cal.setTime(dateFormat.parse(datumPocetka));
                cal.add(Calendar.DATE, 30);
                datumDO = dateFormat.format(cal.getTime());
                System.out.println(datumDO);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        RasbusDB dbFacade = (RasbusDB) RuntimeAccess.getInstance().getServiceBean("rasbusDB");
        try {
            dbFacade.begin();
            String sql = "select distinct BUS_VOZNJA from voznja WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) = '" + datumPocetka + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) = '" + datumDO + "')) AND VOZAC_VOZNJA IN ( " + selectiraniID + ")";
            Session session = dbFacade.getDataServiceManager().getSession();
            SQLQuery query = session.createSQLQuery(sql);@SuppressWarnings("unchecked")
            List < Voznja > rsBusevi = query.list();

            if (rsBusevi.size() > 0) {
                result = true;
            }
        } catch (Exception e) {
            result = false;
            e.printStackTrace();
            dbFacade.rollback();
        }
        return result;
    }

    /**
     * vraca voznje za određeni period po 
     * @throws ParseException 
     */
    public ArrayList < VoznjaByBus > getVoznjeVozac(String periodPar, String datumPocetka, String listaOdabranihVozaca) {

        System.out.println("Parametri: " + periodPar + " " + datumPocetka + " " + listaOdabranihVozaca);

        ConnectionBus connBus = new ConnectionBus();
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String datumDO = "0001-01-01";
        String period = periodPar;

        ArrayList < VoznjaByBus > result = new ArrayList < VoznjaByBus > ();

        //izracunavanje datuma perioda
        if (period.equals("1 Dan")) {
            try {
                cal.setTime(dateFormat.parse(datumPocetka));
                cal.add(Calendar.DATE, 0);
                datumDO = dateFormat.format(cal.getTime());
                System.out.println(datumDO);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (period.equals("1 Tjedan")) {
            try {
                cal.setTime(dateFormat.parse(datumPocetka));
                cal.add(Calendar.DATE, 7);
                datumDO = dateFormat.format(cal.getTime());
                System.out.println(datumDO);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (period.equals("1 Mjesec")) {
            try {
                cal.setTime(dateFormat.parse(datumPocetka));
                cal.add(Calendar.DATE, 30);
                datumDO = dateFormat.format(cal.getTime());
                System.out.println(datumDO);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        String selectiraniID = listaOdabranihVozaca.substring(0, listaOdabranihVozaca.length() - 1);
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd.mm.yyyy H:m:s");

        System.out.println("Datum do:" + datumDO);
        System.out.println("select distinct VOZAC_VOZNJA from voznja WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) = '" + datumPocetka + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) = '" + datumDO + "')) AND VOZAC_VOZNJA IN ( " + selectiraniID + ")");

        ResultSet rsVozaci = connBus.executeSelectQuery("select distinct VOZAC_VOZNJA from voznja WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) = '" + datumPocetka + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) = '" + datumDO + "')) AND VOZAC_VOZNJA IN ( " + selectiraniID + ")");
        try {
            while (rsVozaci.next()) {
                System.out.println("select v.OZNAKA_VOZNJA as oznakaVoznje, v.OPIS_VOZNJA as opisVoznje, bu.GARAZNI_BROJ_BUS as garazni_broj, v.idvoznja, DATE_FORMAT (v.VRIJEME_POLASKA_VOZNJA, '%d.%m.%Y') as datum_polaska, DATE_FORMAT (v.VRIJEME_DOLASKA_VOZNJA, '%d.%m.%Y') as datum_dolaska, DATE_FORMAT(v.VRIJEME_POLASKA_VOZNJA, '%T') as vrijeme_polaska, DATE_FORMAT(v.VRIJEME_DOLASKA_VOZNJA, '%T') as vrijeme_dolaska, v.BUS_VOZNJA, b.PREZIME_VOZAC as prezime, b.IME_VOZAC as ime from rasbus.voznja as v INNER JOIN rasbus.vozac as b ON b.idvozac = v.VOZAC_VOZNJA INNER JOIN rasbus.bus as bu on bu.idBus = v.BUS_VOZNJA WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "')) AND v.VOZAC_VOZNJA = " + rsVozaci.getString("VOZAC_VOZNJA"));
                ResultSet rs = connBus.executeSelectQuery("select v.OZNAKA_VOZNJA as oznakaVoznje, v.OPIS_VOZNJA as opisVoznje, bu.GARAZNI_BROJ_BUS as garazni_broj, v.idvoznja, DATE_FORMAT (v.VRIJEME_POLASKA_VOZNJA, '%d.%m.%Y') as datum_polaska, DATE_FORMAT (v.VRIJEME_DOLASKA_VOZNJA, '%d.%m.%Y') as datum_dolaska, DATE_FORMAT(v.VRIJEME_POLASKA_VOZNJA, '%T') as vrijeme_polaska, DATE_FORMAT(v.VRIJEME_DOLASKA_VOZNJA, '%T') as vrijeme_dolaska, v.BUS_VOZNJA, b.PREZIME_VOZAC as prezime, b.IME_VOZAC as ime from rasbus.voznja as v INNER JOIN rasbus.vozac as b ON b.idvozac = v.VOZAC_VOZNJA INNER JOIN rasbus.bus as bu on bu.idBus = v.BUS_VOZNJA WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "')) AND v.VOZAC_VOZNJA = " + rsVozaci.getString("VOZAC_VOZNJA"));
                while (rs.next()) {
                    VoznjaByBus voznja = new VoznjaByBus();
                    voznja.setDatumDolaska(dateFormat2.parse(rs.getString("datum_dolaska") + " " + rs.getString("vrijeme_dolaska")));
                    voznja.setVrijemeDolaska(dateFormat2.parse(rs.getString("datum_dolaska") + " " + rs.getString("vrijeme_dolaska")));
                    voznja.setDatumPolaska(dateFormat2.parse(rs.getString("datum_polaska") + " " + rs.getString("vrijeme_polaska")));
                    voznja.setVrijemePolaska(dateFormat2.parse(rs.getString("datum_polaska") + " " + rs.getString("vrijeme_polaska")));
                    voznja.setGarazniBroj(rs.getString("garazni_broj"));
                    voznja.setIdVoznja(Integer.parseInt(rs.getString("idvoznja")));
                    voznja.setImeVozaca(rs.getString("Ime"));
                    voznja.setPrezimeVozaca(rs.getString("Prezime"));
                    voznja.setOznakaVoznje(rs.getString("oznakaVoznje"));
                    voznja.setOpisVoznje(rs.getString("opisVoznje"));
                    result.add(voznja);
                }
            }
            System.out.println("Rezultat: " + result);
            return result;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        } catch (ParseException pe) {
            // TODO Auto-generated catch block
            pe.printStackTrace();
            return null;
        }
    }

    public String getGrafickiPrikazVozac(String periodPar, String datumPocetka, String listaOdabranihVozaca) {

        String result = "";
        //za izračunavanje do kojeg datuma perioda ide upit iz baze za pregled vožnji
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        String datumDO = "0001-01-01";
        String datumi[] = datumPocetka.split("-");
        String period = periodPar;
        //privremeno rjesenje dok ne otkrijem kaako na sucelju dobiti format datuma s tockama
        String datum = datumi[2] + "." + datumi[1] + "." + datumi[0];
        String selectiraniID = listaOdabranihVozaca.substring(0, listaOdabranihVozaca.length() - 1);

        System.out.println(selectiraniID.length());
        System.out.println(period + " " + datum + " " + selectiraniID);
        //System.out.println("select idvoznja, DATE_FORMAT (VRIJEME_POLASKA_VOZNJA, '%d.%m.%Y'), DATE_FORMAT (VRIJEME_DOLASKA_VOZNJA, '%d.%m.%Y'), BUS_VOZNJA from voznja WHERE DATE(VRIJEME_POLASKA_VOZNJA) = '" + datum + "' AND BUS_VOZNJA IN (" +  selectiraniID + ")");
        //izracunavanje datuma perioda
        if (period.equals("1 Dan")) {
            try {
                cal.setTime(dateFormat.parse(datumPocetka));
                cal.add(Calendar.DATE, 0);
                datumDO = dateFormat.format(cal.getTime());
                System.out.println(datumDO);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (period.equals("1 Tjedan")) {
            try {
                cal.setTime(dateFormat.parse(datumPocetka));
                cal.add(Calendar.DATE, 7);
                datumDO = dateFormat.format(cal.getTime());
                System.out.println(datumDO);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (period.equals("1 Mjesec")) {
            try {
                cal.setTime(dateFormat.parse(datumPocetka));
                cal.add(Calendar.DATE, 30);
                datumDO = dateFormat.format(cal.getTime());
                System.out.println(datumDO);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        try {
            ConnectionBus connBus = new ConnectionBus();
            ResultSet rsVozaci = connBus.executeSelectQuery("select distinct VOZAC_VOZNJA from voznja WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) = '" + datumPocetka + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) = '" + datumDO + "')) AND VOZAC_VOZNJA IN ( " + selectiraniID + ")");
            System.out.println("select distinct VOZAC_VOZNJA from voznja WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) = '" + datumPocetka + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) = '" + datumDO + "')) AND VOZAC_VOZNJA IN ( " + selectiraniID + ")");
            System.out.println(rsVozaci.isBeforeFirst());
            if (rsVozaci.isBeforeFirst() && period.equals("1 Dan")) {
                result = "{\"busevi\":[ ";
                while (rsVozaci.next()) {
                    result += " {\"GB\":\"" + rsVozaci.getString("VOZAC_VOZNJA") + "\", \"voznje\": [";
                    System.out.println("dnevni - select v.idvoznja, DATE_FORMAT (v.VRIJEME_POLASKA_VOZNJA, '%d.%m.%Y') as datum_polaska, DATE_FORMAT (v.VRIJEME_DOLASKA_VOZNJA, '%d.%m.%Y') as datum_dolaska, DATE_FORMAT(v.VRIJEME_POLASKA_VOZNJA, '%T') as vrijeme_polaska, DATE_FORMAT(v.VRIJEME_DOLASKA_VOZNJA, '%T') as vrijeme_dolaska, v.BUS_VOZNJA, b.PREZIME_VOZAC as prezime, b.IME_VOZAC as ime from rasbus.voznja as v INNER JOIN rasbus.vozac as b ON b.idvozac = v.VOZAC_VOZNJA WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "')) AND VOZAC_VOZNJA = " + rsVozaci.getString("VOZAC_VOZNJA"));
                    ResultSet rs = connBus.executeSelectQuery("select v.idvoznja, DATE_FORMAT (v.VRIJEME_POLASKA_VOZNJA, '%d.%m.%Y') as datum_polaska, DATE_FORMAT (v.VRIJEME_DOLASKA_VOZNJA, '%d.%m.%Y') as datum_dolaska, DATE_FORMAT(v.VRIJEME_POLASKA_VOZNJA, '%T') as vrijeme_polaska, DATE_FORMAT(v.VRIJEME_DOLASKA_VOZNJA, '%T') as vrijeme_dolaska, v.BUS_VOZNJA, b.PREZIME_VOZAC as prezime, b.IME_VOZAC as ime, bu.MODEL_BUS as modelBus, bu.MARKA_BUS as markaBus, bu.GARAZNI_BROJ_BUS as garazniBroj from rasbus.voznja as v INNER JOIN rasbus.vozac as b ON b.idvozac = v.VOZAC_VOZNJA INNER JOIN rasbus.bus as bu ON bu.idBus = v.BUS_VOZNJA WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "')) AND VOZAC_VOZNJA = " + rsVozaci.getString("VOZAC_VOZNJA"));
                    while (rs.next()) {
                        result += " {\"IDvoznje\":\"" + rs.getString("idvoznja") + "\", \"Vozac\":\"" + rs.getString("Ime") + " " + rs.getString("Prezime") + "\", \"pocetak_datum\":\"" + rs.getString("datum_polaska") + "\",\"pocetak_vrijeme\":\"" + rs.getString("vrijeme_polaska") + "\",\"kraj_datum\":\"" + rs.getString("datum_dolaska") + "\",\"kraj_vrijeme\":\"" + rs.getString("vrijeme_dolaska") + "\",\"pregledOD\":\"" + datum + "\",\"vozac\":\"" + rs.getString("prezime") + " " + rs.getString("ime") + "\",\"bus\":\"" + rs.getString("modelBus") + " " + rs.getString("markaBus") + "\"},";
                    }
                    result = result.substring(0, result.length() - 1);
                    result += "]},";
                }
                result = result.substring(0, result.length() - 1);
                result += "]}";
                System.out.println(result);
            } else if (rsVozaci.isBeforeFirst() && period.equals("1 Tjedan")) {
                result = "{\"busevi\":[ ";
                while (rsVozaci.next()) {
                    result += " {\"GB\":\"" + rsVozaci.getString("VOZAC_VOZNJA") + "\", \"voznje\": [";
                    System.out.println("tjedni - select v.idvoznja, DATE_FORMAT (v.VRIJEME_POLASKA_VOZNJA, '%d.%m.%Y') as datum_polaska, DATE_FORMAT (v.VRIJEME_DOLASKA_VOZNJA, '%d.%m.%Y') as datum_dolaska, DATE_FORMAT(v.VRIJEME_POLASKA_VOZNJA, '%T') as vrijeme_polaska, DATE_FORMAT(v.VRIJEME_DOLASKA_VOZNJA, '%T') as vrijeme_dolaska, v.BUS_VOZNJA, b.PREZIME_VOZAC as prezime, b.IME_VOZAC as ime from rasbus.voznja as v INNER JOIN rasbus.vozac as b ON b.idvozac = v.VOZAC_VOZNJA WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "')) AND VOZAC_VOZNJA = " + rsVozaci.getString("VOZAC_VOZNJA"));
                    ResultSet rs = connBus.executeSelectQuery("select v.idvoznja, DATE_FORMAT (v.VRIJEME_POLASKA_VOZNJA, '%d.%m.%Y') as datum_polaska, DATE_FORMAT (v.VRIJEME_DOLASKA_VOZNJA, '%d.%m.%Y') as datum_dolaska, DATE_FORMAT(v.VRIJEME_POLASKA_VOZNJA, '%T') as vrijeme_polaska, DATE_FORMAT(v.VRIJEME_DOLASKA_VOZNJA, '%T') as vrijeme_dolaska, v.BUS_VOZNJA, b.PREZIME_VOZAC as prezime, b.IME_VOZAC as ime, bu.MODEL_BUS as modelBus, bu.MARKA_BUS as markaBus, bu.GARAZNI_BROJ_BUS as garazniBroj from rasbus.voznja as v INNER JOIN rasbus.vozac as b ON b.idvozac = v.VOZAC_VOZNJA INNER JOIN rasbus.bus as bu ON bu.idBus = v.BUS_VOZNJA WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "')) AND VOZAC_VOZNJA = " + rsVozaci.getString("VOZAC_VOZNJA"));
                    while (rs.next()) {
                        result += " {\"IDvoznje\":\"" + rs.getString("idvoznja") + "\", \"Vozac\":\"" + rs.getString("Ime") + " " + rs.getString("Prezime") + "\", \"pocetak_datum\":\"" + rs.getString("datum_polaska") + "\",\"pocetak_vrijeme\":\"" + rs.getString("vrijeme_polaska") + "\",\"kraj_datum\":\"" + rs.getString("datum_dolaska") + "\",\"kraj_vrijeme\":\"" + rs.getString("vrijeme_dolaska") + "\",\"pregledOD\":\"" + datum + "\",\"vozac\":\"" + rs.getString("prezime") + " " + rs.getString("ime") + "\",\"bus\":\"" + rs.getString("modelBus") + " " + rs.getString("markaBus") + "\"},";
                    }
                    result = result.substring(0, result.length() - 1);
                    result += "]},";
                }
                result = result.substring(0, result.length() - 1);
                result += "]}";
                System.out.println(result + "tjedni");
            } else if (rsVozaci.isBeforeFirst() && period.equals("1 Mjesec")) {
                result = "{\"busevi\":[ ";
                while (rsVozaci.next()) {
                    result += " {\"GB\":\"" + rsVozaci.getString("VOZAC_VOZNJA") + "\", \"voznje\": [";
                    System.out.println("mjesecni - select v.idvoznja, DATE_FORMAT (v.VRIJEME_POLASKA_VOZNJA, '%d.%m.%Y') as datum_polaska, DATE_FORMAT (v.VRIJEME_DOLASKA_VOZNJA, '%d.%m.%Y') as datum_dolaska, DATE_FORMAT(v.VRIJEME_POLASKA_VOZNJA, '%T') as vrijeme_polaska, DATE_FORMAT(v.VRIJEME_DOLASKA_VOZNJA, '%T') as vrijeme_dolaska, v.BUS_VOZNJA, b.PREZIME_VOZAC as prezime, b.IME_VOZAC as ime from rasbus.voznja as v INNER JOIN rasbus.vozac as b ON b.idvozac = v.VOZAC_VOZNJA WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "')) AND VOZAC_VOZNJA = " + rsVozaci.getString("VOZAC_VOZNJA"));
                    ResultSet rs = connBus.executeSelectQuery("select v.idvoznja, DATE_FORMAT (v.VRIJEME_POLASKA_VOZNJA, '%d.%m.%Y') as datum_polaska, DATE_FORMAT (v.VRIJEME_DOLASKA_VOZNJA, '%d.%m.%Y') as datum_dolaska, DATE_FORMAT(v.VRIJEME_POLASKA_VOZNJA, '%T') as vrijeme_polaska, DATE_FORMAT(v.VRIJEME_DOLASKA_VOZNJA, '%T') as vrijeme_dolaska, v.BUS_VOZNJA, b.PREZIME_VOZAC as prezime, b.IME_VOZAC as ime, bu.MODEL_BUS as modelBus, bu.MARKA_BUS as markaBus, bu.GARAZNI_BROJ_BUS as garazniBroj from rasbus.voznja as v INNER JOIN rasbus.vozac as b ON b.idvozac = v.VOZAC_VOZNJA INNER JOIN rasbus.bus as bu ON bu.idBus = v.BUS_VOZNJA WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumPocetka + "' AND '" + datumDO + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumPocetka + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumDO + "')) AND VOZAC_VOZNJA = " + rsVozaci.getString("VOZAC_VOZNJA"));
                    while (rs.next()) {
                        result += " {\"IDvoznje\":\"" + rs.getString("idvoznja") + "\", \"Vozac\":\"" + rs.getString("Ime") + " " + rs.getString("Prezime") + "\", \"pocetak_datum\":\"" + rs.getString("datum_polaska") + "\",\"pocetak_vrijeme\":\"" + rs.getString("vrijeme_polaska") + "\",\"kraj_datum\":\"" + rs.getString("datum_dolaska") + "\",\"kraj_vrijeme\":\"" + rs.getString("vrijeme_dolaska") + "\",\"pregledOD\":\"" + datum + "\",\"vozac\":\"" + rs.getString("prezime") + " " + rs.getString("ime") + "\",\"bus\":\"" + rs.getString("modelBus") + " " + rs.getString("markaBus") + "\"},";
                    }
                    result = result.substring(0, result.length() - 1);
                    result += "]},";
                }
                result = result.substring(0, result.length() - 1);
                result += "]}";
                System.out.println(result + "mjesecni");
            } else {
                result = "0";
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    }

    public List < Voznja > getVoznjaForm(String mjestoPolaska, String mjestoDolaska, String prezimeVozaca, String garazniBroj, String vrijemePolaska, String vrijemeDolaska, String oznakaVoznje) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        RasbusDB dbFacade = (RasbusDB) RuntimeAccess.getInstance().getServiceBean("rasbusDB");
        String sql = "SELECT voz FROM Voznja voz " +
        		" INNER JOIN fetch voz.bus " +
        		" INNER JOIN fetch voz.vozac " +
        		" LEFT JOIN fetch voz.zatvorenavoznja " +
        " where voz.idvoznja > 0 ";
        if (StringUtils.isNotEmpty(mjestoPolaska) && mjestoPolaska != null && !mjestoPolaska.equals("null")) sql += " AND voz.mjestoPolaskaVoznja LIKE '" + mjestoPolaska + "%' ";
        if (StringUtils.isNotEmpty(mjestoDolaska) && mjestoDolaska != null && !mjestoDolaska.equals("null")) sql += " AND voz.mjestoDolaskaVoznja LIKE '" + mjestoDolaska + "%' ";
        if (StringUtils.isNotEmpty(prezimeVozaca) && prezimeVozaca != null && !prezimeVozaca.equals("null")) sql += " AND voz.vozac.prezimeVozac LIKE '" + prezimeVozaca + "%' ";
        if (StringUtils.isNotEmpty(oznakaVoznje) && oznakaVoznje != null && !oznakaVoznje.equals("null")) sql += " AND voz.oznakaVoznja LIKE '" + oznakaVoznje + "%' ";
        if (StringUtils.isNotEmpty(garazniBroj) && garazniBroj != null && !garazniBroj.equals("null")) sql += " AND voz.bus.garazniBrojBus LIKE '" + garazniBroj + "%' ";
        if (StringUtils.isNotEmpty(vrijemePolaska) && vrijemePolaska != null && !vrijemePolaska.equals("null")) sql += " AND DATE (voz.vrijemePolaskaVoznja) = :vrijemePolaska ";
        if (StringUtils.isNotEmpty(vrijemeDolaska) && vrijemeDolaska != null && !vrijemeDolaska.equals("null")) sql += " AND DATE (voz.vrijemeDolaskaVoznja) = :vrijemeDolaska ";

        dbFacade.begin();
        Session session = dbFacade.getDataServiceManager().getSession();
        Query query = session.createQuery(sql);
        if (StringUtils.isNotEmpty(vrijemePolaska) && vrijemePolaska != null && !vrijemePolaska.equals("null")) try {
            cal.setTimeInMillis(new Long(vrijemePolaska));
            System.out.println(cal);
            query.setParameter("vrijemePolaska", (Date) formatter.parse(formatter.format(cal.getTime())));
        } catch (HibernateException e) {
            e.printStackTrace();
            dbFacade.rollback();
            return null;
        } catch (ParseException e) {
			e.printStackTrace();
			dbFacade.rollback();
            return null;
		}
        if (StringUtils.isNotEmpty(vrijemeDolaska) && vrijemeDolaska != null && !vrijemeDolaska.equals("null")) {
            try {
                cal.setTimeInMillis(new Long(vrijemeDolaska));
                System.out.println(cal);
                query.setParameter("vrijemeDolaska", (Date) formatter.parse(formatter.format(cal.getTime())));
            } catch (HibernateException e) {
                e.printStackTrace();
                dbFacade.rollback();
                return null;
            } catch (ParseException e) {
				e.printStackTrace();
				dbFacade.rollback();
	            return null;
			}
        }
        System.out.println(query.getQueryString());@SuppressWarnings("unchecked")
        List < Voznja > rsBusevi = query.list();
        dbFacade.commit();
        return rsBusevi;
    }

    /**
     * Slobodni busevi za formu pregled slobodnih resursa u odabranom terminu
     * @param pocetakVoznje
     * @param krajVoznje
     * @return
     */
    @SuppressWarnings("unchecked")
    public List < Bus > getSlobodniBusevi(String pocetakVoznje, String krajVoznje) {
        System.out.println("sada");
        System.out.println(pocetakVoznje);
        List < Bus > rsBusevi = null;
        RasbusDB dbFacade = (RasbusDB) RuntimeAccess.getInstance().getServiceBean("rasbusDB");
        dbFacade.begin();
        try {

            Session session = dbFacade.getDataServiceManager().getSession();
            String sql = "SELECT * FROM bus WHERE idBus NOT IN (SELECT BUS_VOZNJA FROM voznja WHERE VRIJEME_POLASKA_VOZNJA BETWEEN :pocetakVoznje AND :krajVoznje OR " + " VRIJEME_DOLASKA_VOZNJA BETWEEN :pocetakVoznje AND :krajVoznje OR " + " (VRIJEME_POLASKA_VOZNJA <= :pocetakVoznje AND VRIJEME_DOLASKA_VOZNJA >= :krajVoznje)) ";

            SQLQuery query = session.createSQLQuery(sql);
            if (StringUtils.isNotEmpty(pocetakVoznje)) query.setParameter("pocetakVoznje", new Date(new Long(pocetakVoznje)));
            if (StringUtils.isNotEmpty(krajVoznje)) query.setParameter("krajVoznje", new Date(new Long(krajVoznje)));
            query.addEntity(Bus.class);
            rsBusevi = query.list();

            dbFacade.commit();
        } catch (HibernateException he) {
            System.out.println(he);
            dbFacade.rollback();
        } catch (Exception e) {
            System.out.println(e);
            dbFacade.rollback();
        }
        System.out.println("Uspješno izvršeno dohvaćanje slobodnih buseva");
        return rsBusevi;
    }

    /**
     * Slobodni vozaci za formu pregled slobodnih resursa u odabranom terminu
     * @param pocetakVoznje
     * @param krajVoznje
     * @return
     */
    @SuppressWarnings("unchecked")
    public List < Vozac > getSlobodniVozaci(String pocetakVoznje, String krajVoznje) {
        List < Vozac > rsVozaci = null;
        RasbusDB dbFacade = (RasbusDB) RuntimeAccess.getInstance().getServiceBean("rasbusDB");
        dbFacade.begin();
        try {

            Session session = dbFacade.getDataServiceManager().getSession();
            //        String sql = "FROM vozac WHERE idvozac NOT IN (SELECT VOZAC_VOZNJA FROM voznja WHERE VRIJEME_POLASKA_VOZNJA BETWEEN :pocetakVoznje AND :krajVoznje OR " +
            //        		" VRIJEME_DOLASKA_VOZNJA BETWEEN :pocetakVoznje AND :krajVoznje OR " +
            //    			" (VRIJEME_POLASKA_VOZNJA <= :pocetakVoznje AND VRIJEME_DOLASKA_VOZNJA >= :krajVoznje)) ";
            String sql = "FROM Vozac vo WHERE vo NOT IN (SELECT v.vozac FROM Voznja v " + " WHERE v.vrijemePolaskaVoznja BETWEEN :pocetakVoznje AND :krajVoznje OR " + " v.vrijemeDolaskaVoznja BETWEEN :pocetakVoznje AND :krajVoznje OR " + " (v.vrijemePolaskaVoznja <= :pocetakVoznje AND v.vrijemeDolaskaVoznja >= :krajVoznje)) ";
            Query query = session.createQuery(sql);
            if (StringUtils.isNotEmpty(pocetakVoznje)) query.setParameter("pocetakVoznje", new Date(new Long(pocetakVoznje)));
            if (StringUtils.isNotEmpty(krajVoznje)) query.setParameter("krajVoznje", new Date(new Long(krajVoznje)));
            rsVozaci = query.list();

            dbFacade.commit();
        } catch (HibernateException he) {
            System.out.println(he);
            dbFacade.rollback();
        } catch (Exception e) {
            System.out.println(e);
            dbFacade.rollback();
        }
        System.out.println("Uspješno izvršeno dohvaćanje slobodnih vozaca");
        return rsVozaci;
    }
    /**
     * Daje zauzete resurse kroz voznje za formu slobodnih resursa 
     * @param pocetakVoznje
     * @param krajVoznje
     * @return
     */
    @SuppressWarnings("unchecked")
    public List < Voznja > getVoznje(String pocetakVoznje, String krajVoznje) {
        List < Voznja > rsVoznje = null;
        RasbusDB dbFacade = (RasbusDB) RuntimeAccess.getInstance().getServiceBean("rasbusDB");
        dbFacade.begin();
        try {

            Session session = dbFacade.getDataServiceManager().getSession();
            String sql = "SELECT v FROM Voznja v " + " INNER JOIN fetch v.bus " + " INNER JOIN fetch v.vozac LEFT JOIN fetch v.zatvorenavoznja " + " WHERE v.vrijemePolaskaVoznja BETWEEN :pocetakVoznje AND :krajVoznje OR " + " v.vrijemeDolaskaVoznja BETWEEN :pocetakVoznje AND :krajVoznje OR " + " (v.vrijemePolaskaVoznja <= :pocetakVoznje AND v.vrijemeDolaskaVoznja >= :krajVoznje) ";
            Query query = session.createQuery(sql);
            if (StringUtils.isNotEmpty(pocetakVoznje)) query.setParameter("pocetakVoznje", new Date(new Long(pocetakVoznje)));
            if (StringUtils.isNotEmpty(krajVoznje)) query.setParameter("krajVoznje", new Date(new Long(krajVoznje)));
            rsVoznje = query.list();

            dbFacade.commit();
        } catch (HibernateException he) {
            System.out.println(he);
            dbFacade.rollback();
        } catch (Exception e) {
            System.out.println(e);
            dbFacade.rollback();
        }
        System.out.println("Uspješno izvršeno dohvaćanje voznji");
        return rsVoznje;
    }


    @SuppressWarnings("unchecked")
	public String getSlobodnaVozilaGraficki(String pocetakVoznje, String krajVoznje) {
        String result = null;
        //za izračunavanje do kojeg datuma perioda ide upit iz baze za pregled vožnji
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat mojformat = new SimpleDateFormat("dd.MM.yyyy");
        SimpleDateFormat vrijemeformat = new SimpleDateFormat("HH:mm:ss");

        if (StringUtils.isNotEmpty(pocetakVoznje) && StringUtils.isNotEmpty(krajVoznje)) {
            Long trajanjeLong = (new Long(krajVoznje) / 86400000) - (new Long(pocetakVoznje) / 86400000) + 1;
            String trajanjeString = String.valueOf(trajanjeLong.intValue());

            //centriranje na sredinu
            String datumminus1 = "";
            String datumplus1 = "";
            String datumminus = "";
            String datumplus = "";
            Integer mnozitelj = 1;
            if (trajanjeString.equals("1") || trajanjeString.equals("2")) {
                mnozitelj = 3;
            } else if (trajanjeString.equals("3") || trajanjeString.equals("4")) {
                mnozitelj = 2;
            } else if (trajanjeString.equals("5") || trajanjeString.equals("6") || trajanjeString.equals("9") || trajanjeString.equals("10") || trajanjeString.equals("13") || trajanjeString.equals("14") || trajanjeString.equals("17") || trajanjeString.equals("18") || trajanjeString.equals("21") || trajanjeString.equals("22")) {
                mnozitelj = 5;
            } else if (trajanjeString.equals("7") || trajanjeString.equals("8") || trajanjeString.equals("11") || trajanjeString.equals("12") || trajanjeString.equals("15") || trajanjeString.equals("16") || trajanjeString.equals("19") || trajanjeString.equals("20") || trajanjeString.equals("23") || trajanjeString.equals("24")) {
                mnozitelj = 4;
            } else if (trajanjeString.equals("25") || trajanjeString.equals("26") || trajanjeString.equals("27")) {
                mnozitelj = 3;
            } else if (trajanjeString.equals("28") || trajanjeString.equals("29")) {
                mnozitelj = 2;
            } else if (trajanjeString.equals("30") || trajanjeString.equals("31")) {
                mnozitelj = 1;
            }
            try {
                cal.setTime(new Date(new Long(pocetakVoznje)));
                cal.add(Calendar.DATE, -mnozitelj);
                datumminus1 = dateFormat.format(cal.getTime());
                datumminus = mojformat.format(dateFormat.parse(datumminus1));
                System.out.println(datumminus + " je u minusu");
                cal.setTime(new Date(new Long(krajVoznje)));
                cal.add(Calendar.DATE, mnozitelj + 1);
                datumplus1 = dateFormat.format(cal.getTime());
                datumplus = mojformat.format(dateFormat.parse(datumplus1));
                System.out.println(datumplus + " je u plusu");

            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.println(trajanjeString + " " + datumminus + "" + datumplus);
            RasbusDB dbFacade = (RasbusDB) RuntimeAccess.getInstance().getServiceBean("rasbusDB");
            try {             
                dbFacade.begin();
                Session session = dbFacade.getDataServiceManager().getSession();
                String sql = "select distinct v.BUS_VOZNJA as IDBus, s.GARAZNI_BROJ_BUS as GBBus, s.MARKA_BUS as markaBUS, s.MODEL_BUS as modelBUS from voznja v " + "INNER JOIN rasbus.bus as s ON s.idBus = v.BUS_VOZNJA WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumminus1 + "' AND '" + datumplus1 + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumminus1 + "' AND '" + datumplus1 + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumminus1 + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumplus1 + "'))";
                String hql = "select v from Voznja v INNER JOIN fetch v.bus WHERE ((DATE(v.vrijemePolaskaVoznja) BETWEEN '" + datumminus1 + "' AND '" + datumplus1 + "') OR (DATE(v.vrijemeDolaskaVoznja) BETWEEN '" + datumminus1 + "' AND '" + datumplus1 + "') OR (DATE(v.vrijemePolaskaVoznja) <= '" + datumminus1 + "' AND DATE(v.vrijemeDolaskaVoznja) >='" + datumplus1 + "'))";
                Query query = session.createQuery(hql); 
                SortedSet<Voznja> uniqueSet = new TreeSet<Voznja>(new Comparator<Voznja>() {
                	public int compare(Voznja o1, Voznja o2){
                		if(o1.getBus().getIdBus() == o2.getBus().getIdBus()){
                			return 0;
                		}else{
                			return 1;
                		}
                	}
				});
                List<Voznja> rsBusevi = query.list();
                uniqueSet.addAll(rsBusevi);                
                rsBusevi = new ArrayList<Voznja>(uniqueSet);
                
                System.out.println(sql);

                //ResultSet rsBusevi2 = null;
                List<Bus> rsBusevi2 = null;
                if (rsBusevi.size() > 0) {
                    query = session.createQuery("select b from Bus b where b NOT IN (select v.bus from Voznja v WHERE ((DATE(v.vrijemePolaskaVoznja) BETWEEN '" + datumminus1 + "' AND '" + datumplus1 + "') OR (DATE(v.vrijemeDolaskaVoznja) BETWEEN '" + datumminus1 + "' AND '" + datumplus1 + "') OR (DATE(v.vrijemePolaskaVoznja) <= '" + datumminus1 + "' AND DATE(v.vrijemeDolaskaVoznja) >='" + datumplus1 + "')))");
                    rsBusevi2 = query.list();
                } else {
                    query = session.createQuery("select b from Bus b");
                    rsBusevi2 = query.list();
                }

                result = "{\"busevi\":[ ";
                System.out.println(result + "prvi");
                if (rsBusevi.size() > 0) {
                    for (Voznja voznja : rsBusevi) {
                        result += " {\"ID\":\"" + voznja.getBus().getIdBus() + "\", \"GB\":\"" + voznja.getBus().getGarazniBrojBus() + "\", \"marka\":\"" + voznja.getBus().getMarkaBus() + "\" ,\"model\":\"" + voznja.getBus().getModelBus() + "\",\"terminOD\":\"" + dateFormat.format(new Date(new Long(pocetakVoznje))) + "\", \"terminDo\":\"" + dateFormat.format(new Date(new Long(krajVoznje))) + "\" ,\"trajanje\":\"" + trajanjeString + "\" , \"mnozitelj\":\"" + mnozitelj + "\",\"voznje\": [";
                        System.out.println("dnevni - select v.idvoznja, DATE_FORMAT (v.VRIJEME_POLASKA_VOZNJA, '%d.%m.%Y') as datum_polaska, DATE_FORMAT (v.VRIJEME_DOLASKA_VOZNJA, '%d.%m.%Y') as datum_dolaska, DATE_FORMAT(v.VRIJEME_POLASKA_VOZNJA, '%T') as vrijeme_polaska, DATE_FORMAT(v.VRIJEME_DOLASKA_VOZNJA, '%T') as vrijeme_dolaska, v.BUS_VOZNJA, b.PREZIME_VOZAC as prezime, b.IME_VOZAC as ime from rasbus.voznja as v INNER JOIN rasbus.vozac as b ON b.idvozac = v.VOZAC_VOZNJA WHERE ((DATE(VRIJEME_POLASKA_VOZNJA) BETWEEN '" + datumminus1 + "' AND '" + datumplus1 + "') OR (DATE(VRIJEME_DOLASKA_VOZNJA) BETWEEN '" + datumminus1 + "' AND '" + datumplus1 + "') OR (DATE(VRIJEME_POLASKA_VOZNJA) <= '" + datumminus1 + "' AND DATE(VRIJEME_DOLASKA_VOZNJA) >='" + datumplus1 + "')) AND BUS_VOZNJA = " + voznja.getBus().getIdBus());
                        query = session.createQuery("select v from Voznja v WHERE ((DATE(v.vrijemePolaskaVoznja) BETWEEN '" + datumminus1 + "' AND '" + datumplus1 + "') OR (DATE(v.vrijemeDolaskaVoznja) BETWEEN '" + datumminus1 + "' AND '" + datumplus1 + "') OR (DATE(v.vrijemePolaskaVoznja) <= '" + datumminus1 + "' AND DATE(v.vrijemeDolaskaVoznja) >='" + datumplus1 + "')) AND v.bus.idBus = " + voznja.getBus().getIdBus() + " ORDER BY v.vrijemeDolaskaVoznja");
                        List<Voznja> rs = query.list();
                        for(Voznja voznjaRow : rs) {
                            result += " {\"IDvoznje\":\"" + voznjaRow.getIdvoznja() + "\", " +
                            		"\"Vozac\":\"" + voznjaRow.getVozac().getImeVozac() + " " + voznjaRow.getVozac().getPrezimeVozac() + "\"," +
                            				" \"pocetak_datum\":\"" + mojformat.format(voznjaRow.getVrijemePolaskaVoznja()) + "\"," +
                            						"\"pocetak_vrijeme\":\"" + vrijemeformat.format(voznjaRow.getVrijemePolaskaVoznja()) + "\"," +
                            								"\"kraj_datum\":\"" + mojformat.format(voznjaRow.getVrijemeDolaskaVoznja()) + "\"," +
                            										"\"kraj_vrijeme\":\"" + vrijemeformat.format(voznjaRow.getVrijemeDolaskaVoznja()) + "\"," +
                            												"\"pregledOD\":\"" + datumminus + "\"," +
                            													"\"oznakaVoznje\":\"" +voznjaRow.getOznakaVoznja()+ "\"," +
                            														"\"vozac\":\"" + voznjaRow.getVozac().getImeVozac() + " " + voznjaRow.getVozac().getPrezimeVozac() + "\"},";
                        }
                        result = result.substring(0, result.length() - 1);
                        result += "]},";
                    }
                }
                if (rsBusevi2.size() > 0) {
                	for (Bus bus : rsBusevi2) {
                        result += " {\"ID\":\"" + bus.getIdBus() + "\", \"GB\":\"" + bus.getGarazniBrojBus() + "\", \"marka\":\"" + bus.getMarkaBus() + "\" ,\"model\":\"" + bus.getModelBus() + "\",\"terminOD\":\"" + dateFormat.format(new Date(new Long(pocetakVoznje))) + "\", \"terminDo\":\"" + dateFormat.format(new Date(new Long(krajVoznje))) + "\" ,\"trajanje\":\"" + trajanjeString + "\" , \"mnozitelj\":\"" + mnozitelj + "\",\"voznje\": ";
                        result = result.substring(0, result.length() - 1);
                        result += "[{\"pregledOD\":\"" + datumminus + "\"}]},";
                    }
                }
                result = result.substring(0, result.length() - 1);
                result += "]}";
                System.out.println(result);
                dbFacade.commit();

            } catch (HibernateException e) {
                e.printStackTrace();
                dbFacade.rollback();
            }catch(Exception ex){
            	ex.printStackTrace();
                dbFacade.rollback();
            }
        }
        return result;
    }
    @SuppressWarnings("unchecked")
	public String getSlobodniVozaciGraficki(String pocetakVoznje, String krajVoznje) {
        String result = null;
        //za izračunavanje do kojeg datuma perioda ide upit iz baze za pregled vožnji
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat mojformat = new SimpleDateFormat("dd.MM.yyyy");
        SimpleDateFormat vrijemeformat = new SimpleDateFormat("HH:mm:ss");

        if (StringUtils.isNotEmpty(pocetakVoznje) && StringUtils.isNotEmpty(krajVoznje)) {
            Long trajanjeLong = (new Long(krajVoznje) / 86400000) - (new Long(pocetakVoznje) / 86400000) + 1;
            String trajanjeString = String.valueOf(trajanjeLong.intValue());

            //centriranje na sredinu
            String datumminus1 = "";
            String datumplus1 = "";
            String datumminus = "";
            String datumplus = "";
            Integer mnozitelj = 1;

            if (trajanjeString.equals("1") || trajanjeString.equals("2")) {
                mnozitelj = 3;
            } else if (trajanjeString.equals("3") || trajanjeString.equals("4")) {
                mnozitelj = 2;
            } else if (trajanjeString.equals("5") || trajanjeString.equals("6") || trajanjeString.equals("9") || trajanjeString.equals("10") || trajanjeString.equals("13") || trajanjeString.equals("14") || trajanjeString.equals("17") || trajanjeString.equals("18") || trajanjeString.equals("21") || trajanjeString.equals("22")) {
                mnozitelj = 5;
            } else if (trajanjeString.equals("7") || trajanjeString.equals("8") || trajanjeString.equals("11") || trajanjeString.equals("12") || trajanjeString.equals("15") || trajanjeString.equals("16") || trajanjeString.equals("19") || trajanjeString.equals("20") || trajanjeString.equals("23") || trajanjeString.equals("24")) {
                mnozitelj = 4;
            } else if (trajanjeString.equals("25") || trajanjeString.equals("26") || trajanjeString.equals("27")) {
                mnozitelj = 3;
            } else if (trajanjeString.equals("28") || trajanjeString.equals("29")) {
                mnozitelj = 2;
            } else if (trajanjeString.equals("30") || trajanjeString.equals("31")) {
                mnozitelj = 1;
            }                  
                        
            try {
                cal.setTime(new Date(new Long(pocetakVoznje)));
                cal.add(Calendar.DATE, -mnozitelj);
                datumminus1 = dateFormat.format(cal.getTime());
                datumminus = mojformat.format(dateFormat.parse(datumminus1));
                System.out.println(datumminus + " je u minusu");
                cal.setTime(new Date(new Long(krajVoznje)));
                cal.add(Calendar.DATE, mnozitelj + 1);
                datumplus1 = dateFormat.format(cal.getTime());
                datumplus = mojformat.format(dateFormat.parse(datumplus1));
                System.out.println(datumplus + " je u plusu");                                

            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.println(trajanjeString + " " + datumminus + "" + datumplus);   
            RasbusDB dbFacade = (RasbusDB) RuntimeAccess.getInstance().getServiceBean("rasbusDB");
            try {
            	dbFacade.begin();
                Session session = dbFacade.getDataServiceManager().getSession();                
                String hql = "select v from Voznja v INNER JOIN fetch v.vozac WHERE ((DATE(v.vrijemePolaskaVoznja) BETWEEN '" + datumminus1 + "' AND '" + datumplus1 + "') OR (DATE(v.vrijemeDolaskaVoznja) BETWEEN '" + datumminus1 + "' AND '" + datumplus1 + "') OR (DATE(v.vrijemePolaskaVoznja) <= '" + datumminus1 + "' AND DATE(v.vrijemeDolaskaVoznja) >='" + datumplus1 + "'))";
                Query query = session.createQuery(hql);                
                List<Voznja> rsBusevi = query.list();   
                SortedSet<Voznja> uniqueSet = new TreeSet<Voznja>(new Comparator<Voznja>() {
                	public int compare(Voznja o1, Voznja o2){
                		if(o1.getVozac().getIdvozac() == o2.getVozac().getIdvozac()){
                			return 0;
                		}else{
                			return 1;
                		}
                	}
				});
                uniqueSet.addAll(rsBusevi);                
                rsBusevi = new ArrayList<Voznja>(uniqueSet);

                List<Vozac> rsBusevi2 = null;
                if (rsBusevi.size() > 0) {                    
                    query = session.createQuery("select voz from Vozac voz where voz NOT IN (select v.vozac from Voznja v WHERE ((DATE(v.vrijemePolaskaVoznja) BETWEEN '" + datumminus1 + "' AND '" + datumplus1 + "') OR (DATE(v.vrijemeDolaskaVoznja) BETWEEN '" + datumminus1 + "' AND '" + datumplus1 + "') OR (DATE(v.vrijemePolaskaVoznja) <= '" + datumminus1 + "' AND DATE(v.vrijemeDolaskaVoznja) >='" + datumplus1 + "')))");
                    rsBusevi2 = query.list();
                } else {
                	query = session.createQuery("select voz from Vozac voz");
                    rsBusevi2 = query.list();
                }

                result = "{\"busevi\":[ ";
                System.out.println(result + "prvi");
                if (rsBusevi.size() > 0) {
                    for(Voznja voznja : rsBusevi) {
                        result += " {\"ID\":\"" + voznja.getVozac().getIdvozac() + "\", \"Ime\":\"" + voznja.getVozac().getImeVozac() + "\", \"Prezime\":\"" + voznja.getVozac().getPrezimeVozac() + "\" ,\"terminOD\":\"" + dateFormat.format(new Date(new Long(pocetakVoznje))) + "\", \"terminDo\":\"" + dateFormat.format(new Date(new Long(krajVoznje))) + "\" ,\"trajanje\":\"" + trajanjeString + "\" , \"mnozitelj\":\"" + mnozitelj + "\",\"voznje\": [";                                                
                        query = session.createQuery("select v from Voznja v WHERE ((DATE(v.vrijemePolaskaVoznja) BETWEEN '" + datumminus1 + "' AND '" + datumplus1 + "') OR (DATE(v.vrijemeDolaskaVoznja) BETWEEN '" + datumminus1 + "' AND '" + datumplus1 + "') OR (DATE(v.vrijemePolaskaVoznja) <= '" + datumminus1 + "' AND DATE(v.vrijemeDolaskaVoznja) >='" + datumplus1 + "')) AND v.vozac.idvozac= " + voznja.getVozac().getIdvozac() + " ORDER BY v.vrijemeDolaskaVoznja");
                        List<Voznja> rs = query.list();
                        for(Voznja voznjaRow : rs) {
                            result += " {\"IDvoznje\":\"" + voznjaRow.getIdvoznja() + "\", " +
                            		"\"bus\":\"" + voznjaRow.getBus().getGarazniBrojBus() + " " + voznjaRow.getBus().getMarkaBus() + " "+ voznjaRow.getBus().getModelBus() +"\", " +
                            				"\"pocetak_datum\":\"" + mojformat.format(voznjaRow.getVrijemePolaskaVoznja()) + "\"," +
                            						"\"pocetak_vrijeme\":\"" + vrijemeformat.format(voznjaRow.getVrijemePolaskaVoznja()) + "\"," +
                            								"\"kraj_datum\":\"" + mojformat.format(voznjaRow.getVrijemeDolaskaVoznja()) + "\"," +
                            										"\"kraj_vrijeme\":\"" + vrijemeformat.format(voznjaRow.getVrijemeDolaskaVoznja()) + "\"," +
                            												"\"pregledOD\":\"" + datumminus + "\"," +
                            													"\"oznakaVoznje\":\"" +voznjaRow.getOznakaVoznja()+ "\"," +
                            														"\"bus\":\"" + voznjaRow.getBus().getGarazniBrojBus() + " " + voznjaRow.getBus().getMarkaBus() + " "+ voznjaRow.getBus().getModelBus() + "\"},";
                        }
                        result = result.substring(0, result.length() - 1);
                        result += "]},";
                    }
                }
                if (rsBusevi2.size() > 0) {
                    for(Vozac vozac : rsBusevi2) {
                        result += " {\"ID\":\"" + vozac.getIdvozac() + "\", \"Ime\":\"" + vozac.getImeVozac() + "\", \"Prezime\":\"" + vozac.getPrezimeVozac() + "\" ,\"terminOD\":\"" + dateFormat.format(new Date(new Long(pocetakVoznje))) + "\", \"terminDo\":\"" + dateFormat.format(new Date(new Long(krajVoznje))) + "\" ,\"trajanje\":\"" + trajanjeString + "\" , \"mnozitelj\":\"" + mnozitelj + "\",\"voznje\": ";
                        result = result.substring(0, result.length() - 1);
                        result += "[{\"pregledOD\":\"" + datumminus + "\"}]},";
                    }
                }
                result = result.substring(0, result.length() - 1);
                result += "]}";
                System.out.println(result);

            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
    
    public List < Voznja > getOtvoreneVoznje(String mjestoPolaska, String mjestoDolaska, String prezimeVozaca, String garazniBroj, String vrijemePolaska, String vrijemeDolaska, String oznakaVoznje) {
        Calendar cal = Calendar.getInstance();
        //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        RasbusDB dbFacade = (RasbusDB) RuntimeAccess.getInstance().getServiceBean("rasbusDB");
        String sql = "SELECT * FROM voznja " + " INNER JOIN vozac on vozac.idvozac = voznja.VOZAC_VOZNJA " + " INNER JOIN bus on bus.idBus = voznja.BUS_VOZNJA " + " where idvoznja > 0 ";
        if (StringUtils.isNotEmpty(mjestoPolaska)) sql += " AND MJESTO_POLASKA_VOZNJA LIKE '" + mjestoPolaska + "%' ";
        if (StringUtils.isNotEmpty(mjestoDolaska)) sql += " AND MJESTO_DOLASKA_VOZNJA LIKE '" + mjestoDolaska + "%' ";
        if (StringUtils.isNotEmpty(oznakaVoznje)) sql += " AND OZNAKA_VOZNJA LIKE '" + oznakaVoznje + "%' ";
        if (StringUtils.isNotEmpty(prezimeVozaca)) sql += " AND vozac.PREZIME_VOZAC LIKE '" + prezimeVozaca + "%' ";        
        if (StringUtils.isNotEmpty(garazniBroj)) sql += " AND bus.GARAZNI_BROJ_BUS LIKE '" + garazniBroj + "%' ";
        if (StringUtils.isNotEmpty(vrijemePolaska)) sql += " AND DATE(VRIJEME_POLASKA_VOZNJA) = :vrijemePolaska ";
        if (StringUtils.isNotEmpty(vrijemeDolaska)) sql += " AND DATE(VRIJEME_DOLASKA_VOZNJA) = :vrijemeDolaska ";
        sql += " AND idvoznja NOT IN (select idzatvorenaVoznja from zatvorenavoznja)";

        dbFacade.begin();
        Session session = dbFacade.getDataServiceManager().getSession();
        SQLQuery query = session.createSQLQuery(sql);
        if (StringUtils.isNotEmpty(vrijemePolaska)) try {
            cal.setTimeInMillis(new Long(vrijemePolaska));
            System.out.println(cal);
            query.setParameter("vrijemePolaska", (Date) cal.getTime());
        } catch (HibernateException e) {
            e.printStackTrace();
            dbFacade.rollback();
            return null;
        }
        if (StringUtils.isNotEmpty(vrijemeDolaska)) {
            try {
                cal.setTimeInMillis(new Long(vrijemeDolaska));
                System.out.println(cal);
                query.setParameter("vrijemeDolaska", (Date) cal.getTime());
            } catch (HibernateException e) {
                e.printStackTrace();
                dbFacade.rollback();
                return null;
            }
        }
        query.addEntity(Voznja.class);
        System.out.println(query.getQueryString());@SuppressWarnings("unchecked")
        List < Voznja > rsBusevi = query.list();
        dbFacade.commit();
        return rsBusevi;
    }

    /**
     * metoda za zatvaranje voznji
     * @param idVoznje
     * @param kilometri
     * @param troskovi
     */
    public void zatvaranjeVoznje(Integer idVoznje, Double kilometri, Double troskovi){
    	RasbusDB dbFacade = (RasbusDB) RuntimeAccess.getInstance().getServiceBean("rasbusDB");
    	try{    	
    		dbFacade.begin();
    		Session session = dbFacade.getDataServiceManager().getSession();
    		Zatvorenavoznja zatvorenaVoznja = new Zatvorenavoznja();
    		zatvorenaVoznja.setIdzatvorenaVoznja(new Integer(idVoznje));
    		zatvorenaVoznja.setKilometri(new Double(kilometri));
    		zatvorenaVoznja.setTroskovi(new Double(troskovi));
    		session.save(zatvorenaVoznja);

    		Voznja voznja = (Voznja)session.load(Voznja.class, new Integer(idVoznje));
    		voznja.setZatvorenavoznja(zatvorenaVoznja);

    		dbFacade.commit();
    	} catch (Exception e) {
    		dbFacade.rollback();
    		System.out.println("Greška kod zatvaranja vožnje!");
    		e.printStackTrace();
    	}
    }
    
    /**
     * daje zatvorene voznje za funkciju naplata voznji
     * @param mjestoPolaska
     * @param mjestoDolaska
     * @param prezimeVozaca
     * @param garazniBroj
     * @param vrijemePolaska
     * @param vrijemeDolaska
     * @return
     */
    public List < Voznja > getZatvoreneVoznje(String mjestoPolaska, String mjestoDolaska, String prezimeVozaca, String garazniBroj, String vrijemePolaska, String vrijemeDolaska, String oznakaVoznje) {
        Calendar cal = Calendar.getInstance();
        //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        RasbusDB dbFacade = (RasbusDB) RuntimeAccess.getInstance().getServiceBean("rasbusDB");
        String sql = "SELECT voz FROM Voznja as voz INNER JOIN FETCH voz.vozac as voc INNER JOIN FETCH voz.bus as bus INNER JOIN FETCH voz.zatvorenavoznja as zatVoznja where voz.idvoznja > 0 " +
        		" AND zatVoznja.naplata = NULL ";
        if (StringUtils.isNotEmpty(mjestoPolaska)) sql += " AND voz.mjestoPolaskaVoznja LIKE '" + mjestoPolaska + "%' ";
        if (StringUtils.isNotEmpty(mjestoDolaska)) sql += " AND voz.mjestoDolaskaVoznja LIKE '" + mjestoDolaska + "%' ";
        if (StringUtils.isNotEmpty(oznakaVoznje)) sql += " AND voz.oznakaVoznja LIKE '" + oznakaVoznje + "%' ";
        if (StringUtils.isNotEmpty(prezimeVozaca)) sql += " AND voc.prezimeVozac LIKE '" + prezimeVozaca + "%' ";
        if (StringUtils.isNotEmpty(garazniBroj)) sql += " AND bus.garazniBrojBus LIKE '" + garazniBroj + "%' ";
        if (StringUtils.isNotEmpty(vrijemePolaska)) sql += " AND DATE(voz.vrijemePolaskaVoznja) = :vrijemePolaska ";
        if (StringUtils.isNotEmpty(vrijemeDolaska)) sql += " AND DATE(voz.vrijemeDolaskaVoznja) = :vrijemeDolaska ";

        dbFacade.begin();
        Session session = dbFacade.getDataServiceManager().getSession();
        Query query = session.createQuery(sql);
        if (StringUtils.isNotEmpty(vrijemePolaska)) try {
            cal.setTimeInMillis(new Long(vrijemePolaska));
            System.out.println(cal);
            query.setParameter("vrijemePolaska", (Date) cal.getTime());
        } catch (HibernateException e) {
            e.printStackTrace();
            dbFacade.rollback();
            return null;
        }
        if (StringUtils.isNotEmpty(vrijemeDolaska)) {
            try {
                cal.setTimeInMillis(new Long(vrijemeDolaska));
                System.out.println(cal);
                query.setParameter("vrijemeDolaska", (Date) cal.getTime());
            } catch (HibernateException e) {
                e.printStackTrace();
                dbFacade.rollback();
                return null;
            }
        }
        //query.addEntity(Voznja.class);
        System.out.println(query.getQueryString());@SuppressWarnings("unchecked")
        List < Voznja > rsVoznje = query.list();
        dbFacade.commit();
        return rsVoznje;
    }
    
    public void naplataVoznje(Integer idVoznje, Double naplaceno){
    	RasbusDB dbFacade = (RasbusDB) RuntimeAccess.getInstance().getServiceBean("rasbusDB");
    	try{    	
    		dbFacade.begin();
       		Session session = dbFacade.getDataServiceManager().getSession();
       		
       		Zatvorenavoznja zatvorenaVoznja = (Zatvorenavoznja)session.load(Zatvorenavoznja.class, new Integer(idVoznje));
       		zatvorenaVoznja.setNaplata((Double)naplaceno);
       		
    		dbFacade.commit();
    	} catch (Exception e) {
    		dbFacade.rollback();
    		System.out.println("Greška kod zatvaranja vožnje!");
    		e.printStackTrace();
    	}
    }
    
    public List < Voznja > getOtvoreneVoznjePregled(Integer sifraNarucitelj, String nazivNarucitelj, String mjestoPolaska, String mjestoDolaska, String prezimeVozaca, String garazniBroj, String vrijemePolaska, String vrijemeDolaska, String oznakaVoznje) {
        Calendar cal = Calendar.getInstance();
        //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        RasbusDB dbFacade = (RasbusDB) RuntimeAccess.getInstance().getServiceBean("rasbusDB");
        String sql = "SELECT voz FROM Voznja as voz INNER JOIN FETCH voz.vozac as voc INNER JOIN FETCH voz.bus as bus LEFT JOIN FETCH voz.narucitelj as naruc where " +
        		" voz.zatvorenavoznja = null ";
        if (StringUtils.isNotEmpty(mjestoPolaska) && mjestoPolaska != null && !mjestoPolaska.equals("null")) sql += " AND voz.mjestoPolaskaVoznja LIKE '" + mjestoPolaska + "%' ";
        if (StringUtils.isNotEmpty(mjestoDolaska) && mjestoDolaska != null && !mjestoDolaska.equals("null")) sql += " AND voz.mjestoDolaskaVoznja LIKE '" + mjestoDolaska + "%' ";
        if (StringUtils.isNotEmpty(prezimeVozaca) && prezimeVozaca != null && !prezimeVozaca.equals("null")) sql += " AND voc.prezimeVozac LIKE '" + prezimeVozaca + "%' ";
        if (StringUtils.isNotEmpty(oznakaVoznje) && oznakaVoznje != null && !oznakaVoznje.equals("null")) sql += " AND voz.oznakaVoznja LIKE '" + oznakaVoznje + "%' ";
        if (StringUtils.isNotEmpty(garazniBroj) && garazniBroj != null && !garazniBroj.equals("null")) sql += " AND bus.garazniBrojBus LIKE '" + garazniBroj + "%' ";
        if (StringUtils.isNotEmpty(vrijemePolaska) && vrijemePolaska != null && !vrijemePolaska.equals("null")) sql += " AND DATE(voz.vrijemePolaskaVoznja) = :vrijemePolaska ";
        if (StringUtils.isNotEmpty(vrijemeDolaska) && vrijemeDolaska != null && !vrijemeDolaska.equals("null")) sql += " AND DATE(voz.vrijemeDolaskaVoznja) = :vrijemeDolaska ";
        if (sifraNarucitelj != null) sql += " AND naruc.idnarucitelj = "+ sifraNarucitelj;
        if (StringUtils.isNotEmpty(nazivNarucitelj) && nazivNarucitelj != null && !nazivNarucitelj.equals("null")) sql += " AND naruc.nazivNarucitelj like '"+ nazivNarucitelj +"%' ";        

        dbFacade.begin();
        Session session = dbFacade.getDataServiceManager().getSession();
        Query query = session.createQuery(sql);
        if (vrijemePolaska != null && StringUtils.isNotEmpty(vrijemePolaska) && !vrijemePolaska.equals("null")) try {
            cal.setTimeInMillis(new Long(vrijemePolaska));
            System.out.println(cal);
            query.setParameter("vrijemePolaska", (Date) cal.getTime());
        } catch (HibernateException e) {
            e.printStackTrace();
            dbFacade.rollback();
            return null;
        }
        if (vrijemeDolaska != null && StringUtils.isNotEmpty(vrijemeDolaska) && !vrijemeDolaska.equals("null")) {
            try {
                cal.setTimeInMillis(new Long(vrijemeDolaska));
                System.out.println(cal);
                query.setParameter("vrijemeDolaska", (Date) cal.getTime());
            } catch (HibernateException e) {
                e.printStackTrace();
                dbFacade.rollback();
                return null;
            }
        }
        //query.addEntity(Voznja.class);
        System.out.println(query.getQueryString());@SuppressWarnings("unchecked")
        List < Voznja > rsVoznje = query.list();
        dbFacade.commit();
        return rsVoznje;
    }
    
    public List < Voznja > getNenaplaceneVoznje(Integer sifraNarucitelj, String nazivNarucitelj, String mjestoPolaska, String mjestoDolaska, String prezimeVozaca, String garazniBroj, String vrijemePolaska, String vrijemeDolaska, String oznakaVoznje) {
        Calendar cal = Calendar.getInstance();
        //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        RasbusDB dbFacade = (RasbusDB) RuntimeAccess.getInstance().getServiceBean("rasbusDB");
        String sql = "SELECT voz FROM Voznja as voz INNER JOIN FETCH voz.vozac as voc INNER JOIN FETCH voz.bus as bus INNER JOIN FETCH voz.zatvorenavoznja as zatVoznja LEFT JOIN FETCH voz.narucitelj as naruc where voz.idvoznja > 0 " +
        		" AND zatVoznja.naplata = NULL ";
        if (StringUtils.isNotEmpty(mjestoPolaska) && mjestoPolaska != null && !mjestoPolaska.equals("null")) sql += " AND voz.mjestoPolaskaVoznja LIKE '" + mjestoPolaska + "%' ";
        if (StringUtils.isNotEmpty(mjestoDolaska) && mjestoDolaska != null && !mjestoDolaska.equals("null")) sql += " AND voz.mjestoDolaskaVoznja LIKE '" + mjestoDolaska + "%' ";
        if (StringUtils.isNotEmpty(prezimeVozaca) && prezimeVozaca != null && !prezimeVozaca.equals("null")) sql += " AND voc.prezimeVozac LIKE '" + prezimeVozaca + "%' ";
        if (StringUtils.isNotEmpty(oznakaVoznje) && oznakaVoznje != null && !oznakaVoznje.equals("null")) sql += " AND voz.oznakaVoznja LIKE '" + oznakaVoznje + "%' ";
        if (StringUtils.isNotEmpty(garazniBroj) && garazniBroj != null && !garazniBroj.equals("null")) sql += " AND bus.garazniBrojBus LIKE '" + garazniBroj + "%' ";
        if (StringUtils.isNotEmpty(vrijemePolaska) && vrijemePolaska != null && !vrijemePolaska.equals("null")) sql += " AND DATE(voz.vrijemePolaskaVoznja) = :vrijemePolaska ";
        if (StringUtils.isNotEmpty(vrijemeDolaska) && vrijemeDolaska != null && !vrijemeDolaska.equals("null")) sql += " AND DATE(voz.vrijemeDolaskaVoznja) = :vrijemeDolaska ";
        if (sifraNarucitelj != null) sql += " AND naruc.idnarucitelj = "+ sifraNarucitelj;
        if (StringUtils.isNotEmpty(nazivNarucitelj) && nazivNarucitelj != null && !nazivNarucitelj.equals("null")) sql += " AND naruc.nazivNarucitelj like '"+ nazivNarucitelj +"%' ";

        dbFacade.begin();
        Session session = dbFacade.getDataServiceManager().getSession();
        Query query = session.createQuery(sql);
        if (vrijemePolaska != null && StringUtils.isNotEmpty(vrijemePolaska) && !vrijemePolaska.equals("null")) try {
            cal.setTimeInMillis(new Long(vrijemePolaska));
            System.out.println(cal);
            query.setParameter("vrijemePolaska", (Date) cal.getTime());
        } catch (HibernateException e) {
            e.printStackTrace();
            dbFacade.rollback();
            return null;
        }
        if (vrijemeDolaska != null && StringUtils.isNotEmpty(vrijemeDolaska) && !vrijemeDolaska.equals("null")) {
            try {
                cal.setTimeInMillis(new Long(vrijemeDolaska));
                System.out.println(cal);
                query.setParameter("vrijemeDolaska", (Date) cal.getTime());
            } catch (HibernateException e) {
                e.printStackTrace();
                dbFacade.rollback();
                return null;
            }
        }
        //query.addEntity(Voznja.class);
        System.out.println(query.getQueryString());@SuppressWarnings("unchecked")
        List < Voznja > rsVoznje = query.list();
        dbFacade.commit();
        return rsVoznje;
    }
    
    
    public List <Voznja> getNaplaceneVoznje(Integer sifraNarucitelj, String nazivNarucitelj, String mjestoPolaska, String mjestoDolaska, String prezimeVozaca, String garazniBroj, String vrijemePolaska, String vrijemeDolaska, String oznakaVoznje) {
        Calendar cal = Calendar.getInstance();
        //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        RasbusDB dbFacade = (RasbusDB) RuntimeAccess.getInstance().getServiceBean("rasbusDB");
        String sql = "SELECT voz FROM Voznja as voz INNER JOIN FETCH voz.vozac as voc INNER JOIN FETCH voz.bus as bus INNER JOIN FETCH voz.zatvorenavoznja as zatVoznja LEFT JOIN FETCH voz.narucitelj as naruc where voz.idvoznja > 0 " +
        		" AND zatVoznja.naplata != NULL ";
        if (StringUtils.isNotEmpty(mjestoPolaska) && mjestoPolaska != null && !mjestoPolaska.equals("null")) sql += " AND voz.mjestoPolaskaVoznja LIKE '" + mjestoPolaska + "%' ";
        if (StringUtils.isNotEmpty(mjestoDolaska) && mjestoDolaska != null && !mjestoDolaska.equals("null")) sql += " AND voz.mjestoDolaskaVoznja LIKE '" + mjestoDolaska + "%' ";
        if (StringUtils.isNotEmpty(prezimeVozaca) && prezimeVozaca != null && !prezimeVozaca.equals("null")) sql += " AND voz.prezimeVozac LIKE '" + prezimeVozaca + "%' ";
        if (StringUtils.isNotEmpty(oznakaVoznje) && oznakaVoznje != null && !oznakaVoznje.equals("null")) sql += " AND voz.oznakaVoznja LIKE '" + oznakaVoznje + "%' ";
        if (StringUtils.isNotEmpty(garazniBroj) && garazniBroj != null && !garazniBroj.equals("null")) sql += " AND bus.garazniBrojBus LIKE '" + garazniBroj + "%' ";
        if (StringUtils.isNotEmpty(vrijemePolaska) && vrijemePolaska != null && !vrijemePolaska.equals("null")) sql += " AND DATE(voz.vrijemePolaskaVoznja) = :vrijemePolaska ";
        if (StringUtils.isNotEmpty(vrijemeDolaska) && vrijemeDolaska != null && !vrijemeDolaska.equals("null")) sql += " AND DATE(voz.vrijemeDolaskaVoznja) = :vrijemeDolaska ";
        if (sifraNarucitelj != null) sql += " AND naruc.idnarucitelj = "+ sifraNarucitelj;
        if (StringUtils.isNotEmpty(nazivNarucitelj) && nazivNarucitelj != null && !nazivNarucitelj.equals("null")) sql += " AND naruc.nazivNarucitelj like '"+ nazivNarucitelj +"%' ";

        dbFacade.begin();
        Session session = dbFacade.getDataServiceManager().getSession();
        Query query = session.createQuery(sql);
        if (StringUtils.isNotEmpty(vrijemePolaska) && vrijemePolaska!= null && !vrijemePolaska.equals("null")) try {
            cal.setTimeInMillis(new Long(vrijemePolaska));
            System.out.println(cal);
            query.setParameter("vrijemePolaska", (Date) cal.getTime());
        } catch (HibernateException e) {
            e.printStackTrace();
            dbFacade.rollback();
            return null;
        }
        if (StringUtils.isNotEmpty(vrijemeDolaska) && vrijemeDolaska!=null && !vrijemeDolaska.equals("null")) {
            try {
                cal.setTimeInMillis(new Long(vrijemeDolaska));
                System.out.println(cal);
                query.setParameter("vrijemeDolaska", (Date) cal.getTime());
            } catch (HibernateException e) {
                e.printStackTrace();
                dbFacade.rollback();
                return null;
            }
        }
        //query.addEntity(Voznja.class);
        System.out.println(query.getQueryString());@SuppressWarnings("unchecked")
        List < Voznja > rsVoznje = query.list();
        dbFacade.commit();
        return rsVoznje;
    }
    
    @SuppressWarnings("unchecked")
    public List <Logovi> getLogovi(String sadrzaj, String vrijemePocetak, String vrijemeKraj){
    	Calendar cal = Calendar.getInstance();
    	List < Logovi > rsLogovi;
    	RasbusDB dbFacade = (RasbusDB) RuntimeAccess.getInstance().getServiceBean("rasbusDB");
    	try{    		
    		String sql = " SELECT log FROM Logovi log where log.idlogovi > 0 ";
    		if (StringUtils.isNotEmpty(sadrzaj) && sadrzaj != null && !sadrzaj.equals("null")) sql += " AND log.sadrzaj LIKE '%" + sadrzaj + "%' ";
    		if (StringUtils.isNotEmpty(vrijemePocetak) && vrijemePocetak != null && !vrijemePocetak.equals("null") && StringUtils.isNotEmpty(vrijemeKraj) && vrijemeKraj != null && !vrijemeKraj.equals("null")) sql += " AND DATE(log.serverskoVrijeme) between :vrijemePocetak and :vrijemeKraj ";
    		sql += " order by log.idlogovi desc ";
    		dbFacade.begin();
    		Session session = dbFacade.getDataServiceManager().getSession();
    		Query query = session.createQuery(sql);
    		if (StringUtils.isNotEmpty(vrijemePocetak) && vrijemePocetak != null && !vrijemePocetak.equals("null") && StringUtils.isNotEmpty(vrijemeKraj) && vrijemeKraj != null && !vrijemeKraj.equals("null")){
    			cal.setTimeInMillis(new Long(vrijemePocetak));
    			query.setParameter("vrijemePocetak", (Date) cal.getTime());
    			cal.setTimeInMillis(new Long(vrijemeKraj));
    			query.setParameter("vrijemeKraj", (Date) cal.getTime());
    		}

    		System.out.println(query.getQueryString());
    		query.setMaxResults(100);
    		rsLogovi = query.list();
    		dbFacade.commit();
    	}catch(Exception ex){
    		ex.printStackTrace();
    		dbFacade.rollback();
    		rsLogovi = null;    		
    	}
    	return rsLogovi;
    }
    
}