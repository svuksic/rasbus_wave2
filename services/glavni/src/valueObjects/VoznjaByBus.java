package valueObjects;

import java.util.Date;

public class VoznjaByBus {
	
	private String nazivBusa;
	private String garazniBroj;
	private Integer idVoznja;
	private Date datumPolaska;
	private Date datumDolaska;
	private Date vrijemePolaska;
	private Date vrijemeDolaska;	
	private String imeVozaca;
	private String prezimeVozaca;
	private String oznakaVoznje;
	private String opisVoznje;
	
	public String getOznakaVoznje() {
		return oznakaVoznje;
	}
	public void setOznakaVoznje(String oznakaVoznje) {
		this.oznakaVoznje = oznakaVoznje;
	}
	public String getOpisVoznje() {
		return opisVoznje;
	}
	public void setOpisVoznje(String opisVoznje) {
		this.opisVoznje = opisVoznje;
	}
	public Date getVrijemePolaska() {
		return vrijemePolaska;
	}
	public void setVrijemePolaska(Date vrijemePolaska) {
		this.vrijemePolaska = vrijemePolaska;
	}
	public Date getVrijemeDolaska() {
		return vrijemeDolaska;
	}
	public void setVrijemeDolaska(Date vrijemeDolaska) {
		this.vrijemeDolaska = vrijemeDolaska;
	}
	
	public String getNazivBusa() {
		return nazivBusa;
	}
	public void setNazivBusa(String nazivBusa) {
		this.nazivBusa = nazivBusa;
	}
	public String getGarazniBroj() {
		return garazniBroj;
	}
	public void setGarazniBroj(String garazniBroj) {
		this.garazniBroj = garazniBroj;
	}
	public Integer getIdVoznja() {
		return idVoznja;
	}
	public void setIdVoznja(Integer idVoznja) {
		this.idVoznja = idVoznja;
	}
	public Date getDatumPolaska() {
		return datumPolaska;
	}
	public void setDatumPolaska(Date datumPolaska) {
		this.datumPolaska = datumPolaska;
	}
	public Date getDatumDolaska() {
		return datumDolaska;
	}
	public void setDatumDolaska(Date datumDolaska) {
		this.datumDolaska = datumDolaska;
	}
	public String getImeVozaca() {
		return imeVozaca;
	}
	public void setImeVozaca(String imeVozaca) {
		this.imeVozaca = imeVozaca;
	}
	public String getPrezimeVozaca() {
		return prezimeVozaca;
	}
	public void setPrezimeVozaca(String prezimeVozaca) {
		this.prezimeVozaca = prezimeVozaca;
	}
	
}
