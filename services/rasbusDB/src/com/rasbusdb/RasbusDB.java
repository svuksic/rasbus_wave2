
package com.rasbusdb;

import java.util.List;
import com.rasbusdb.data.output.GetWarningsOsobnaRtnType;
import com.rasbusdb.data.output.GetWarningsPutovnicaRtnType;
import com.rasbusdb.data.output.GetWarningsRtnType;
import com.rasbusdb.data.output.GetWarningsVozackaRtnType;
import com.wavemaker.json.type.TypeDefinition;
import com.wavemaker.runtime.data.DataServiceManager;
import com.wavemaker.runtime.data.DataServiceManagerAccess;
import com.wavemaker.runtime.data.TaskManager;
import com.wavemaker.runtime.service.LiveDataService;
import com.wavemaker.runtime.service.PagingOptions;
import com.wavemaker.runtime.service.PropertyOptions;
import com.wavemaker.runtime.service.TypedServiceReturn;


/**
 *  Operations for service "rasbusDB"
 *  04/01/2013 16:58:53
 * 
 */
@SuppressWarnings("unchecked")
public class RasbusDB
    implements DataServiceManagerAccess, LiveDataService
{

    private DataServiceManager dsMgr;
    private TaskManager taskMgr;

    public List<GetWarningsRtnType> getWarnings(PagingOptions pagingOptions) {
        return ((List<GetWarningsRtnType> ) dsMgr.invoke(taskMgr.getQueryTask(), (RasbusDBConstants.getWarningsQueryName), pagingOptions));
    }

    public com.rasbusdb.data.User getUserById(Integer id, PagingOptions pagingOptions) {
        List<com.rasbusdb.data.User> rtn = ((List<com.rasbusdb.data.User> ) dsMgr.invoke(taskMgr.getQueryTask(), (RasbusDBConstants.getUserByIdQueryName), id, pagingOptions));
        if (rtn.isEmpty()) {
            return null;
        } else {
            return rtn.get(0);
        }
    }

    public List<GetWarningsVozackaRtnType> getWarningsVozacka(PagingOptions pagingOptions) {
        return ((List<GetWarningsVozackaRtnType> ) dsMgr.invoke(taskMgr.getQueryTask(), (RasbusDBConstants.getWarningsVozackaQueryName), pagingOptions));
    }

    public List<GetWarningsPutovnicaRtnType> getWarningsPutovnica(PagingOptions pagingOptions) {
        return ((List<GetWarningsPutovnicaRtnType> ) dsMgr.invoke(taskMgr.getQueryTask(), (RasbusDBConstants.getWarningsPutovnicaQueryName), pagingOptions));
    }

    public List<GetWarningsOsobnaRtnType> getWarningsOsobna(PagingOptions pagingOptions) {
        return ((List<GetWarningsOsobnaRtnType> ) dsMgr.invoke(taskMgr.getQueryTask(), (RasbusDBConstants.getWarningsOsobnaQueryName), pagingOptions));
    }

    public Object insert(Object o) {
        return dsMgr.invoke(taskMgr.getInsertTask(), o);
    }

    public TypedServiceReturn read(TypeDefinition rootType, Object o, PropertyOptions propertyOptions, PagingOptions pagingOptions) {
        return ((TypedServiceReturn) dsMgr.invoke(taskMgr.getReadTask(), rootType, o, propertyOptions, pagingOptions));
    }

    public Object update(Object o) {
        return dsMgr.invoke(taskMgr.getUpdateTask(), o);
    }

    public void delete(Object o) {
        dsMgr.invoke(taskMgr.getDeleteTask(), o);
    }

    public void begin() {
        dsMgr.begin();
    }

    public void commit() {
        dsMgr.commit();
    }

    public void rollback() {
        dsMgr.rollback();
    }

    public DataServiceManager getDataServiceManager() {
        return dsMgr;
    }

    public void setDataServiceManager(DataServiceManager dsMgr) {
        this.dsMgr = dsMgr;
    }

    public TaskManager getTaskManager() {
        return taskMgr;
    }

    public void setTaskManager(TaskManager taskMgr) {
        this.taskMgr = taskMgr;
    }

}
