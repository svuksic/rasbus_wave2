
package com.rasbusdb;



/**
 *  Query names for service "rasbusDB"
 *  04/01/2013 16:58:53
 * 
 */
public class RasbusDBConstants {

    public final static String getWarningsQueryName = "getWarnings";
    public final static String getUserByIdQueryName = "getUserById";
    public final static String getWarningsVozackaQueryName = "getWarningsVozacka";
    public final static String getWarningsPutovnicaQueryName = "getWarningsPutovnica";
    public final static String getWarningsOsobnaQueryName = "getWarningsOsobna";

}
