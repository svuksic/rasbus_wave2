
package com.rasbusdb.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  rasbusDB.Role
 *  02/05/2013 18:53:16
 * 
 */
public class Role {

    private Integer idrole;
    private String naziv;
    private String opis;
    private Set<com.rasbusdb.data.User> users = new HashSet<com.rasbusdb.data.User>();

    public Integer getIdrole() {
        return idrole;
    }

    public void setIdrole(Integer idrole) {
        this.idrole = idrole;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public Set<com.rasbusdb.data.User> getUsers() {
        return users;
    }

    public void setUsers(Set<com.rasbusdb.data.User> users) {
        this.users = users;
    }

}
