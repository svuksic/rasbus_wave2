
package com.rasbusdb.data.output;

import java.util.Date;


/**
 * Generated for query "getWarningsPutovnica" on 02/05/2013 18:53:23
 * 
 */
public class GetWarningsPutovnicaRtnType {

    private Integer id;
    private String ime;
    private String prezime;
    private Date putovnica;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public Date getPutovnica() {
        return putovnica;
    }

    public void setPutovnica(Date putovnica) {
        this.putovnica = putovnica;
    }

}
