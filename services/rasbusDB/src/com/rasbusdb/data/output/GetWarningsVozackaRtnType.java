
package com.rasbusdb.data.output;

import java.util.Date;


/**
 * Generated for query "getWarningsVozacka" on 02/05/2013 18:53:23
 * 
 */
public class GetWarningsVozackaRtnType {

    private Integer id;
    private String ime;
    private String prezime;
    private Date vozacka;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public Date getVozacka() {
        return vozacka;
    }

    public void setVozacka(Date vozacka) {
        this.vozacka = vozacka;
    }

}
