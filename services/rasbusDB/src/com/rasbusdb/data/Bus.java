
package com.rasbusdb.data;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;


/**
 *  rasbusDB.Bus
 *  02/05/2013 18:53:16
 * 
 */
public class Bus {

    private Integer idBus;
    private String garazniBrojBus;
    private String modelBus;
    private String markaBus;
    private Integer brojSjedalaBus;
    private Date datumKupnjeBus;
    private Date datumZadnjeRegistracijeBus;
    private Date datumZadnjegServisaBus;
    private String napomenaBus;
    private Set<com.rasbusdb.data.Voznja> voznjas = new HashSet<com.rasbusdb.data.Voznja>();

    public Integer getIdBus() {
        return idBus;
    }

    public void setIdBus(Integer idBus) {
        this.idBus = idBus;
    }

    public String getGarazniBrojBus() {
        return garazniBrojBus;
    }

    public void setGarazniBrojBus(String garazniBrojBus) {
        this.garazniBrojBus = garazniBrojBus;
    }

    public String getModelBus() {
        return modelBus;
    }

    public void setModelBus(String modelBus) {
        this.modelBus = modelBus;
    }

    public String getMarkaBus() {
        return markaBus;
    }

    public void setMarkaBus(String markaBus) {
        this.markaBus = markaBus;
    }

    public Integer getBrojSjedalaBus() {
        return brojSjedalaBus;
    }

    public void setBrojSjedalaBus(Integer brojSjedalaBus) {
        this.brojSjedalaBus = brojSjedalaBus;
    }

    public Date getDatumKupnjeBus() {
        return datumKupnjeBus;
    }

    public void setDatumKupnjeBus(Date datumKupnjeBus) {
        this.datumKupnjeBus = datumKupnjeBus;
    }

    public Date getDatumZadnjeRegistracijeBus() {
        return datumZadnjeRegistracijeBus;
    }

    public void setDatumZadnjeRegistracijeBus(Date datumZadnjeRegistracijeBus) {
        this.datumZadnjeRegistracijeBus = datumZadnjeRegistracijeBus;
    }

    public Date getDatumZadnjegServisaBus() {
        return datumZadnjegServisaBus;
    }

    public void setDatumZadnjegServisaBus(Date datumZadnjegServisaBus) {
        this.datumZadnjegServisaBus = datumZadnjegServisaBus;
    }

    public String getNapomenaBus() {
        return napomenaBus;
    }

    public void setNapomenaBus(String napomenaBus) {
        this.napomenaBus = napomenaBus;
    }

    public Set<com.rasbusdb.data.Voznja> getVoznjas() {
        return voznjas;
    }

    public void setVoznjas(Set<com.rasbusdb.data.Voznja> voznjas) {
        this.voznjas = voznjas;
    }

}
