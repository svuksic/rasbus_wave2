
package com.rasbusdb.data;

import java.util.Date;


/**
 *  rasbusDB.Voznja
 *  02/05/2013 18:53:16
 * 
 */
public class Voznja {

    private Integer idvoznja;
    private Vozac vozac;
    private Bus bus;
    private Zatvorenavoznja zatvorenavoznja;
    private Narucitelj narucitelj;
    private String oznakaVoznja;
    private String opisVoznja;
    private String mjestoPolaskaVoznja;
    private String mjestoDolaskaVoznja;
    private Date vrijemePolaskaVoznja;
    private Date vrijemeDolaskaVoznja;
    private String ugovorenaCijenaVoznja;

    public Integer getIdvoznja() {
        return idvoznja;
    }

    public void setIdvoznja(Integer idvoznja) {
        this.idvoznja = idvoznja;
    }

    public Vozac getVozac() {
        return vozac;
    }

    public void setVozac(Vozac vozac) {
        this.vozac = vozac;
    }

    public Bus getBus() {
        return bus;
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    public Zatvorenavoznja getZatvorenavoznja() {
        return zatvorenavoznja;
    }

    public void setZatvorenavoznja(Zatvorenavoznja zatvorenavoznja) {
        this.zatvorenavoznja = zatvorenavoznja;
    }

    public Narucitelj getNarucitelj() {
        return narucitelj;
    }

    public void setNarucitelj(Narucitelj narucitelj) {
        this.narucitelj = narucitelj;
    }

    public String getOznakaVoznja() {
        return oznakaVoznja;
    }

    public void setOznakaVoznja(String oznakaVoznja) {
        this.oznakaVoznja = oznakaVoznja;
    }

    public String getOpisVoznja() {
        return opisVoznja;
    }

    public void setOpisVoznja(String opisVoznja) {
        this.opisVoznja = opisVoznja;
    }

    public String getMjestoPolaskaVoznja() {
        return mjestoPolaskaVoznja;
    }

    public void setMjestoPolaskaVoznja(String mjestoPolaskaVoznja) {
        this.mjestoPolaskaVoznja = mjestoPolaskaVoznja;
    }

    public String getMjestoDolaskaVoznja() {
        return mjestoDolaskaVoznja;
    }

    public void setMjestoDolaskaVoznja(String mjestoDolaskaVoznja) {
        this.mjestoDolaskaVoznja = mjestoDolaskaVoznja;
    }

    public Date getVrijemePolaskaVoznja() {
        return vrijemePolaskaVoznja;
    }

    public void setVrijemePolaskaVoznja(Date vrijemePolaskaVoznja) {
        this.vrijemePolaskaVoznja = vrijemePolaskaVoznja;
    }

    public Date getVrijemeDolaskaVoznja() {
        return vrijemeDolaskaVoznja;
    }

    public void setVrijemeDolaskaVoznja(Date vrijemeDolaskaVoznja) {
        this.vrijemeDolaskaVoznja = vrijemeDolaskaVoznja;
    }

    public String getUgovorenaCijenaVoznja() {
        return ugovorenaCijenaVoznja;
    }

    public void setUgovorenaCijenaVoznja(String ugovorenaCijenaVoznja) {
        this.ugovorenaCijenaVoznja = ugovorenaCijenaVoznja;
    }

}
