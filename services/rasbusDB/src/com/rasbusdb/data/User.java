
package com.rasbusdb.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  rasbusDB.User
 *  02/05/2013 18:53:16
 * 
 */
public class User {

    private Integer iduser;
    private Role role;
    private String ime;
    private String prezime;
    private String username;
    private String password;
    private Set<com.rasbusdb.data.Logovi> logovis = new HashSet<com.rasbusdb.data.Logovi>();

    public Integer getIduser() {
        return iduser;
    }

    public void setIduser(Integer iduser) {
        this.iduser = iduser;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<com.rasbusdb.data.Logovi> getLogovis() {
        return logovis;
    }

    public void setLogovis(Set<com.rasbusdb.data.Logovi> logovis) {
        this.logovis = logovis;
    }

}
