
package com.rasbusdb.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  rasbusDB.Narucitelj
 *  02/05/2013 18:53:16
 * 
 */
public class Narucitelj {

    private Integer idnarucitelj;
    private String nazivNarucitelj;
    private String adresaNarucitelj;
    private String odgovornaOsbaNarucitelj;
    private Set<com.rasbusdb.data.Voznja> voznjas = new HashSet<com.rasbusdb.data.Voznja>();

    public Integer getIdnarucitelj() {
        return idnarucitelj;
    }

    public void setIdnarucitelj(Integer idnarucitelj) {
        this.idnarucitelj = idnarucitelj;
    }

    public String getNazivNarucitelj() {
        return nazivNarucitelj;
    }

    public void setNazivNarucitelj(String nazivNarucitelj) {
        this.nazivNarucitelj = nazivNarucitelj;
    }

    public String getAdresaNarucitelj() {
        return adresaNarucitelj;
    }

    public void setAdresaNarucitelj(String adresaNarucitelj) {
        this.adresaNarucitelj = adresaNarucitelj;
    }

    public String getOdgovornaOsbaNarucitelj() {
        return odgovornaOsbaNarucitelj;
    }

    public void setOdgovornaOsbaNarucitelj(String odgovornaOsbaNarucitelj) {
        this.odgovornaOsbaNarucitelj = odgovornaOsbaNarucitelj;
    }

    public Set<com.rasbusdb.data.Voznja> getVoznjas() {
        return voznjas;
    }

    public void setVoznjas(Set<com.rasbusdb.data.Voznja> voznjas) {
        this.voznjas = voznjas;
    }

}
