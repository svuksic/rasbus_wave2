
package com.rasbusdb.data;

import java.util.Date;


/**
 *  rasbusDB.Logovi
 *  02/05/2013 18:53:16
 * 
 */
public class Logovi {

    private Integer idlogovi;
    private User user;
    private String sadrzaj;
    private Date serverskoVrijeme;

    public Integer getIdlogovi() {
        return idlogovi;
    }

    public void setIdlogovi(Integer idlogovi) {
        this.idlogovi = idlogovi;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getSadrzaj() {
        return sadrzaj;
    }

    public void setSadrzaj(String sadrzaj) {
        this.sadrzaj = sadrzaj;
    }

    public Date getServerskoVrijeme() {
        return serverskoVrijeme;
    }

    public void setServerskoVrijeme(Date serverskoVrijeme) {
        this.serverskoVrijeme = serverskoVrijeme;
    }

}
