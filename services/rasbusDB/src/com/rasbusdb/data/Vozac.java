
package com.rasbusdb.data;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;


/**
 *  rasbusDB.Vozac
 *  02/05/2013 18:53:16
 * 
 */
public class Vozac {

    private Integer idvozac;
    private String imeVozac;
    private String prezimeVozac;
    private Date datumRodjenjaVozac;
    private Date datumVozackaVozac;
    private Date datumPutovnicaVozac;
    private Date datumOsobnaVozac;
    private Set<com.rasbusdb.data.Voznja> voznjas = new HashSet<com.rasbusdb.data.Voznja>();

    public Integer getIdvozac() {
        return idvozac;
    }

    public void setIdvozac(Integer idvozac) {
        this.idvozac = idvozac;
    }

    public String getImeVozac() {
        return imeVozac;
    }

    public void setImeVozac(String imeVozac) {
        this.imeVozac = imeVozac;
    }

    public String getPrezimeVozac() {
        return prezimeVozac;
    }

    public void setPrezimeVozac(String prezimeVozac) {
        this.prezimeVozac = prezimeVozac;
    }

    public Date getDatumRodjenjaVozac() {
        return datumRodjenjaVozac;
    }

    public void setDatumRodjenjaVozac(Date datumRodjenjaVozac) {
        this.datumRodjenjaVozac = datumRodjenjaVozac;
    }

    public Date getDatumVozackaVozac() {
        return datumVozackaVozac;
    }

    public void setDatumVozackaVozac(Date datumVozackaVozac) {
        this.datumVozackaVozac = datumVozackaVozac;
    }

    public Date getDatumPutovnicaVozac() {
        return datumPutovnicaVozac;
    }

    public void setDatumPutovnicaVozac(Date datumPutovnicaVozac) {
        this.datumPutovnicaVozac = datumPutovnicaVozac;
    }

    public Date getDatumOsobnaVozac() {
        return datumOsobnaVozac;
    }

    public void setDatumOsobnaVozac(Date datumOsobnaVozac) {
        this.datumOsobnaVozac = datumOsobnaVozac;
    }

    public Set<com.rasbusdb.data.Voznja> getVoznjas() {
        return voznjas;
    }

    public void setVoznjas(Set<com.rasbusdb.data.Voznja> voznjas) {
        this.voznjas = voznjas;
    }

}
