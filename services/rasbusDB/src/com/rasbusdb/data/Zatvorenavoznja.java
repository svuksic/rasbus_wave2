
package com.rasbusdb.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  rasbusDB.Zatvorenavoznja
 *  02/05/2013 18:53:16
 * 
 */
public class Zatvorenavoznja {

    private Integer idzatvorenaVoznja;
    private Double kilometri;
    private Double troskovi;
    private Double naplata;
    private Set<com.rasbusdb.data.Voznja> voznjas = new HashSet<com.rasbusdb.data.Voznja>();

    public Integer getIdzatvorenaVoznja() {
        return idzatvorenaVoznja;
    }

    public void setIdzatvorenaVoznja(Integer idzatvorenaVoznja) {
        this.idzatvorenaVoznja = idzatvorenaVoznja;
    }

    public Double getKilometri() {
        return kilometri;
    }

    public void setKilometri(Double kilometri) {
        this.kilometri = kilometri;
    }

    public Double getTroskovi() {
        return troskovi;
    }

    public void setTroskovi(Double troskovi) {
        this.troskovi = troskovi;
    }

    public Double getNaplata() {
        return naplata;
    }

    public void setNaplata(Double naplata) {
        this.naplata = naplata;
    }

    public Set<com.rasbusdb.data.Voznja> getVoznjas() {
        return voznjas;
    }

    public void setVoznjas(Set<com.rasbusdb.data.Voznja> voznjas) {
        this.voznjas = voznjas;
    }

}
