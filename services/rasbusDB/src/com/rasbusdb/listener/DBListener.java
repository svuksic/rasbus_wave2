package com.rasbusdb.listener;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;

import com.rasbusdb.RasbusDB;
import com.rasbusdb.data.Logovi;
import com.rasbusdb.data.User;
import com.wavemaker.json.type.reflect.ObjectReflectTypeDefinition;
import com.wavemaker.runtime.RuntimeAccess;
import com.wavemaker.runtime.data.DataServiceEventListener;
import com.wavemaker.runtime.security.SecurityService;
import com.wavemaker.runtime.service.PagingOptions;
import com.wavemaker.runtime.service.PropertyOptions;
import com.wavemaker.runtime.service.ServiceWire;

//listener koji presrece svaki upis u bazu iz livevariable i logira
public class DBListener extends DataServiceEventListener {

	public Object[] preOperation(ServiceWire serviceWire, String operationName, Object[] params) {		
		super.preOperation(serviceWire, operationName, params);
		SecurityService securityService = (SecurityService) RuntimeAccess.getInstance().getServiceBean("securityService");;
		RasbusDB dbFacade = (RasbusDB) RuntimeAccess.getInstance().getServiceBean("rasbusDB");
		// Perform your validation here by inspecting parameters ...
		String paramValues = "";
		if(params != null){
			for(Object par : params){
				if(par != null && !par.getClass().equals(PagingOptions.class) && !par.getClass().equals(PropertyOptions.class) && !par.getClass().equals(ObjectReflectTypeDefinition.class)){
					paramValues = getString(par);
					if(!paramValues.equals("")){						
						java.util.Date date = new java.util.Date();
						String vrijeme = date.toString();
						String korisnik = "KORISNIK: "+securityService.getUserName();
						paramValues = vrijeme +" "+ korisnik +" "+operationName+" "+paramValues;
						dbFacade.begin();
						Session session = dbFacade.getDataServiceManager().getSession();
						Logovi log = new Logovi();
						log.setSadrzaj(paramValues);
						log.setServerskoVrijeme(date);
						log.setUser((User)session.load(User.class, Integer.parseInt(securityService.getUserId())));
						session.persist(log);
						dbFacade.commit();
					}
					System.out.println("Kod automatskog upisa xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx-"+ paramValues);	
				}
			}
			
		}
		return params;
	}
	
	//genericka metoda koja releksijom dohvaca propeertie i vrijesnosti prosljedjenog objekta
	public <T> String getString(T object)
	{
		String objectString = object.getClass().getSimpleName()+" [";
		java.lang.reflect.Field[] arrayOfFields = object.getClass().getDeclaredFields();
		java.lang.reflect.Method[] arrayOfMethods = object.getClass().getMethods();
		HashSet<String> hashSetMethods = new HashSet<String>();
		
		for(Method m : arrayOfMethods)
		{
			hashSetMethods.add(m.getName());
		}
		
		for(Field f : arrayOfFields)
		{
			String fieldName = f.getName();
			String fieldNameUpper = fieldName.substring(0,1).toUpperCase() + fieldName.substring(1);
			String getterProperty = "get" + fieldNameUpper;
			if(hashSetMethods.contains(getterProperty))
			{
				try {
					java.lang.reflect.Method getter = object.getClass().getMethod(getterProperty);	
					Object getterResult = getter.invoke(object);
					if (getterResult != null) objectString = objectString + fieldName + "=" + getterResult.toString() + ";";
				} catch (SecurityException e) {
					Logger.getLogger(object.getClass().getName()).log(Level.SEVERE, e.getMessage(), e);	
					e.printStackTrace();				
				} catch (NoSuchMethodException e) {							
					Logger.getLogger(object.getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
					e.printStackTrace();									
				} catch (IllegalArgumentException e) {
					Logger.getLogger(object.getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					Logger.getLogger(object.getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					Logger.getLogger(object.getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
					e.printStackTrace();
				}
			}
		}
		objectString += "]";
		return objectString;
	}

}